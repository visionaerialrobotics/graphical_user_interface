/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/vehicle_dynamics_viewer.h"
#include "aerostack_msgs/RequestProcesses.h"
#include "droneMsgsROS/dronePose.h"
#include <Eigen/Geometry>
#include <QMessageBox>
#include <cstdio>
#include <gtest/gtest.h>
#include <iostream>
#include <math.h>
#include <string>

ros::NodeHandle* nh;

TEST(GraphicalUserInterfaceTests, vehicleDynamicsTest)
{
  ros::NodeHandle nh;

  std::string estimated_pose_str;

  std::string drone_id;
  std::string drone_id_namespace;

  ros::Publisher estimated_pose_pub;

  droneMsgsROS::dronePose pose_msg;

  nh.param<std::string>("drone_id", drone_id, "1");
  nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);

  nh.param<std::string>("EstimatedPose_droneGMR_wrt_GFF", estimated_pose_str, "EstimatedPose_droneGMR_wrt_GFF");
  system(
      ("xfce4-terminal  \ --tab --title \"Self Localization Selector\"  --command \"bash -c 'roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
            drone_id_namespace:=" +
       drone_id_namespace + " \
            drone_id_int:=" +
       drone_id + "  \
            my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
          .c_str());
  system(
      ("xfce4-terminal  \ --tab --title \"Process manager\"  --command \"bash -c 'roslaunch process_manager_process process_manager_process.launch --wait \
            drone_id_namespace:=" +
       drone_id_namespace + " \
            drone_id_int:=" +
       drone_id + "  \
            my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
          .c_str());
  system(
      ("xfce4-terminal  \ --tab --title \"Vehicle Dynamics\"  --command \"bash -c 'roslaunch vehicle_dynamics vehicle_dynamics.launch --wait \
             drone_id_namespace:=" +
       drone_id_namespace + " \
             drone_id_int:=" +
       drone_id + "  \
             my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
          .c_str());

  ros::ServiceClient start_process_cli =
      nh.serviceClient<aerostack_msgs::RequestProcesses>("/" + drone_id_namespace + "/start_processes");
  aerostack_msgs::RequestProcesses::Request req;
  aerostack_msgs::RequestProcesses::Response res;
  req.processes.push_back("self_localization_selector_process");

  estimated_pose_pub = nh.advertise<droneMsgsROS::dronePose>('/' + drone_id_namespace + '/' + estimated_pose_str, 1);

  pose_msg.x = 1;
  pose_msg.y = 1;
  pose_msg.z = 1;
  pose_msg.roll = -0.349066;
  pose_msg.pitch = 0.785398;
  pose_msg.yaw = 0.523599;
  while (!res.acknowledge)
  {
    start_process_cli.call(req, res);
  }

  ros::Duration(2).sleep();
  estimated_pose_pub.publish(pose_msg);

  ros::Rate rate(30);
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();

  std::string response;
  std::cout << "\033[1;34m Is position (1,1,1) and YPR (30,40,-20) shown in vehicle dynamics? (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem receiving pose\033[0m\n" << std::endl;
  }
  EXPECT_TRUE(response == "Y" || response == "y");
}

/*--------------------------------------------*/
/*  Main  */

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());

  nh = new ros::NodeHandle;

  return RUN_ALL_TESTS();
}
