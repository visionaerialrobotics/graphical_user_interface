/*!*******************************************************************************************
 *  \file       vehicle_dynamics_viewer_view.h
 *  \brief      Vehicle Dynamics Viewer View definition file.
 *  \details    General layout for the vehicle dynamics component. Widgets can be dynamically added to it.
 *  \author     German Quintero
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef VEHICLE_DYNAMICS_VIEWER_VIEW_H
#define VEHICLE_DYNAMICS_VIEWER_VIEW_H

#include "aerostack_msgs/WindowIdentifier.h"

#include <QWidget>
#include <QRect>
#include <QGuiApplication>
#include <QScreen>
#include <QProcess>

#include "vehicle_dynamics_viewer.h"
#include "ui_vehicle_dynamics_viewer_view.h"

#include <thread>
#include <chrono>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "fstream"

// For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui
{
class VehicleDynamicsViewerView;
}

class VehicleDynamicsViewerView : public QWidget
{
  Q_OBJECT

public:
  // Constructor & Destructor
  explicit VehicleDynamicsViewerView(int argc, char** argv, QWidget* parent = 0);

  ros::NodeHandle n;

  std::string drone_id_namespace;

  boost::property_tree::ptree root;

  ros::Publisher window_closed_publ;
  ros::Subscriber window_opened_subs;

  std::string window_opened_topic;
  std::string window_closed_topic;

  aerostack_msgs::WindowIdentifier windowClosedMsgs;
  aerostack_msgs::WindowIdentifier windowOpenedMsgs;

  ~VehicleDynamicsViewerView();

public:
  /*!********************************************************************************************************************
   *  \brief      This method returns the VehicleDynamics widget
   **********************************************************************************************************************/
  VehicleDynamicsViewer* getVehicleDynamicsViewer();
  /*!********************************************************************************************************************
   *  \brief      This method is the responsible for seting up connections.
   *********************************************************************************************************************/
  void setUp();

  /*!************************************************************************
   *  \brief   Activated when a window is closed.
   ***************************************************************************/
  void windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg);
  /*!************************************************************************
   *  \brief  Kills the process
   ***************************************************************************/
  void killMe();

public Q_SLOTS:

  /*!********************************************************************************************************************
   *  \brief      This method notifies main window that the widget was closed
   *********************************************************************************************************************/
  void closeEvent(QCloseEvent* event);

private:
  // Layout
  Ui::VehicleDynamicsViewerView* ui;

  // Widget
  VehicleDynamicsViewer* vehicleDynamics;
};

#endif  // VEHICLE_DYNAMICS_VIEWER_VIEW_H
