/*!*******************************************************************************************
 *  \file       vehicle_dynamics_viewer.h
 *  \brief      Vehicle Dynamics Viewer definition file.
 *  \details    The vehicle dynamics viewer wiget shows the drone's current position.
 *  \author     German Quintero
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef VEHICLE_DYNAMICS_VIEWER_H
#define VEHICLE_DYNAMICS_VIEWER_H

#include <ros/ros.h>
#include "geometry_msgs/Pose.h"
#include <QWidget>
#include <Eigen/Geometry>

#include <angles/angles.h>
#include "ui_vehicle_dynamics_viewer.h"
#include "odometry_state_receiver.h"

#include <tf/tf.h>

// For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui
{
class VehicleDynamicsViewer;
}

class VehicleDynamicsViewer : public QWidget
{
  Q_OBJECT

public:
  // Constructor & Destructor
  explicit VehicleDynamicsViewer(QWidget* parent = 0);
  ~VehicleDynamicsViewer();
  void toEulerAngle(const Eigen::Quaternionf& q, float& roll, float& pitch, float& yaw);

public Q_SLOTS:
  /*!********************************************************************************************************************
   *  \brief      This method updates the drone position displayed.
   *********************************************************************************************************************/
  void updateDynamicsPanel();

private:
  // Layout
  Ui::VehicleDynamicsViewer* ui;
  std::string rosnamespace;

  OdometryStateReceiver* odometryReceiver;
  double roll, pitch, yaw;
};

#endif  // VEHICLE_DYNAMICS_VIEWER_H
