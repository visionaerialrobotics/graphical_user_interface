
/*!*********************************************************************************
 *  \file       first_view_process_main.cpp
 *  \brief      FirstView main file.
 *  \details    This file implements the main function of the FirstView.
 *  \authors    Daniel Rabasco García.
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/first_person_view_process.h"
#include "../include/hud_camera.h"
#include <QApplication>
#include <ros/ros.h>
#include <signal.h>
#include <thread>

void signalhandler(int sig)
{
  if (sig == SIGINT)
  {
    qApp->quit();
  }
  else if (sig == SIGTERM)
  {
    qApp->quit();
  }
}

/*void spinnerThread(FirstView *first_view){

     ros::Rate rate(6);
    while(ros::ok())
    {
         ros::spinOnce();
         if(!first_view->getDronePublishing())
           first_view->run();
         else
           first_view->setDronePublishing(false);


         rate.sleep();
    }
}*/

void spinnerThread(FirstView* first_view)
{
  ros::spin();
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "First_Person_Viewer");
  QApplication app(argc, argv);
  ros::NodeHandle n;

  ImagesReceiver* images_receiver = new ImagesReceiver();

  FirstView* first_view = new FirstView(n);

  first_view->setUp();
  images_receiver->openSubscriptions(n, first_view->drone_id_namespace);

  try
  {
    first_view->start();
  }
  catch (std::exception& exception)
  {
    first_view->notifyError(first_view->SafeguardRecoverableError, 0, "ownStart()", exception.what());
    first_view->stop();
  }

  HUDCamera hud_camera(first_view, images_receiver);

  hud_camera.show();

  std::thread thr(&spinnerThread, first_view);

  signal(SIGINT, signalhandler);
  signal(SIGTERM, signalhandler);

  app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
  int result = app.exec();
  return result;
}
