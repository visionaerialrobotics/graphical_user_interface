
/*!*******************************************************************************************
 *  \file       first_view_process.cpp
 *  \brief      FirstView implementation file.
 *  \details    This file implements the FirstView class.
 *  \authors    Daniel Rabasco García.
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/first_person_view_process.h"

FirstView::FirstView(ros::NodeHandle n) : it_(n)
{
  nh_ = n;
  drone_publishing = false;
}

FirstView::~FirstView()
{
}

void FirstView::ownSetUp()
{
  ros::NodeHandle private_nh("~");

  // private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "/drone1");

  // private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
}

void FirstView::ownStart()
{
  // Subscribe to input video feed and other feeds and publish output video feeds
  nh_.param<std::string>("pose_topic", drone_pose_subscription, "pose");
  nh_.param<std::string>("speeds_topic", drone_speeds_subscription, "speeds");
  nh_.param<std::string>("self_localization", odometry_namespace, "self_localization");
  pose_sub_ = nh_.subscribe("/" + drone_id_namespace + "/" + odometry_namespace + "/" + drone_pose_subscription, 1000,
                            &FirstView::poseCallback, this);
  spd_sub_ = nh_.subscribe("/" + drone_id_namespace + "/" + odometry_namespace + "/" + drone_speeds_subscription, 1000,
                           &FirstView::spdCallback, this);
  batt_sub_ = nh_.subscribe("/" + drone_id_namespace + "/battery", 1000, &FirstView::battCallback, this);
  image_sub_ = it_.subscribe("/" + drone_id_namespace + VIDEO_STREAM_INPUT, 1000, &FirstView::imageCallback, this);
  image_pub_big_ = it_.advertise("/" + drone_id_namespace + "/camera/overlay/image_raw/big", 1000);
  image_pub_small_ = it_.advertise("/" + drone_id_namespace + "/camera/overlay/image_raw/small", 1000);
}

void FirstView::ownStop()
{
  pose_sub_.shutdown();
  spd_sub_.shutdown();
  batt_sub_.shutdown();
  image_sub_.shutdown();
}

void FirstView::ownRun()
{
  // if(!drone_publishing)
  image_converter.imageOverlayCbBlack(image_pub_small_, image_pub_big_);
}

void FirstView::imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  drone_publishing = true;
  image_converter.imageOverlayCb(msg, image_pub_small_, image_pub_big_);
}

void FirstView::poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  image_converter.poseInfoCb(msg);
}

void FirstView::spdCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
  image_converter.spdInfoCb(msg);
}

void FirstView::battCallback(const droneMsgsROS::battery::ConstPtr& msg)
{
  image_converter.battInfoCb(msg);
}

bool FirstView::getDronePublishing()
{
  return drone_publishing;
}

void FirstView::setDronePublishing(bool publishing)
{
  drone_publishing = publishing;
}
