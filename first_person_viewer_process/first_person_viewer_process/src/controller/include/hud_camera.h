/*!*******************************************************************************************
 *  \file       camera_overlay_small.h
 *  \brief      HUDCamera definition file.
 *  \details    This file includes the HUDCamera class declaration.
 *  \author     Daniel Rabasco
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef CAMERAOVERLAYSMALL_H
#define CAMERAOVERLAYSMALL_H

#include "aerostack_msgs/WindowIdentifier.h"

#include <QWidget>
#include <QRect>
#include <QGuiApplication>
#include <QScreen>
#include <QProcess>

#include "images_receiver.h"
#include "ui_hud_camera.h"
#include "first_person_view_process.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "fstream"

namespace Ui
{
class HUDCamera;
}

class HUDCamera : public QWidget
{
  Q_OBJECT

public:
  explicit HUDCamera(FirstView* first_person = 0, ImagesReceiver* imgReceiver = 0, QWidget* parent = 0);
  ~HUDCamera();
  QPixmap pixmap;

public Q_SLOTS:
  void updateImageOverlaySmall(const QPixmap* image);
  void updateImageOverlayBig(const QPixmap* image);
  void setOverlayBig(bool overlay_big);
  void on_toolButton_clicked();

  /*!********************************************************************************************************************
   *  \brief      This method notifies main window that the widget was closed
   *********************************************************************************************************************/
  void closeEvent(QCloseEvent* event);

private:
  Ui::HUDCamera* ui;

  ros::NodeHandle n;

  ImagesReceiver* image_receiver;
  FirstView* first_person_view;

  QPixmap pix;

  bool overlay_big;
  int cont;
  std::string drone_id_namespace;

  ros::Publisher window_closed_publ;
  ros::Subscriber window_opened_subs;

  std::string window_opened_topic;
  std::string window_closed_topic;

  aerostack_msgs::WindowIdentifier windowClosedMsgs;
  aerostack_msgs::WindowIdentifier windowOpenedMsgs;

  boost::property_tree::ptree root;

  /*!********************************************************************************************************************
   *  \brief      This method is the responsible for seting up connections.
   *********************************************************************************************************************/
  void setUp();
  /*!************************************************************************
   *  \brief   Activated when a window is closed.
   ***************************************************************************/
  void windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg);
  /*!************************************************************************
   *  \brief  Kills the process
   ***************************************************************************/
  void killMe();
};

#endif
