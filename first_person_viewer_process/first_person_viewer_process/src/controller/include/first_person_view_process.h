/*!*********************************************************************************
 *  \file       first_view_process.h
 *  \brief      FirstView definition file.
 *  \details    This file contains the FirstView declaration.
 *              To obtain more information about it's definition consult
 *              the behavior_specialist_process.cpp file.
 *  \authors    Daniel Rabasco.
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/


#ifndef FIRST_VIEW_PROCESS_H
#define FIRST_VIEW_PROCESS_H

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include "image_converter.h"
#include "images_receiver.h"

#include <robot_process.h>

static std::string VIDEO_STREAM_INPUT = "/camera/front/image_raw";  // name of the receiving topic

class FirstView : public RobotProcess
{
private:
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_big_, image_pub_small_;
  ros::Subscriber pose_sub_, batt_sub_, spd_sub_;

  ImageConverter image_converter;

  bool drone_publishing;

public:
  FirstView(ros::NodeHandle n);
  ros::NodeHandle nh_;

  std::string drone_id_namespace;  // The drone name
  std::string drone_pose_subscription;
  std::string drone_speeds_subscription;
  std::string odometry_namespace;
  ~FirstView();

private:  // DroneProcess
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();

public:
  void imageCallback(const sensor_msgs::ImageConstPtr& msg);
  void poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
  void spdCallback(const geometry_msgs::TwistStamped::ConstPtr& msg);
  void battCallback(const droneMsgsROS::battery::ConstPtr& msg);
  bool getDronePublishing();
  void setDronePublishing(bool drone_publishing);
};
#endif
