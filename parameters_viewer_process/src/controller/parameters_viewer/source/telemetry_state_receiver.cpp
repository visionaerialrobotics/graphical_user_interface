/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  collector
  Launch a ROS node to subscribe topics.
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/

#include "../include/telemetry_state_receiver.h"

/*****************************************************************************
** Implementation
*****************************************************************************/

TelemetryStateReceiver::TelemetryStateReceiver()
{
  subscriptions_complete = false;
}

TelemetryStateReceiver::~TelemetryStateReceiver()
{
}

void TelemetryStateReceiver::openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace)
{
  if (!nodeHandle.getParam("drone_driver_command_drone_command_pitch_roll",
                           drone_driver_command_drone_command_pitch_roll))
    drone_driver_command_drone_command_pitch_roll = "command/pitch_roll";

  if (!nodeHandle.getParam("drone_driver_command_drone_command_daltitude",
                           drone_driver_command_drone_command_daltitude))
    drone_driver_command_drone_command_daltitude = "command/dAltitude";

  if (!nodeHandle.getParam("drone_driver_command_drone_command_dyaw", drone_driver_command_drone_command_dyaw))
    drone_driver_command_drone_command_dyaw = "command/dYaw";

  if (!nodeHandle.getParam("drone_driver_command_drone_hl_command", drone_driver_command_drone_hl_command))
    drone_driver_command_drone_hl_command = "command/high_level";

  if (!nodeHandle.getParam("drone_driver_sensor_imu", drone_driver_sensor_imu))
    drone_driver_sensor_imu = "imu";

  if (!nodeHandle.getParam("drone_driver_sensor_temperature", drone_driver_sensor_temperature))
    drone_driver_sensor_temperature = "temperature";

  if (!nodeHandle.getParam("drone_driver_sensor_magnetometer", drone_driver_sensor_magnetometer))
    drone_driver_sensor_magnetometer = "magnetometer";

  if (!nodeHandle.getParam("drone_driver_sensor_battery", drone_driver_sensor_battery))
    drone_driver_sensor_battery = "battery";

  if (!nodeHandle.getParam("drone_driver_sensor_altitude", drone_driver_sensor_altitude))
    drone_driver_sensor_altitude = "altitude";

  if (!nodeHandle.getParam("drone_driver_sensor_rotation_angles", drone_driver_sensor_rotation_angles))
    drone_driver_sensor_rotation_angles = "rotation_angles";

  if (!nodeHandle.getParam("drone_driver_sensor_ground_speed", drone_driver_sensor_ground_speed))
    drone_driver_sensor_ground_speed = "ground_speed";

  if (!nodeHandle.getParam("drone_driver_sensor_pressure", drone_driver_sensor_pressure))
    drone_driver_sensor_pressure = "pressure";

  if (!nodeHandle.getParam("drone_driver_sensor_status", drone_driver_sensor_status))
    drone_driver_sensor_status = "status";

  if (!nodeHandle.getParam("drone_pelican_like_simulator_control_input_subscriber",
                           drone_pelican_like_simulator_control_input_subscriber))
    drone_pelican_like_simulator_control_input_subscriber = "drone_pelican_like_simulator_control_input_subscriber";

  if (!nodeHandle.getParam("drone_okto_like_simulator_okto_commands_subscriber",
                           drone_okto_like_simulator_okto_commands_subscriber))
    drone_okto_like_simulator_okto_commands_subscriber = "drone_okto_like_simulator_okto_commands_subscriber";

  // Commands
  drone_pitchRoll_cmd_subs =
      nodeHandle.subscribe(rosnamespace + "/" + drone_driver_command_drone_command_pitch_roll, 1,
                           &TelemetryStateReceiver::dronePitchRollCmdCallback, this);  // command/pitch_roll
  drone_dAltitude_cmd_subs =
      nodeHandle.subscribe(rosnamespace + "/" + drone_driver_command_drone_command_daltitude, 1,
                           &TelemetryStateReceiver::droneDAltitudeCmdCallback, this);  // command/dAltitude
  drone_dYaw_cmd_subs = nodeHandle.subscribe(rosnamespace + "/" + drone_driver_command_drone_command_dyaw, 1,
                                             &TelemetryStateReceiver::droneDYawCmdCallback, this);  // command/dYaw
  drone_HLCmd_subs = nodeHandle.subscribe(rosnamespace + "/" + drone_driver_command_drone_hl_command, 1,
                                          &TelemetryStateReceiver::droneHLCallback, this);  // command/high_level
  // DroneLLCmdSubs=nodeHandle.subscribe(ros::this_node::getNamespace() + "/" +
  // drone_driver_command_drone_ll_autopilot_command, 1, &telemetryStateReceiver::droneLLCallback,
  // this);//command/low_level

  // Sensor
  imu_subs =
      nodeHandle.subscribe(rosnamespace + "/" + drone_driver_sensor_imu, 1, &TelemetryStateReceiver::imuCallback, this);
  temperature_subs = nodeHandle.subscribe(rosnamespace + "/" + drone_driver_sensor_temperature, 1,
                                          &TelemetryStateReceiver::temperatureCallback, this);
  magnetometer_subs = nodeHandle.subscribe(rosnamespace + "/" + drone_driver_sensor_magnetometer, 1,
                                           &TelemetryStateReceiver::magnetometerCallback, this);
  battery_subs = nodeHandle.subscribe(rosnamespace + "/" + drone_driver_sensor_battery, 1,
                                      &TelemetryStateReceiver::batteryCallback, this);
  altitude_subs = nodeHandle.subscribe(rosnamespace + "/" + drone_driver_sensor_altitude, 1,
                                       &TelemetryStateReceiver::altitudeCallback, this);
  rotation_angles_subs = nodeHandle.subscribe(rosnamespace + "/" + drone_driver_sensor_rotation_angles, 1,
                                              &TelemetryStateReceiver::rotationAnglesCallback, this);
  ground_speed_subs = nodeHandle.subscribe(rosnamespace + "/" + drone_driver_sensor_ground_speed, 1,
                                           &TelemetryStateReceiver::groundSpeedCallback, this);
  pressure_subs = nodeHandle.subscribe(rosnamespace + "/" + drone_driver_sensor_pressure, 1,
                                       &TelemetryStateReceiver::pressureCallback, this);
  drone_status_subs = nodeHandle.subscribe(rosnamespace + "/" + drone_driver_sensor_status, 1,
                                           &TelemetryStateReceiver::droneStatusSensorCallback, this);

  subscriptions_complete = true;
  Q_EMIT parameterReceived();
}

bool TelemetryStateReceiver::ready()
{
  if (!subscriptions_complete)
    return false;
  return true;  // Used this way instead of "return subscriptions_complete" due to preserve add more conditions
}

void TelemetryStateReceiver::dronePitchRollCmdCallback(const droneMsgsROS::dronePitchRollCmd::ConstPtr& msg)
{
  drone_pitchRoll_cmd_msgs = *msg;
  return;
}

void TelemetryStateReceiver::imuCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
  imu_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::magnetometerCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
  magnetometer_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::batteryCallback(const droneMsgsROS::battery::ConstPtr& msg)
{
  battery_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::altitudeCallback(const droneMsgsROS::droneAltitude::ConstPtr& msg)
{
  altitude_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::droneStatusSensorCallback(const droneMsgsROS::droneStatus::ConstPtr& msg)
{
  drone_status_msgs = *msg;  // status type
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::rotationAnglesCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
  rotation_angles_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::dronePositionCallback(const droneMsgsROS::dronePose::ConstPtr& msg)
{
  drone_pose_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::groundSpeedCallback(const droneMsgsROS::vector2Stamped::ConstPtr& msg)
{
  ground_speed_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::temperatureCallback(const sensor_msgs::Temperature::ConstPtr& msg)
{
  temperature = *msg;
}

void TelemetryStateReceiver::pressureCallback(const sensor_msgs::FluidPressure::ConstPtr& msg)
{
  fluid_pressure = *msg;
}

void TelemetryStateReceiver::droneDAltitudeCmdCallback(const droneMsgsROS::droneDAltitudeCmd::ConstPtr& msg)
{
  drone_dAltitude_cmd_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::droneDYawCmdCallback(const droneMsgsROS::droneDYawCmd::ConstPtr& msg)
{
  drone_dyaw_cmd_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::droneHLCallback(const droneMsgsROS::droneCommand::ConstPtr& msg)
{
  drone_command_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}

void TelemetryStateReceiver::droneLLCallback(const droneMsgsROS::droneDYawCmd::ConstPtr& msg)
{
  drone_dyaw_cmd_msgs = *msg;
  Q_EMIT parameterReceived();
  return;
}
