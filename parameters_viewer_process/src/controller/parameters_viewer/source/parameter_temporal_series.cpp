/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  2D Parameter temporal series window
  Plotting of odometer parameters selected in real time.
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

#include "../include/parameter_temporal_series.h"

ParameterTemporalSeries::ParameterTemporalSeries(int argc, char** argv, QWidget* parent)
  : QWidget(parent), ui(new Ui::parameterTemporalSeries)
{
  ui->setupUi(this);

  // window always on top
  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);
  setWindowIcon(QIcon(":/img/img/parameters_viewer.png"));

  telem_receiver = new TelemetryStateReceiver();
  odometry = new OdometryStateReceiver();

  ros::start();
  ros::NodeHandle n;

  if (ros::this_node::getNamespace().compare("/") == 0)
    rosnamespace.append("drone1");  // default namespace
  else
    rosnamespace.append(ros::this_node::getNamespace());

  telem_receiver->openSubscriptions(n, rosnamespace);
  odometry->openSubscriptions(n, rosnamespace);

  ui->spin_box_max_axis->setReadOnly(false);
  ui->spin_box_min_axis->setReadOnly(false);

  dialog = new ParametersViewerDialog(this);

  // initial parameters
  parameters = dialog->getSelectedParameters();

  plot = new DataPlot(ui->plot_widget, telem_receiver, odometry, &parameters);

  ui->plot_widget->resize(600, 480);

  printf("height: %d \n", ui->plot_widget->size().height());
  printf("width: %d \n", ui->plot_widget->size().width());
  plot->setCanvasBackground(QBrush(Qt::white));
  this->initTree(parameters, ui->tree_widget);

  setSignalHandlers();
  updateParametersValue();

  // Settings connections
  setUp();
}
void ParameterTemporalSeries::clearFocus()
{
  std::cout << "Editing line finished" << std::endl;
  ui->spin_box_min_axis->clearFocus();
  ui->spin_box_max_axis->clearFocus();
}

void ParameterTemporalSeries::setSignalHandlers()
{
  connect(ui->tree_widget, SIGNAL(itemChanged(QTreeWidgetItem*, int)), plot, SLOT(clickToPlot(QTreeWidgetItem*, int)));
  connect(ui->spin_box_min_axis, SIGNAL(valueChanged(int)), plot, SLOT(resizeAxisYMinLimit(int)));
  connect(ui->spin_box_max_axis, SIGNAL(valueChanged(int)), plot, SLOT(resizeAxisYMaxLimit(int)));
  connect(ui->save_plot_button, SIGNAL(clicked()), plot, SLOT(saveAsSVG()));
  connect(telem_receiver, SIGNAL(parameterReceived()), this, SLOT(updateParametersValue()));
  connect(odometry, SIGNAL(parameterReceived()), this, SLOT(updateParametersValue()));
  connect(ui->stop_button, SIGNAL(clicked()), this, SLOT(onStopButton()));
  connect(ui->spin_box_min_axis, SIGNAL(editingFinished()), this, SLOT(clearFocus()));
  connect(ui->spin_box_max_axis, SIGNAL(editingFinished()), this, SLOT(clearFocus()));

  connect(dialog, SIGNAL(finished(int)), this, SLOT(dialogIsFinished(int)));
}

void ParameterTemporalSeries::resizeEvent(QResizeEvent* event)
{
  plot->resize(ui->plot_widget->size());
}

void ParameterTemporalSeries::setUp()
{
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");

  n.param<std::string>("window_opened", window_opened_topic, "window_opened");
  n.param<std::string>("window_closed", window_closed_topic, "window_closed");

  // Subscribers
  window_opened_subs = n.subscribe("/" + drone_id_namespace + "/" + window_opened_topic, 10,
                                   &ParameterTemporalSeries::windowOpenCallback, this);

  // Publishers
  window_closed_publ =
      n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace + "/" + window_closed_topic, 1, true);
}

void ParameterTemporalSeries::initParameterList(QStringList list, QTreeWidget* tree)
{
  ui->tree_widget->setIndentation(15);
  for (int i = 0; i < list.size(); ++i)
  {
    QTreeWidgetItem* itm = new QTreeWidgetItem(ui->tree_widget);
    QPixmap pixmap(15, 40);
    pixmap.fill(QColor("white"));
    QIcon red_icon(pixmap);
    itm->setIcon(0, red_icon);
    itm->setText(0, list.at(i));
    itm->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
    itm->setCheckState(0, Qt::Unchecked);
    ui->tree_widget->addTopLevelItem(itm);
  }
}

void ParameterTemporalSeries::initTree(QMap<QString, QStringList> topic_list, QTreeWidget* tree)
{
  QMapIterator<QString, QStringList> i(topic_list);
  while (i.hasNext())
  {
    i.next();
    qDebug() << i.key();

    this->addRootTree(i.key(), i.value(), tree);
    tree->expandAll();
  }
}

void ParameterTemporalSeries::addRootTree(QString name, QStringList list, QTreeWidget* tree)
{
  QTreeWidgetItem* itm = new QTreeWidgetItem(tree);
  itm->setText(0, name);
  tree->addTopLevelItem(itm);
  tree->setIndentation(15);
  addChildTree(itm, list, "");
}

void ParameterTemporalSeries::addChildTree(QTreeWidgetItem* parent, QStringList list, QString description)
{
  for (int i = 0; i < list.size(); ++i)
  {
    QTreeWidgetItem* itm = new QTreeWidgetItem();
    QPixmap pixmap(15, 40);
    pixmap.fill(QColor("white"));
    QIcon red_icon(pixmap);
    itm->setIcon(0, red_icon);
    itm->setText(0, list.at(i));
    itm->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
    itm->setCheckState(0, Qt::Unchecked);
    parent->addChild(itm);
  }
}

void ParameterTemporalSeries::onTextFilterChange(const QString& arg1)
{
  QRegExp regExp(arg1, Qt::CaseInsensitive, QRegExp::Wildcard);
  ui->tree_widget->blockSignals(true);
  ui->tree_widget->clear();

  QMapIterator<QString, QStringList> i(parameters);
  while (i.hasNext())
  {
    i.next();
    qDebug() << ui->tree_widget->isFirstItemColumnSpanned(ui->tree_widget->itemAt(0, 0));
    this->addRootTree(i.key(), i.value().filter(regExp), ui->tree_widget);
    qDebug() << "filter data" << i.value().filter(regExp);
  }
  qDebug() << "connect";
}

void ParameterTemporalSeries::onShowUnits(bool click)
{
  QStringList headers;
  if (click)
  {
    headers << "Parameters"
            << "Value"
            << "Unit";
    QMapIterator<QString, QStringList> i(parameters);
    QStringList list;
    while (i.hasNext())
    {
      i.next();
      list = i.value();
      QString itemName;
      for (int j = 0; j < list.size(); ++j)
      {
        itemName = i.key() + "/" + list.at(j);
        if (plot->selected_items.contains(itemName))
        {
          QTreeWidgetItem* itemList = plot->items[itemName];
          itemList->setText(2, "float");
        }
      }
    }
    ui->tree_widget->setColumnCount(3);
  }
  else
  {
    headers << "Parameters"
            << "Value";
    ui->tree_widget->setColumnCount(2);
  }
  ui->tree_widget->setHeaderLabels(headers);
}

void ParameterTemporalSeries::onStopButton()
{
  if (plot->is_stop_pressed)
  {
    plot->is_stop_pressed = false;
    ui->stop_button->setText("Stop");
  }
  else
  {
    plot->is_stop_pressed = true;
    ui->stop_button->setText("Start");
  }
}

void ParameterTemporalSeries::updateParametersValue()
{
  QTreeWidgetItem* ekfItem = ui->tree_widget->topLevelItem(1);
  QTreeWidgetItem* telemetryItem = ui->tree_widget->topLevelItem(2);
  QTreeWidgetItem* controllerItem = ui->tree_widget->topLevelItem(0);

  for (int i = 0; i < controllerItem->childCount(); i++)
  {
    QTreeWidgetItem* child = controllerItem->child(i);

    if (child->text(0) == "xci")
    {
      child->setText(
          1,
          QString::number(((double)((int)(odometry->drone_controller_pose_msgs.pose.position.x * 100))) / 100) + "  m");
    }
    if (child->text(0) == "yci")
    {
      child->setText(
          1,
          QString::number(((double)((int)(odometry->drone_controller_pose_msgs.pose.position.y * 100))) / 100) + "  m");
    }
    if (child->text(0) == "zci")
    {
      child->setText(
          1,
          QString::number(((double)((int)(odometry->drone_controller_pose_msgs.pose.position.z * 100))) / 100) + "  m");
    }
    if (child->text(0) == "yawci")
    {
      child->setText(1, QString::number(((double)((int)(odometry->yaw_c * 100))) / 100) + "  rad");
    }
    if (child->text(0) == "pitchci")
    {
      child->setText(1, QString::number(((double)((int)(odometry->pitch_c * 100))) / 100) + "  rad");
    }
    if (child->text(0) == "rollci")
    {
      child->setText(1, QString::number(((double)((int)(odometry->roll_c * 100))) / 100) + "  rad");
    }
    if (child->text(0) == "vxfi")
    {
      child->setText(
          1,
          QString::number(((double)((int)(odometry->drone_controller_speeds_msgs.twist.linear.x * 100))) / 100) + "  "
                                                                                                                  "m/"
                                                                                                                  "s");
    }
    if (child->text(0) == "vyfi")
    {
      child->setText(
          1,
          QString::number(((double)((int)(odometry->drone_controller_speeds_msgs.twist.linear.y * 100))) / 100) + "  "
                                                                                                                  "m/"
                                                                                                                  "s");
    }
    if (child->text(0) == "vzfi")
    {
      child->setText(
          1,
          QString::number(((double)((int)(odometry->drone_controller_speeds_msgs.twist.linear.z * 100))) / 100) + "  "
                                                                                                                  "m/"
                                                                                                                  "s");
    }
    if (child->text(0) == "dyawfi")
    {
      child->setText(
          1,
          QString::number(((double)((int)(odometry->drone_controller_speeds_msgs.twist.angular.x * 100))) / 100) + "  "
                                                                                                                   "m/"
                                                                                                                   "s");
    }
  }

  for (int i = 0; i < telemetryItem->childCount(); i++)
  {
    QTreeWidgetItem* child = telemetryItem->child(i);

    if (child->text(0) == "yaw")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->rotation_angles_msgs.vector.z * 100))) / 100) +
                            "  deg");
    }
    if (child->text(0) == "pitch")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->rotation_angles_msgs.vector.y * 100))) / 100) +
                            "  deg");
    }
    if (child->text(0) == "roll")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->rotation_angles_msgs.vector.x * 100))) / 100) +
                            "  deg");
    }
    if (child->text(0) == "IMU.ang.vel.x")
    {
      child->setText(1,
                     QString::number(((double)((int)(telem_receiver->imu_msgs.angular_velocity.x * 100))) / 100) + "  "
                                                                                                                   "m/"
                                                                                                                   "s");
    }
    if (child->text(0) == "IMU.ang.vel.y")
    {
      child->setText(1,
                     QString::number(((double)((int)(telem_receiver->imu_msgs.angular_velocity.y * 100))) / 100) + "  "
                                                                                                                   "m/"
                                                                                                                   "s");
    }
    if (child->text(0) == "IMU.ang.vel.z")
    {
      child->setText(1,
                     QString::number(((double)((int)(telem_receiver->imu_msgs.angular_velocity.z * 100))) / 100) + "  "
                                                                                                                   "m/"
                                                                                                                   "s");
    }
    if (child->text(0) == "IMU.accel.x")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->imu_msgs.linear_acceleration.x * 100))) / 100) +
                            "  deg");
    }
    if (child->text(0) == "IMU.accel.y")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->imu_msgs.linear_acceleration.y * 100))) / 100) +
                            "  deg");
    }
    if (child->text(0) == "IMU.accel.z")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->imu_msgs.linear_acceleration.z * 100))) / 100) +
                            "  deg");
    }
    if (child->text(0) == "IMU.roll")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->imu_msgs.orientation.x * 100))) / 100) + "  "
                                                                                                                 "deg");
    }
    if (child->text(0) == "IMU.pitch")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->imu_msgs.orientation.y * 100))) / 100) + "  "
                                                                                                                 "deg");
    }
    if (child->text(0) == "IMU.yaw")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->imu_msgs.orientation.z * 100))) / 100) + "  "
                                                                                                                 "deg");
    }
    if (child->text(0) == "IMU.yawPitch")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->imu_msgs.orientation.w * 100))) / 100) + "  "
                                                                                                                 "deg");
    }
    if (child->text(0) == "altitude")
    {
      child->setText(1, QString::number(((double)((int)((-1) * telem_receiver->altitude_msgs.altitude * 100))) / 100) +
                            "  m");
    }
    if (child->text(0) == "altitudeSpeed")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->altitude_msgs.altitude_speed * 100))) / 100) +
                            "  m/s");
    }

    if (child->text(0) == "mag.X")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->magnetometer_msgs.vector.x * 100))) / 100));
    }

    if (child->text(0) == "mag.Y")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->magnetometer_msgs.vector.y * 100))) / 100));
    }
    if (child->text(0) == "mag.Z")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->magnetometer_msgs.vector.z * 100))) / 100));
    }

    if (child->text(0) == "groundSpeed.X")
    {
      child->setText(1,
                     QString::number(((double)((int)(telem_receiver->ground_speed_msgs.vector.x * 100))) / 100) + "  "
                                                                                                                  "m/"
                                                                                                                  "s");
    }

    if (child->text(0) == "groundSpeed.Y")
    {
      child->setText(1,
                     QString::number(((double)((int)(telem_receiver->ground_speed_msgs.vector.y * 100))) / 100) + "  "
                                                                                                                  "m/"
                                                                                                                  "s");
    }

    if (child->text(0) == "temperature")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->temperature.temperature * 100))) / 100) + "  "
                                                                                                                  "de"
                                                                                                                  "g");
    }
    if (child->text(0) == "preassure")
    {
      child->setText(1, QString::number(((double)((int)(telem_receiver->fluid_pressure.fluid_pressure * 100))) / 100) +
                            "  Pa");
    }
  }

  for (int i = 0; i < ekfItem->childCount(); i++)
  {
    QTreeWidgetItem* child = ekfItem->child(i);

    if (child->text(0) == "pos.x")
    {
      child->setText(1,
                     QString::number(((double)((int)(odometry->drone_pose_msgs.pose.position.x * 100))) / 100) + "  m");
    }
    if (child->text(0) == "pos.y")
    {
      child->setText(1,
                     QString::number(((double)((int)(odometry->drone_pose_msgs.pose.position.y * 100))) / 100) + "  m");
    }
    if (child->text(0) == "pos.z")
    {
      child->setText(1,
                     QString::number(((double)((int)(odometry->drone_pose_msgs.pose.position.z * 100))) / 100) + "  m");
    }
    if (child->text(0) == "yaw")
    {
      child->setText(1, QString::number(((double)((int)(odometry->yaw * 100))) / 100) + "  rad");
    }
    if (child->text(0) == "pitch")
    {
      child->setText(1, QString::number(((double)((int)(odometry->pitch * 100))) / 100) + "  rad");
    }
    if (child->text(0) == "roll")
    {
      child->setText(1, QString::number(((double)((int)(odometry->roll * 100))) / 100) + "  rad");
    }

    if (child->text(0) == "dx")
    {
      child->setText(1,
                     QString::number(((double)((int)(odometry->drone_speeds_msgs.twist.linear.x * 100))) / 100) + "  "
                                                                                                                  "m/"
                                                                                                                  "s");
    }
    if (child->text(0) == "dy")
    {
      child->setText(1,
                     QString::number(((double)((int)(odometry->drone_speeds_msgs.twist.linear.y * 100))) / 100) + "  "
                                                                                                                  "m/"
                                                                                                                  "s");
    }
    if (child->text(0) == "dz")
    {
      child->setText(1,
                     QString::number(((double)((int)(odometry->drone_speeds_msgs.twist.linear.z * 100))) / 100) + "  "
                                                                                                                  "m/"
                                                                                                                  "s");
    }
    if (child->text(0) == "dyaw")
    {
      child->setText(1,
                     QString::number(((double)((int)(odometry->drone_speeds_msgs.twist.angular.x * 100))) / 100) + "  "
                                                                                                                   "rad"
                                                                                                                   "/"
                                                                                                                   "s");
    }
    if (child->text(0) == "dpitch")
    {
      child->setText(1,
                     QString::number(((double)((int)(odometry->drone_speeds_msgs.twist.angular.y * 100))) / 100) + "  "
                                                                                                                   "rad"
                                                                                                                   "/"
                                                                                                                   "s");
    }
    if (child->text(0) == "droll")
    {
      child->setText(1,
                     QString::number(((double)((int)(odometry->drone_speeds_msgs.twist.angular.z * 100))) / 100) + "  "
                                                                                                                   "rad"
                                                                                                                   "/"
                                                                                                                   "s");
    }
  }
}

void ParameterTemporalSeries::on_toolButton_clicked()
{
  dialog->show();
}

void ParameterTemporalSeries::dialogIsFinished(int)
{
  ui->tree_widget->clear();
  parameters = dialog->getSelectedParameters();

  this->initTree(parameters, ui->tree_widget);

  plot->parameters_list = plot->setCurveLabels(parameters);

  plot->selected_items.clear();
  plot->d_y.clear();

  for (const QString& key : plot->curves.keys())
  {
    plot->curves[key]->setVisible(false);
  }
  plot->curves.clear();
  plot->replot();
  plot->initCurves();
  plot->data_count = 0;
}

ParameterTemporalSeries::~ParameterTemporalSeries()
{
  delete ui;
  delete plot;
}
void ParameterTemporalSeries::closeEvent(QCloseEvent* event)
{
  windowClosedMsgs.id = 12;
  window_closed_publ.publish(windowClosedMsgs);
}
void ParameterTemporalSeries::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void ParameterTemporalSeries::windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg)
{
  aerostack_msgs::WindowIdentifier windowOpenedMsgs = msg;

  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_MISSION_INTERPRETER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_PYTHON ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_TML)
  {
    windowClosedMsgs.id = 12;
    window_closed_publ.publish(windowClosedMsgs);
    killMe();
  }
  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::MINIMIZE_MAIN_WINDOW)
    showMinimized();
}
