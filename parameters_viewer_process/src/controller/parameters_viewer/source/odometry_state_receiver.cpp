/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  OdometryStateReceiver
  Launch a ROS node to subscribe topics.
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/

#include "../include/odometry_state_receiver.h"

/*****************************************************************************
** Implementation
*****************************************************************************/

OdometryStateReceiver::OdometryStateReceiver()
{
  subscriptions_complete = false;
}

void OdometryStateReceiver::openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace)
{
  readParams(nodeHandle);

  DroneEstimatedPoseSubs = nodeHandle.subscribe(rosnamespace + "/" + odometry_namespace + "/" + drone_pose_subscription,
                                                1, &OdometryStateReceiver::droneEstimatedPoseCallback, this);

  DroneEstimatedSpeedSubs =
      nodeHandle.subscribe(rosnamespace + "/" + odometry_namespace + "/" + drone_speeds_subscription, 1,
                           &OdometryStateReceiver::droneEstimatedSpeedCallback, this);
  // Topic communications controller
  // Controller references (rebroadcasts): control mode and position, speed and trajectory references
  DroneTrajectoryPositionSubs =
      nodeHandle.subscribe(rosnamespace + "/" + drone_logger_position_ref_rebroadcast_subscription, 1,
                           &OdometryStateReceiver::dronePoseCallback, this);

  DroneTrajectorySpeedsSubs = nodeHandle.subscribe(rosnamespace + "/" + drone_logger_speed_ref_rebroadcast_subscription,
                                                   1, &OdometryStateReceiver::droneSpeedsCallback, this);

  subscriptions_complete = true;

  Q_EMIT parameterReceived();
}

bool OdometryStateReceiver::ready()
{
  if (!subscriptions_complete)
    return false;
  return true;  // Used this way instead of "return subscriptions_complete" due to preserve add more conditions
}

OdometryStateReceiver::~OdometryStateReceiver()
{
}

void OdometryStateReceiver::readParams(ros::NodeHandle nodeHandle)
{
  nodeHandle.param<std::string>("pose_topic", drone_pose_subscription, "pose");
  nodeHandle.param<std::string>("speed_topic", drone_speeds_subscription, "speeds");
  nodeHandle.param<std::string>("self_localization", odometry_namespace, "self_localization");
  nodeHandle.param<std::string>("drone_logger_pose_ref_rebroadcast_subscription",
                                drone_logger_position_ref_rebroadcast_subscription,
                                "trajectoryControllerPoseReferencesRebroadcast");
  nodeHandle.param<std::string>("drone_logger_twist_ref_rebroadcast_subscription",
                                drone_logger_speed_ref_rebroadcast_subscription,
                                "trajectoryControllerTwistReferencesRebroadcast");
}

void OdometryStateReceiver::droneEstimatedPoseCallback(const geometry_msgs::PoseStamped msg)
{
  drone_pose_msgs = msg;

  Eigen::Quaternionf q(drone_pose_msgs.pose.orientation.w, drone_pose_msgs.pose.orientation.x,
                       drone_pose_msgs.pose.orientation.y, drone_pose_msgs.pose.orientation.z);

  toEulerAngle(q, roll, pitch, yaw);
  Q_EMIT parameterReceived();
  Q_EMIT updateStatus();
  // printDroneGMREstimatedPoseCallback(msg);
  Q_EMIT updateDronePosition();
  return;
}

void OdometryStateReceiver::droneEstimatedSpeedCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
  drone_speeds_msgs = *msg;
  Q_EMIT parameterReceived();
  Q_EMIT updateStatus();
  // printDroneGMREstimatedSpeedCallback(msg);
  return;
}

void OdometryStateReceiver::dronePoseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  drone_controller_pose_msgs = *msg;
  // Vehicle orientation
  Eigen::Quaternionf q(msg->pose.orientation.w, msg->pose.orientation.x, msg->pose.orientation.y,
                       msg->pose.orientation.z);
  toEulerAngle(q, roll_c, pitch_c, yaw_c);

  Q_EMIT parameterReceived();
  Q_EMIT updateStatus();
  // printDronePoseCallback(msg);
  return;
}

void OdometryStateReceiver::droneSpeedsCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
  drone_controller_speeds_msgs = *msg;
  Q_EMIT parameterReceived();
  Q_EMIT updateStatus();
  // printDroneSpeedsCallback(msg);
  return;
}

void OdometryStateReceiver::toEulerAngle(const Eigen::Quaternionf& q, float& roll, float& pitch, float& yaw)
{
  // roll (x-axis rotation)
  double sinr_cosp = +2.0 * (q.w() * q.x() + q.y() * q.z());
  double cosr_cosp = +1.0 - 2.0 * (q.x() * q.x() + q.y() * q.y());
  roll = atan2(sinr_cosp, cosr_cosp);

  // pitch (y-axis rotation)
  double sinp = +2.0 * (q.w() * q.y() - q.z() * q.x());
  if (fabs(sinp) >= 1)
    pitch = copysign(M_PI / 2, sinp);  // use 90 degrees if out of range
  else
    pitch = asin(sinp);

  // yaw (z-axis rotation)
  double siny_cosp = +2.0 * (q.w() * q.z() + q.x() * q.y());
  double cosy_cosp = +1.0 - 2.0 * (q.y() * q.y() + q.z() * q.z());
  yaw = atan2(siny_cosp, cosy_cosp);
}
