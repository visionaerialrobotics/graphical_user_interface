/*!*******************************************************************************************
 *  \file       odometry_state_receiver.h
 *  \brief      OdometryStateReceiver definition file.
 *  \details    This file includes the OdometryStateReceiver class declaration. To obtain more
 *              information about it's definition consult the odometry_state_receiver.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef HumanMachineInterface_ODOMETRYSTATERECEIVER_H_
#define HumanMachineInterface_ODOMETRYSTATERECEIVER_H_

#include <ros/ros.h>
#include <ros/network.h>
#include <Eigen/Geometry>

/*Messages*/
#include "aerostack_msgs/AliveSignal.h"
#include "aerostack_msgs/ProcessError.h"
#include "droneMsgsROS/ErrorType.h"
#include "droneMsgsROS/obsVector.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include "geometry_msgs/Point.h"
#include "std_msgs/String.h"
#include "std_msgs/Float32MultiArray.h"

#include <QString>
#include <QThread>
#include <QtDebug>
#include <QStringListModel>

#include <string>
#include <sstream>
#include <tf/tf.h>

/*****************************************************************************
** Class
*****************************************************************************/

class OdometryStateReceiver : public QObject
{
  Q_OBJECT
public:
  OdometryStateReceiver();
  virtual ~OdometryStateReceiver();

  void run();
  bool ready();

  geometry_msgs::TwistStamped drone_speeds_msgs;
  geometry_msgs::PoseStamped drone_pose_msgs;

  geometry_msgs::TwistStamped drone_controller_speeds_msgs;
  geometry_msgs::PoseStamped drone_controller_pose_msgs;

  void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);
  void readParams(ros::NodeHandle nodeHandle);
  float roll, pitch, yaw;
  float roll_c, pitch_c, yaw_c;
  void toEulerAngle(const Eigen::Quaternionf& q, float& roll, float& pitch, float& yaw);

Q_SIGNALS:

  void parameterReceived();
  void updateStatus();
  void supervisorStateReceived();
  void updateDronePosition();

private:
  bool subscriptions_complete;

  std::string drone_pose_subscription;
  std::string drone_speeds_subscription;
  std::string odometry_namespace;

  std::string drone_logger_position_ref_rebroadcast_subscription;
  std::string drone_logger_speed_ref_rebroadcast_subscription;

  int init_argc;
  int real_time;
  char** init_argv;

  ros::Subscriber DroneEstimatedPoseSubs;
  void droneEstimatedPoseCallback(const geometry_msgs::PoseStamped msg);

  ros::Subscriber DroneEstimatedSpeedSubs;
  void droneEstimatedSpeedCallback(const geometry_msgs::TwistStamped::ConstPtr& msg);

  ros::Subscriber DroneTrajectoryPositionSubs;
  void dronePoseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);

  ros::Subscriber DroneTrajectorySpeedsSubs;
  void droneSpeedsCallback(const geometry_msgs::TwistStamped::ConstPtr& msg);
};

#endif /* HumanMachineInterface_ODOMETRYSTATERECEIVER_H_ */
