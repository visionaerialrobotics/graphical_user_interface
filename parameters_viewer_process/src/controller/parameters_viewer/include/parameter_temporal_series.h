/*!*******************************************************************************************
 *  \file       parameter_temporal_series.h
 *  \brief      ParameterTemporalSeries definition file.
 *  \details    This file includes the ParameterTemporalSeries class declaration. To obtain more
 *              information about it's definition consult the parameter_temporal_series.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef PARAMETERTEMPORALSERIES_H
#define PARAMETERTEMPORALSERIES_H

#include "aerostack_msgs/WindowIdentifier.h"

#include <QDockWidget>
#include <QTreeWidgetItem>
#include <QListWidgetItem>
#include <QTreeWidget>
#include <QString>
#include <QLabel>
#include <QLayout>
#include <QDockWidget>
#include <QWidget>
#include <QCheckBox>
#include <QVariant>
#include <QMap>
#include <QMapIterator>  //QMapIterator>
#include <QRegExp>
#include <QTreeWidget>  //QTreeWidget>
#include <QProcess>

#include "telemetry_state_receiver.h"
#include "odometry_state_receiver.h"
#include "parameters_viewer_dialog.h"
#include "ui_viewparametertemporalseries.h"

#include <tf/tf.h>
#include <angles/angles.h>
#include "data_plot.h"
#include <qwt/qwt.h>
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_curve.h>
#include <cmath>
#include <list>

namespace Ui
{
class parameterTemporalSeries;
}

class ParameterTemporalSeries : public QWidget
{
  Q_OBJECT

  /*!********************************************************************************************************************
   *  \brief      This method initializes the tree
   **********************************************************************************************************************/
  void initTree(QMap<QString, QStringList> topic_list, QTreeWidget* tree);
  /*!********************************************************************************************************************
   *  \brief      This method initialized the parameters list
   **********************************************************************************************************************/
  void initParameterList(QStringList list, QTreeWidget* tree);
  /*!********************************************************************************************************************
   *  \brief      This method adds a root to the tree
   **********************************************************************************************************************/
  void addRootTree(QString name, QStringList list, QTreeWidget* tree);
  /*!********************************************************************************************************************
   *  \brief      This method adds a child to a tree item
   **********************************************************************************************************************/
  void addChildTree(QTreeWidgetItem* parent, QStringList list, QString description);
  /*!********************************************************************************************************************
   *  \brief      This method sets the needed Qt connections
   **********************************************************************************************************************/
  void setSignalHandlers();

public:
  explicit ParameterTemporalSeries(int argc, char** argv, QWidget* parent = 0);
  void resizeEvent(QResizeEvent* event);
  ~ParameterTemporalSeries();

  ros::NodeHandle n;

  QStringList telemetry;
  QStringList ekf;
  QStringList controller;
  QStringList arucoSlam;

  DataPlot* plot;
  ParametersViewerDialog* dialog;

  std::string rosnamespace;
  std::string drone_id_namespace;
  ros::Publisher window_closed_publ;
  ros::Subscriber window_opened_subs;

  std::string window_opened_topic;
  std::string window_closed_topic;

  aerostack_msgs::WindowIdentifier windowClosedMsgs;
  aerostack_msgs::WindowIdentifier windowOpenedMsgs;

  double roll, pitch, yaw;

public Q_SLOTS:

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the filters text is changed
   **********************************************************************************************************************/
  void onTextFilterChange(const QString& arg1);

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user wants to see the units
   **********************************************************************************************************************/
  void onShowUnits(bool click);

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the stop button is clicked
   **********************************************************************************************************************/
  void onStopButton();

  /*!********************************************************************************************************************
   *  \brief      This slot updates the parameters values
   **********************************************************************************************************************/
  void updateParametersValue();

  /*!********************************************************************************************************************
   *  \brief      This slot clears the widget focus
   **********************************************************************************************************************/
  void clearFocus();

  /*!********************************************************************************************************************
   *  \brief      This method shows a dialog to select parameters to be shown
   **********************************************************************************************************************/
  void on_toolButton_clicked();
  /*!********************************************************************************************************************
   *  \brief      This method shows a dialog to select parameters to be shown
   **********************************************************************************************************************/

  void dialogIsFinished(int);
  /*!********************************************************************************************************************
   *  \brief      This method notifies main window that the widget was closed
   *********************************************************************************************************************/
  void closeEvent(QCloseEvent* event);

private:
  Ui::parameterTemporalSeries* ui;
  QStringList paramsTelemetry;
  TelemetryStateReceiver* telem_receiver;
  OdometryStateReceiver* odometry;
  QMap<QString, QStringList> parameters;

  /*!********************************************************************************************************************
   *  \brief      This method is the responsible for seting up connections.
   *********************************************************************************************************************/
  void setUp();
  /*!************************************************************************
   *  \brief   Activated when a window is closed.
   ***************************************************************************/
  void windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg);
  /*!************************************************************************
   *  \brief  Kills the process
   ***************************************************************************/
  void killMe();
};

#endif  // PARAMETERTEMPORALSERIES_H
