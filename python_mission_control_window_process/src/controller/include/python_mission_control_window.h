/*!*******************************************************************************************
 *  \file      python_mission_control_window.h
 *  \brief     Python  Mission Control Window definition file.
 *  \details   The control panel shows drone-related information and manages the buttons for interaction with the drone.
 *  \author    Daniel Del Olmo
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef PYTHON_MISSION_CONTROL_WINDOW_H
#define PYTHON_MISSION_CONTROL_WINDOW_H

#include <ros/ros.h>
#include <droneMsgsROS/openMissionFile.h>
#include <aerostack_msgs/RequestBehavior.h>
#include <aerostack_msgs/BehaviorCommand.h>
#include <droneMsgsROS/ListOfBehaviors.h>
#include <aerostack_msgs/BehaviorEvent.h>
#include "droneMsgsROS/CompletedMission.h"
#include "droneMsgsROS/battery.h"
#include "droneMsgsROS/configurationFolder.h"
#include <droneMsgsROS/openMissionFile.h>
#include "std_msgs/Bool.h"

#include <QObject>
#include <QWidget>
#include <QString>
#include <QTime>
#include <QTimer>
#include <QMessageBox>
#include <QDir>

#include <thread>
#include <iostream>
#include <stdio.h>
#include <chrono>
#include "std_srvs/Empty.h"
#include "std_msgs/Bool.h"

#include "ui_python_mission_control_window.h"

namespace Ui
{
class PythonMissionControlWindow;
}

class PythonMissionControlWindow : public QWidget
{
  Q_OBJECT

public:
  // Constructor & Destructor
  explicit PythonMissionControlWindow(QWidget* parent);
  ~PythonMissionControlWindow();

  /*!************************************************************************
   *  \brief   This method change the control buttons when mission is finished
   ***************************************************************************/
  void displayMissionCompleted();
  void displayMissionStarted();

private:
  // Layout
  Ui::PythonMissionControlWindow* ui;

  ros::NodeHandle n;
  ros::ServiceClient activate_behavior_srv;
  ros::Subscriber list_of_behaviors_sub;
  ros::Subscriber completed_mission_subs;
  ros::Subscriber battery_subs;
  ros::Subscriber wificonnection_subs;
  ros::ServiceClient configuration_folder_client;
  ros::Subscriber behavior_event_sub;
  ros::Publisher mission_state_publ;
  droneMsgsROS::configurationFolder::Request request;
  droneMsgsROS::configurationFolder::Response response;
  aerostack_msgs::RequestBehavior::Request req;
  aerostack_msgs::RequestBehavior::Response res;
  aerostack_msgs::BehaviorCommand behavior;
  droneMsgsROS::battery battery_msgs;
  std_msgs::Bool missionStateMsgs;
  aerostack_msgs::RequestBehavior::Response res_activate;
  aerostack_msgs::RequestBehavior::Request req_activate;

  std::string list_of_active_behaviors;
  std::string behavior_event;
  std::string python_srv;
  std::string activate_behavior;
  std::string python_mission;
  std::string drone_id_namespace;
  std::string completed_mission_topic;
  std::string drone_driver_sensor_battery;
  std::string wifi_connection_topic;
  std::string configuration_folder;
  std::string configuration_folder_service;
  std::string homePath;
  std::string default_folder;

  QTimer* flight_timer;  // Timer that sends the timeout signal every second.
  QTime* current_time;
  QMessageBox msgBox;
  QString completed_mission;

  std::string mission_state_topic;

  int d_interval;
  int d_timerId;
  bool running;
  bool is_wifi_connected;

  /*!********************************************************************************************************************
   *  \brief This method initializes the timer that informs about the time the drone has been flying.
   *  \param ms The interval at which the timer works.
   *********************************************************************************************************************/
  void setTimerInterval(double ms);

  /*!************************************************************************
   *  \brief  This method load the python file.
   *************************************************************************/
  void loadPythonFile();

  void behaviorEventCallBack(const aerostack_msgs::BehaviorEvent& msg);

  /*!********************************************************************************************************************
   *  \brief      This callback is executed when the list of behaviors is modified.
   *  \details    It's purpose is to control the state of the drone and the actions the GUI should allow the user to
   *execute.
   *********************************************************************************************************************/
  void newBehaviorCallback(const droneMsgsROS::ListOfBehaviors& msg);

  /*!************************************************************************
   *  \brief   Processes the status of the current mission.
   ***************************************************************************/
  void completedMissionCallback(const droneMsgsROS::CompletedMission& msg);

  /*!************************************************************************
   *  \brief   Receives the battery status.
   ***************************************************************************/
  void batteryCallback(const droneMsgsROS::battery::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the state of the WiFi connection
   **************************************************************************/
  void wifiConnectionCheckCallback(const std_msgs::Bool::ConstPtr& msg);

public Q_SLOTS:

  /*!********************************************************************************************************************
   *  \brief      This method takes action when the user wants to make the drone to land.
   *********************************************************************************************************************/
  void landPythonButton();

  /*!********************************************************************************************************************
   *  \brief      This method starts a python execution
   *********************************************************************************************************************/
  void executePythonMission();

  /*!********************************************************************************************************************
   *  \brief      This method cancels the current python execution
   *********************************************************************************************************************/
  void abortPythonMission();

  /*!********************************************************************************************************************
   *  \brief      This method informs about the time the drone has been flying.
   *********************************************************************************************************************/
  void flightTime();

Q_SIGNALS:
};

#endif  // PYTHON_MISSION_CONTROL_WINDOW_H
