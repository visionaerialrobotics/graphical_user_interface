/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  PythonMissionControlWindow
  @author  Abraham Carrera, Daniel Del Olmo
  @date    04-2018
  @version 3.0
*/

#include "../include/python_mission_control_window.h"

PythonMissionControlWindow::PythonMissionControlWindow(QWidget* parent)
  : QWidget(parent), ui(new Ui::PythonMissionControlWindow)  // initialize ui member
{
  ui->setupUi(this);  // connects all ui's triggers
  ui->abort_python_button->hide();
  ui->emergency_land_python_button->hide();

  running = false;

  // Flight time
  this->current_time = new QTime(0, 0, 0);
  setTimerInterval(1000);  // 1 second = 1000
  flight_timer = new QTimer(this);
  flight_timer->start(1000);

  // Default configuration folder
  homePath = QDir::homePath().toStdString();
  default_folder = homePath + "/workspace/ros/aerostack_catkin_ws/src/aerostack_stack/configs" + "/drone1";

  // Nodes
  n.param<std::string>("pml_mission_interpreter", python_srv, "python_based_mission_interpreter_process");
  n.param<std::string>("activate_behavior", activate_behavior, "activate_behavior");
  n.param<std::string>("list_of_active_behaviors", list_of_active_behaviors, "list_of_active_behaviors");
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("configuration_folder", configuration_folder_service, "configuration_folder");
  n.param<std::string>("mission_state", mission_state_topic, "mission_state");
  n.param<std::string>("behavior_event", behavior_event, "behavior_event");

  if (!n.getParam("drone_Completed_Mission_Topic_Name", completed_mission_topic))
    completed_mission_topic = "completed_mission";

  if (!n.getParam("drone_driver_sensor_battery", drone_driver_sensor_battery))
    drone_driver_sensor_battery = "battery";

  if (!n.getParam("wifiIsOk", wifi_connection_topic))
    wifi_connection_topic = "wifiIsOk";

  // Service communications
  activate_behavior_srv =
      n.serviceClient<aerostack_msgs::RequestBehavior>("/" + drone_id_namespace + "/" + activate_behavior);
  configuration_folder_client = n.serviceClient<droneMsgsROS::configurationFolder>(configuration_folder_service);
  configuration_folder_client.call(request, response);
  configuration_folder = response.folder;

  // Subscriptions
  list_of_behaviors_sub = n.subscribe("/" + drone_id_namespace + "/" + list_of_active_behaviors, 1000,
                                      &PythonMissionControlWindow::newBehaviorCallback, this);
  behavior_event_sub = n.subscribe("/" + drone_id_namespace + "/" + behavior_event, 1000,
                                   &PythonMissionControlWindow::behaviorEventCallBack, this);
  completed_mission_subs =
      n.subscribe(completed_mission_topic, 1, &PythonMissionControlWindow::completedMissionCallback, this);
  battery_subs = n.subscribe("/" + drone_id_namespace + "/" + drone_driver_sensor_battery, 1,
                             &PythonMissionControlWindow::batteryCallback, this);
  wificonnection_subs = n.subscribe("/" + drone_id_namespace + "/" + wifi_connection_topic, 1,
                                    &PythonMissionControlWindow::wifiConnectionCheckCallback, this);
  ui->value_vehicle->setText(QString::fromUtf8(drone_id_namespace.c_str()));

  // Publishers
  mission_state_publ = n.advertise<std_msgs::Bool>("/" + drone_id_namespace + "/" + mission_state_topic, 1, true);

  // Load tree
  loadPythonFile();

  // Connects
  QObject::connect(ui->execute_python_button, SIGNAL(clicked()), this, SLOT(executePythonMission()));
  QObject::connect(ui->emergency_land_python_button, SIGNAL(clicked()), this, SLOT(landPythonButton()));
  QObject::connect(ui->abort_python_button, SIGNAL(clicked()), this, SLOT(abortPythonMission()));
  QObject::connect(flight_timer, SIGNAL(timeout()), this, SLOT(flightTime()));
}

PythonMissionControlWindow::~PythonMissionControlWindow()
{
  delete ui;
  delete current_time;
  delete flight_timer;
}

void PythonMissionControlWindow::flightTime()
{
  if (running)
  {
    this->current_time->setHMS(this->current_time->addSecs(+1).hour(), this->current_time->addSecs(+1).minute(),
                               this->current_time->addSecs(+1).second());
    QString text = this->current_time->toString();
    ui->value_flight_time->setText(text);
  }
}

void PythonMissionControlWindow::setTimerInterval(double ms)
{
  d_interval = qRound(ms);
  if (d_interval >= 0)
    d_timerId = startTimer(d_interval);
}

void PythonMissionControlWindow::loadPythonFile()
{
  droneMsgsROS::openMissionFile msg;
  if (configuration_folder == "")
    msg.request.mission_file_path = default_folder + "/mission.py";

  else
    msg.request.mission_file_path = configuration_folder + "/mission.py";

  ros::service::call("/" + drone_id_namespace + "/" + "python_based_mission_interpreter_process/select_mission_file",
                     msg);
}

void PythonMissionControlWindow::executePythonMission()
{
  std_srvs::Empty msg;
  ros::service::call("/" + drone_id_namespace + "/" + python_srv + "/start", msg);
  displayMissionStarted();
  /*ui->selection_mode->setEnabled(false); //No se puede ejecutar otras misiones
  connect->setMissionMode();
    ->> clear_button->show();
        object_controller->setMissionMode(true);
        received_data_processor->setMissionMode();
        setFocusPolicy(Qt::NoFocus);*/
}

void PythonMissionControlWindow::landPythonButton()
{
  std_srvs::Empty msg;
  ros::service::call("/" + drone_id_namespace + "/" + python_srv + "/stop", msg);
  behavior.name = "LAND";
  req.behavior = behavior;
  activate_behavior_srv.call(req, res);
  if (!res.ack)
    std::cout << res.error_message << std::endl;

  displayMissionCompleted();
}

void PythonMissionControlWindow::abortPythonMission()
{
  std_srvs::Empty msg;
  ros::service::call("/" + drone_id_namespace + "/" + python_srv + "/stop", msg);
  aerostack_msgs::BehaviorCommand behavior;
  behavior.name = "KEEP_HOVERING";
  req_activate.behavior = behavior;
  activate_behavior_srv.call(req_activate, res_activate);

  if (!res_activate.ack)
    std::cout << res_activate.error_message << std::endl;

  displayMissionCompleted();
  running = false;
}

void PythonMissionControlWindow::displayMissionCompleted()
{
  ui->abort_python_button->hide();
  ui->emergency_land_python_button->hide();
  std::this_thread::sleep_for(std::chrono::milliseconds(5));
  ui->execute_python_button->show();

  missionStateMsgs.data = false;
  mission_state_publ.publish(missionStateMsgs);
}

void PythonMissionControlWindow::displayMissionStarted()
{
  ui->execute_python_button->hide();
  ui->abort_python_button->show();
  ui->emergency_land_python_button->show();
  running = true;
  missionStateMsgs.data = true;

  mission_state_publ.publish(missionStateMsgs);
}

void PythonMissionControlWindow::behaviorEventCallBack(const aerostack_msgs::BehaviorEvent& msg)
{
  /* std::cout << "Behavior uid: " << msg.uid << std::endl;
   std::cout << "Behavior name: " << msg.name << std::endl;*/
}

void PythonMissionControlWindow::completedMissionCallback(const droneMsgsROS::CompletedMission& ack)
{
  displayMissionCompleted();
  running = false;
  std::cout << "Mission completed" << std::endl;
}

void PythonMissionControlWindow::newBehaviorCallback(const droneMsgsROS::ListOfBehaviors& msg)
{
  if (msg.behaviors.size() != 0)
  {
    for (int i = 0; i < msg.behaviors.size(); i++)
    {
      if (msg.behaviors[i] == "TAKE_OFF")
      {
        this->current_time->setHMS(00, 00, 00);
        QString text = this->current_time->toString();
        ui->value_flight_time->setText(text);
        running = true;
      }
      else if (msg.behaviors[i] == "LAND")
      {
        running = false;
      }
    }
    // Battery
    if (battery_msgs.batteryPercent <= 25.0 && battery_msgs.batteryPercent != 0)
    {
      QPalette* palette = new QPalette();
      palette->setColor(QPalette::WindowText, Qt::red);
      ui->value_battery->setPalette(*palette);
    }
    ui->value_battery->setText(QString::number(battery_msgs.batteryPercent) + "%");

    // Connection
    if (is_wifi_connected)
      ui->value_wifi->setText("Connected");
    else
      ui->value_wifi->setText("Disconnected");
  }
}

void PythonMissionControlWindow::batteryCallback(const droneMsgsROS::battery::ConstPtr& msg)
{
  battery_msgs = *msg;
  ui->value_battery->setText(QString::number(battery_msgs.batteryPercent) + "%");
  // Q_EMIT parameterReceived();
  return;
}

void PythonMissionControlWindow::wifiConnectionCheckCallback(const std_msgs::Bool::ConstPtr& msg)
{
  is_wifi_connected = msg->data;
}
