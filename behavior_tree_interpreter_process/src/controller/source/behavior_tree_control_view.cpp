/*
  BehaviorTreeEditorView
  @author  Daniel Del Olmo
  @date    04-2019
  @version 3.0
*/

#include "../include/behavior_tree_control_view.h"

BehaviorTreeControlView::BehaviorTreeControlView(int argc, char** argv, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BehaviorTreeControlView)
{
  //window always on top
  setWindowIcon(QIcon(":/img/img/tree_control.png"));
  setWindowTitle("BEHAVIOR TREE CONTROL");
  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);

  ui->setupUi(this); //connects all ui's triggers

  //Add the control panel widget
  behavior_tree_control = new BehaviorTreeControl(this); 
  ui->gridLayout->addWidget(behavior_tree_control);

  //Settings Widget
  setWidgetDimensions();

  //Establishment of connections
  setUp();
}


BehaviorTreeControlView::~BehaviorTreeControlView()
{
  delete ui;
  delete behavior_tree_control;
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Getters and setters
--------------------------------------------------------------
------------------------------------------------------------*/
BehaviorTreeControl* BehaviorTreeControlView::getBehaviorTreeControl()
{
  return behavior_tree_control;
}

void BehaviorTreeControlView::setWidgetDimensions(){
  namespace pt = boost::property_tree;

  std::string layout_dir=std::getenv("AEROSTACK_STACK")+std::string("/stack/ground_control_system/graphical_user_interface/layouts/layout.json");

  pt::read_json(layout_dir, root);

  QScreen *screen = QGuiApplication::primaryScreen();
  QRect  screenGeometry = screen->geometry();

  int y0 = screenGeometry.height()/2;
  int x0 = screenGeometry.width()/2;
  int height = root.get<int>("14.height");
  int width= root.get<int>("14.width");

  this->resize(width,height);
  this->move(x0+root.get<int>("14.position.x"),y0+root.get<int>("14.position.y"));
}

void BehaviorTreeControlView::setUp()
{
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("activate_behavior", activate_behavior, "request_behavior_activation");
  n.param<std::string>("initiate_behaviors", initiate_behaviors, "request_behavior_deactivation");


  //Service comunications
  activate_behavior_srv=n.serviceClient<aerostack_msgs::RequestBehaviorActivation>("/"+drone_id_namespace+"/"+activate_behavior);
  initiate_behaviors_srv=n.serviceClient<droneMsgsROS::InitiateBehaviors>("/"+drone_id_namespace+"/"+initiate_behaviors);
  initiate_behaviors_srv.call(initiate_behaviors_msg);

  //Subscribers

  n.param<std::string>("window_opened", window_opened_topic, "window_opened");
  n.param<std::string>("window_closed", window_closed_topic, "window_closed");

  //Subscribers
  window_opened_subs = n.subscribe("/" + drone_id_namespace +  "/" + window_opened_topic, 10, &BehaviorTreeControlView::windowOpenCallback, this);

  //Publishers
  window_closed_publ = n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace +  "/" + window_closed_topic , 1, true);
}


/*------------------------------------------------------------
--------------------------------------------------------------
                Handlers for the main widget
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeControlView::loadTreeError()
{
  windowClosedMsgs.id=14;
  window_closed_publ.publish(windowClosedMsgs);
  killMe();
}

void BehaviorTreeControlView::closeEvent(QCloseEvent *event)
{
  windowClosedMsgs.id=14;
  window_closed_publ.publish(windowClosedMsgs);
}

void BehaviorTreeControlView::killMe()
{
#ifdef Q_OS_WIN
  enum { ExitCode = 0 };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif // Q_OS_WIN
}

void BehaviorTreeControlView::windowOpenCallback(const aerostack_msgs::WindowIdentifier &msg)
{
  aerostack_msgs::WindowIdentifier windowOpenedMsgs = msg;

  if(windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN || windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER ||windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::PYTHON_MISSION_INTERPRETER ||windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT ||windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::PYTHON_CONTROL || windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::ALPHANUMERIC_INTERFACE_CONTROL || windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::TML_CONTROL || windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::KEYBOARD_CONTROL ||windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_PYTHON || windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_TML){

    windowClosedMsgs.id=14;
    window_closed_publ.publish(windowClosedMsgs);
    killMe();
  }

  if (windowOpenedMsgs.id==aerostack_msgs::WindowIdentifier::MINIMIZE_MAIN_WINDOW )
    showMinimized();
}
