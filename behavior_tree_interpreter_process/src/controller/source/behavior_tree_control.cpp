/*
  BehaviorTreeControl
  @author  Abraham Carrera, Daniel Del Olmo
  @date    10-2018
  @version 3.0
*/

#include "../include/behavior_tree_control.h"

BehaviorTreeControl::BehaviorTreeControl(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::BehaviorTreeControl)
{
  ui->setupUi(this); //connects all ui's triggers
  ui->abort_tree_button->hide();
  ui->emergency_land_tree_button->hide();

  tree = new BehaviorTree(this);
  expand_text_button = new QCheckBox("Collapse text", this);
  tree_label = new QLabel("Behavior Tree");
  beliefs_label = new QLabel("Variables");
  beliefs_text = new QTextEdit(this);
  behavior_viewer = (BehaviorTreeControlView*) parent;
  
  //Widgets
  this->QWidget::setWindowTitle(QString::fromStdString("Behavior Tree"));
  ui->gridLayout_6->addWidget(tree_label, 0, 0, 1, 1);
  ui->gridLayout_6->addWidget(tree, 1, 0, 1, 1);
  ui->gridLayout_6->addWidget(expand_text_button, 2, 0, 1, 1);
  ui->gridLayout_6->addWidget(beliefs_label, 3, 0, 1, 1);
  ui->gridLayout_6->addWidget(beliefs_text, 4, 0, 1, 1);

  is_takenOff = false;
  correct_format = false;
  beliefs_text->setMaximumHeight(80);
  beliefs_text->setText(QString::fromStdString(""));
  ui->value_battery->setText(QString::number(100) +  "%");

  //Set Flight time
  this->current_time = new QTime(0, 0, 0);
  setTimerInterval(1000);// 1 second = 1000
  flight_timer = new QTimer(this);
  flight_timer->start(1000);

  //Default configuration folder
  homePath = QDir::homePath().toStdString();

  //Establishment of connections
  setUp();

  //Settings keyboard control
  setKeyboard();

  //Load tree
  loadTreeFile();

  //Connects
  QObject::connect(ui->execute_tree_button, SIGNAL(clicked()), this, SLOT(executeTreeMission()));
  QObject::connect(ui->emergency_land_tree_button, SIGNAL(clicked()), this, SLOT(landTreeMission()));
  QObject::connect(ui->abort_tree_button, SIGNAL(clicked()), this, SLOT(abortTreeMission()));
  QObject::connect(flight_timer, SIGNAL(timeout()), this, SLOT(setFlightTime()));
  QObject::connect(expand_text_button, SIGNAL(stateChanged(int)), tree, SLOT(expandTreeText(int)));
}

/*------------------------------------------------------------
--------------------------------------------------------------
                      Destructor
--------------------------------------------------------------  
------------------------------------------------------------*/
BehaviorTreeControl::~BehaviorTreeControl()
{
  delete ui;
  delete flight_timer;
  delete current_time;
  delete tree;
  delete expand_text_button;
  delete tree_label;
  delete beliefs_label;
  delete beliefs_text;
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Getters and setters
--------------------------------------------------------------
------------------------------------------------------------*/
BehaviorTree* BehaviorTreeControl::getBehaviorTree()
{
  return this -> tree;
}

std::string BehaviorTreeControl::getText()
{
  std::string result = this->beliefs_text->toPlainText().toUtf8().constData();
  return result;
}

void BehaviorTreeControl::setText(std::string texto)
{
  text = QString(texto.c_str());
}

void BehaviorTreeControl::setTimerInterval(double ms)
{
  d_interval = qRound(ms);
  if (d_interval >= 0 )
    d_timerId = startTimer(d_interval);
}

void BehaviorTreeControl::setFlightTime()
{
  if (is_takenOff)
  {
    this->current_time->setHMS(this->current_time->addSecs(+1).hour(), this->current_time->addSecs(+1).minute(), this->current_time->addSecs(+1).second());
    ui->value_flight_time->setText(this->current_time->toString());
  }
}

void BehaviorTreeControl::setStartBlockingTextInput()
{
  ui->abort_tree_button->show();
  ui->emergency_land_tree_button->show();
  ui->execute_tree_button->hide();

  beliefs_text->setReadOnly(true);
}

void BehaviorTreeControl::setStopBlockingTextInput()
{
  ui->abort_tree_button->hide();
  ui->emergency_land_tree_button->hide();
  ui->execute_tree_button->show();

  tree->connectCustomContextMenu();
  beliefs_text->setReadOnly(false);

  missionStateMsgs.data=false;
  mission_state_publ.publish(missionStateMsgs);  
}

void BehaviorTreeControl::update()
{
  beliefs_text->setText(text);
}

void BehaviorTreeControl::setUp()
{
  //Nodes
  n.param<std::string>("activate_behavior", activate_behavior, "request_behavior_activation");
  n.param<std::string>("list_of_active_behaviors", list_of_active_behaviors, "list_of_active_behaviors");
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("behavior_tree_execute", behavior_tree_execute, "behavior_tree_execute");
  n.param<std::string>("mission_state", mission_state_topic, "mission_state");
  n.param<std::string>("configuration_folder", configuration_folder_service, "configuration_folder");
  n.param<std::string>("check_behavior_format", check_behavior_format, "check_behavior_format");
  n.param<std::string>("my_stack_directory", my_stack_directory, homePath + "/workspace/ros/aerostack_catkin_ws/src/aerostack_stack/");

  if (!n.getParam("drone_driver_sensor_battery", drone_driver_sensor_battery))
    drone_driver_sensor_battery = "battery";

  if (!n.getParam("wifiIsOk", wifi_connection_topic))
    wifi_connection_topic = "wifiIsOk";

  //Service communications
  check_behavior_format_srv=n.serviceClient<aerostack_msgs::CheckBehaviorFormat>("/"+drone_id_namespace+"/"+check_behavior_format);
  activate_behavior_srv = n.serviceClient<aerostack_msgs::RequestBehaviorActivation>("/" + drone_id_namespace +  "/" + activate_behavior);
  configuration_folder_client = n.serviceClient<droneMsgsROS::configurationFolder>("/"+configuration_folder_service);
  configuration_folder_client.call(request, response);
  configuration_folder = response.folder;

  if (configuration_folder == "")
    configuration_folder = my_stack_directory +"/configs/"+ drone_id_namespace;

  std::cout << "configuration folder: " << configuration_folder << std::endl;

  //Subscribers
  list_of_behaviors_sub = n.subscribe("/" + drone_id_namespace +  "/" + list_of_active_behaviors, 1000, &BehaviorTreeControl::newBehaviorCallback, this);
  battery_subs = n.subscribe("/" + drone_id_namespace + "/" + drone_driver_sensor_battery, 1, &BehaviorTreeControl::batteryCallback, this);
  wificonnection_subs = n.subscribe("/" + drone_id_namespace + "/"  + wifi_connection_topic, 1, &BehaviorTreeControl::wifiConnectionCheckCallback, this);
  ui->value_vehicle->setText( QString::fromUtf8(drone_id_namespace.c_str()));

  //Publishers
  mission_state_publ=n.advertise<std_msgs::Bool>("/"+ drone_id_namespace +  "/" +mission_state_topic ,1, true);
}

/*------------------------------------------------------------
--------------------------------------------------------------
                    File handlers
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeControl::loadTreeFile()
{
  file_route = configuration_folder + "/behavior_tree_mission_file.yaml";

  std::ifstream aux_file(file_route);

  if (aux_file.fail())
  {
    windowManager('c', "Loading mission error", "behavior_tree_mission_file.yaml is not defined in configuration folder. Please create it using the Behavior Tree Editor.");
  }

  else
  {
    TreeFileManager tfm;
    root_node = tfm.loadTree(file_route, beliefs_text);
    if (root_node != nullptr && checkTree(root_node))
    {
        correct_format = true;
        tree->createMissionByTreeItem(root_node);
        tree->show();
        tree_label->show();
    }
  }
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Check format tree
--------------------------------------------------------------
------------------------------------------------------------*/
bool BehaviorTreeControl::checkTree(TreeItem *item)
{
  bool isCorrect = true;
  bool correctChildren = true;

  NodeType item_nodetype = item->getNodeType();

  std::string node_type_name = "";
  std::string incorrect_children = " has an incorrect number of children. ";
  std::string correct_error = ". Please correct this error before trying again.";

  switch(item_nodetype)
  {
    //The following nodes can't have children
    case NodeType::BEHAVIOR:
      node_type_name = "BEHAVIOR";

    case NodeType::QUERY: 
     node_type_name = node_type_name.empty()? "QUERY" : node_type_name;

    case NodeType::ADD_BELIEF:
      node_type_name = node_type_name.empty()? "ADD_BELIEF" : node_type_name;

    case NodeType::REMOVE_BELIEF:
    {
      node_type_name = node_type_name.empty()? "REMOVE_BELIEF" : node_type_name;

      if(item->childCount() != 0)
      {
        windowManager('c', "Bad tree format", "The node '" + item->getNodeName() + "'" + incorrect_children + "A node of '" + node_type_name + "' type can not have children" + correct_error);
        correctChildren = false;
      }

      if(item_nodetype == NodeType::BEHAVIOR)
      {
        behavior_msg.name = item->getBehaviorType();
        behavior_msg.arguments = this->processData(item->getNodeAttributes());
        check_format_msg_req.behavior = behavior_msg;
        check_behavior_format_srv.call(check_format_msg_req, check_format_msg_res);

        if(!check_format_msg_res.ack)
        {
          error_behavior = "The node '" + item->getNodeName() + "' has invalid arguments.\n\n";
          error_behavior += check_format_msg_res.error_message;
          windowManager('c', "Bad tree format", error_behavior);
          correctChildren = false;
        }
      } 
      break;
    }

    //The following nodes can have just one child
    case NodeType::SUCCEEDER:
      node_type_name = "SUCCEEDER";

    case NodeType::INVERTER:
      node_type_name = node_type_name.empty()? "INVERTER" : node_type_name;

    case NodeType::REPEATER:
    {
      node_type_name = node_type_name.empty()? "REPEATER" : node_type_name;

      if(item->childCount() != 1)
      {
        windowManager('c', "Bad tree format", "The node '" + item->getNodeName() + "'" + incorrect_children + "A node of '" + node_type_name + "' type can have just one child" + correct_error);
        correctChildren = false;
      }
      break;
    }

    //The following nodes must have at least one child
    case NodeType::SEQUENCE:
      node_type_name = "SEQUENCE";

    case NodeType::PARALLEL:
      node_type_name = node_type_name.empty()? "PARALLEL" : node_type_name;

    case NodeType::REPEAT_UNTIL_FAIL:
      node_type_name = node_type_name.empty()? "REPEAT_UNTIL_FAIL" : node_type_name;

    case NodeType::SELECTOR:
    {
      node_type_name = node_type_name.empty()? "SELECTOR" : node_type_name;

      if(item->childCount() < 1)
      {
        windowManager('c', "Bad tree format", "The node '" + item->getNodeName() + "'" + incorrect_children + "A node of '" + node_type_name + "' type must have at least one child" + correct_error);
        correctChildren = false;
      }
      break;
    }
  }

  if(!correctChildren)
    return false;

  //Checking each of it's children recursively
  //If current checked node is wrong then stop check 
  if(item->childCount()>0)
  {
    for(int i = 0; i < item->childCount(); i++)
    {
      isCorrect = isCorrect && checkTree(item->child(i));
    }
  }

  return isCorrect;
}

std::string BehaviorTreeControl::outsideProcessData(std::string raw_arguments)
{
  std::string result = this->processData(raw_arguments);
  return result;
}

std::string BehaviorTreeControl::processData(std::string raw_arguments) 
{
  std::string text = getText();
  if (text != "") 
  {
    YAML::Node node_beliefs = YAML::Load(text);
    for(YAML::const_iterator it=node_beliefs.begin();it!=node_beliefs.end();++it) 
    {
      std::string name = SUBSTITUTION_S + it->first.as<std::string>();
      if (raw_arguments.find(name)) 
      {
        std::string data = processType(it);
        boost::replace_all(raw_arguments, name, data);
      }
    }
  }
  return raw_arguments;
}

std::string BehaviorTreeControl::processType(YAML::const_iterator it) 
{
  std::string res = "";
  switch (it->second.Type()) 
  {
    case YAML::NodeType::Scalar: 
    {
      res = it->second.as<std::string>();
      break;
    }
    case YAML::NodeType::Sequence: 
    {
      std::vector<double> vec = it->second.as<std::vector<double>>();
      std::ostringstream oss;
      if (!vec.empty())
      {
        std::copy(vec.begin(), vec.end()-1,
        std::ostream_iterator<double>(oss, ","));
        oss << vec.back();
      }
      res = "[" + oss.str() + "]";
      break;
    }
  }
  return res;
}

/*------------------------------------------------------------
--------------------------------------------------------------
                    Button actions
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeControl::landTreeMission()
{  
  behavior_msg.name = "LAND";
  req.behavior = behavior_msg;
  activate_behavior_srv.call(req, res);
  if (!res.ack)
    std::cout << res.error_message << std::endl;
}

void BehaviorTreeControl::executeTreeMission()
{  
  tree->executeTree();

  missionStateMsgs.data=true;
  mission_state_publ.publish(missionStateMsgs);

}

void BehaviorTreeControl::abortTreeMission()
{
  is_takenOff = false;
  tree->cancelTree();
}

/*------------------------------------------------------------
--------------------------------------------------------------
                  Keyboard manager
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeControl::setKeyboard()
{ 
  isAKeyPressed = false;

  setFocusPolicy(Qt::StrongFocus);
  acceptedKeys.insert(0x01000012, false); //Tecla UP
  acceptedKeys.insert(0x01000013, false); //Tecla DOWN
  acceptedKeys.insert(0x01000014, false); //Tecla LEFT
  acceptedKeys.insert(0x01000015, false); //Tecla RIGHT
  acceptedKeys.insert(0x51, false); //Tecla Q
  acceptedKeys.insert(0x41, false); //Tecla A
  acceptedKeys.insert(0x54, false); //Tecla T
  acceptedKeys.insert(0x59, false); //Tecla Y
  acceptedKeys.insert(0x48, false); //Tecla H
  acceptedKeys.insert(0x5a, false); //Tecla Z
  acceptedKeys.insert(0x58, false); //Tecla X
  acceptedKeys.insert(0x52, false); //Tecla R
}

void BehaviorTreeControl::keyPressEvent(QKeyEvent *e)
{
  QWidget::keyPressEvent(e);
  
  if(!isAKeyPressed && acceptedKeys.contains(e->key()))
  {
    isAKeyPressed = true;
    switch(e->key())
    {
      case Qt::Key_R:
      {
        if(!e->isAutoRepeat())
        {
          acceptedKeys.insert(0x52, true);
          onResetCommandButton();
        }
        break;
      }
      case Qt::Key_T:
      {
        if(!e->isAutoRepeat())
        {
          acceptedKeys.insert(0x54, true);
          onTakeOffButton();
        }
        break;
      }
      case Qt::Key_Y:
      {
        if(!e->isAutoRepeat())
        {
          acceptedKeys.insert(0x59, true);
          onLandButton();
        }
        break;
      }
      case Qt::Key_H:
      {
        acceptedKeys.insert(0x48, true);
        aerostack_msgs::RequestBehaviorActivation::Request msg;
        aerostack_msgs::RequestBehaviorActivation::Response res;
        aerostack_msgs::BehaviorCommandPriority behavior;
        behavior.name = "KEEP_HOVERING_WITH_PID_CONTROL";
        behavior.priority = 3;
        msg.behavior = behavior;
        activate_behavior_srv.call(msg,res);
        if(!res.ack)
          std::cout << res.error_message << std::endl;
        break;
      }
      case Qt::Key_Right:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000014, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.priority = 3;
          behavior.arguments = "direction: \"RIGHT\"\nspeed: 0.4";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Left:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000012, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"LEFT\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Down:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000015, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"BACKWARD\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
          break;
      }
      case Qt::Key_Up:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000013, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"FORWARD\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
          break;
      }
      case Qt::Key_Q:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x51, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"UP\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_A:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x41, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"DOWN\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Z:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x5a, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "ROTATE_WITH_PID_CONTROL";
          behavior.arguments = "relative_angle: +179";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_X:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x58, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "ROTATE_WITH_PID_CONTROL";
          behavior.arguments = "relative_angle: -179";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
    }
  }
}

void BehaviorTreeControl::onTakeOffButton()
{
  aerostack_msgs::RequestBehaviorActivation::Request msg;
  aerostack_msgs::RequestBehaviorActivation::Response res;
  aerostack_msgs::BehaviorCommandPriority behavior;
  behavior.name = "TAKE_OFF";
  behavior.priority = 3;
  msg.behavior = behavior;
  activate_behavior_srv.call(msg,res);
  if(!res.ack)
    std::cout << res.error_message << std::endl;
}

void BehaviorTreeControl::onLandButton()
{

  
  aerostack_msgs::RequestBehaviorActivation::Request msg;
  aerostack_msgs::RequestBehaviorActivation::Response res;
  aerostack_msgs::BehaviorCommandPriority behavior;
  behavior.name = "LAND";
  behavior.priority = 3;
  msg.behavior = behavior;
  activate_behavior_srv.call(msg,res);
  if(!res.ack)
    std::cout << res.error_message << std::endl;
}

void BehaviorTreeControl::onResetCommandButton()
{
  /** NOTE:
  Shows strange behaviour when the drone has been ordered to rotate previously,
  and a stabilize command was not issued after the rotation.
 */
  aerostack_msgs::RequestBehaviorActivation::Request msg;
  aerostack_msgs::RequestBehaviorActivation::Response res;
  aerostack_msgs::BehaviorCommandPriority behavior;
  behavior.name = "ROTATE_WITH_PID_CONTROL";
  behavior.arguments = "angle: 0";
  behavior.priority = 3;
  msg.behavior = behavior;
  activate_behavior_srv.call(msg,res);
  if(!res.ack)
    std::cout << res.error_message << std::endl;
}

void BehaviorTreeControl::keyReleaseEvent(QKeyEvent *e)
{
    if(e->isAutoRepeat() || !acceptedKeys.contains(e->key()))
    {
      isAKeyPressed = false;
      e->ignore();
    }
    else if(acceptedKeys.contains(e->key()) && acceptedKeys.value(e->key()))
    {
      acceptedKeys.insert(e->key(), false);
      aerostack_msgs::RequestBehaviorActivation::Request msg;
      aerostack_msgs::RequestBehaviorActivation::Response res;
      aerostack_msgs::BehaviorCommandPriority behavior;
      behavior.name = "KEEP_HOVERING_WITH_PID_CONTROL";
      msg.behavior = behavior;
      behavior.priority = 1;
      if(e->key() != Qt::Key_Y && e->key() != Qt::Key_T && e->key() != Qt::Key_R)
       // activate_behavior_srv.call(msg,res);
      if(!res.ack)
          std::cout << res.error_message << std::endl;
      isAKeyPressed = false;
      QWidget::keyReleaseEvent(e);
    }
    else
    {
      isAKeyPressed = false;
      e->ignore();
      QWidget::keyReleaseEvent(e);
    }
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Messages output manager
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeControl::windowManager(char type, std::string title, std::string message)
{
  QMessageBox *msg_error = new QMessageBox(QMessageBox::Critical, title.c_str(), message.c_str(), QMessageBox::Ok,this);
  msg_error->setWindowFlags(msg_error->windowFlags() | Qt::WindowStaysOnTopHint);
  msg_error->exec();
  
  if(type == 'c')
    behavior_viewer -> loadTreeError();
}

/*------------------------------------------------------------
--------------------------------------------------------------
                      Callback methods
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeControl::newBehaviorCallback(const droneMsgsROS::ListOfBehaviors &msg)
{
  if (msg.behaviors.size() != 0)
  {
    for (int i = 0; i < msg.behaviors.size(); i++)
    {
      if (msg.behaviors[i] == "TAKE_OFF")
      {
        this->current_time->setHMS(00, 00, 00);
        ui->value_flight_time->setText(this->current_time->toString());
        is_takenOff = true;
      }
      else if (msg.behaviors[i] == "LAND")
        is_takenOff = false;
    }
    //Battery
    if (battery_msgs.batteryPercent <= 25.0 && battery_msgs.batteryPercent != 0)
    {
      QPalette* palette = new QPalette();
      palette->setColor(QPalette::WindowText, Qt::red);
      ui->value_battery->setPalette(*palette);
    }
    ui->value_battery->setText(QString::number(battery_msgs.batteryPercent) +  "%");

    //Connection
    if (is_wifi_connected)
      ui->value_wifi->setText("Connected");
    else
      ui->value_wifi->setText("Disconnected");
  }
}

void BehaviorTreeControl::batteryCallback(const droneMsgsROS::battery::ConstPtr& msg)
{
  battery_msgs = *msg;
  if (battery_msgs.batteryPercent <= 25.0 && battery_msgs.batteryPercent != 0)
  {
    QPalette* palette = new QPalette();
    palette->setColor(QPalette::WindowText, Qt::red);
    ui->value_battery->setPalette(*palette);
  }
  ui->value_battery->setText(QString::number(battery_msgs.batteryPercent) +  "%");
}

void BehaviorTreeControl::wifiConnectionCheckCallback(const std_msgs::Bool::ConstPtr& msg)
{
  is_wifi_connected = msg->data;
  if (is_wifi_connected)
    ui->value_wifi->setText("Connected");
  else
    ui->value_wifi->setText("Disconnected");
}
