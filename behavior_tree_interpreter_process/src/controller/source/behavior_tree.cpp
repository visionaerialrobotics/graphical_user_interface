/*
  BehaviorTree
  @author  Daniel Del Olmo, Jorge Luis Pascual, Carlos Valencia.
  @date    10-2018
  @version 3.0
*/

#include "../include/behavior_tree.h"
#include "../include/tree_item.h" //TreeItem class is included here to prevent recursive includes with tree_item.

BehaviorTree::BehaviorTree(QWidget *parent) : QTreeWidget(parent)
{
	//window always on top
	QWidget::setLocale(QLocale());
	this->headerItem()->setHidden(true);
	this->setContextMenuPolicy(Qt::CustomContextMenu);
	this->resize(600, 300);
	
	qRegisterMetaType<QVector<int>>("QVector<int>");

	control_parent = (BehaviorTreeControl*) parent;

	has_root = false;
	running = false;
	isTextExpanded = true;

	//signal is emitted when the widget's contextMenuPolicy is Qt::CustomContextMenu
	QObject::connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onCustomContextMenu(const QPoint &)));
	QObject::connect(this, SIGNAL(executionStarted()), parent, SLOT(setStartBlockingTextInput()));

	//Style sheet customization
	setStyleTreeSheet();

    //Establishment of connections
    setUp();
}

/*------------------------------------------------------------
--------------------------------------------------------------
                      Destructor
--------------------------------------------------------------  
------------------------------------------------------------*/
BehaviorTree::~BehaviorTree()
{

}

/*------------------------------------------------------------
--------------------------------------------------------------
                Getters and setters
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTree::setStyleTreeSheet()
{
	setIconSize(QSize(25, 25));
	this->setStyleSheet(" \
		QTreeView::branch { \
        	background: white; \
		} \
		QTreeView::branch:has-siblings:!adjoins-item { \
			border-image: url(:/images/images/vline.png) 0; \
		} \
		QTreeView::branch:has-siblings:adjoins-item { \
			border-image: url(:/images/images/branch-more.png) 0; \
		} \
		QTreeView::branch:!has-children:!has-siblings:adjoins-item { \
			border-image: url(:/images/images/branch-end.png) 0; \
		} \
		QTreeView::branch:has-children:!has-siblings:closed, \
		QTreeView::branch:closed:has-children:has-siblings { \
        	border-image: url(:/images/images/branch-more_proto_mas.png) 0; \
		} \
		QTreeView::branch:has-children:!has-siblings:closed, \
		QTreeView::branch:closed:has-children:!has-siblings { \
        	border-image: url(:/images/images/branch-end_proto_mas.png) 0; \
		} \
		QTreeView::branch:open:has-children:has-siblings  { \
        	border-image: url(:/images/images/branch-more_proto_menos.png) 0; \
		} \
		QTreeView::branch:open:has-children:!has-siblings  { \
        	border-image: url(:/images/images/branch-end_proto_menos.png) 0; \
		} \
		QTreeView::item:selected { \
      background-color:transparent; \
      color:black; \
    } \
		");
}

bool BehaviorTree::isRunning()
{
	return running;
}

BehaviorTreeControl* BehaviorTree::getVisualizer()
{
	return control_parent;
}

ExecutionTree * BehaviorTree::getExecutionTree(){
	if(running)
		return this -> et;
	else
		return nullptr;
}

void BehaviorTree::setUp()
{
   	n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
    n.param<std::string>("activate_behavior", activate_behavior, "request_behavior_activation");
    activate_behavior_srv=n.serviceClient<aerostack_msgs::RequestBehaviorActivation>("/"+drone_id_namespace+"/"+activate_behavior);
}

/*------------------------------------------------------------
--------------------------------------------------------------
                      Buttons actions
--------------------------------------------------------------  
------------------------------------------------------------*/
void BehaviorTree::createMissionByTreeItem(TreeItem * root)
{
	root_item = root;
	this->addTopLevelItem(root_item);
	if (root != 0) {
		if (isTextExpanded) {
			expandTreeText(0);
		}
		else {
			expandTreeText(2);
		}
		this->expandAll();
	}
}

/*
This function is called whenever we do a right click
It creates the context menu and disables any functionality that cannot be executed. This depends on where we right clicked.
*/
void BehaviorTree::onCustomContextMenu(const QPoint &p)
{
	if(itemAt(p)!=0)
	{
		TreeItem *item_clicked;
		if(is_menu_created)
		{
			contextMenu->close();
			delete contextMenu;
			is_menu_created = false;
		}
		contextMenu = new QMenu("Menu", this);
		is_menu_created = true;
		item_clicked = (TreeItem*)itemAt(p);
		TreeItem *item_clicked_parent;

		int children;
		//Check if the clicked node can have children
		if(item_clicked->getNodeType()==NodeType::SUCCEEDER)
		{
			children = item_clicked->childCount();
		}
		if(item_clicked->getNodeType()==NodeType::INVERTER)
		{
			children = item_clicked->childCount();
		}
		if(item_clicked->getNodeType()==NodeType::SELECTOR)
		{
			children = item_clicked->childCount();
		}

		//Check if the clicked node can have siblings
		item_clicked_parent = item_clicked->getParent();

		point = p;
        
		QAction action("Execute tree from this node", this);	
		contextMenu->addAction(&action);
		connect(&action, SIGNAL(triggered()), this, SLOT(executeTreeFromItem()));
		contextMenu->exec(mapToGlobal(p));
	}
	else if (itemAt(p)==0)
	{
		point = p;
		if(is_menu_created)
		{
			contextMenu->close();
			delete contextMenu;
			is_menu_created = false;
		}
		is_menu_created = true;
		contextMenu = new QMenu("Menu", this);
	}
}

/*
This function adds a root to the tree if it doesn't have one already
*/
void BehaviorTree::addTopLevelItem(TreeItem *top_level_item)
{
	if (has_root) {
		if (root_before != 0) {
			delete root_before;
		}
	}
	if (top_level_item != 0) {
		root_before = top_level_item;
		has_root = true;
		top_level_item->setRoot(true);
	}
	else {
		has_root = false;
	}
	QTreeWidget::addTopLevelItem(top_level_item);
}

/*
This function creates an ExecutionTree object to execute the tree's nodes, starting at the root
*/
void BehaviorTree::executeTree()
{
	if(has_root){
		if (running) 
		{
			et->cancelExecution();
			joinExecutionTree();
		}
		et = new ExecutionTree(this);
		running = true;
		if(control_parent->correct_format)
		{		
				QObject::connect(qApp,SIGNAL(lastWindowClosed()), et, SLOT(cancelExecution())); //connectQAppToExecutionTree
				connectExecuteTree(this,et,true);
				running = true;
				et->setColor(root_item,COLOR_BLACK);
				executing_tree = new std::thread(std::ref(ExecutionTree::executeParallelTree),et,root_item, 0);
				disconnectCustomContextMenu();
				Q_EMIT(executionStarted());
		} 
		else 
		{
			running = false;
		}
	}

	else
		QMessageBox::information(this,tr("Cannot execute tree"),"The behavior tree is not defined or defined incorrectly");
}

/*
This function creates an ExecutionTree object to execute the tree's nodes, starting at the clicked item
*/
void BehaviorTree::executeTreeFromItem()
{ 
	TreeItem * item_to_execute = (TreeItem*)itemAt(point);
	TreeItem * parent_item_to_execute;
	if(!item_to_execute->isRoot())
	{			
		if (running) 
		{
			et->cancelExecution();
			joinExecutionTree();
		}
		parent_item_to_execute = item_to_execute->getParent();
		int ignored_items = 0;
 		et = new ExecutionTree(this);
		running = true;
		//if(control_parent->checkTree(parent_item_to_execute))
		if(control_parent->correct_format)
		{
			std::vector<TreeItem*> children = parent_item_to_execute->getChildren();
			std::vector<TreeItem*>::iterator iterator = children.begin();
			int i = 0;
			for(; iterator != children.end();)
			{
				if(item_to_execute == children.at(i))
				{
					ignored_items = i;
					break;
				} else {
					++iterator;
					i++;
				}
			}
			
			QObject::connect(qApp,SIGNAL(lastWindowClosed()), et, SLOT(cancelExecution()));
			disconnectCustomContextMenu();
			connectExecuteTree(this,et,true);
			et->setColor(parent_item_to_execute,COLOR_BLACK);
			executing_tree = new std::thread(std::ref(ExecutionTree::executeParallelTree),et,parent_item_to_execute, ignored_items);
            
			Q_EMIT(executionStarted());
		}
		else 
		{
			running = false;
		}
	} 
	else 
	{
		if (running) 
		{
			et->cancelExecution();
			joinExecutionTree();
		}
 		et = new ExecutionTree(this);
		//if(control_parent->checkTree(item_to_execute))
		if(control_parent->correct_format)
		{	
	      running = true;
          QObject::connect(qApp,SIGNAL(lastWindowClosed()), et, SLOT(cancelExecution()));
		  connectExecuteTree(this,et,true); 
		  executing_tree = new std::thread(std::ref(ExecutionTree::executeParallelTree),et,item_to_execute, 0);
	      Q_EMIT(executionStarted());
		}
		else 
		{
			running = false;
		}
	}
}

void BehaviorTree::joinExecutionTree() {
	executing_tree->join();
	ExecutionTree::mutex.lock();
	disconnectExecuteTree(this,et);
	QObject::disconnect(qApp,SIGNAL(lastWindowClosed()), et, SLOT(cancelExecution()));
	delete executing_tree;
	running = false;
	delete et;
	if (is_menu_created) {
		contextMenu->close();
		delete contextMenu;
		is_menu_created = false;
	}
	ExecutionTree::mutex.unlock();
}


void BehaviorTree::updateBackground()
{
	ExecutionTree::mutex.lock();
	repaint();
	ExecutionTree::mutex.unlock();
}

void BehaviorTree::cancelTree()
{
	if (running) {
		aerostack_msgs::BehaviorCommandPriority behavior;
		behavior.name = "KEEP_HOVERING_WITH_PID_CONTROL";
                behavior.priority=1;
        req_activate.behavior = behavior;
       	activate_behavior_srv.call(req_activate,res_activate);

       	if(!res_activate.ack)
          std::cout << res_activate.error_message << std::endl;

		Q_EMIT(cancelExecutionSignal());
	}
}

void BehaviorTree::windowFinished(TreeItem *the_item)
{
	this->expandAll();
	if (isTextExpanded) {
		expandTreeText(0);
	}
	else {
		expandTreeText(2);
	}
	QObject::connect(window, SIGNAL(windowAccepted(TreeItem *)), this, SLOT(windowFinished(TreeItem *)));
}

/*
Next function is called whenever we want to add a child
*/

void BehaviorTree::addChild(const QPoint &p)
{
	parent_item_for_child = (TreeItem*)itemAt(p);
	window = new BehaviorDialog(this, parent_item_for_child);
	window->show();
	QObject::connect(window, SIGNAL(windowAccepted(TreeItem *)), this, SLOT(windowFinished(TreeItem *)));
}

void BehaviorTree::expandTreeText(int checkState)
{
	if (has_root) {
		if(checkState == 2)
		{
			minimizeText(root_item);
			isTextExpanded=false;
		}
		else if(checkState == 0)
		{
			expandText(root_item);
			isTextExpanded=true;
		}
	}
}

void BehaviorTree::expandText(TreeItem *item)
{
	std::string text = item->getNodeName();
	text = text +  item->getPartialNodeName();
	item->setText(0, QString::fromStdString(text));
	if(item->childCount()>0)
	{
		for(int i = 0; i < item->childCount(); i++)
		{
			expandText(item->child(i));
		}
	}
}

void BehaviorTree::minimizeText(TreeItem *item)
{
	item->setText(0, QString::fromStdString(item->getNodeName()));
	if(item->childCount()>0)
	{
		for(int i = 0; i < item->childCount(); i++)
		{
			minimizeText(item->child(i));
		}
	}
}

void BehaviorTree::connectExecuteTree(QTreeWidget* behavior_tree, QObject* et,bool from_item)
{
	if(from_item)
	{
	  QObject::connect(behavior_tree, SIGNAL(cancelExecutionSignal()), et, SLOT(cancelExecution()));
	}

	BehaviorTree *bt = (BehaviorTree*) behavior_tree;

	QObject::connect(et, SIGNAL(finished()), behavior_tree, SLOT(joinExecutionTree()));
	QObject::connect(et, SIGNAL(finished()), bt->getVisualizer(), SLOT(setStopBlockingTextInput()));
	QObject::connect(et, SIGNAL(updateText()), bt->getVisualizer(), SLOT(update()));
	QObject::connect(behavior_tree, SIGNAL(executionStarted()), bt->getVisualizer(), SLOT(setStartBlockingTextInput()));
}

void BehaviorTree::disconnectExecuteTree(QTreeWidget* behavior_tree, QObject* et)
{	
    BehaviorTree *bt = (BehaviorTree*) behavior_tree;

	QObject::disconnect(behavior_tree, SIGNAL(cancelExecutionSignal()), et, SLOT(cancelExecution()));
	QObject::disconnect(et, SIGNAL(finished()), behavior_tree, SLOT(joinExecutionTree()));
	QObject::disconnect(et, SIGNAL(updateText()),bt->getVisualizer(), SLOT(update()));
	QObject::disconnect(et, SIGNAL(finished()),bt->getVisualizer(), SLOT(setStopBlockingTextInput()));
	QObject::disconnect(behavior_tree, SIGNAL(executionStarted()), bt->getVisualizer(), SLOT(setStartBlockingTextInput()));
}

void BehaviorTree::connectCustomContextMenu()
{
	QObject::connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onCustomContextMenu(const QPoint &)));
}


void BehaviorTree::disconnectCustomContextMenu()
{
	QObject::disconnect(this, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onCustomContextMenu(const QPoint &)));
}

