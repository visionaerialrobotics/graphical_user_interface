/*!*******************************************************************************************
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  Camera view - Shows the camera views displayed in full screen
  @author  Yolanda de la Hoz Simón
  @date    06-2015
  @version 1.0
*/

#include "../include/camera_main_option.h"

/*****************************************************************************
** Implementation
*****************************************************************************/

CameraMainOption::CameraMainOption(QWidget* parent, ImagesReceiver* imgReceiver)
  : QWidget(parent), ui(new Ui::cameraMainOption)
{
  ui->setupUi(this);
  image_receiver = imgReceiver;

  current_image = 1;

  QSignalMapper* signalMapper = new QSignalMapper(this);
  connect(ui->right_button, SIGNAL(clicked()), signalMapper, SLOT(map()));
  connect(ui->right_button, SIGNAL(clicked()), signalMapper, SLOT(map()));
  signalMapper->setMapping(ui->right_button, 1);
  signalMapper->setMapping(ui->left_button, 2);
  connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(changeCurrentCamera(int)));

  QObject::connect(image_receiver, SIGNAL(Update_Image(const QPixmap*, int)), this,
                   SLOT(updateImage(const QPixmap*, int)));
}

void CameraMainOption::saveCameraImages(const int camera_view_manager)
{
  if (camera_view_manager == 0)
  {
    QImage imageObject = pix.toImage();
    QByteArray bytes;
    QBuffer buffer(&bytes);
    buffer.open(QIODevice::WriteOnly);
    pix.save(&buffer, "PNG");  // writes pixmap into bytes in PNG format
    QString imagePath = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("JPEG (*.jpg *.jpeg);;PNG (*.png)"));
    imageObject.save(imagePath);
  }
}

void CameraMainOption::changeCurrentCamera(int direction)
{
  if (direction == 1)
    current_image++;
  else
    current_image--;
}

void CameraMainOption::updateImage(const QPixmap* image, int id_camera)
{
  pix = *image;
  if (!image->isNull())
  {
    ui->main_image->setPixmap(pix);
    ui->main_image->setScaledContents(true);
  }
}

CameraMainOption::~CameraMainOption()
{
  delete ui;
}
