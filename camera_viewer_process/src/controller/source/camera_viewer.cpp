/*!*******************************************************************************************
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  Camera viewer - Widget with all camera options
  @author  Carlos Valencia Laray
  @date    11-2016
  @version 1.0
*/
#include "../include/camera_viewer.h"

CameraViewer::CameraViewer(int argc, char** argv, QWidget* parent) : QWidget(parent), ui(new Ui::CameraViewer)
{
  ui->setupUi(this);
  receiver = new ImagesReceiver();
  surface_inspection = ui->surface_inspection;

  // window always on top
  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);
  setWindowIcon(QIcon(":/img/img/camera_viewer.png"));

  // Camera UI layout change according to button pressed.
  connect(ui->one_camera_button, SIGNAL(clicked()), this, SLOT(displayOneCamera()));
  connect(ui->main_camera_button, SIGNAL(clicked()), this, SLOT(displayMainGridCamera()));
  connect(ui->four_camera_button, SIGNAL(clicked()), this, SLOT(displayFourGridCamera()));
  connect(surface_inspection, SIGNAL(stateChanged(int)), this, SLOT(surfaceInspectionChanged(int)));

  // Save image
  connect(ui->save_image_button, SIGNAL(clicked()), this, SLOT(saveCurrentCameraView()));

  initializeCameraView();  // Camera widget

  // Settings connections
  setUp();
}

CameraViewer::~CameraViewer()
{
  delete ui;
}

void CameraViewer::setUp()
{
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");

  n.param<std::string>("window_opened", window_opened_topic, "window_opened");
  n.param<std::string>("window_closed", window_closed_topic, "window_closed");

  // Subscribers
  window_opened_subs =
      n.subscribe("/" + drone_id_namespace + "/" + window_opened_topic, 10, &CameraViewer::windowOpenCallback, this);

  // Publishers
  window_closed_publ =
      n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace + "/" + window_closed_topic, 1, true);
}

void CameraViewer::saveCurrentCameraView()
{
  Q_EMIT saveImage(camera_view_manager);
}

void CameraViewer::initializeCameraView()
{
  camera_view_manager = 0;
  one_option = new CameraMainOption(this, receiver);
  mainoption = new CameraDisplayOption(this, receiver);
  fourCamera = new CameraGridOption(this, receiver);

  ui->grid_camera->addWidget(one_option, 0, 0);
  ui->grid_camera->addWidget(mainoption, 1, 0);
  ui->grid_camera->addWidget(fourCamera, 2, 0);
  mainoption->hide();
  fourCamera->hide();

  QObject::connect(this, SIGNAL(saveImage(const int)), one_option, SLOT(saveCameraImages(const int)));

  displayOneCamera();
}

void CameraViewer::displayOneCamera()
{
  if (!is_open_one_camera_view)
  {
    if (is_open_main_camera_view)
      mainoption->hide();
    else if (is_open_four_camera_view)
      fourCamera->hide();
    camera_view_manager = 0;
    one_option->show();
    is_open_one_camera_view = true;
    is_open_four_camera_view = false;
    is_open_main_camera_view = false;
  }
}

void CameraViewer::displayMainGridCamera()
{
  if (!is_open_main_camera_view)
  {
    if (is_open_one_camera_view)
      one_option->hide();
    else if (is_open_four_camera_view)
      fourCamera->hide();
    camera_view_manager = 1;
    mainoption->show();
    is_open_main_camera_view = true;
    is_open_four_camera_view = false;
    is_open_one_camera_view = false;
  }
}

void CameraViewer::displayFourGridCamera()
{
  if (!is_open_four_camera_view)
  {
    if (is_open_main_camera_view)
      mainoption->hide();
    else if (is_open_one_camera_view)
      one_option->hide();
    camera_view_manager = 3;
    fourCamera->show();
    is_open_four_camera_view = true;
    is_open_main_camera_view = false;
    is_open_one_camera_view = false;
  }
}

void CameraViewer::surfaceInspectionChanged(int i)
{
  if (i == 2)
  {
    receiver->setInspectionMode(true);
  }
  else if (i == 0)
  {
    receiver->setInspectionMode(false);
  }
}
void CameraViewer::closeEvent(QCloseEvent* event)
{
  windowClosedMsgs.id = 13;
  window_closed_publ.publish(windowClosedMsgs);
}
void CameraViewer::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void CameraViewer::windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg)
{
  windowOpenedMsgs = msg;

  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_MISSION_INTERPRETER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_PYTHON ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_TML)
  {
    windowClosedMsgs.id = 13;
    window_closed_publ.publish(windowClosedMsgs);
    killMe();
  }

  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::MINIMIZE_MAIN_WINDOW)
    showMinimized();
}
