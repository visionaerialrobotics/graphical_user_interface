/*!*******************************************************************************************
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  Camera view - show the camera views displayed in a grid
  @author  Yolanda de la Hoz Simón
  @date    06-2015
  @version 1.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/
#include "../include/camera_grid_option.h"
/*****************************************************************************
** Implementation
*****************************************************************************/

CameraGridOption::CameraGridOption(QWidget* parent, ImagesReceiver* img_receiver)
  : QWidget(parent), ui(new Ui::CameraGridOption)
{
  ui->setupUi(this);

  image_receiver = img_receiver;

  QObject::connect(image_receiver, SIGNAL(Update_Image(const QPixmap*, int)), this,
                   SLOT(updateImage(const QPixmap*, int)));
}

void CameraGridOption::updateImage(const QPixmap* image, int id_camera)
{
  switch (id_camera)
  {
    case 1:
    {
      pixmap1 = *image;
      if (!image->isNull())
      {
        ui->camera1_frame->setScaledContents(true);
        ui->camera1_frame->setPixmap(pixmap1);
      }
      break;
    }
    case 2:
    {
      pixmap2 = *image;
      if (!image->isNull())
      {
        ui->camera2_frame->setPixmap(pixmap2);
        ui->camera2_frame->setScaledContents(true);
      }
      break;
    }
    case 3:
    {
      pixmap3 = *image;
      if (!image->isNull())
      {
        ui->camera3_frame->setPixmap(pixmap3);
        ui->camera3_frame->setScaledContents(true);
      }
      break;
    }
    case 4:
    {
      pixmap4 = *image;
      if (!image->isNull())
      {
        ui->camera4_frame->setPixmap(pixmap4);
        ui->camera4_frame->setScaledContents(true);
      }
      break;
    }
  }
}

CameraGridOption::~CameraGridOption()
{
  delete ui;
}
