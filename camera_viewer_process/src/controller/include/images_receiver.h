/*!*******************************************************************************************
 *  \file       images_receiver.h
 *  \brief      ImagesReceiver definition file.
 *  \details    This file includes the ImagesReceiver class declaration. To obtain more
 *              information about it's definition consult the images_receiver.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/

#ifndef IMAGESRECEIVER_H
#define IMAGESRECEIVER_H

/*****************************************************************************
** Includes
*****************************************************************************/
#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>
#include <sstream>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include "communication_definition.h"

#include <QThread>
#include <QString>
#include <QStringListModel>
#include <QPixmap>

class ImagesReceiver : public QObject
{
  Q_OBJECT

public:
  ImagesReceiver();
  virtual ~ImagesReceiver();

  /*!************************************************************************
   *  \brief   Opens the subscriptions to all the needed topics and assigns the callbacks to them
   ***************************************************************************/
  void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);

  /*!************************************************************************
   *  \brief  Checks if the subscriptions have been opened.
   *  \return True if the subscriptions opened correctly, false otherwise.
   ***************************************************************************/
  bool ready();

  /*!************************************************************************
   *  \brief   Sets the surface inspection on or off.
   ***************************************************************************/
  void setInspectionMode(bool b);

  void changeHudActivated();

  bool getHudActivated();

private:
  bool subscriptions_complete;

  std::string drone_console_interface_sensor_bottom_camera;
  std::string drone_console_interface_sensor_front_camera;
  std::string drone_console_interface_sensor_overlay_camera_big;
  std::string drone_console_interface_sensor_overlay_camera_small;
  std::string surface_inspection_topic;

  bool surface_inspection;
  bool hud_activated;
  bool receiving_drone_images;

  QPixmap px;
  QPixmap px_big;
  QPixmap px_small;

  ros::MultiThreadedSpinner threadSpin;

  image_transport::Subscriber image_bottom_sub_;
  image_transport::Subscriber image_front_sub_;
  image_transport::Subscriber image_surface_inspection_sub_;
  image_transport::Subscriber image_overlay_big_;
  image_transport::Subscriber image_overlay_small_;

  /*!************************************************************************
   *  \brief   Processes the images received from the bottom camera.
   ***************************************************************************/
  void imagesBottomReceptionCallback(const sensor_msgs::ImageConstPtr& msg);

  /*!************************************************************************
   *  \brief   Processes the images received from the front camera.
   ***************************************************************************/
  void imagesFrontReceptionCallback(const sensor_msgs::ImageConstPtr& msg);

  /*!************************************************************************
   *  \brief   Processes the images receives from the surface inspection process.
   ***************************************************************************/
  void imagesSurfaceInspectionCallback(const sensor_msgs::ImageConstPtr& msg);

  /*!************************************************************************
   *  \brief   Processes the images to show them on the small overlay.
   ***************************************************************************/
  void imagesOverlaySmallReceptionCallback(const sensor_msgs::ImageConstPtr& msg);

  /*!************************************************************************
   *  \brief   Processes the images to show them on the big overlay.
   ***************************************************************************/
  void imagesOverlayBigReceptionCallback(const sensor_msgs::ImageConstPtr& msg);

  /*!************************************************************************
   *  \brief   Makes a format transformation from the raw images to QImage format.
   *  \return  Returns the QImage object that corresponds with the received image.
   ***************************************************************************/
  QImage cvtCvMat2QImage(const cv::Mat& image);

Q_SIGNALS:
  void Update_Image(const QPixmap* image, int id_camera);
  void Update_Image_Overlay_Big(const QPixmap* image);
  void Update_Image_Overlay_Small(const QPixmap* image);
};

#endif  // IMAGESRECEIVER_H