/*!*******************************************************************************************
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include <cstdio>
#include <gtest/gtest.h>
#include <ros/ros.h>
#include <thread>

#include <iostream>

/*QApplication* app;
BehaviorTreeDesignView* w;*/
int ar;
bool finished = false;
int total_subtests = 0;
int passed_subtests = 0;

void spinnerThread()
{
  while (!finished)
  {
    ros::spinOnce();
  }
}

void displaySubtestResults()
{
  std::cout << "\033[1;33m TOTAL SUBTESTS: " << total_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m SUBTESTS PASSED: " << passed_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m % PASSED: " << (passed_subtests * 100.0) / (total_subtests * 1.0) << "\033[0m" << std::endl;
}

void test()
{
  total_subtests++;
  std::string response;
  std::cout << "\033[1;34m Does a console window called 'Alphanumeric Interface Control' appear in the left half o the "
               "screen (centered in vertical axis)? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in position or appearance of the widget\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Is Battery value 100 in 'Odometry measures' section? (Y/N)\033[0m" << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in receiving odometry stream\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Is x=4 and y=4 in 'Position estimates' sections? (Y/N)\033[0m" << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in receiving position estimates\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Is 'Controller' started (is_started=1)? (Y/N) \033[0m" << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in starting trajectory controller \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Are position estimates xci=4, yci=4 and zci=0 in 'Controller' section? \033[0m" << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in receiving data from trajectory controller\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;

  std::cout << "\033[1;34m Press key 't' on your keyboard. Has the state changed to 'Taking off' first and 'Hovering' "
               "then? \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in changing state by keyboard\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Have different values appeared in 'Commands' section? \033[0m" << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in showing commanded values \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  displaySubtestResults();

  finished = true;
}

TEST(GraphicalUserInterfaceTests, alphanumericInterfaceTest)
{
  std::thread thr(&test);
  thr.join();
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  system(
      "xfce4-terminal  \--tab --title \"Behavior Coordinator\"  --command \"bash -c 'roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
           my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &");
  sleep(2);
  system("bash $AEROSTACK_STACK/launchers/launcher_simulated_quadrotor_basic_3.0.sh");
  sleep(2);

  system(
      "xfce4-terminal  \--title \"Alphanumeric Interface Control\" --geometry 72x22+238+400 --command \"bash -c 'roslaunch alphanumeric_interface alphanumeric_interface.launch --wait \
           my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &");
  ar = argc;
  /* app= new QApplication (argc, nullptr);
   w= new BehaviorTreeDesignView (argc, nullptr);
   w->show();*/

  std::thread thr(&spinnerThread);

  return RUN_ALL_TESTS();
  thr.join();
}
