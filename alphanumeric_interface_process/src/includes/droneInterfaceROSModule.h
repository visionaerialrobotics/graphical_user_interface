/*!*******************************************************************************************
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
//////////////////////////////////////////////////////
//  droneInterfaceROSModule.h
//
//  Created on: Dec 19, 2013
//      Author: joselusl
//
//  Last modification on: Dec 23, 2013
//      Author: joselusl
//
//  Description: class to create an interface
//
//////////////////////////////////////////////////////

#ifndef _DRONE_INTERFACE_ROS_MODULE_H
#define _DRONE_INTERFACE_ROS_MODULE_H

// I/O stream
// std::cout
#include <iostream>
#include <string>

#include <algorithm>
#include <list>
#include <iterator>

// Math
// M_PI
#include <cmath>
#include <thread>
#include <chrono>

//// ROS  ///////
#include "ros/ros.h"
#include <droneMsgsROS/InitiateBehaviors.h>
#include <aerostack_msgs/RequestBehavior.h>

// IMU
#include "sensor_msgs/Imu.h"

// Temperature
//#include "sensor_msgs/Temperature.h" // compatibility with ROS Fuerte

// Magnetometer and RotationAngles
#include "geometry_msgs/Vector3Stamped.h"

// Battery
#include "droneMsgsROS/battery.h"

// Altitude
#include "droneMsgsROS/droneAltitude.h"

// Ground Speed
#include "droneMsgsROS/vector2Stamped.h"

// Pressure
//#include "sensor_msgs/FluidPressure.h" // compatibility with ROS Fuerte

// Drone Status
#include "droneMsgsROS/droneStatus.h"

// BottomCamera and FrontCamera
#include "sensor_msgs/Image.h"
//#include <image_transport/image_transport.h> // compatibility with ROS Fuerte

#include <cv_bridge/cv_bridge.h>
//#include <sensor_msgs/image_encodings.h> // compatibility with ROS Fuerte

/////// OpenCV ///////
#include <opencv2/core/core.hpp>  // Basic OpenCV structures (cv::Mat, Scalar)
#include "droneMsgsROS/getFlightAnim.h"
#include "droneMsgsROS/FlightAnimation.h"

// Custom commands
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"

#include "droneMsgsROS/droneCommand.h"

#include "aerostack_msgs/AliveSignal.h"

#include "droneModuleROS.h"
#include "communication_definition.h"

// Console
#include <curses.h>

#include "aerostack_msgs/WindowIdentifier.h"

#include <signal.h>
#include <thread>

#define FREQ_INTERFACE 200.0

class DroneInterfaceROSModule : public DroneModule
{
  // Publisher
protected:
  ros::Publisher DronePitchRollCmdPubl;
  ros::Publisher DroneDAltitudeCmdPubl;
  ros::Publisher DroneDYawCmdPubl;
  ros::Publisher DroneCommandPubl;

public:
  bool publishPitchRollCmd();
  bool publishDAltitudeCmd();
  bool publishDYawCmd();
  bool publishDroneCmd();

  ros::Publisher window_closed_publ;
  ros::Subscriber window_opened_subs;

  std::string window_opened_topic;
  std::string window_closed_topic;

  aerostack_msgs::WindowIdentifier windowClosedMsgs;
  aerostack_msgs::WindowIdentifier windowOpenedMsgs;

  std::string drone_id_namespace;
  // Subscriber
protected:
  ros::Subscriber ImuSubs;
  void imuCallback(const sensor_msgs::Imu::ConstPtr& msg);

  //    ros::Subscriber TemperatureSubs;
  //    void temperatureCallback(const sensor_msgs::Temperature::ConstPtr& msg);

  ros::Subscriber MagnetometerSubs;
  void magnetometerCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg);

  ros::Subscriber BatterySubs;
  void batteryCallback(const droneMsgsROS::battery::ConstPtr& msg);

  ros::Subscriber AltitudeSubs;
  void altitudeCallback(const droneMsgsROS::droneAltitude::ConstPtr& msg);

  ros::Subscriber RotationAnglesSubs;
  void rotationAnglesCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg);

  ros::Subscriber GroundSpeedSubs;
  void groundSpeedCallback(const droneMsgsROS::vector2Stamped::ConstPtr& msg);

  //    ros::Subscriber PressureSubs;
  //    void pressureCallback(const sensor_msgs::FluidPressure::ConstPtr& msg);

  ros::Subscriber DroneStatusSubs;
  void droneStatusCallback(const droneMsgsROS::droneStatus::ConstPtr& msg);

  ros::Subscriber BottomCameraSubs;
  void bottomCameraCallback(const sensor_msgs::ImageConstPtr& msg);

  ros::Subscriber FrontCameraSubs;
  void frontCameraCallback(const sensor_msgs::ImageConstPtr& msg);

  // Command
  ros::Subscriber DronePitchRollCmdSubs;
  void dronePitchRollCmdCallback(const droneMsgsROS::dronePitchRollCmd::ConstPtr& msg);

  ros::Subscriber DroneDAltitudeCmdSubs;
  void droneDAltitudeCmdCallback(const droneMsgsROS::droneDAltitudeCmd::ConstPtr& msg);

  ros::Subscriber DroneDYawCmdSubs;
  void droneDYawCmdCallback(const droneMsgsROS::droneDYawCmd::ConstPtr& msg);

  ros::Subscriber DroneCommandSubs;
  void droneCommandCallback(const droneMsgsROS::droneCommand::ConstPtr& msg);

  // Modules state
  ros::Subscriber ProcessAliveSignalSub;
  void ProcessAliveSignalCallback(const aerostack_msgs::AliveSignal::ConstPtr& msg);
  aerostack_msgs::AliveSignal ProcessAliveSignalMsg;
  std::list<std::string> modulesListName;

  // Msgs
public:
  sensor_msgs::Imu ImuMsgs;
  //    sensor_msgs::Temperature TemperatureMsgs;
  geometry_msgs::Vector3Stamped MagnetometerMsgs;
  droneMsgsROS::battery BatteryMsgs;
  droneMsgsROS::droneAltitude AltitudeMsgs;
  geometry_msgs::Vector3Stamped RotationAnglesMsgs;
  droneMsgsROS::vector2Stamped GroundSpeedMsgs;
  //    sensor_msgs::FluidPressure PressureMsgs;
  droneMsgsROS::droneStatus DroneStatusMsgs;

protected:
  cv_bridge::CvImagePtr cvBottomImage;

public:
  bool newBottomImage;
  cv::Mat bottomImage;

protected:
  cv_bridge::CvImagePtr cvFrontImage;

public:
  bool newFrontImage;
  cv::Mat frontImage;

public:
  droneMsgsROS::dronePitchRollCmd DronePitchRollCmdMsgs;
  droneMsgsROS::droneDAltitudeCmd DroneDAltitudeCmdMsgs;
  droneMsgsROS::droneDYawCmd DroneDYawCmdMsgs;
  droneMsgsROS::droneCommand DroneCommandMsgs;

  // Constructors and destructors
public:
  DroneInterfaceROSModule();
  ~DroneInterfaceROSModule();

  // Init and close
public:
  void init();
  void close();

  // Open
public:
  void open(ros::NodeHandle& nIn, std::string moduleName);
  void openTopicWindow(ros::NodeHandle& n);

  // Reset
protected:
  bool resetValues();

  // Start
protected:
  bool startVal();

  // Stop
protected:
  bool stopVal();

  // Run
public:
  bool run();
  /*!************************************************************************
   *  \brief   Activated when a window is closed.
   ***************************************************************************/
  void windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg);
  /*!************************************************************************
   *  \brief  Kills the process
   ***************************************************************************/
  void killMe();

public:
  void clearCmd();

public:  // interface_jp
  void drone_take_off();
  void drone_force_take_off();
  void drone_land();
  void drone_hover();
  void drone_emergency_stop();
  void drone_reset();
  void drone_move();
  std::stringstream* getOdometryStream();
  std::stringstream* getDroneCommandsStream();

private:
  std::stringstream interface_printout_stream;
};

#endif
