/*!*******************************************************************************************
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef DRONELOCALIZERINTERFACE_H
#define DRONELOCALIZERINTERFACE_H

#include "dronemoduleinterface.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "communication_definition.h"

class DroneLocalizerInterface : public DroneModuleInterface
{
public:
  DroneLocalizerInterface(const std::string& module_name_str_in, const ModuleNames::name module_name_enum_in);
  ~DroneLocalizerInterface();

  void open(ros::NodeHandle& nIn);

  inline const droneMsgsROS::dronePose& last_drone_estimated_GMRwrtGFF_pose_msg()
  {
    return last_drone_estimated_GMRwrtGFF_pose_msg_;
  }
  inline const droneMsgsROS::droneSpeeds& last_drone_estimated_GMRwrtGFF_speeds_msg()
  {
    return last_drone_estimated_GMRwrtGFF_speeds_msg_;
  }

private:
  ros::Subscriber drone_estimated_GMR_pose_subscriber;
  ros::Subscriber drone_estimated_GMR_speeds_subscriber;
  void drone_estimated_GMR_pose_callback_function(const droneMsgsROS::dronePose& msg)
  {
    last_drone_estimated_GMRwrtGFF_pose_msg_ = (msg);
  }
  void drone_estimated_GMR_speeds_callback_function(const droneMsgsROS::droneSpeeds& msg)
  {
    last_drone_estimated_GMRwrtGFF_speeds_msg_ = (msg);
  }
  droneMsgsROS::dronePose last_drone_estimated_GMRwrtGFF_pose_msg_;
  droneMsgsROS::droneSpeeds last_drone_estimated_GMRwrtGFF_speeds_msg_;

public:
  std::stringstream* getLocalizerState_Stream();
  std::stringstream* getPositionEstimates_GMRwrtGFF_Stream();
  std::stringstream* getSpeedEstimates_GMRwrtGFF_Stream();

private:
  std::stringstream interface_printout_stream;
};

#endif  // DRONELOCALIZERINTERFACE_H
