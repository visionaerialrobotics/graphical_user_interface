/*!*******************************************************************************************
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/


#include "../includes/droneInterfaceROSModule.h"

////// IMU ////////
DroneInterfaceROSModule::DroneInterfaceROSModule() : DroneModule(droneModule::active, FREQ_INTERFACE)
{
  init();
  return;
}

DroneInterfaceROSModule::~DroneInterfaceROSModule()
{
  return;
}

void DroneInterfaceROSModule::init()
{
  // Imu

  // Battery
  BatteryMsgs.batteryPercent = 0.0;

  // Altitude
  AltitudeMsgs.altitude = 0.0;
  AltitudeMsgs.altitude_speed = 0.0;
  AltitudeMsgs.var_altitude = 0.0;
  AltitudeMsgs.var_altitude_speed = 0.0;

  newBottomImage = false;
  newFrontImage = false;

  modulesListName.clear();

  std::string initiate_behaviors;
  std::string activate_behavior;

  droneMsgsROS::InitiateBehaviors msg;
  aerostack_msgs::RequestBehavior::Request msg2;
  aerostack_msgs::RequestBehavior::Response res;
  aerostack_msgs::BehaviorCommand behavior;

  behavior.name = "SELF_LOCALIZE_BY_ODOMETRY";
  msg2.behavior = behavior;

  n.param<std::string>("initiate_behaviors", initiate_behaviors, "initiate_behaviors");
  n.param<std::string>("activate_behavior", activate_behavior, "activate_behavior");
  ros::ServiceClient initiate_behaviors_srv;
  ros::ServiceClient activate_behavior_srv;

  initiate_behaviors_srv = n.serviceClient<droneMsgsROS::InitiateBehaviors>(initiate_behaviors);
  activate_behavior_srv = n.serviceClient<aerostack_msgs::RequestBehavior>(activate_behavior);

  initiate_behaviors_srv.call(msg);
  activate_behavior_srv.call(msg2, res);
  if (!res.ack)
    std::cout << res.error_message << std::endl;

  clearCmd();

  return;
}

void DroneInterfaceROSModule::close()
{
}

void DroneInterfaceROSModule::open(ros::NodeHandle& nIn, std::string moduleName)
{
  // Node
  DroneModule::open(nIn, moduleName);

  // init();

  // Configuration

  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("window_opened", window_opened_topic, "window_opened");
  n.param<std::string>("window_closed", window_closed_topic, "window_closed");
  // Subscriber
  ImuSubs = n.subscribe("/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_SENSOR_IMU, 1,
                        &DroneInterfaceROSModule::imuCallback, this);
  //    TemperatureSubs=n.subscribe(DRONE_CONSOLE_INTERFACE_SENSOR_TEMPERATURE, 1,
  //    &DroneInterfaceROSModule::temperatureCallback, this);
  MagnetometerSubs = n.subscribe("/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_SENSOR_MAGNETOMETER, 1,
                                 &DroneInterfaceROSModule::magnetometerCallback, this);
  BatterySubs = n.subscribe("/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_SENSOR_BATTERY, 1,
                            &DroneInterfaceROSModule::batteryCallback, this);
  AltitudeSubs = n.subscribe("/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_SENSOR_ALTITUDE, 1,
                             &DroneInterfaceROSModule::altitudeCallback, this);
  RotationAnglesSubs = n.subscribe("/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_SENSOR_ROTATION_ANGLES, 1,
                                   &DroneInterfaceROSModule::rotationAnglesCallback, this);
  GroundSpeedSubs = n.subscribe("/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_SENSOR_GROUND_SPEED, 1,
                                &DroneInterfaceROSModule::groundSpeedCallback, this);
  //    PressureSubs=n.subscribe(DRONE_CONSOLE_INTERFACE_SENSOR_PRESSURE, 1, &DroneInterfaceROSModule::pressureCallback,
  //    this);
  DroneStatusSubs = n.subscribe("/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_SENSOR_STATUS, 1,
                                &DroneInterfaceROSModule::droneStatusCallback, this);

  //    BottomCameraSubs=n.subscribe(DRONE_CONSOLE_INTERFACE_SENSOR_BOTTOM_CAMERA, 1,
  //    &DroneInterfaceROSModule::bottomCameraCallback, this);
  //    FrontCameraSubs=n.subscribe(DRONE_CONSOLE_INTERFACE_SENSOR_FRONT_CAMERA, 1,
  //    &DroneInterfaceROSModule::frontCameraCallback, this);

  // Cmd Subscribers
  DronePitchRollCmdSubs = n.subscribe("/" + drone_id_namespace + "/" +
                                          DRONE_CONSOLE_INTERFACE_COMMAND_DRONE_COMMAND_PITCH_ROLL_SUBSCRIPTION,
                                      1, &DroneInterfaceROSModule::dronePitchRollCmdCallback, this);
  DroneDAltitudeCmdSubs =
      n.subscribe("/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_COMMAND_DRONE_COMMAND_DALTITUDE_SUBSCRIPTION,
                  1, &DroneInterfaceROSModule::droneDAltitudeCmdCallback, this);
  DroneDYawCmdSubs =
      n.subscribe("/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_COMMAND_DRONE_COMMAND_DYAW_SUBSCRIPTION, 1,
                  &DroneInterfaceROSModule::droneDYawCmdCallback, this);
  DroneCommandSubs =
      n.subscribe("/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_COMMAND_DRONE_HL_COMMAND_SUBSCRIPTION, 1,
                  &DroneInterfaceROSModule::droneCommandCallback, this);

  ProcessAliveSignalSub =
      n.subscribe("process_alive_signal", 10, &DroneInterfaceROSModule::ProcessAliveSignalCallback, this);

  // Publishers
  DronePitchRollCmdPubl = n.advertise<droneMsgsROS::dronePitchRollCmd>(
      "/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_COMMAND_DRONE_COMMAND_PITCH_ROLL_PUBLICATION, 1, true);
  DroneDAltitudeCmdPubl = n.advertise<droneMsgsROS::droneDAltitudeCmd>(
      "/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_COMMAND_DRONE_COMMAND_DALTITUDE_PUBLICATION, 1, true);
  DroneDYawCmdPubl = n.advertise<droneMsgsROS::droneDYawCmd>(
      "/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_COMMAND_DRONE_COMMAND_DYAW_PUBLICATION, 1, true);
  DroneCommandPubl = n.advertise<droneMsgsROS::droneCommand>(
      "/" + drone_id_namespace + "/" + DRONE_CONSOLE_INTERFACE_COMMAND_DRONE_HL_COMMAND_PUBLICATION, 1, true);

  // Subscribers
  window_opened_subs = n.subscribe("/" + drone_id_namespace + "/" + window_opened_topic, 10,
                                   &DroneInterfaceROSModule::windowOpenCallback, this);

  // Publishers
  window_closed_publ =
      n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace + "/" + window_closed_topic, 1, true);

  // Flag of module opened
  droneModuleOpened = true;

  // Autostart
  moduleStarted = false;

  // End
  return;
}

// Reset
bool DroneInterfaceROSModule::resetValues()
{
  return true;
}

// Start
bool DroneInterfaceROSModule::startVal()
{
  return true;
}

// Stop
bool DroneInterfaceROSModule::stopVal()
{
  return true;
}

// Run
bool DroneInterfaceROSModule::run()
{
  return true;
}

void DroneInterfaceROSModule::clearCmd()
{
  // DronePitchRollCmd
  DronePitchRollCmdMsgs.pitchCmd = 0.0;
  DronePitchRollCmdMsgs.rollCmd = 0.0;

  DroneDAltitudeCmdMsgs.dAltitudeCmd = 0.0;

  DroneDYawCmdMsgs.dYawCmd = 0.0;
  return;
}

void DroneInterfaceROSModule::imuCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
  ImuMsgs = *msg;
  return;
}

// void DroneInterfaceROSModule::temperatureCallback(const sensor_msgs::Temperature::ConstPtr& msg)
//{
//    TemperatureMsgs=*msg;
//    return;
//}

void DroneInterfaceROSModule::magnetometerCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
  MagnetometerMsgs = *msg;
  return;
}

void DroneInterfaceROSModule::batteryCallback(const droneMsgsROS::battery::ConstPtr& msg)
{
  BatteryMsgs = *msg;
  return;
}

void DroneInterfaceROSModule::altitudeCallback(const droneMsgsROS::droneAltitude::ConstPtr& msg)
{
  AltitudeMsgs = *msg;
  return;
}

void DroneInterfaceROSModule::rotationAnglesCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
  RotationAnglesMsgs = *msg;
  return;
}

void DroneInterfaceROSModule::groundSpeedCallback(const droneMsgsROS::vector2Stamped::ConstPtr& msg)
{
  GroundSpeedMsgs = *msg;
  return;
}

// void DroneInterfaceROSModule::pressureCallback(const sensor_msgs::FluidPressure::ConstPtr& msg)
//{
//    PressureMsgs=*msg;
//    return;
//}

void DroneInterfaceROSModule::droneStatusCallback(const droneMsgsROS::droneStatus::ConstPtr& msg)
{
  DroneStatusMsgs = *msg;
  return;
}

// void DroneInterfaceROSModule::bottomCameraCallback(const sensor_msgs::ImageConstPtr& msg)
//{
//    //http://www.ros.org/wiki/cv_bridge/Tutorials/UsingCvBridgeToConvertBetweenROSImagesAndOpenCVImages
//    try
//    {
//        cvBottomImage = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
//        newBottomImage=true;
//    }
//    catch (cv_bridge::Exception& e)
//    {
//        ROS_ERROR("cv_bridge exception: %s", e.what());
//        newBottomImage=false;
//        return;
//    }
//    bottomImage=cvBottomImage->image;
//    return;
//}

// void DroneInterfaceROSModule::frontCameraCallback(const sensor_msgs::ImageConstPtr& msg)
//{
//    //http://www.ros.org/wiki/cv_bridge/Tutorials/UsingCvBridgeToConvertBetweenROSImagesAndOpenCVImages
//    try
//    {
//        cvFrontImage = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
//        newFrontImage=true;
//    }
//    catch (cv_bridge::Exception& e)
//    {
//        ROS_ERROR("cv_bridge exception: %s", e.what());
//        newFrontImage=false;
//        return;
//    }
//    frontImage=cvFrontImage->image;
//    return;
//}

void DroneInterfaceROSModule::dronePitchRollCmdCallback(const droneMsgsROS::dronePitchRollCmd::ConstPtr& msg)
{
  DronePitchRollCmdMsgs = *msg;
  return;
}

void DroneInterfaceROSModule::droneDAltitudeCmdCallback(const droneMsgsROS::droneDAltitudeCmd::ConstPtr& msg)
{
  DroneDAltitudeCmdMsgs = *msg;
  return;
}

void DroneInterfaceROSModule::droneDYawCmdCallback(const droneMsgsROS::droneDYawCmd::ConstPtr& msg)
{
  DroneDYawCmdMsgs = *msg;
  return;
}

void DroneInterfaceROSModule::droneCommandCallback(const droneMsgsROS::droneCommand::ConstPtr& msg)
{
  DroneCommandMsgs = *msg;
  return;
}

void DroneInterfaceROSModule::ProcessAliveSignalCallback(const aerostack_msgs::AliveSignal::ConstPtr& msg)
{
  ProcessAliveSignalMsg = *msg;

  //    move(0,43);
  //    printw("Drone Modules:");

  //    std::list<std::string>::iterator it=std::find(modulesListName.begin(), modulesListName.end(),
  //    ProcessAliveSignalMsg.process_name); if (it == modulesListName.end())
  //    {
  //        // Modulo no existe
  //        modulesListName.push_back(ProcessAliveSignalMsg.process_name);
  //    }

  //    it=std::find(modulesListName.begin(), modulesListName.end(), ProcessAliveSignalMsg.process_name);
  //    int numModule=std::distance(modulesListName.begin(),it);
  //    move(1+numModule,45);

  //    std::string processName=ProcessAliveSignalMsg.process_name;
  //    processName.erase(processName.begin(),processName.begin()+8);
  //    std::string processState;
  //    //=std::to_string(ProcessAliveSignalMsg.current_state.state);
  //    if(ProcessAliveSignalMsg.current_state.state==8)
  //        processState="0";
  //    else if(ProcessAliveSignalMsg.current_state.state==7)
  //        processState="1";
  //    std::string processInfoMessage=processName+": "+processState;
  //    printw(processInfoMessage.c_str());

  return;
}

bool DroneInterfaceROSModule::publishPitchRollCmd()
{
  DronePitchRollCmdPubl.publish(DronePitchRollCmdMsgs);

  return true;
}

bool DroneInterfaceROSModule::publishDAltitudeCmd()
{
  DroneDAltitudeCmdPubl.publish(DroneDAltitudeCmdMsgs);

  return true;
}

bool DroneInterfaceROSModule::publishDYawCmd()
{
  DroneDYawCmdPubl.publish(DroneDYawCmdMsgs);

  return true;
}

bool DroneInterfaceROSModule::publishDroneCmd()
{
  DroneCommandPubl.publish(DroneCommandMsgs);

  return true;
}

void DroneInterfaceROSModule::drone_take_off()
{
  clearCmd();
  DroneCommandMsgs.command = droneMsgsROS::droneCommand::TAKE_OFF;
  publishDroneCmd();
}

void DroneInterfaceROSModule::drone_force_take_off()
{
  clearCmd();

  // Reset if needed
  if (DroneStatusMsgs.status == droneMsgsROS::droneStatus::UNKNOWN)
  {
    DroneCommandMsgs.command = droneMsgsROS::droneCommand::RESET;
    publishDroneCmd();
    ros::spinOnce();
  }
  // Wait until reseted!
  while (DroneStatusMsgs.status == droneMsgsROS::droneStatus::UNKNOWN)
  {
    ros::spinOnce();
  }
  drone_take_off();
}

void DroneInterfaceROSModule::drone_land()
{
  clearCmd();
  DroneCommandMsgs.command = droneMsgsROS::droneCommand::LAND;
  publishDroneCmd();
}

void DroneInterfaceROSModule::drone_hover()
{
  clearCmd();
  DroneCommandMsgs.command = droneMsgsROS::droneCommand::HOVER;
  publishDroneCmd();
}

void DroneInterfaceROSModule::drone_emergency_stop()
{
  clearCmd();
  DroneCommandMsgs.command = droneMsgsROS::droneCommand::RESET;
  publishDroneCmd();
}

void DroneInterfaceROSModule::drone_reset()
{
  drone_emergency_stop();
}

void DroneInterfaceROSModule::drone_move()
{
  DroneCommandMsgs.command = droneMsgsROS::droneCommand::MOVE;
  publishDroneCmd();
}

std::stringstream* DroneInterfaceROSModule::getOdometryStream()
{
  interface_printout_stream.clear();
  interface_printout_stream.str(std::string());
  interface_printout_stream << std::endl
                            << "+Odometry measures:" << std::endl
                            << " Battery: " << BatteryMsgs.batteryPercent << "%"
                            << "    " << std::endl
                            << " Yaw:   " << RotationAnglesMsgs.vector.z << " deg"
                            << "    " << std::endl
                            << " Pitch: " << RotationAnglesMsgs.vector.y << " deg"
                            << "    " << std::endl
                            << " Roll:  " << RotationAnglesMsgs.vector.x << " deg"
                            << "    " << std::endl
                            << " z (-H): " << AltitudeMsgs.altitude << " m"
                            << "    " << std::endl
                            << " vx: " << GroundSpeedMsgs.vector.x << " m/s"
                            << "    " << std::endl
                            << " vy: " << GroundSpeedMsgs.vector.y << " m/s"
                            << "    " << std::endl;
  //        Note: ardrone does always returns 0 as vz/altitude_speed value
  //        << " vz: " << AltitudeMsgs.altitude_speed << " m/s" << std::endl;
  //        << " ax: " << ImuMsgs.linear_acceleration.x << " m/s2" << std::endl
  //        << " ay: " << ImuMsgs.linear_acceleration.y << " m/s2" << std::endl
  //        << " az: " << ImuMsgs.linear_acceleration.z << " m/s2" << std::endl
  //        << " MagX: " << MagnetometerMsgs.vector.x << " (TBA)" << std::endl
  //        << " MagY: " << MagnetometerMsgs.vector.y << " (TBA)" << std::endl
  //        << " MagZ: " << MagnetometerMsgs.vector.z << " (TBA)" << std::endl
  //        << " Press: " << PressureMsgs.fluid_pressure << " (TBA)" << std::endl;

  return &interface_printout_stream;
}

std::stringstream* DroneInterfaceROSModule::getDroneCommandsStream()
{
  interface_printout_stream.clear();
  interface_printout_stream.str(std::string());
  interface_printout_stream << "+Commands:" << std::endl
                            << " Pitch: " << DronePitchRollCmdMsgs.pitchCmd << "    " << std::endl
                            << " Roll:  " << DronePitchRollCmdMsgs.rollCmd << "    " << std::endl
                            << " dYaw:  " << DroneDYawCmdMsgs.dYawCmd << "    " << std::endl
                            << " dh:    " << DroneDAltitudeCmdMsgs.dAltitudeCmd << "    " << std::endl;

  return &interface_printout_stream;
}

void DroneInterfaceROSModule::killMe()
{
  raise(SIGKILL);
}

void DroneInterfaceROSModule::windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg)
{
  windowOpenedMsgs = msg;

  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_MISSION_INTERPRETER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::KEYBOARD_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::TML_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_PYTHON ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_TML)
  {
    windowClosedMsgs.id = 11;
    window_closed_publ.publish(windowClosedMsgs);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    killMe();
  }
}
