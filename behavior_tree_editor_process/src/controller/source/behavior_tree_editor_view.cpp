/*
  BehaviorTreeEditorView
  @author  Daniel Del Olmo
  @date    03-2019
  @version 3.0
*/

#include "../include/behavior_tree_editor_view.h"

BehaviorTreeDesignView::BehaviorTreeDesignView(int argc, char** argv, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BehaviorTreeDesignView)
{
    //window always on top
    setWindowIcon(QIcon(":/img/img/design_behavior_tree_icon.png"));
    setWindowTitle("Behavior Tree Editor");
    Qt::WindowFlags flags = windowFlags();
    setWindowFlags(flags | Qt::WindowStaysOnTopHint);

    ui->setupUi(this); //connects all ui's triggers

    //Add the behavior tree design widget.
    behavior_tree_editor = new BehaviorTreeDesign(this);
    ui->gridLayout->addWidget(behavior_tree_editor);

    //Settings Widget
    setWidgetDimensions();

    //Establishment of connections
    setUp();
}

/*------------------------------------------------------------
--------------------------------------------------------------
                      Destructor
--------------------------------------------------------------  
------------------------------------------------------------*/
BehaviorTreeDesignView::~BehaviorTreeDesignView()
{
    delete ui;
    delete behavior_tree_editor;
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Getters and setters
--------------------------------------------------------------
------------------------------------------------------------*/
BehaviorTreeDesign* BehaviorTreeDesignView::getBehaviorTreeDesign()
{
    return behavior_tree_editor;
}

void BehaviorTreeDesignView::setWidgetDimensions(){
    namespace pt = boost::property_tree;

    std::string layout_dir=std::getenv("AEROSTACK_STACK")+std::string("/stack/ground_control_system/graphical_user_interface/layouts/layout.json");

    pt::read_json(layout_dir, root);

    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();

    int y0 = screenGeometry.height()/2;
    int x0 = screenGeometry.width()/2;
    int height = root.get<int>("2.height");
    int width= root.get<int>("2.width");

    this->resize(width,height);
    this->move(x0+root.get<int>("2.position.x"),y0+root.get<int>("2.position.y"));
}

void BehaviorTreeDesignView::setUp()
{
    //Nodes
    n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
    n.param<std::string>("window_opened", window_opened_topic, "window_opened");
    n.param<std::string>("window_closed", window_closed_topic, "window_closed");

    //Subscribers
    window_opened_subs = n.subscribe("/" + drone_id_namespace +  "/" + window_opened_topic, 10, &BehaviorTreeDesignView::windowOpenCallback, this);

    //Publishers
    window_closed_publ = n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace +  "/" + window_closed_topic , 1, true);
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Handlers for the main widget
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeDesignView::closeWindow()
{
    //windowClosedMsgs.id = aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN;
    //window_closed_publ.publish(windowClosedMsgs);
    this->close();
}

void BehaviorTreeDesignView::closeEvent (QCloseEvent *event)
{
    windowClosedMsgs.id = aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN;
    window_closed_publ.publish(windowClosedMsgs);
}
void BehaviorTreeDesignView::killMe()
{
#ifdef Q_OS_WIN
    enum { ExitCode = 0 };
    ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
    qint64 pid = QCoreApplication::applicationPid();
    QProcess::startDetached("kill -9 " + QString::number(pid));
#endif // Q_OS_WIN
}

void BehaviorTreeDesignView::windowOpenCallback(const aerostack_msgs::WindowIdentifier &msg)
{
    windowOpenedMsgs = msg;

    if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT || windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER || windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_MISSION_INTERPRETER || windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::KEYBOARD_CONTROL || windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_CONTROL || windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::ALPHANUMERIC_INTERFACE_CONTROL || windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::TML_CONTROL || windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_CONTROL || windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_PYTHON || windowOpenedMsgs.id ==  aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_TML) 
    {
        windowClosedMsgs.id = aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN;
        window_closed_publ.publish(windowClosedMsgs);
        killMe();
    }

    if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::MINIMIZE_MAIN_WINDOW )
        showMinimized();
}
