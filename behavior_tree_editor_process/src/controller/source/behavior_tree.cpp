/*
  BehaviorTree
  @author  Daniel Del Olmo
  @date    03-2019
  @version 3.0
*/

#include "../include/behavior_tree.h"
#include "../include/tree_item.h" //TreeItem class is included here to prevent recursive includes with tree_item.

BehaviorTree::BehaviorTree(QWidget *parent) : QTreeWidget(parent)
{
	//Widget configuration
	QWidget::setLocale(QLocale());
	this->headerItem()->setHidden(true);
	this->setContextMenuPolicy(Qt::CustomContextMenu);
    this->resize(600, 300);
	
	qRegisterMetaType<QVector<int>>("QVector<int>");

	visualizer_parent = (BehaviorTreeDesign*) parent;

	has_root = false;
	isTextExpanded = true;

	//signal is emitted when the widget's contextMenuPolicy is Qt::CustomContextMenu
	QObject::connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onCustomContextMenu(const QPoint &)));

	//Style sheet customization
	setIconSize(QSize(25, 25));
	this->setStyleSheet(" \
		QTreeView::branch { \
        	background: white; \
		} \
		QTreeView::branch:has-siblings:!adjoins-item { \
			border-image: url(:/images/images/vline.png) 0; \
		} \
		QTreeView::branch:has-siblings:adjoins-item { \
			border-image: url(:/images/images/branch-more.png) 0; \
		} \
		QTreeView::branch:!has-children:!has-siblings:adjoins-item { \
			border-image: url(:/images/images/branch-end.png) 0; \
		} \
		QTreeView::branch:has-children:!has-siblings:closed, \
		QTreeView::branch:closed:has-children:has-siblings { \
        	border-image: url(:/images/images/branch-more_proto_mas.png) 0; \
		} \
		QTreeView::branch:has-children:!has-siblings:closed, \
		QTreeView::branch:closed:has-children:!has-siblings { \
        	border-image: url(:/images/images/branch-end_proto_mas.png) 0; \
		} \
		QTreeView::branch:open:has-children:has-siblings  { \
        	border-image: url(:/images/images/branch-more_proto_menos.png) 0; \
		} \
		QTreeView::branch:open:has-children:!has-siblings  { \
        	border-image: url(:/images/images/branch-end_proto_menos.png) 0; \
		} \
		QTreeView::item:selected { \
      background-color:transparent; \
      color:black; \
    } \
		");

  //Establishment of connections
  setUp();
}

BehaviorTree::~BehaviorTree()
{

}

/*------------------------------------------------------------
--------------------------------------------------------------
                	Getters and Setters
--------------------------------------------------------------
------------------------------------------------------------*/
TreeItem * BehaviorTree::getRoot() {
	if(has_root)
		return root_item;
	else
		return nullptr;
}

BehaviorTreeDesign* BehaviorTree::getVisualizer()
{
	return visualizer_parent;
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Establishment of connections
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTree::setUp()
{

}

/*------------------------------------------------------------
--------------------------------------------------------------
 This function always executes when the tree is first created
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTree::createMission(const QString &texto)
{
	root_item = new TreeItem((TreeItem*) 0, NodeType::SEQUENCE);
	this->addTopLevelItem(root_item);
	std::string partial_mission_name = " [Execute all actions in sequence until one fails]";
	root_item->setPartialNodeName(partial_mission_name);
	std::string mission_name = texto.toUtf8().constData() + partial_mission_name;
	root_item->setNodeName(texto.toUtf8().constData());
	root_item->setText(0, QString::fromStdString(mission_name));
	QPixmap icono_pixmap = QPixmap(":/images/images/sequence.png");
	QIcon icono_sequence = QIcon(icono_pixmap);
	root_item->setIcon(0,icono_sequence);
	this->show();
}

void BehaviorTree::createMissionByTreeItem(TreeItem * root)
{
	root_item = root;
	this->addTopLevelItem(root_item);
	if (root != 0) {
		if (isTextExpanded) {
			expandTreeText(0);
		}
		else {
			expandTreeText(2);
		}
		this->expandAll();
	}
}

TreeItem * BehaviorTree::copyBehaviorTree(TreeItem * item, bool root, TreeItem * inherited_parent) 
{
	TreeItem * copy = 0;
	if (item != 0) {
		if (root) {
			copy = new TreeItem(0);
			copy->modifyNode(item->getNodeName(), item->getNodeType(), item->getBehaviorType(), item->isRecurrent(), item->isActivated(),item->getNodeAttributes());
		}
		else {
			copy = new TreeItem(inherited_parent);
			copy->modifyNode(item->getNodeName(), item->getNodeType(), item->getBehaviorType(), item->isRecurrent(), item->isActivated(),item->getNodeAttributes());
		}
		int children = item->childCount();
		for (int i = 0; i < children; i++) {
			copy->addChild(copyBehaviorTree(item->child(i),false,copy));
		}
	}
	return copy;
}


/*------------------------------------------------------------
--------------------------------------------------------------
				Rigth click manager
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTree::onCustomContextMenu(const QPoint &p)
{
	//When user click behavior tree window
	if(itemAt(p)!=0)
	{
		//When click node
		TreeItem *item_clicked;
		if(is_menu_created)
		{
			contextMenu->close();
			delete contextMenu;
			is_menu_created = false;
		}
		contextMenu = new QMenu("Menu", this);
		is_menu_created = true;
		item_clicked = (TreeItem*)itemAt(p);
		TreeItem *item_clicked_parent;

		bool isSucceeder=false;
		bool isInverter=false;
		bool isSelector=false;
		bool isBehavior = false;
		bool isQuery = false;
		bool isAddBelief = false;
		bool canHaveSiblings = false;
		int children;
		//Check if the clicked node can have children
		if(item_clicked->getNodeType()==NodeType::SUCCEEDER)
		{
			isSucceeder = true;
			children = item_clicked->childCount();
		}
		if(item_clicked->getNodeType()==NodeType::INVERTER)
		{
			isInverter = true;
			children = item_clicked->childCount();
		}
		if(item_clicked->getNodeType()==NodeType::SELECTOR)
		{
			isSelector = true;
			children = item_clicked->childCount();
		}
		if(item_clicked->getNodeType() == NodeType::BEHAVIOR)
		{
			isBehavior = true;
		}
		if(item_clicked->getNodeType() == NodeType::QUERY)
		{
			isQuery = true;
		}
		if(item_clicked->getNodeType() == NodeType::ADD_BELIEF)
		{
			isAddBelief = true;
		}

		//Check if the clicked node can have siblings
		item_clicked_parent = item_clicked->getParent();
		if(item_clicked_parent != 0)
		{
			if(item_clicked_parent->getNodeType() != NodeType::SUCCEEDER 
					&& item_clicked_parent->getNodeType() != NodeType::INVERTER 
					&& item_clicked_parent->getNodeType() != NodeType::SELECTOR)
			{
				canHaveSiblings=true;
			}
			else if((item_clicked_parent->getNodeType() == NodeType::SUCCEEDER || item_clicked_parent->getNodeType() == NodeType::INVERTER || item_clicked_parent->getNodeType() == NodeType::SELECTOR) 
				&& item_clicked_parent->childCount() < 1)
			{
				canHaveSiblings=true;
			}
		}
		//QUESTION6: Tab refresh tree
		point = p;
		QAction action1("Add child", this);
		QAction action2("Add sibling", this);
		QAction action3("Delete this node", this);
		QAction action4("Modify this node", this);
		QAction action5("Refresh tree", this);

		if((isSucceeder || isInverter) && (children < 1))
			contextMenu->addAction(&action1);

		else if (!isSucceeder && !isInverter && !isBehavior && !isQuery && !isAddBelief)
			contextMenu->addAction(&action1);

		if(canHaveSiblings)
			contextMenu->addAction(&action2);

		contextMenu->addAction(&action3);

		if(item_clicked != root_item)
		{
			contextMenu->addAction(&action4);
		}

		if(item_clicked->isRoot())
			contextMenu->addAction(&action5);
		

		connect(&action1, SIGNAL(triggered()), this, SLOT(addChildAux()));
		connect(&action2, SIGNAL(triggered()), this, SLOT(addSiblingAux()));
		connect(&action3, SIGNAL(triggered()), this, SLOT(removeItemWidgetAtAux()));
		connect(&action4, SIGNAL(triggered()), this, SLOT(modifyItemWidgetAux()));
		connect(&action5, SIGNAL(triggered()), this, SLOT(refreshTree()));

		contextMenu->exec(mapToGlobal(p));
	}
	else if (itemAt(p)==0)
	{
		point = p;
		if(is_menu_created)
		{
			contextMenu->close();
			delete contextMenu;
			is_menu_created = false;
		}
		is_menu_created = true;
		contextMenu = new QMenu("Menu", this);
	}
}

//This function adds a root to the tree if it doesn't have one already
void BehaviorTree::addTopLevelItem(TreeItem *top_level_item)
{
	if (has_root) {
		if (root_before != 0) {
			delete root_before;
		}
	}
	if (top_level_item != 0) {
		root_before = top_level_item;
		has_root = true;
		top_level_item->setRoot(true);
	}
	else {
		has_root = false;
	}
	QTreeWidget::addTopLevelItem(top_level_item);
}

void BehaviorTree::windowFinished(TreeItem *the_item)
{
	this->expandAll();
	if (isTextExpanded) {
		expandTreeText(0);
	}
	else {
		expandTreeText(2);
	}
	QObject::connect(window, SIGNAL(windowAccepted(TreeItem *)), this, SLOT(windowFinished(TreeItem *)));
}

/*------------------------------------------------------------
--------------------------------------------------------------
                	Node actions
--------------------------------------------------------------
------------------------------------------------------------*/

//Next two functions are called whenever we want to add a child
void BehaviorTree::addChildAux()
{
	connect(this, SIGNAL(executeAddChildAction(const QPoint &)), this, SLOT(addChild(const QPoint &)));
	Q_EMIT(executeAddChildAction(point));
	disconnect(this, SIGNAL(executeAddChildAction(const QPoint &)), this, SLOT(addChild(const QPoint &)));
}

void BehaviorTree::addChild(const QPoint &p)
{
	parent_item_for_child = (TreeItem*)itemAt(p);
	window = new BehaviorDialog(this, parent_item_for_child);
	window->show();
	QObject::connect(window, SIGNAL(windowAccepted(TreeItem *)), this, SLOT(windowFinished(TreeItem *)));
}

//Next two functions are called whenever we want to add a sibling
void BehaviorTree::addSiblingAux()
{
	connect(this, SIGNAL(executeAddSiblingAction(const QPoint &)), this, SLOT(addSibling(const QPoint &)));
	Q_EMIT(executeAddSiblingAction(point));
	disconnect(this, SIGNAL(executeAddSiblingAction(const QPoint &)), this, SLOT(addSibling(const QPoint &)));
}

void BehaviorTree::addSibling(const QPoint &p)
{
	parent_item_for_sibling = (TreeItem*)itemAt(p);
	parent_item_for_sibling = parent_item_for_sibling->TreeItem::getParent();
	window = new BehaviorDialog(this, parent_item_for_sibling);
	window->show();	
	QObject::connect(window, SIGNAL(windowAccepted(TreeItem *)), this, SLOT(windowFinished(TreeItem *)));
}

//Next two functions are called whenever we want to modify any node
void BehaviorTree::modifyItemWidgetAux()
{
	connect(this, SIGNAL(executeModifyItemWidgetAction(const QPoint &)), this, SLOT(modifyItemWidget(const QPoint &)));
	Q_EMIT(executeModifyItemWidgetAction(point));
	disconnect(this, SIGNAL(executeModifyItemWidgetAction(const QPoint &)), this, SLOT(modifyItemWidget(const QPoint &)));
}

void BehaviorTree::modifyItemWidget(const QPoint &p)
{
	parent_item_for_modify = (TreeItem*)itemAt(p);
	parent_item_for_modify = parent_item_for_modify->TreeItem::getParent();
	window = new BehaviorDialog(this, parent_item_for_modify, (TreeItem*)itemAt(p));
	window->show();
	QObject::connect(window, SIGNAL(windowAccepted(TreeItem *)), this, SLOT(windowFinished(TreeItem *)));
}

//Next two functions are called whenever we want to remove any node
void BehaviorTree::removeItemWidgetAtAux()
{
	connect(this, SIGNAL(executeRemoveItemAction(const QPoint &)), this, SLOT(removeItemWidgetAt(const QPoint &)));
	Q_EMIT(executeRemoveItemAction(point));
}

void BehaviorTree::removeItemWidgetAt(const QPoint &p)
{
	disconnect(this, SIGNAL(executeRemoveItemAction(const QPoint &)), this, SLOT(removeItemWidgetAt(const QPoint &)));
	if ((BehaviorTree*)itemAt(p) != this){
		TreeItem *item_to_remove = (TreeItem*)itemAt(p);
		TreeItem *item_to_remove_back = item_to_remove;
		item_to_remove->removeItemWidget();
		if (item_to_remove_back->isRoot())
		{
			has_root = false;
			this->clear();
		}
	}
}

//Next function refresh the behavior tree
void BehaviorTree::refreshTree()
{
	std::string route = visualizer_parent->getFileRoute();
	visualizer_parent->loadTreeFile(route);
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Collapse text actions
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTree::expandTreeText(int checkState)
{
	if (has_root) {
		if(checkState == 2)
		{
			minimizeText(root_item);
			isTextExpanded=false;
		}
		else if(checkState == 0)
		{
			expandText(root_item);
			isTextExpanded=true;
		}
	}
}

void BehaviorTree::expandText(TreeItem *item)
{
	std::string text = item->getNodeName();
	text = text +  item->getPartialNodeName();
	item->setText(0, QString::fromStdString(text));
	if(item->childCount()>0)
	{
		for(int i = 0; i < item->childCount(); i++)
		{
			expandText(item->child(i));
		}
	}
}

void BehaviorTree::minimizeText(TreeItem *item)
{
	item->setText(0, QString::fromStdString(item->getNodeName()));
	if(item->childCount()>0)
	{
		for(int i = 0; i < item->childCount(); i++)
		{
			minimizeText(item->child(i));
		}
	}
}

void BehaviorTree::createMissionDialog() 
{
  mission_dialog = new MissionSpecificationDialog();
  QObject::connect(mission_dialog, SIGNAL(missionNameSelected(const QString &)), this, SLOT(createMission(const QString &)));
  mission_dialog->show();
}