/*TreeFileManager
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/tree_file_manager.h"

TreeFileManager::TreeFileManager()
{

}

TreeFileManager::~TreeFileManager()
{

}

void TreeFileManager::saveTree(TreeItem* root_item, std::string variables, std::string save_route)
{
    id_count = 1;
    int parent = id_count;
    std::string aux;
    YAML::Node node;
    std::ofstream fout(save_route);
    emitter << YAML::BeginMap; //Mapa Global
    emitter << YAML::Key << "nodes"; 
    if(root_item != nullptr)
    {   
      emitter << YAML::BeginSeq; //Secuencia de nodos
      emitter << YAML::BeginMap; //Mapa dentro de cada nodo
      emitter << YAML::Key << "id" << YAML::Value << id_count;
      emitter << YAML::Key << "node_name" << YAML::Value << root_item->getNodeName();
      emitter << YAML::Key << "node_type" << YAML::Value << root_item->nodeTypeToString(root_item->getNodeType());
      emitter << YAML::Key << "behavior_type" << YAML::Value << root_item->getBehaviorType();
      emitter << YAML::Key << "arguments" << YAML::Value << root_item->getNodeAttributes();
      emitter << YAML::Key << "is_recurrent" << YAML::Value << root_item->isRecurrent();
      emitter << YAML::Key << "activate" << YAML::Value << root_item->isActivated();
      emitter << YAML::Key << "parent" << YAML::Value << 0;
      emitter << YAML::EndMap;
      if(root_item->childCount()>0)
      {
        for(int i = 0; i < root_item->childCount(); i++)
        {
          id_count++;
          recursive_save(root_item->child(i), id_count, parent);
        }
      }
      //After this loop we have all the nodes set up in the emitter
      emitter << YAML::EndSeq;
      //TODO: save variables
    }
    else 
    {
      emitter << YAML::Value << YAML::Null;
    }
    aux = " " + variables + " ";
    emitter << YAML::Key << "variables" << YAML::Value << aux ;
    emitter << YAML::EndMap;
    fout << emitter.c_str();
}

void TreeFileManager::recursive_save(TreeItem* item, int id, int parent)
{ 
    emitter << YAML::BeginMap; //Mapa dentro de cada nodo
    emitter << YAML::Key << "id" << YAML::Value << id;
    emitter << YAML::Key << "node_name" << YAML::Value << item->getNodeName();
    emitter << YAML::Key << "node_type" << YAML::Value << item->nodeTypeToString(item->getNodeType());
    emitter << YAML::Key << "behavior_type" << YAML::Value << item->getBehaviorType();
    emitter << YAML::Key << "arguments" << YAML::Value << item->getNodeAttributes();
    emitter << YAML::Key << "is_recurrent" << YAML::Value << item->isRecurrent();
    emitter << YAML::Key << "activate" << YAML::Value << item->isActivated();
    emitter << YAML::Key << "parent" << YAML::Value << parent;
    emitter << YAML::EndMap;
    if(item->childCount()>0)
    {
      int parent_aux = id_count;
      for(int i = 0; i < item->childCount(); i++)
      {
        id_count++;
        recursive_save(item->child(i), id_count, parent_aux);
      }
    }
}


TreeItem* TreeFileManager::loadTree(std::string load_route, QTextEdit * vis)
{
  TreeItem* root = nullptr;
  YAML::Node archive = YAML::LoadFile(load_route);
  YAML::Node nodes;
  YAML::Node variables;

  if (!(nodes = archive["nodes"]))
  {
    error_message.setWindowTitle(QString::fromStdString("Loading mission error"));
    error_message.setText(QString::fromStdString("The behavior tree mission file is badly structured. Unable to import."));
    error_message.setWindowFlags(error_message.windowFlags() | Qt::WindowStaysOnTopHint);
    error_message.exec();
    return nullptr;
  }
  if (!(variables = archive["variables"]))
  {
    error_message.setWindowTitle(QString::fromStdString("Loading mission error"));
    error_message.setText(QString::fromStdString("The behavior tree mission file is badly structured. Unable to import."));
    error_message.setWindowFlags(error_message.windowFlags() | Qt::WindowStaysOnTopHint);
    error_message.exec();
    return nullptr;
  }
  std::vector<TreeItem*> item;
  std::map<int, TreeItem*> m;
  std::map<int, int> map_parent;
  if(archive.size() != 0) 
  { 
    for (std::size_t i=0;i<nodes.size();i++) {
      YAML::Node node = nodes[i];
      int parent = node["parent"].as<int>();
      if(parent == 0)
      {
        TreeItem * item = new TreeItem(0);
        int id = node["id"].as<int>();
        std::string node_name;
        bool activate;
        bool recurrent;
        std::string arguments;
        NodeType type;
        std::string btype;
        try
        {
           node_name = node["node_name"].as<std::string>(); 
           type = item->stringToNodeType(node["node_type"].as<std::string>()); 
           btype = node["behavior_type"].as<std::string>(); 
           recurrent = node["is_recurrent"].as<bool>(); 
           activate = node["activate"].as<bool>();
           arguments = node["arguments"].as<std::string>(); 
        } catch (const std::exception &e)
        {
          error_message.setWindowTitle(QString::fromStdString("Loading mission error"));
          error_message.setText(QString::fromStdString("The behavior tree mission file is badly structured. Unable to import."));
          error_message.setWindowFlags(error_message.windowFlags() | Qt::WindowStaysOnTopHint);
          error_message.exec();
          return nullptr;
        }
        item->modifyNode(node_name, type, btype, recurrent, activate, arguments);
        m[id] = item;
        map_parent[id] = parent;
      } else 
      {
        TreeItem * item = new TreeItem(m[parent]);
        YAML::Node node = nodes[i];
        int id;
        std::string node_name;
        NodeType type;
        std::string btype;
        bool recurrent;
        bool activate;
        std::string arguments;
        try
        {
           id = node["id"].as<int>(); 
           node_name = node["node_name"].as<std::string>(); 
           btype = node["behavior_type"].as<std::string>();
           type = item->stringToNodeType(node["node_type"].as<std::string>()); 
           recurrent = node["is_recurrent"].as<bool>(); 
           activate = node["activate"].as<bool>(); 
           arguments = node["arguments"].as<std::string>(); 
        } catch (const std::exception &e)
        {
          error_message.setWindowTitle(QString::fromStdString("Loading mission error"));
          error_message.setText(QString::fromStdString("The behavior tree mission file is badly structured. Unable to import."));
          error_message.setWindowFlags(error_message.windowFlags() | Qt::WindowStaysOnTopHint);
          error_message.exec();
          return nullptr;
        }
        item->modifyNode(node_name, type, btype, recurrent, activate, arguments);
        m[id] = item;
        map_parent[id] = parent;
      }
    }
    for (const auto& kv : map_parent) 
    {
      if (kv.second == 0) 
      {
        m[kv.first]->setRoot(true);
        root = m[kv.first];
      }
      else 
      {
        m[kv.second]->addChild(m[kv.first]);
      }
    }
    std::string str;
    try
    {
       str = archive["variables"].as<std::string>(); 
    } catch (const std::exception &e){
      error_message.setWindowTitle(QString::fromStdString("Loading mission error"));
      error_message.setText(QString::fromStdString("The behavior tree mission file is badly structured. Unable to import."));
      error_message.setWindowFlags(error_message.windowFlags() | Qt::WindowStaysOnTopHint);
      error_message.exec();
      return nullptr;
    }
    str.erase(0,1);
    str.erase(str.end()-1,str.end());
    vis->setText(QString(str.c_str()));
    return root;
  } else 
  {
    error_message.setWindowTitle(QString::fromStdString("Loading mission error"));
    error_message.setText(QString::fromStdString("The behavior tree mission file is empty. Unable to import."));
    error_message.setWindowFlags(error_message.windowFlags() | Qt::WindowStaysOnTopHint);
    error_message.exec();
    return nullptr;
  }
}
