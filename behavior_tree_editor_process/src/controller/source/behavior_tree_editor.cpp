/*
  BehaviorTreeEditor
  @author  Daniel Del Olmo
  @date    03-2019
  @version 3.0
  @description This class creates widgets inside windows. Also it manage
                buttons actions
*/
#include "../include/behavior_tree_editor.h"

BehaviorTreeDesign::BehaviorTreeDesign(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::BehaviorTreeDesign)
{
  ui->setupUi(this); //connects all ui's triggers
  
  expand_text_button = new QCheckBox("Collapse tree", this);
  export_button = new QPushButton("Export");
  import_button = new QPushButton("Import");
  new_button = new QPushButton("New");
  accept_button = new QPushButton("Accept");
  cancel_button = new QPushButton("Cancel");
  save_button = new QPushButton("Save");
  tree = new BehaviorTree(this);
  tree_label = new QLabel("Behavior Tree");
  beliefs_label = new QLabel("Variables");
  beliefs_text = new QTextEdit(this);

  my_parent = (BehaviorTreeDesignView*) parent;

  //QUESTION9: Create autosave_tree button or autosave_tree action in behavior tree
  //like refres_action, and connect with acceptTreeEdition

  //Widgets
  this->QWidget::setWindowTitle(QString::fromStdString("Behavior Tree"));
  ui->gridLayout->addWidget(tree_label, 0, 0, 1, 1);
  ui->gridLayout->addWidget(tree, 1, 0, 1, 6);
  ui->gridLayout->addWidget(expand_text_button, 2, 0, 1, 1);
  ui->gridLayout->addWidget(beliefs_label, 3, 0, 1, 1);
  ui->gridLayout->addWidget(beliefs_text, 4, 0, 1, 6);
  ui->gridLayout->addWidget(accept_button, 5, 5, 1, 1);
  ui->gridLayout->addWidget(cancel_button, 5, 4, 1, 1);
  ui->gridLayout->addWidget(save_button, 5, 3, 1, 1);
  ui->gridLayout->addWidget(export_button, 5, 2, 1, 1);
  ui->gridLayout->addWidget(import_button, 5, 1, 1, 1);
  ui->gridLayout->addWidget(new_button, 5, 0, 1, 1);

  beliefs_text->setMaximumHeight(120);
  
  //Default configuration folder
  homePath = QDir::homePath().toStdString();


  //Establishment of connections
  setUp();

  //Save initial tree
  visualized_tree = tree;
  original_tree_copy = visualized_tree->copyBehaviorTree(visualized_tree->getRoot(), true, 0);

  //Connects
  QObject::connect(expand_text_button, SIGNAL(stateChanged(int)), visualized_tree, SLOT(expandTreeText(int)));
  QObject::connect(cancel_button, SIGNAL(clicked()), this, SLOT(cancelTreeEdition()));
  QObject::connect(accept_button, SIGNAL(clicked()), this, SLOT(acceptTreeEdition()));
  QObject::connect(save_button, SIGNAL(clicked()), this, SLOT(saveTreeEdition()));
  QObject::connect(export_button, SIGNAL(clicked()), this, SLOT(exportBehaviorTreeMission()));
  QObject::connect(import_button, SIGNAL(clicked()), this, SLOT(importBehaviorTreeMission()));
  QObject::connect(new_button, SIGNAL(clicked()), visualized_tree, SLOT(createMissionDialog()));

  //Set configuration folder
  setFileRoute();

  //Load tree
  importing = false;
  char esc_char = 27; 
  list_of_errors << "Wrong format in following file: '" << file_route << "'. Fix the following bugs before importing file:\n";
 
  loadTreeFile(file_route);
}

/*------------------------------------------------------------
--------------------------------------------------------------
                      Destructor
--------------------------------------------------------------
------------------------------------------------------------*/
BehaviorTreeDesign::~BehaviorTreeDesign()
{
  delete ui;
  delete tree;
  delete tree_label;
  delete beliefs_label;
  delete beliefs_text;
  delete expand_text_button;
  delete export_button;
  delete import_button;
  delete new_button;
  delete accept_button;
  delete cancel_button;
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Establishment of connections
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeDesign::setUp()
{  
  //Nodes
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("configuration_folder", configuration_folder_service, "configuration_folder");
  n.param<std::string>("check_behavior_format_srv", check_behavior_format, "check_behavior_format");
  n.param<std::string>("my_stack_directory", my_stack_directory, homePath + "/workspace/ros/aerostack_catkin_ws/src/aerostack_stack/");
  //Service communications
  configuration_folder_client = n.serviceClient<droneMsgsROS::configurationFolder>("/"+configuration_folder_service);
  configuration_folder_client.call(req, res);
  configuration_folder = res.folder;
   if (configuration_folder == "")
    configuration_folder = my_stack_directory +"/configs/"+ drone_id_namespace;
  check_behavior_format_srv=n.serviceClient<aerostack_msgs::CheckBehaviorFormat>("/"+drone_id_namespace+"/"+check_behavior_format);
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Getters and setters
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeDesign::setText(std::string texto)
{
  text = QString(texto.c_str());
}

std::string BehaviorTreeDesign::getText()
{
  std::string result = this->beliefs_text->toPlainText().toUtf8().constData();
  return result;
}

std::string BehaviorTreeDesign::getFileRoute()
{
  setFileRoute();
  return this->file_route;
}

BehaviorTree* BehaviorTreeDesign::getBehaviorTree()
{
  return this -> tree;
}

/*------------------------------------------------------------
--------------------------------------------------------------
                    Button actions
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeDesign::acceptTreeEdition()
{
  //Tree format would be correct
  //if(checkTree(visualized_tree->getRoot()))
  //{
  TreeFileManager tfm; //Meter en metodo auxiliar
  tfm.saveTree(visualized_tree->getRoot(), getText(), file_route); //Meter en metodo auxiliar
  my_parent->closeWindow();
  //}
}

void BehaviorTreeDesign::saveTreeEdition()
{
  //Tree format would be correct
  if(checkTree(visualized_tree->getRoot()))
  {
    TreeFileManager tfm; //Meter en metodo auxiliar
    tfm.saveTree(visualized_tree->getRoot(), getText(), file_route); //Meter en metodo auxiliar
  }

  else 
    windowManager('e', "Bad tree format", list_of_errors.str());
}

void BehaviorTreeDesign::cancelTreeEdition()
{
  windowManager('c', "Save Changes?", "<b>Do you want to save the changes in configuration folder?</b> \n\n\nYour changes will be lost if you don't save them.");
}

void BehaviorTreeDesign::importBehaviorTreeMission()
{
  tree_mission = QFileDialog::getOpenFileName(this, tr("Select File"), "", tr("YAML file (*.yaml)"));
  if (tree_mission != ""){
    importing = true;
    loadTreeFile(tree_mission.toUtf8().constData());
  }
  //else //Print error when load file 
}

void BehaviorTreeDesign::exportBehaviorTreeMission()
{
  filename = QFileDialog::getSaveFileName(this, tr("Save File"), "/home", "YAML File (*.yaml)");
  filestd = filename.toStdString();
  if (filestd != "") {
    if (filestd.find(".yaml") == std::string::npos)
      filestd = filestd + ".yaml";

    TreeFileManager tfm;//Meter en metodo auxiliar
    tfm.saveTree(visualized_tree->getRoot(), getText(), filestd); //Meter en metodo auxiliar
  }
}

/*------------------------------------------------------------
--------------------------------------------------------------
                      Files manager
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeDesign::setFileRoute()
{

    file_route = configuration_folder + "/behavior_tree_mission_file.yaml";
}

void BehaviorTreeDesign::loadTreeFile(std::string tree_mission)
{
  std::cout << "Behavior tree mission file: " << tree_mission << std::endl;

  aux_file.open(tree_mission, std::fstream::in | std::fstream::out | std::fstream::app);

  if (aux_file.is_open())
  {
    aux_file.seekp(0, std::ios::end);
    size_t size = aux_file.tellg();
    if (size != 0)
    {
      TreeFileManager tfm;
      root_node = tfm.loadTree(tree_mission, beliefs_text);

      type_message = "import";

      if(checkTree(root_node)){
        type_message = "";
        if (root_node != nullptr)
          visualized_tree->createMissionByTreeItem(root_node);
      }
      else{
        windowManager('e', "Bad tree format", list_of_errors.str());

        if (!importing){
          my_parent->closeWindow();
        }
      }

    }
  }
  tree->show();
  tree_label->show();
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Check format tree
--------------------------------------------------------------
------------------------------------------------------------*/
bool BehaviorTreeDesign::checkTree(TreeItem *item)
{
  bool isCorrect = true;
  bool correctChildren = true;

  NodeType item_nodetype = item->getNodeType();

  std::string node_type_name = "";
  std::string wrong_children = "Wrong number of children in node '";

  switch(item_nodetype)
  {
    //The following nodes can't have children
    case NodeType::BEHAVIOR:
      node_type_name = "BEHAVIOR";

    case NodeType::QUERY: 
     node_type_name = node_type_name.empty()? "QUERY" : node_type_name;

    case NodeType::ADD_BELIEF:
      node_type_name = node_type_name.empty()? "ADD_BELIEF" : node_type_name;

    case NodeType::REMOVE_BELIEF:
    {
      node_type_name = node_type_name.empty()? "REMOVE_BELIEF" : node_type_name;

      if(item->childCount() != 0)
      {
        list_of_errors <<  "\n  - " << wrong_children << item->getNodeName() << "': A node of '" << node_type_name << "' type can not have children.\n" << std::endl;
        correctChildren = false;
      }

      if(item_nodetype == NodeType::BEHAVIOR)
      {
        behavior_msg.name = item->getBehaviorType();
        behavior_msg.arguments = this->processData(item->getNodeAttributes());
        check_format_msg_req.behavior = behavior_msg;
        check_behavior_format_srv.call(check_format_msg_req, check_format_msg_res);

        std::cout << "error_message" << check_format_msg_res.error_message << '\n';
        if(!check_format_msg_res.ack)
        {          
          if (check_format_msg_res.error_message.find("configuration") == std::string::npos)
            error_behavior_type = "\n   - Invalid arguments in node '" + item->getNodeName() + "': ";

          else
            error_behavior_type = "\n   - The node '" + item->getNodeName() + "' is an invalid behavior: ";
          
          error_behavior_type += check_format_msg_res.error_message + ".\n";
          list_of_errors << error_behavior_type;
          correctChildren = false;
        }
      } 
      break;
    }

    //The following nodes can have just one child
    case NodeType::SUCCEEDER:
      node_type_name = "SUCCEEDER";

    case NodeType::INVERTER:
      node_type_name = node_type_name.empty()? "INVERTER" : node_type_name;

    case NodeType::REPEATER:
    {
      node_type_name = node_type_name.empty()? "REPEATER" : node_type_name;

      if(item->childCount() != 1)
      {
        list_of_errors << "\n-  " << wrong_children << item->getNodeName() << "': A node of '" << node_type_name << "' type can have just one child.\n";
        correctChildren = false;
      }
      break;
    }

    //The following nodes must have at least one child
    case NodeType::SEQUENCE:
      node_type_name = "SEQUENCE";

    case NodeType::PARALLEL:
      node_type_name = node_type_name.empty()? "PARALLEL" : node_type_name;

    case NodeType::REPEAT_UNTIL_FAIL:
      node_type_name = node_type_name.empty()? "REPEAT_UNTIL_FAIL" : node_type_name;

    case NodeType::SELECTOR:
    {
      node_type_name = node_type_name.empty()? "SELECTOR" : node_type_name;

      if(item->childCount() < 1)
      {
        list_of_errors << "\n- " << wrong_children << item->getNodeName() << "': A node of '" << node_type_name << "' type must have at least one child.\n";
        correctChildren = false;
      }
      break;
    }
  }

  if(!correctChildren)
    return false;

  //Checking each of its children recursively
  if(item->childCount()>0)
  {
    for(int i = 0; i < item->childCount(); i++)
    { 
      correctCheck = checkTree(item->child(i));
      isCorrect = isCorrect && correctCheck;
    }
  }

  return isCorrect;
}

std::string BehaviorTreeDesign::processData(std::string raw_arguments) 
{
  std::string text = this->getText();
  if (text != "") 
  {
    YAML::Node node_beliefs = YAML::Load(text);
    for(YAML::const_iterator it=node_beliefs.begin();it!=node_beliefs.end();++it) 
    {
      std::string name = "+" + it->first.as<std::string>();
      if (raw_arguments.find(name)) 
      {
        std::string data = processType(it);
        boost::replace_all(raw_arguments, name, data);
      }
    }
  }
  return raw_arguments;
}

std::string BehaviorTreeDesign::processType(YAML::const_iterator it) 
{
  std::string res = "";
  switch (it->second.Type()) 
  {
    case YAML::NodeType::Scalar: 
    {
      res = it->second.as<std::string>();
      break;
    }
    case YAML::NodeType::Sequence: 
    {
      std::vector<double> vec = it->second.as<std::vector<double>>();
      std::ostringstream oss;
      if (!vec.empty())
      {
        std::copy(vec.begin(), vec.end()-1,
        std::ostream_iterator<double>(oss, ","));
        oss << vec.back();
      }
      res = "[" + oss.str() + "]";
      break;
    }
  }
  return res;
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Messages output manager
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeDesign::windowManager(char type, std::string title, std::string message){
  switch(type){
    case 'c': //cancel
    {
      QMessageBox message_window(QMessageBox::Question, tr("Save Changes?"), tr(message.c_str()));
      message_window.setWindowFlags(message_window.windowFlags() | Qt::WindowStaysOnTopHint);
  
      m_cancel_button = message_window.addButton(tr("Cancel"), QMessageBox::NoRole);
      m_dont_save_button = message_window.addButton(tr("Don't Save"), QMessageBox::NoRole);
      m_save_button = message_window.addButton(tr("Save"), QMessageBox::YesRole);
      message_window.exec();

      if (message_window.clickedButton() == m_save_button)
        acceptTreeEdition();

      else if (message_window.clickedButton() == m_dont_save_button)
      {
        visualized_tree->createMissionByTreeItem(visualized_tree->copyBehaviorTree(original_tree_copy, true, 0));
        if (original_tree_copy != 0)
          delete original_tree_copy;

        my_parent->closeWindow();
      }
         
      break;
    }

    case 's': //saved sucessfully
    {
      QMessageBox *msg_error = new QMessageBox(QMessageBox::Information, title.c_str(), message.c_str(), QMessageBox::Ok,this);
      msg_error->setWindowFlags(msg_error->windowFlags() | Qt::WindowStaysOnTopHint);
      msg_error->exec();
      break;
    }
    
    case 'e': //error
    {
        QMessageBox *msg_error = new QMessageBox(QMessageBox::Critical, title.c_str(), message.c_str(), QMessageBox::Ok,this);
        msg_error->setWindowFlags(msg_error->windowFlags() | Qt::WindowStaysOnTopHint);
        msg_error->exec();
      break;
    }

    default:
      break;
  }
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Auxiliar methods
--------------------------------------------------------------
------------------------------------------------------------*/
std::ostream& bold_on(std::ostream& os)
{
    return os << "\e[1m";
}

std::ostream& bold_off(std::ostream& os)
{
    return os << "\e[0m";
}
