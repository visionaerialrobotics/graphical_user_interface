
/*
  Main Qt GUI
  Initialize the behavior tree editor GUI
  @author  Daniel Del Olmo Orejana
  @date    04-2018
  @version 3.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <stdio.h>
#include <ros/ros.h>
#include "../include/behavior_tree_editor_view.h"
#include <QApplication>
#include <signal.h>

/*****************************************************************************
** Implementation
*****************************************************************************/

void signalhandler(int sig)
{
    if (sig == SIGINT)
    {
        qApp->quit();
    }
    else if (sig == SIGTERM)
    {
        qApp->quit();
    }
}

void spinnerThread(){
  ros::spin();
}

int main(int argc, char *argv[])
{
  ros::init(argc,argv,"behavior_tree_editor");// ros node started.
  QApplication app(argc, argv);
  BehaviorTreeDesignView w(argc, argv);

  w.show();
  std::thread thr(&spinnerThread);

  signal(SIGINT,signalhandler);
  signal(SIGTERM,signalhandler);

  app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
  int result = app.exec();
  return result;
}
