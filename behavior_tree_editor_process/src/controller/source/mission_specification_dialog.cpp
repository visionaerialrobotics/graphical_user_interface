/*
  MissionSpecificationDialog
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/mission_specification_dialog.h"

MissionSpecificationDialog::MissionSpecificationDialog(QWidget *parent) : QDialog(parent), ui(new Ui::MissionSpecificationDialog)
{
  ui->setupUi(this);
  
  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);
  
  my_layout = ui->layout;
  this->setModal(true);

  connect(ui->accept_pushbutton, SIGNAL(clicked()), this, SLOT(actionAccept()));
  connect(ui->cancel_pushbutton, SIGNAL(clicked()), this, SLOT(actionCancel()));

}

MissionSpecificationDialog::~MissionSpecificationDialog()
{
  delete ui;
}

void MissionSpecificationDialog::actionAccept()
{
  if(!this->ui->line_edit_name->text().isEmpty())
  {
    QString mission_name; 
    mission_name = ui->line_edit_name->text();
    Q_EMIT(missionNameSelected(mission_name));
    Q_EMIT(close());
  } 
  else
  {
    QMessageBox *error_message = new QMessageBox(QMessageBox::Critical, "Parameters error", "The behavior tree name cannot be empty. Please insert a name.", QMessageBox::Ok,this);
    error_message->setWindowFlags(error_message->windowFlags() | Qt::WindowStaysOnTopHint);
    ui->line_edit_name->setStyleSheet("padding: 2.9 2.9px; border-style: outset; border-width: 1px; border-radius: 5px; border-color: red;");
    error_message->exec();
    ui->line_edit_name->setStyleSheet("");
  }
}

void MissionSpecificationDialog::actionCancel()
{
  this->hide();
}
