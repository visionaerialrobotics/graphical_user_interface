/*!*******************************************************************************************
 *  \file      behavior_tree_editor_view.h
 *  \brief     Behavior tree Design View definition file.
 *  \details   Layout for the Behavior tree Design component. Widgets can be dynamically added to it.
 *  \author    Daniel Del Olmo
********************************************************************************************/

#ifndef BEHAVIOR_TREE_EDITOR_VIEW_H
#define BEHAVIOR_TREE_EDITOR_VIEW_H

#include <ros/ros.h>
#include "aerostack_msgs/WindowIdentifier.h"

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QRect>
#include <QGuiApplication>
#include <QScreen>
#include <QProcess>
#include <QCloseEvent>

#include "behavior_tree_editor.h"
#include "ui_behavior_tree_design_view.h"

#include <thread>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "fstream"
namespace Ui {
class BehaviorTreeDesignView;
}

class BehaviorTreeDesign;

class BehaviorTreeDesignView : public QWidget
{
    Q_OBJECT

public:
    //Constructor & Destructor
    explicit BehaviorTreeDesignView(int argc, char** argv, QWidget *parent = 0);
    ~BehaviorTreeDesignView();

public:
    /*!********************************************************************************************************************
  *  \brief      This method is executed when the user clicks the cancel or accept button within the edition mode.
  **********************************************************************************************************************/
    void closeWindow();

private:
    Ui::BehaviorTreeDesignView *ui;
    BehaviorTreeDesign *behavior_tree_editor;
    ros::NodeHandle n;

    ros::Publisher window_closed_publ;
    ros::Subscriber window_opened_subs;

    std::string window_opened_topic;
    std::string window_closed_topic;


    aerostack_msgs::WindowIdentifier windowClosedMsgs;
    aerostack_msgs::WindowIdentifier windowOpenedMsgs;
    
    std::string drone_id_namespace;
    boost::property_tree::ptree root;
    
    /*!********************************************************************************************************************
  *  \brief      This method is the responsible for seting up connections.
  *********************************************************************************************************************/
    void setUp();
    /*!************************************************************************
  *  \brief   Activated when a window is closed.
  ***************************************************************************/
    void windowOpenCallback(const aerostack_msgs::WindowIdentifier &msg);
    /*!************************************************************************
   *  \brief  Kills the process
   ***************************************************************************/
    void killMe();

    BehaviorTreeDesign *getBehaviorTreeDesign();
    void setWidgetDimensions();

public Q_SLOTS:

    /*!********************************************************************************************************************
  *  \brief      Event launched when user closes the window
  **********************************************************************************************************************/
    void closeEvent (QCloseEvent *event);
};

#endif // BEHAVIOR_TREE_EDITOR_VIEW_H
