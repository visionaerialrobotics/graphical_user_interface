/*!*******************************************************************************************
 *  \file       behavior_tree_editor.h
 *  \brief      BehaviorTreeDesign definition file.
 *  \details    The BehaviorTreeDesign, along with behavior_dialog, behavior_tree and tree_item allows a user to create
 *              missions with a tree structure via the HMI. Allows for importing .yaml file and edit the behavior tree imported.
 *  \author     Abraham Carrera, Daniel Del Olmo
 ********************************************************************************************/
#ifndef BEHAVIOR_TREE_EDITOR_H
#define BEHAVIOR_TREE_EDITOR_H

#include <ros/ros.h>
#include "droneMsgsROS/configurationFolder.h"

#include <QWidget>
#include <QTreeWidget>
#include <QDialog>
#include <QGridLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QLabel>
#include <QCheckBox>
#include <QFileDialog>
#include <QObject>
#include <QString>
#include <QAbstractButton>
#include <QDir>
#include <QMessageBox>

#include <iostream>
#include <thread>
#include <stdio.h>
#include <dirent.h>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <boost/algorithm/string/replace.hpp>
#include "boost/algorithm/string.hpp"

#include "behavior_tree.h"
#include "behavior_tree_editor_view.h"
#include "tree_file_manager.h"
#include "ui_behavior_tree_design.h"
#include "yaml-cpp/yaml.h"

#include <aerostack_msgs/CheckBehaviorFormat.h>
#include <aerostack_msgs/BehaviorCommandPriority.h>

namespace Ui {
class BehaviorTreeDesign;
}

class BehaviorTree;
class BehaviorTreeDesignView;

class BehaviorTreeDesign : public QWidget
{

Q_OBJECT

public:
  explicit BehaviorTreeDesign(QWidget *parent);
  ~BehaviorTreeDesign();

  /*!********************************************************************************************************************
  *  \brief      This method returns the current variables' textbox content
  **********************************************************************************************************************/
  std::string getText();

  /*!********************************************************************************************************************
  *  \brief      This method copies the given text to the private variable 'text' which conforms the variables' textbox content
  **********************************************************************************************************************/
  void setText(std::string texto);

  /*!********************************************************************************************************************
  *  \brief      This method is executed when the user wants to load a tree from a file.
  **********************************************************************************************************************/
  void loadTreeFile(std::string tree_mission);

  std::string getFileRoute();

private:
  Ui::BehaviorTreeDesign * ui;

  ros::NodeHandle n;
  ros::ServiceClient configuration_folder_client;
  ros::ServiceClient check_behavior_format_srv;
  droneMsgsROS::configurationFolder::Request req;
  droneMsgsROS::configurationFolder::Response res;
  aerostack_msgs::CheckBehaviorFormat check_format_msg;
  aerostack_msgs::CheckBehaviorFormat::Request check_format_msg_req;
  aerostack_msgs::CheckBehaviorFormat::Response check_format_msg_res;
  aerostack_msgs::BehaviorCommandPriority behavior_msg;

  BehaviorTree * visualized_tree;
  BehaviorTree * tree;
  BehaviorTreeDesignView * my_parent;

  TreeItem * original_tree_copy;
  TreeItem * root_node;
  
  QLabel * tree_label;
  QLabel * beliefs_label;
  QTextEdit * beliefs_text;
  QString text;
  QWidget * variables_widget;
  QCheckBox * expand_text_button;
  QPushButton * export_button;
  QPushButton * import_button;
  QPushButton * accept_button;
  QPushButton * new_button;
  QPushButton * cancel_button;
  QPushButton * save_button;
  QMessageBox cancel_message_window;
  QString filename;
  QString tree_mission;
  QMessageBox * msg_error;
  QAbstractButton * m_save_button;
  QAbstractButton * m_cancel_button;
  QAbstractButton * m_dont_save_button;

  std::string filestd;
  std::fstream aux_file;
  std::string configuration_folder;
  std::string configuration_folder_service;
  std::string homePath;
  std::string file_route;
  std::string check_behavior_format;
  std::string drone_id_namespace;
  std::string error_children;
  std::string error_behavior;
  std::string error_behavior_type;
  std::string type_message;
  std::stringstream list_of_errors;
  std::string node_type_name;
  std::string wrong_children;  
  std::string my_stack_directory;

  bool editing;
  bool importing;
  bool correctCheck;

  /*!********************************************************************************************************************
  *  \brief      This method returns the BehaviorTree
  **********************************************************************************************************************/
  BehaviorTree* getBehaviorTree();

  /*!********************************************************************************************************************
  *  \brief      This method is the responsible for seting up connections.
  *********************************************************************************************************************/
  void setUp();

  void setFileRoute();

  void windowManager(char type, std::string title, std::string message);

  /*!********************************************************************************************************************
   *  \brief   This slot checks if a tree is correctly defined
   **********************************************************************************************************************/
  bool checkTree(TreeItem *item);

  std::string processData(std::string raw_arguments);

  /*!********************************************************************************************************************
   *  \brief   This slot checks if an item is correctly defined
   **********************************************************************************************************************/
  bool checkItem(TreeItem *item);

public Q_SLOTS:

  /*!********************************************************************************************************************
  *  \brief      This method let's the user select which behavior tree mission he wants to execute
  *********************************************************************************************************************/
  void importBehaviorTreeMission();

  /*!********************************************************************************************************************
  *  \brief      This slot is executed when the user wants stop and save editing the tree.
  **********************************************************************************************************************/
  void acceptTreeEdition();

  /*!********************************************************************************************************************
  *  \brief      This slot is executed when the user wants save editing the tree.
  **********************************************************************************************************************/
  void saveTreeEdition();

  /*!********************************************************************************************************************
  *  \brief      This slot is executed when the user wants to save the tree to a file.
  **********************************************************************************************************************/
  void exportBehaviorTreeMission();

  /*!********************************************************************************************************************
  *  \brief      This slot is executed when the user cancels the editing of a tree.
  **********************************************************************************************************************/
  void cancelTreeEdition();

  /*!********************************************************************************************************************
  *  \brief      This method helps the processQueryData method by processing the argument data differently depending on which type it is.
  **********************************************************************************************************************/
  std::string processType(YAML::const_iterator it);

Q_SIGNALS:

  /*!********************************************************************************************************************
  *  \brief      This signal is emitted when the user starts the execution of the tree.
  **********************************************************************************************************************/
  void executeTreeSignal();

  /*!********************************************************************************************************************
  *  \brief      This signal is emitted when the user cancels the execution of the tree.
  **********************************************************************************************************************/
  void cancelTreeSignal();

  /*!********************************************************************************************************************
  *  \brief      This signal is emitted when the user clicks the cancel or accept button within the edition mode.
  **********************************************************************************************************************/
  void closeWindowSignal();
};

#endif
