/*!*******************************************************************************************
 *  \file       behavior_tree.h
 *  \brief      BehaviorTree definition file.
 *  \details    The BehaviorTree, along with behavior_dialog, behavior_tree_editor and tree_item allows a user to create
 *							missions with a tree structure via the HMI.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef BEHAVIOR_TREE_H
#define BEHAVIOR_TREE_H

#include <QTreeWidget>
#include <QWidget>
#include <QTreeWidgetItem>
#include <QMouseEvent>
#include <QString>
#include <QPoint>
#include <QMenu>
#include <QIcon>
#include <QPixmap>
#include <QBrush>
#include <QColor>
#include <QTextEdit>
#include <QCloseEvent>
#include <QMessageBox>
#include <QErrorMessage>
#include <QApplication>

#include <iostream>

#include "behavior_tree_editor.h"
#include "behavior_dialog.h"
#include "mission_specification_dialog.h"
#include "yaml-cpp/yaml.h"

class TreeItem;
class BehaviorTreeDesign;
class EnvironmentViewerConnection;

class BehaviorTree : public QTreeWidget
{
    Q_OBJECT
public:
    explicit BehaviorTree(QWidget *parent = 0);
    ~BehaviorTree();

    /*!********************************************************************************************************************
  *  \brief      This method adds a top level item to the tree
  **********************************************************************************************************************/
    void addTopLevelItem(TreeItem *);

    /*!***************************BehaviorTree *visualized_tree;*****************************************************************************************
  *  \brief     This method gets the Tree design
  *  \return		Returns the BehaviorTreeDesign
  **********************************************************************************************************************/
    BehaviorTreeDesign* getVisualizer();

    /*!********************************************************************************************************************
  *  \brief 		 This method gets the tree's root
  *  \return     Returns the root TreeItem
  **********************************************************************************************************************/
    TreeItem * getRoot();

    /*!********************************************************************************************************************
  *  \brief 		 This method makes a security copy of the Behavior Tree
  *  \return 		 Returns the root of the copied tree
  **********************************************************************************************************************/
    TreeItem * copyBehaviorTree(TreeItem * item, bool root, TreeItem * inherited_parent);

protected:

    /*!********************************************************************************************************************
   *  \brief      This method expands all the tree's nodes text
   **********************************************************************************************************************/
    void expandText(TreeItem *item);

    /*!********************************************************************************************************************
   *  \brief      This method minimizes all the tree's nodes text
   **********************************************************************************************************************/
    void minimizeText(TreeItem *item);

private:
    TreeItem *root_item;
    TreeItem *parent_item_for_child;
    TreeItem *parent_item_for_sibling;
    TreeItem *parent_item_for_modify;
    TreeItem * root_before;
    BehaviorDialog *window;
    BehaviorTreeDesign * visualizer_parent;
    MissionSpecificationDialog *mission_dialog;

    QMenu *contextMenu;
    QPoint point;
    QMessageBox error_message;

    std::thread * executing_tree;
    bool is_menu_created = false;
    bool isTextExpanded;
    bool has_root;

    /*!********************************************************************************************************************
    *  \brief      This method is the responsible for seting up connections.
    *********************************************************************************************************************/
    void setUp();

public Q_SLOTS:
    /*!********************************************************************************************************************
   *  \brief  This slot is executed when the user right clicks anywhere inside the tree's widget.
              It creates the context menu and disables any functionality that cannot be executed. 
              This depends on where we right clicked
   **********************************************************************************************************************/
    void onCustomContextMenu(const QPoint &);

    /*!********************************************************************************************************************
   *  \brief   This slot is executed when a mission is created.
   **********************************************************************************************************************/
    void createMission(const QString &);

    /*!********************************************************************************************************************
   *  \brief   This slot is executed when a mission is created with a given root node.
   **********************************************************************************************************************/
    void createMissionByTreeItem(TreeItem * root);

    /*!********************************************************************************************************************
   *  \brief   This slot is executed when the BehaviorDialog window is closed.
   **********************************************************************************************************************/
    void windowFinished(TreeItem *);

    /*!********************************************************************************************************************
   *  \brief   This slot is executed when the user wants to remove a certain node.
   **********************************************************************************************************************/
    void removeItemWidgetAt(const QPoint &);

    /*!********************************************************************************************************************
   *  \brief   Auxiliary slot
   **********************************************************************************************************************/
    void removeItemWidgetAtAux();

    /*!********************************************************************************************************************
   *  \brief   Auxiliary slot
   **********************************************************************************************************************/
    void addChildAux();

    /*!********************************************************************************************************************
   *  \brief   Auxiliary slot
   **********************************************************************************************************************/
    void addSiblingAux();

    /*!********************************************************************************************************************
   *  \brief   This slot is executed when the user wants to add a child node
   **********************************************************************************************************************/
    void addChild(const QPoint &);

    /*!********************************************************************************************************************
   *  \brief   This slot is executed when the user wants to add a sibling node
   **********************************************************************************************************************/
    void addSibling(const QPoint &);

    /*!********************************************************************************************************************
   *  \brief   Auxiliary slot
   **********************************************************************************************************************/
    void modifyItemWidgetAux();

    /*!********************************************************************************************************************
   *  \brief   This slot is executed when the user wants to modify a node's data
   **********************************************************************************************************************/
    void modifyItemWidget(const QPoint &);

    /*!********************************************************************************************************************
   *  \brief   This slot expands or minimizes the nodes' text
   **********************************************************************************************************************/
    void expandTreeText(int);

    /*!********************************************************************************************************************
  *  \brief   This slot pops up the mission creation dialog
  **********************************************************************************************************************/
    void createMissionDialog();

    /*!********************************************************************************************************************
  *  \brief   refresh the behavior tree_item
  **********************************************************************************************************************/
    void refreshTree();

Q_SIGNALS:

    /*!********************************************************************************************************************
   *  \brief   Emitted when a node is removed
   **********************************************************************************************************************/
    void executeRemoveItemAction(const QPoint &);

    /*!********************************************************************************************************************
   *  \brief   Emitted when a child node is added
   **********************************************************************************************************************/
    void executeAddChildAction(const QPoint &);

    /*!********************************************************************************************************************
   *  \brief   Emitted when a sibling node is added
   **********************************************************************************************************************/
    void executeAddSiblingAction(const QPoint &);

    /*!********************************************************************************************************************
   *  \brief   Emitted when a node is being modified
   **********************************************************************************************************************/
    void executeModifyItemWidgetAction(const QPoint &);

};

#endif //BEHAVIOR_TREE
