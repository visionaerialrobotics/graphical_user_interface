#include <gtest/gtest.h>
#include <QMessageBox>
#include <ros/ros.h>
#include "behavior_tree_editor_view.h"
#include <cstdio>
#include <QApplication>


#include <iostream>



QApplication* app;
BehaviorTreeDesignView* w;
int ar;
bool finished=false;
int total_subtests=0;
int passed_subtests=0;

void spinnerThread(){
    while(!finished){
        ros::spinOnce();
    }

}

void displaySubtestResults(){
    std::cout << "\033[1;33m TOTAL SUBTESTS: "<<total_subtests<<"\033[0m"<<std::endl;
    std::cout << "\033[1;33m SUBTESTS PASSED: "<<passed_subtests<<"\033[0m"<<std::endl;
    std::cout << "\033[1;33m % PASSED: "<<(passed_subtests*100.0)/(total_subtests*1.0)<<"\033[0m"<<std::endl;

}

void test(){
    total_subtests++;
    std::string response;
    std::cout << "\033[1;34m Does a Widget called Behavior Tree Mission Design appear in the left half o the screen (centered in vertical axis and with its righ side in the middle of the screen)? (Y/N) \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in position or appearance of the widget\033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }

    total_subtests++;
    std::cout << "\033[1;34m Click on the 'New' button. Does 'Create a New Mission' window asking for a mission name appear? (Y/N)\033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in 'Create a New Mission' window  creation\033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Click on the 'Cancel' button. Does the 'Create a New Mission' window disappear? (Y/N)\033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in 'Cancel' button in 'Create a New Mission' window\033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Click on 'New' button and press 'Accept'. Does a 'No parameters error' dialog appear? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Not displaying alert when trying to add empty name mission \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m  Insert 'mission1' as mission name and click 'Accept' button. Does a new mission tree (only root node with mission name) appear in the 'Behavior Tree Mission Design' window, 'Behavior Tree' section? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in the creation of a new tree\033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }

    total_subtests++;

    std::cout << "\033[1;34m Right click on the root node. Does a context dialog with options 'Add child' and 'Delete node' appear? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in the creation of the context dialog for the root node\033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Click on 'Add child' in the context dialog. Does a 'Node Editor' dialog appear? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in the creation of the 'Node Editor' dialog \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Click on the 'Cancel' button. Does the 'Node Editor' window disappear? (Y/N)\033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in 'Cancel' button in  'Node Editor' window\033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Click again on 'Add child' in the context dialog and press 'Accept' button. Does a 'No parameters error' dialog appear? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Not displaying alert when trying to add empty name node \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;

    std::cout << "\033[1;34m Insert 'color(red)' in Node name section. Does a node descending from 'mission1' with name 'color(red)' appear in Behavior Tree section? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem creating new belief nodes \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Click again on 'Add child' in the context dialog, select 'BEHAVIOR' in node type, 'EXECUTE BEHAVIOR' in mode and 'TAKE_OFF' in behavior. Insert 'Take off' as node name. Does a node descending from 'mission1' with name 'Take off' appear in Behavior Tree section? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Not displaying alert when trying to add empty name node \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Right click on the 'take off' node. Does a context dialog with options 'Add sibling', 'Delete this node' and 'Modify this node' appear? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in the creation of the context dialog for the tree nodes\033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Click  on 'Add sibling' in the context dialog, select 'BEHAVIOR' in node type, 'EXECUTE BEHAVIOR' in mode an 'GO_TO_POINT' in behavior. Insert 'Go to A' as node name and 'coordinates: [2, 2, 0.5]' as parameters and press 'Accept'. Does a node descending from 'mission1' with name 'Go to A' appear in Behavior Tree section? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem creating nodes with parameters \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Right click on the 'go to A' node and 'Modify this node'. Does a 'Node Editor' appear with the parameters previously introduced? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in the creation of the Node Editor when modifying node\033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Change node name to 'Go to point A' and press accept. Has the node name changed in 'Behavior Tree' section? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in modifying nodes\033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }

    total_subtests++;
    std::cout << "\033[1;34m Right click again on root node and left click on 'Add child' in the context dialog. Select Node type 'SEQUENCE', insert 'Secuence 1' as node name and press accept. Does a a new node called 'Secuence 1' with the hability to be a parent (yellow arrow icon) appear? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in creating SEQUENCE nodes \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;

    std::cout << "\033[1;34m Right click  on  node 'Secuence 1'. Does a context 'Menu with options 'Add child', 'Add sibbling', 'Delete this node' and 'Modify this node' appear? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in creating SEQUENCE nodes \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Click on 'Add Child'. Select Node type BEHAVIOR, mode EXECUTE BEHAVIOR and behavior LAND. Insert node name 'Land'. Does a node descending from 'sequence 1' with name 'land' appear in Behavior Tree section? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in creating child nodes of  SEQUENCE nodes \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Right click  on  node 'Secuence 1' and left click on Delete this node. Does 'Sequence1' and its offspring disapears from Behavior Tree? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in  deleting  nodes \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }

    total_subtests++;
    std::cout << "\033[1;34m Click on check box 'Colapse text'. Does the text between brackets in each node desapear? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in  collapsing text \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }

    total_subtests++;
    std::cout << "\033[1;34m Click on 'Export' button. Does a dialog asking for a directory and file name to save the tree appear? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in  export dialog \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }

    total_subtests++;
    std::cout << "\033[1;34m Enter the file name and the directory and press 'Save' button. Is there a new file .yaml in the selected folder with the selected name? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in exporting behavior tree \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }

    total_subtests++;
    std::cout << "\033[1;34m Right click in the root node and select the option 'Delete this node' in the context dialog'. Now press 'Import' button, select the previously exported file and click 'Accept'.Does the tree created before appear again after deleting and importing it? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in importing behavior tree \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }

    total_subtests++;
    std::cout << "\033[1;34m Click on the button 'Cancel'. Does an alert dialog appear asking us to save the tree before leaving? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in creating cancelation alert dialog \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }

    total_subtests++;
    std::cout << "\033[1;34m Click on the button 'Cancel'. Does (only) the alert dialog disappear? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in canceling behavior tree cancelation \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }
    total_subtests++;
    std::cout << "\033[1;34m Click on the 'Accept' button (or 'Save' option in canceling dialog). Does the widget disappear and does the file $AEROSTACK_STACK/configs/drone1 have your tree writen? \033[0m"<<std::endl;
    getline(std::cin, response);
    EXPECT_TRUE(response=="Y" ||response=="y");
    if(response!="Y" && response!="y"){
        std::cout << "\033[1;31m Problem in accepting behavior tree \033[0m\n"<<std::endl;
    }
    else{
        passed_subtests++;
    }

    displaySubtestResults();

    app->exit();
    finished=true;

}


TEST(GraphicalUserInterfaceTests, behaviorDesignTest)
{

    std::thread thr(&test);
    app->exec();
    thr.join();
}



int main(int argc, char** argv){
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, ros::this_node::getName());

    system("xfce4-terminal  \--tab --title \"Behavior Coordinator\"  --command \"bash -c 'roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
           my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &");
    ar=argc;
    app= new QApplication (argc, nullptr);
    w= new BehaviorTreeDesignView (argc, nullptr);
    w->show();

    std::thread thr(&spinnerThread);

    return RUN_ALL_TESTS();
    thr.join();


}
