/*!*******************************************************************************************
 *  \file       object_controller.h
 *  \brief      ObjectController definition file.
 *  \details    In charge of setting, drawing and modifying map items.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef OBJECT_CONTROLLER_H
#define OBJECT_CONTROLLER_H

#include <iostream>
#include <QWidget>
#include <QGraphicsView>
#include <QObject>
#include <QGridLayout>
#include <QWheelEvent>
#include <QGraphicsTextItem>
#include <QApplication>
#include <QString>
#include <boost/format.hpp>
#include <QGraphicsEllipseItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneDragDropEvent>
#include <cmath>
#include <QSizeF>
#include <QTextDocument>
#include <time.h>

#define PI 3.14159265

class ObjectController : public QGraphicsView
{
  Q_OBJECT

public:
  explicit ObjectController(QWidget* parent = 0);
  ~ObjectController();
  void setMap(double x_meters, double y_meters, double x_init = 0, double y_init = 0);
  bool isMapSet();
  void clearTrajectories();

  struct Point
  {
    double x;
    double y;
  };

  struct Map
  {
    bool set = false;
    double x_meters;
    double y_meters;
    double x_init = 0;
    double y_init = 0;
  } map;

  struct Drone
  {
    int id;
    double x_size;
    double y_size;
    double x_coor;
    double y_coor;
    double degrees;
    double take_off;
    bool selected = false;
    QGraphicsPixmapItem* item;
    QGraphicsLineItem* arrow;
    std::vector<Point> points;
  };

  struct Wall
  {
    int id;
    double x_size;
    double y_size;
    double x_coor;
    double y_coor;
    double degrees;
    std::string description;
    std::string virtual_description;
    QGraphicsRectItem* item;
    bool selected = false;
  };

  struct Aruco
  {
    int id;
    double x_coor;
    double y_coor;
    double z_coor;
    double degrees;
  };

  struct Pole
  {
    int id;
    double x_radius;
    double y_radius;
    double x_size;
    double y_size;
    double x_coor;
    double y_coor;
    Aruco aruco[4];
    bool selected = false;
    QGraphicsPixmapItem* item;
  };

  struct Landmark
  {
    int id;
    double x_size;
    double y_size;
    double x_coor;
    double y_coor;
    double degrees;
    Aruco aruco[2];
    bool selected = false;
    QGraphicsPixmapItem* item;
  };

  struct Objects
  {
    std::vector<Drone> drones;
    std::vector<Wall> walls;
    std::vector<Pole> poles;
    std::vector<Landmark> landmarks;
    std::vector<std::vector<Point>> trajectories;
    Map map;
  };

private:
  int x_center;
  int y_center;
  bool drag = false;
  bool click = false;
  clock_t time;
  int x_drag;
  int y_drag;

  bool sticky = false;

  Objects objects;
  Objects previous_status;

  double pix_per_meter = 100;
  int pix_guide;

  // Drone default values
  double default_drone_x_size = 0.6;
  double default_drone_y_size = 0.6;
  double default_drone_degrees = 90;  //+Y orientation
  double default_take_off_value = 0.7;
  double default_real_drone_percent = 0.8;

  int default_drone_id = 1;
  int default_wall_id = 10000;
  int default_pole_id = 40000;
  int default_landmark_id = 60000;
  int default_aruco_id = 10;

  double default_aruco_z = 1.3;

  QGraphicsScene* scene;
  QGraphicsTextItem* coordinates;
  QPen pencil;
  QBrush brush;
  QGraphicsEllipseItem* sticky_ellipse;

  bool delete_on_unselect = false;

  bool mission_mode = false;

  double coor_x;
  double coor_y;

  /*!********************************************************************************************************************
   *  \brief      This method refreshes the graphical representation of the map by erasing it and drawing it.
   **********************************************************************************************************************/
  void reDraw();

  /*!********************************************************************************************************************
   *  \brief      This method draws the map grids
   **********************************************************************************************************************/
  void drawGuide();

  /*!********************************************************************************************************************
   *  \brief      This method draws the outer limits of the map
   **********************************************************************************************************************/
  void drawMap();

  /*!********************************************************************************************************************
   *  \brief      This method makes calls to all the methods that draw the different types of objects.
   **********************************************************************************************************************/
  void drawObjects();

  /*!********************************************************************************************************************
   *  \brief      This method draws the drone or drones on the map
   **********************************************************************************************************************/
  void drawDrone(int i);

  /*!********************************************************************************************************************
   *  \brief      This method draws the walls on the map
   **********************************************************************************************************************/
  void drawWall(int i);

  /*!********************************************************************************************************************
   *  \brief      This method draws the poles on the map
   **********************************************************************************************************************/
  void drawPole(int i);

  /*!********************************************************************************************************************
   *  \brief      This method draws the landmarks on the map
   **********************************************************************************************************************/
  void drawLandmarks(int i);

  /*!********************************************************************************************************************
   *  \brief      This method draws the
   **********************************************************************************************************************/
  void drawPoints();

  /*!********************************************************************************************************************
   *  \brief      This method draws the trajectories that represent the drone's moves.
   **********************************************************************************************************************/
  void drawTrajectories();

  /*!********************************************************************************************************************
   *  \brief      This method sets the number of pixels that correspond with one meter
   **********************************************************************************************************************/
  void setAutoPixPerMeter();

  /*!********************************************************************************************************************
   *  \brief      This method is used to set the initial four walls that delimitate the boundaries of the map
   **********************************************************************************************************************/
  void setAutoMapWalls();

  /*!********************************************************************************************************************
   *  \brief      This method is used to do some custom conversions to decimal numbers.
   **********************************************************************************************************************/
  double approachNumber(double i);

  /*!********************************************************************************************************************
   *  \brief      This method returns in result a vector with the real coordinates that correspond with the given
   *coordinates.
   **********************************************************************************************************************/
  void toMapCoor(int x, int y, double* result);

  /*!********************************************************************************************************************
   *  \brief      This method returns in result a vector with the virtual coordinates that correspond with the given
   *coordinates.
   **********************************************************************************************************************/
  void toSceneCoor(double x, double y, int* result);

  /*!********************************************************************************************************************
   *  \brief      This is a overwritten Qt event that will force the map to refresh whenever a QResizeEvent happens.
   **********************************************************************************************************************/
  void resizeEvent(QResizeEvent* event);
  /*!********************************************************************************************************************
   *  \brief      This is an overwritten Qt event that allows the user to zoom in or out with the mouse wheel.
   **********************************************************************************************************************/
  void wheelEvent(QWheelEvent* event);
  /*!********************************************************************************************************************
   *  \brief      This method draws the coordinates and places them in the correct position
   **********************************************************************************************************************/
  void drawCoordinates(int x, int y);

  /*!********************************************************************************************************************
   *  \brief      This is an overwritten Qt event that allows the user to move the map by dragging it, or move an object
   *by dragging it aswell
   **********************************************************************************************************************/
  void mouseMoveEvent(QMouseEvent* event);

  /*!********************************************************************************************************************
   *  \brief      This is an overwritten Qt event that links the mouseclick with adding to the map the previously
   *selected object
   **********************************************************************************************************************/
  void mousePressEvent(QMouseEvent* event);

  /*!********************************************************************************************************************
   *  \brief      This is an overwritten Qt event that forces the map to be refreshed whenever the user releases the
   *mouse button
   **********************************************************************************************************************/
  void mouseReleaseEvent(QMouseEvent* event);

  /*!********************************************************************************************************************
   *  \brief      This method adds a new drone to the drone vector
   **********************************************************************************************************************/
  void addNewDrone();

  /*!********************************************************************************************************************
   *  \brief      This method adds a new wall to the walls vector
   **********************************************************************************************************************/
  void addNewWall(std::string description = "wall", double x_size = 0.5, double y_size = 0.5, double x_coor = 0,
                  double y_coor = 0);

  /*!********************************************************************************************************************
   *  \brief      This method adds a new pole to the poles vector
   **********************************************************************************************************************/
  void addNewPole(double x_coor = 0, double y_coor = 0, double x_radius = 0.2, double y_radius = 0.2);

  /*!********************************************************************************************************************
   *  \brief      This method automatically sets the needed characteristics to a pole's arucos
   **********************************************************************************************************************/
  Pole autoSetPoleArucos(Pole pole, int aruco0, int aruco1, int aruco2, int aruco3);

  /*!********************************************************************************************************************
   *  \brief      This method adds a new landmark to the landmarks vector
   **********************************************************************************************************************/
  void addNewLandmark(double x_coor = 0, double y_coor = 0);

  /*!********************************************************************************************************************
   *  \brief      This method automatically sets the needed characteristics to a landmark's arucos
   **********************************************************************************************************************/
  Landmark autoSetLandmarkArucos(Landmark landmark, int aruco0, int aruco1);

public:
  /*!********************************************************************************************************************
   *  \brief      This method returns the list of current objects.
   **********************************************************************************************************************/
  Objects getObjects();

  /*!********************************************************************************************************************
   *  \brief      This method sets the list of objects to the given list and refreshes the map.
   **********************************************************************************************************************/
  void setObjects(Objects objects);

  /*!********************************************************************************************************************
   *  \brief      This method updates the drone's position
   **********************************************************************************************************************/
  void updateDrone(Drone drone, int drone_pos);

  /*!********************************************************************************************************************
   *  \brief      This method updates the drone's trajectory
   **********************************************************************************************************************/
  void updateTrajectory(std::vector<Point> trajectory);

  /*!********************************************************************************************************************
   *  \brief      This method updates the poles
   **********************************************************************************************************************/
  void updateObstacles(Pole pole);

Q_SIGNALS:
};

#endif
