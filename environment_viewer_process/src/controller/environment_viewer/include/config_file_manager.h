/*!*******************************************************************************************
 *  \file       config_file_manager.h
 *  \brief      ConfigFileManager definition file.
 *  \details    The ConfigFileManager translates configuration files and creates intermediate files in order to use them
 *in the map editor. \author     Jorge Luis Pascual, Carlos Valencia. \copyright Copyright 2017 Universidad Politecnica
 *de Madrid (UPM)
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef CONFIG_FILE_MANAGER_H
#define CONFIG_FILE_MANAGER_H

#include <ros/ros.h>
#include <iostream>
#include <QObject>
#include <pugixml.hpp>
#include "object_controller.h"
#include <QMessageBox>
#include "droneMsgsROS/configurationFolder.h"

class ConfigFileManager : public QObject
{
  Q_OBJECT

public:
  explicit ConfigFileManager();
  ~ConfigFileManager();

  /*!********************************************************************************************************************
   *  \brief      This method saves the map objects to file with a given filename.
   **********************************************************************************************************************/
  void saveObjects(std::string filename, ObjectController::Objects objects);

  /*!********************************************************************************************************************
   *  \brief      This method loads the objects described in a given file to the GUI map editor.
   **********************************************************************************************************************/
  ObjectController::Objects loadObjects(std::string filename);

  /*!********************************************************************************************************************
   *  \brief      This method imports all the objects and map characteristics to this process' variables.
   **********************************************************************************************************************/
  ObjectController::Objects importObjects();

  /*!********************************************************************************************************************
   *  \brief      This method exports all the objects described in this process' variables to the ObjectController
   *process using different exporting methods described below.
   **********************************************************************************************************************/
  bool exportObjects(QWidget* object_controller);

private:
  ros::NodeHandle n;
  ObjectController::Objects objects;

  std::string my_stack_directory;
  std::string drone_id_namespace;
  std::string config_dir;
  std::string drone_dir;
  std::string filename;
  std::string configuration_folder;
  std::string configuration_folder_service;
  ros::ServiceClient configuration_folder_client;
  droneMsgsROS::configurationFolder::Request req;
  droneMsgsROS::configurationFolder::Response res;

  /*!********************************************************************************************************************
   *  \brief      This method exports the drone or drones to the ObjectController
   **********************************************************************************************************************/
  bool exportDrone(int i);

  /*!********************************************************************************************************************
   *  \brief      This method exports the map characteristics to the ObjectController
   **********************************************************************************************************************/
  void exportMapData();

  /*!********************************************************************************************************************
   *  \brief      This method exports the drone or drones' characteristics to the ObjectController
   **********************************************************************************************************************/
  void exportDroneData(int i);

  /*!********************************************************************************************************************
   *  \brief      This method exports the obstacles (walls, poles) to the ObjectController
   **********************************************************************************************************************/
  bool exportObstaclesData();

  /*!********************************************************************************************************************
   *  \brief      This method exports the Arucos to the ObjectController
   **********************************************************************************************************************/
  void exportArucosData();

  /*!********************************************************************************************************************
   *  \brief      This method translates a string to a vector of double type digits
   **********************************************************************************************************************/
  std::vector<double> returnValuesPtr(std::string value);

  /*!********************************************************************************************************************
   *  \brief      This method returns a certain Aruco within the vector of Arucos
   **********************************************************************************************************************/
  ObjectController::Aruco findArucoById(std::vector<ObjectController::Aruco> arucos, int id);

  /*!********************************************************************************************************************
   *  \brief      This method checks if a group of Arucos form together a pole or not
   **********************************************************************************************************************/
  bool isPole(pugi::xml_node node);

  /*!********************************************************************************************************************
   *  \brief      This method creates the Pole object with all its pertinent details
   **********************************************************************************************************************/
  ObjectController::Pole returnCompletedPole(ObjectController::Pole pole);

  /*!********************************************************************************************************************
   *  \brief      This method creates the Landmark object with all its pertinent details
   **********************************************************************************************************************/
  ObjectController::Landmark returnCompletedLandmark(ObjectController::Landmark land);

  /*!********************************************************************************************************************
   *  \brief      This method is a customized fmod(double x, double y). It uses the fmod(double x, double y) function.
   **********************************************************************************************************************/
  double mod(double a, double b);
};

#endif
