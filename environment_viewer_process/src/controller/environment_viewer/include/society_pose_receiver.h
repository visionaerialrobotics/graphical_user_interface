/*!******************************************************************************************* 
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef SOCIETY_RECEIVER_H
#define SOCIETY_RECEIVER_H

#include <ros/ros.h>
#include <droneMsgsROS/societyPose.h>
#include <droneMsgsROS/droneInfo.h>
#include <QObject>

class SocietyPoseReceiver : public QObject
{
  Q_OBJECT

public:
  SocietyPoseReceiver();
  ~SocietyPoseReceiver();

  /*!************************************************************************
   *  \brief   This method gets the drone's position.
   *  \return  Returns the drone's position.
   ***************************************************************************/
  droneMsgsROS::societyPose::_societyDrone_type getDronesPosition();

  /*!************************************************************************
   *  \brief   This method opens the subscriptions that this process needs.
   ***************************************************************************/
  void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);

private:
  ros::Subscriber drones_position_sub;
  std::string drones_position_topic;
  droneMsgsROS::societyPose message;
  droneMsgsROS::societyPose::_societyDrone_type positions;

  /*!************************************************************************
   *  \brief   This process receives the drone's position data and refreshes it.
   ***************************************************************************/
  void dronesPositionCallback(const droneMsgsROS::societyPose::ConstPtr& msg);

Q_SIGNALS:
  void poseReceived();
};

#endif  // SOCIETY_RECEIVER
