/*!************************************************************************************
 *  \file      trayectory_abs_ref_command_receiver.h
 *  \brief     Get the real trajectory of the drone.
 *  \details   This file includes the headers of the class.
 *
 *  \author    Alberto Camporredondo
 *  \version   1.0
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef TRAJECTORY_ABS_REF_COMMAND_RECEIVER_H
#define TRAJECTORY_ABS_REF_COMMAND_RECEIVER_H

#include <ros/ros.h>
#include <QObject>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>

class TrajectoryAbsRefCommandReceiver : public QObject
{
  Q_OBJECT

public:
  TrajectoryAbsRefCommandReceiver();
  ~TrajectoryAbsRefCommandReceiver();

  /*!************************************************************************
   *  \brief   This method opens the subscriptions needed by the process.
   ***************************************************************************/
  void openSubscriptions(ros::NodeHandle n, std::string ros_namespace);

  /*!************************************************************************
   *  \brief   This method checks if the subscriptions are successfully opened
   *  \return  Returns true if they're succesfully opened, false otherwise
   ***************************************************************************/
  bool isReady();

  /*!************************************************************************
   *  \brief   This method returns the trajectory vector received by the message
   *  \return  Trajectory vector
   ***************************************************************************/
  droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type getTrajectoryVector();

private:
  bool subscriptions_complete;
  std::string trajectory_topic;
  std::string new_trajectory_topic;  
  ros::Subscriber obstacle_sub;
  ros::Subscriber new_obstacle_sub;
  droneMsgsROS::dronePositionTrajectoryRefCommand trajectory_msg;
  droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type drone_trajectory_vector;
  droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type::value_type new_point;
  nav_msgs::Path new_trajectory;
  /*!************************************************************************
   *  \brief   This method emit a SIGNAL when a droneMsgsROS::dronePositionTrajectoryRefCommand is received
   ***************************************************************************/
  void trajectoryCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr& msg);
  void newTrajectoryCallback(const nav_msgs::Path::ConstPtr& msg);

Q_SIGNALS:
  void sendTrajectory();
};

#endif  // TRAJECTORY_ABS_REF_COMMAND_RECEIVER_H
