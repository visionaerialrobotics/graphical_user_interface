/*!*******************************************************************************************
 *  \file       received_data_processor.h
 *  \brief      ReceivedDataProcessor defintion file.
 *  \details    This file process new parameters in order to modify the environment.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef RECEIVED_DATA_H
#define RECEIVED_DATA_H

#include <ros/ros.h>
#include <Eigen/Geometry>
/*Messages*/
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "geometry_msgs/PoseStamped.h"

#include <angles/angles.h>
#include <tf/tf.h>
#include <iostream>
#include <QObject>

#include "odometry_state_receiver.h"
#include "mission_state_receiver.h"
#include "obstacles_receiver.h"
#include "trajectory_abs_ref_command_receiver.h"
#include "society_pose_receiver.h"
#include "object_controller.h"

class EnvironmentViewerConnection;

class ReceivedDataProcessor : public QObject
{
  Q_OBJECT

public:
  explicit ReceivedDataProcessor(ObjectController* object_controller = 0, OdometryStateReceiver* odometry_receiver = 0,
                                 ObstaclesReceiver* obstacles_receiver = 0, MissionStateReceiver* mission_receiver = 0,
                                 TrajectoryAbsRefCommandReceiver* command_receiver = 0,
                                 SocietyPoseReceiver* society_receiver = 0);
  ~ReceivedDataProcessor();
  void toEulerAngle(const Eigen::Quaternionf& q, float& roll, float& pitch, float& yaw);

private:
  OdometryStateReceiver* odometry_receiver;
  ObstaclesReceiver* obstacles_receiver;
  MissionStateReceiver* mission_receiver;
  TrajectoryAbsRefCommandReceiver* command_receiver;
  SocietyPoseReceiver* society_receiver;
  ObjectController* object_controller;

  std::string drone_id_namespace;
  ros::NodeHandle n;
  double precision = 0.05;

  float roll, pitch, yaw;

public Q_SLOTS:
  /*!********************************************************************************************************************
   *  \brief      This slot is executed when new drone's data is received
   **********************************************************************************************************************/
  void updateDrone();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when new obstacle's data is received
   **********************************************************************************************************************/
  void updateObstacles();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when new trajectories' data is received
   **********************************************************************************************************************/
  void updateTrajectory();

  /*!********************************************************************************************************************
   *  \brief      This slot updates the trajectories
   **********************************************************************************************************************/
  void updateDronesTrajectories();

  /*!************************************************************************
    *  \brief  Connects the different topic receiver objects to the ReceivedDataProcessor object
    *  \param The parameter objects are as follow: OdometryStateReceiver, ObstaclesReceiver,
             TrajectoryAbsRefCommandReceiver, SocietyPoseReceiver and MissionStateReceiver
    *  \use   This function is used when a ReceivedDataProcessor object is created
    *  \return --
  ***************************************************************************/
  void connectReceiversToReceivedDataProcessor(QObject* odometry_receiver, QObject* obstacles_receiver,
                                               QObject* command_receiver, QObject* society_receiver,
                                               QObject* mission_receiver);
};

#endif  // RECEIVED_DATA_H
