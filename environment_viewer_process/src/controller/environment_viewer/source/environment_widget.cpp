/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  EnvironmentWidget
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/environment_widget.h"
#include <QProcess>
#include <iostream>

EnvironmentWidget::EnvironmentWidget(int argc, char** argv, QWidget* parent)
  : QWidget(parent), ui(new Ui::EnvironmentWidget)
{
  setWindowIcon(QIcon(":/images/images/environment_viewer.png"));
  setWindowTitle("Environment Viewer");

  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);

  ros::start();
  ros::NodeHandle n;

  if (ros::this_node::getNamespace().compare("/") == 0)
    rosnamespace.append("/drone1");  // default namespace
  else
    rosnamespace.append(ros::this_node::getNamespace());

  odometry_receiver = new OdometryStateReceiver();
  mission_planner_receiver = new MissionStateReceiver();
  obstacle_receiver = new ObstaclesReceiver();
  trajectory_abs_ref_commander_receiver = new TrajectoryAbsRefCommandReceiver();
  society_pose_receiver = new SocietyPoseReceiver();

  odometry_receiver->openSubscriptions(n, rosnamespace);
  mission_planner_receiver->openSubscriptions(n, rosnamespace);
  obstacle_receiver->openSubscriptions(n, rosnamespace);
  trajectory_abs_ref_commander_receiver->openSubscriptions(n, rosnamespace);
  society_pose_receiver->openSubscriptions(n, rosnamespace);

  std::locale::global(std::locale::classic());
  ui->setupUi(this);
  n.param<std::string>("my_stack_workspace", my_stack_workspace, "~/workspace/ros/aerostack_catkin_ws");
  layout = ui->gridLayout;

  object_controller = new ObjectController(this);
  layout->addWidget(object_controller, 1, 0, 1, 7);

  config_file_manager = new ConfigFileManager();
  received_data_processor =
      new ReceivedDataProcessor(object_controller, odometry_receiver, obstacle_receiver, mission_planner_receiver,
                                trajectory_abs_ref_commander_receiver, society_pose_receiver);
  clear_button = new QPushButton("Clear");

  layout->addWidget(clear_button, 3, 6, 1, 1);

  QObject::connect(clear_button, SIGNAL(clicked()), this, SLOT(clearTrajectories()));

  n.param<std::string>("prefixed_layout", prefixed_layout, "normal");
  importMap();
  namespace pt = boost::property_tree;

  std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                        "graphical_user_interface/layouts/layout.json");

  pt::read_json(layout_dir, root);

  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;

  int height = root.get<int>("3.height");
  int width = root.get<int>("3.width");

  this->resize(width, height);
  this->move(x0 + root.get<int>("3.position." + prefixed_layout + ".x"),
             y0 + root.get<int>("3.position." + prefixed_layout + ".y"));
  setUp();
}

EnvironmentWidget::~EnvironmentWidget()
{
  delete ui;
  delete object_controller;
  delete layout;
  delete config_file_manager;
}

void EnvironmentWidget::setUp()
{
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");

  n.param<std::string>("window_opened", window_opened_topic, "window_opened");
  n.param<std::string>("window_closed", window_closed_topic, "window_closed");

  // Subscribers
  window_opened_subs = n.subscribe("/" + drone_id_namespace + "/" + window_opened_topic, 10,
                                   &EnvironmentWidget::windowOpenCallback, this);

  // Publishers
  window_closed_publ =
      n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace + "/" + window_closed_topic, 1, true);
}

void EnvironmentWidget::importMap()
{
  ObjectController::Objects objects;

  objects = config_file_manager->importObjects();

  object_controller->setObjects(objects);
}

void EnvironmentWidget::clearTrajectories()
{
  object_controller->clearTrajectories();
}

void EnvironmentWidget::closeEvent(QCloseEvent* event)
{
  windowClosedMsgs.id = aerostack_msgs::WindowIdentifier::ENVIRONMENT_VIEWER;
  window_closed_publ.publish(windowClosedMsgs);
}

void EnvironmentWidget::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void EnvironmentWidget::windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg)
{
  windowOpenedMsgs = msg;

  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_MISSION_INTERPRETER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_PYTHON ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_TML)
  {
    windowClosedMsgs.id = aerostack_msgs::WindowIdentifier::ENVIRONMENT_VIEWER;
    window_closed_publ.publish(windowClosedMsgs);
    killMe();
  }

  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::MINIMIZE_MAIN_WINDOW)
    showMinimized();

  namespace pt = boost::property_tree;

  std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                        "graphical_user_interface/layouts/layout.json");

  pt::read_json(layout_dir, root);

  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;
  int height = root.get<int>("3.height");
  int width = root.get<int>("3.width");

  if (windowOpenedMsgs.id == 4 || windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::KEYBOARD_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::TML_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_CONTROL)
    this->move(x0 + root.get<int>("3.position.center.x"), y0 + root.get<int>("3.position.center.y"));

  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::ALPHANUMERIC_INTERFACE_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN)
    this->move(x0 + root.get<int>("3.position.right.x"), y0 + root.get<int>("3.position.right.y"));
}
