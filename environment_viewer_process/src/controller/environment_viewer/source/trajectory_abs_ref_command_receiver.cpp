/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*!************************************************************************************
 *  \file      trayectory_abs_ref_command_receiver.cpp
 *  \brief     Get the real trajectory of the drone.
 *  \details   This file includes the implementation of the class.
 *
 *  \author    Alberto Camporredondo
 *  \version   1.0
 *************************************************************************************/

#include "../include/trajectory_abs_ref_command_receiver.h"

TrajectoryAbsRefCommandReceiver::TrajectoryAbsRefCommandReceiver()
{
  subscriptions_complete = false;
}

TrajectoryAbsRefCommandReceiver::~TrajectoryAbsRefCommandReceiver()
{
}

/*!********************************************************************
 *  \brief      This method allows the process to subscribe to a topic
 *  \param      ROS NodeHandler
 *  \param      ROS namespace
 **********************************************************************/
void TrajectoryAbsRefCommandReceiver::openSubscriptions(ros::NodeHandle n, std::string ros_namespace)
{
  if (n.getParam("trajectory_topic", trajectory_topic))
    trajectory_topic = "droneTrajectoryAbsRefCommand";
  if (n.getParam("new_trajectory_topic", new_trajectory_topic))
    new_trajectory_topic = "motion_reference/trajectory";

  obstacle_sub = n.subscribe(trajectory_topic, 1, &TrajectoryAbsRefCommandReceiver::trajectoryCallback, this);
  new_obstacle_sub = n.subscribe(new_trajectory_topic, 1, &TrajectoryAbsRefCommandReceiver::newTrajectoryCallback, this);

  subscriptions_complete = true;
}

/*!********************************************************************
 *  \brief      This method allows the process to know if the process
 *              is already subscribe to a topic
 **********************************************************************/
bool TrajectoryAbsRefCommandReceiver::isReady()
{
  return subscriptions_complete;
}

/*!***********************************************************************************************
 *  \brief      This method emit a SIGNAL when a droneMsgsROS::dronePositionTrajectoryRefCommand
 *              is received
 *  \param      droneMsgsROS::obstaclesTwoDim::ConstPtr
 *************************************************************************************************/
void TrajectoryAbsRefCommandReceiver::trajectoryCallback(
    const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr& msg)
{
  trajectory_msg = *msg;
  drone_trajectory_vector = trajectory_msg.droneTrajectory;
  Q_EMIT sendTrajectory();
}

//New trajectory topic
void TrajectoryAbsRefCommandReceiver::newTrajectoryCallback(
    const nav_msgs::Path::ConstPtr& msg)
{
  new_trajectory = *msg;
  drone_trajectory_vector.clear();
  for (int i= 0; i < new_trajectory.poses.size(); i++){
    new_point.x = new_trajectory.poses[i].pose.position.x;
    new_point.y = new_trajectory.poses[i].pose.position.y;
    new_point.z = new_trajectory.poses[i].pose.position.z;
    drone_trajectory_vector.push_back(new_point);
  }
  Q_EMIT sendTrajectory();
}

/*!***********************************************************************************************
 *  \brief      This method returns the trajectory vector received by the message
 *  \param      droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type
 *************************************************************************************************/
droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type
TrajectoryAbsRefCommandReceiver::getTrajectoryVector()
{
  return drone_trajectory_vector;
}
