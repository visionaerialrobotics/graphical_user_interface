/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/mission_state_receiver.h"
#include <sstream>

MissionStateReceiver::MissionStateReceiver()
{
  subscriptions_complete = false;
}

MissionStateReceiver::~MissionStateReceiver()
{
}

void MissionStateReceiver::openSubscriptions(ros::NodeHandle n, std::string rosnamespace)
{
  // Topic communications
  if (!n.getParam("drone_Current_Task_Topic_Name", current_task_topic))
    current_task_topic = "current_task";

  if (!n.getParam("drone_Approved_Action_Topic_Name", current_action_topic))
    current_action_topic = "approved_action";

  if (!n.getParam("drone_Completed_Mission_Topic_Name", completed_mission_topic))
    completed_mission_topic = "completed_mission";

  if (!n.getParam("drone_Completed_Action_Topic_Name", completed_action_topic))
    completed_action_topic = "completed_action";

  if (!n.getParam("public_event_topic", public_event_topic))
    public_event_topic = "/public_event";

  if (!n.getParam("event_topic", event_topic))
    event_topic = "event";

  if (!n.getParam("my_stack_directory", my_stack_directory))
    my_stack_directory = "$(env AEROSTACK_STACK)";

  current_task_subs = n.subscribe(current_task_topic, 1, &MissionStateReceiver::currentTaskCallback, this);
  current_action_subs = n.subscribe(current_action_topic, 1, &MissionStateReceiver::currentActionCallback, this);
  completed_mission_subs =
      n.subscribe(completed_mission_topic, 1, &MissionStateReceiver::completedMissionCallback, this);
  completed_action_subs = n.subscribe(completed_action_topic, 1, &MissionStateReceiver::completedActionCallback, this);
  public_event_sub = n.subscribe(public_event_topic, 1, &MissionStateReceiver::publicEventCallback, this);
  event_sub = n.subscribe(event_topic, 1, &MissionStateReceiver::externalMissionStartedEvent, this);

  subscriptions_complete = true;
  is_autonomous_mode_active = false;

  // Service communications
  this->rosnamespace = rosnamespace;
  mission_name_client = n.serviceClient<droneMsgsROS::missionName>("/" + rosnamespace + "/" + "mission_name");
  start_mission_client =
      n.serviceClient<std_srvs::Empty>("/" + rosnamespace + "/" + "task_based_mission_planner_process/start");
  stop_mission_client =
      n.serviceClient<std_srvs::Empty>("/" + rosnamespace + "/" + "task_based_mission_planner_process/stop");
}

bool MissionStateReceiver::ready()
{
  return subscriptions_complete;
}

void MissionStateReceiver::activateAutonomousMode()
{
  is_autonomous_mode_active = true;
  requestMissionName();
}

void MissionStateReceiver::deactivateAutonomousMode()
{
  is_autonomous_mode_active = false;
}

void MissionStateReceiver::currentActionCallback(const droneMsgsROS::actionData::ConstPtr& msg)
{
  current_action = *msg;
  std::vector<droneMsgsROS::actionArguments> action_arguments;
  // QString arguments;

  if (current_action.ack == true)
  {
    QString accion;  // Action to display
    switch (current_action.mpAction)
    {
      case droneMsgsROS::actionData::TAKE_OFF:
      {
        accion = "TAKE OFF";
        arguments = "";
        break;
      }
      case droneMsgsROS::actionData::HOVER:
      {
        accion = "WAIT";
        action_arguments = current_action.arguments;
        arguments = QString::number(action_arguments.at(0).value.at(0));
        // Send the action arguments in case it is needed to display them.
        Q_EMIT actionArguments(arguments);
        break;
      }
      case droneMsgsROS::actionData::LAND:
      {
        accion = "LAND";
        arguments = "";
        break;
      }
      case droneMsgsROS::actionData::STABILIZE:
      {
        accion = "STABILIZE";
        arguments = "";
        break;
      }
      case droneMsgsROS::actionData::MOVE:
      {
        accion = "MOVE";
        arguments = "";
        break;
      }
      case droneMsgsROS::actionData::GO_TO_POINT:
      {
        accion = "GO TO POINT";
        action_arguments = current_action.arguments;
        arguments = "(" + QString::number(action_arguments.at(0).value.at(0)) + ", " +
                    QString::number(action_arguments.at(0).value.at(1)) + ", " +
                    QString::number(action_arguments.at(0).value.at(2)) + ")";
        Q_EMIT actionArguments(arguments);
        break;
      }
      case droneMsgsROS::actionData::ROTATE_YAW:
      {
        accion = "ROTATE YAW";
        action_arguments = current_action.arguments;
        arguments = "(" + QString::number(action_arguments.at(0).value.at(0)) + ")";
        Q_EMIT actionArguments(arguments);
        break;
      }
      case droneMsgsROS::actionData::FLIP:
      {
        accion = "FLIP";
        arguments = "";
        break;
      }
      case droneMsgsROS::actionData::FLIP_RIGHT:
      {
        accion = "FLIP RIGHT";
        arguments = "";
        break;
      }
      case droneMsgsROS::actionData::FLIP_LEFT:
      {
        accion = "FLIP LEFT";
        arguments = "";
        break;
      }
      case droneMsgsROS::actionData::FLIP_FRONT:
      {
        accion = "FLIP FRONT";
        arguments = "";
        break;
      }
      case droneMsgsROS::actionData::FLIP_BACK:
      {
        accion = "FLIP BACK";
        arguments = "";
        break;
      }
      case droneMsgsROS::actionData::UNKNOWN:
      {
        accion = "UNKNOWN";
        arguments = "";
        break;
      }
    }
    Q_EMIT actionReceived(accion);
  }
}

void MissionStateReceiver::currentTaskCallback(const std_msgs::String::ConstPtr& msg)
{
  task_name = msg->data;
  // Send task received for display.
  Q_EMIT taskReceived(QString::fromStdString(task_name));
}

void MissionStateReceiver::completedMissionCallback(const droneMsgsROS::CompletedMission::ConstPtr& msg)
{
  QString completed_mission = QString::fromStdString(msg->result);
  // Send mission completion status.
  Q_EMIT missionCompleted(completed_mission);
}

void MissionStateReceiver::completedActionCallback(const droneMsgsROS::CompletedAction::ConstPtr& msg)
{
  droneMsgsROS::CompletedAction completed_action = *msg;
  // Send action completion status.
  Q_EMIT actionCompleted(completed_action.final_state, completed_action.timeout);
}

void MissionStateReceiver::publicEventCallback(const droneMsgsROS::PublicEvent::ConstPtr& msg)
{
  droneMsgsROS::PublicEvent event = *msg;

  if (event.event.category == droneMsgsROS::Event::MISSION_STATE_CHANGED)
  {
    std::string t = event.event.arguments[0];
    if (t == "STARTED")
    {
      if (active_drones.size() == 0)
      {
        Q_EMIT(commonMissionStarted());
      }
      active_drones.push_back(event.author);
    }
    else
    {
      for (int i = 0; i < active_drones.size(); i++)
      {
        if (event.author == active_drones[i])
        {
          active_drones.erase(active_drones.begin() + i);
          if (active_drones.size() == 0)
          {
            Q_EMIT(commonMissionCompleted());
          }
          break;
        }
      }
    }
  }
}

void MissionStateReceiver::externalMissionStartedEvent(const droneMsgsROS::Event::ConstPtr& msg)
{
  droneMsgsROS::Event event = *msg;
  if (event.category == droneMsgsROS::Event::MISSION_STATE_CHANGED)
  {
    std::string t = event.arguments[0];
    if (t == "STARTED")
    {
      Q_EMIT(externalMissionStarted());
    }
  }
}

bool MissionStateReceiver::requestMissionName()
{
  std::string mission_name;
  std::string filename;
  droneMsgsROS::missionName msg;
  pugi::xml_document document;
  filename = my_stack_directory + "/configs" + "/" + rosnamespace + "/mission_specification_file.xml";
  if (document.load_file(filename.c_str()))
  {
    pugi::xml_node node = document;
    mission_name = node.child("mission").attribute("root_task").value();
    // Send name of the mission
    Q_EMIT missionLoaded(QString::fromStdString(mission_name));
    return true;
  }
  else
  {
    return false;
    /*ERROR service could not be called*/
  }
}

/** UNUSED */
bool MissionStateReceiver::loadMission(const std::string file_path)
{
  droneMsgsROS::openMissionFile loadSrvMsg;
  loadSrvMsg.request.mission_file_path = file_path;

  if (load_mission_client.call(loadSrvMsg))
  {
    if (loadSrvMsg.response.ack)
    {
      // Show dialog. Mission specification file opened successfully.
      // Update mission name.
      Q_EMIT missionLoaded(QString::fromStdString(loadSrvMsg.response.mission_name));
      return true;
    }
    else
    {
      // Show dialog. Detected errors in mission specification file.
      Q_EMIT missionErrors(loadSrvMsg.response.error_messages);
      return true;
    }
  }
  else
  {
    /*ERROR service could not be called*/
    return false;
  }
}

bool MissionStateReceiver::startMission()
{
  std_srvs::Empty emptySrvMsg;
  // Mission start
  if (start_mission_client.call(emptySrvMsg))
  {
    Q_EMIT missionStarted();
    return true;
  }
  else
  {
    return false;
    /*ERROR service could not be called*/
  }
}

bool MissionStateReceiver::abortMission()
{
  std_srvs::Empty emptySrvMsg;
  // Mission abort
  if (stop_mission_client.call(emptySrvMsg))
  {
    Q_EMIT missionCompleted("mission aborted");
    return true;
  }
  else
  {
    return false;
    /*ERROR service could not be called*/
  }
}

void MissionStateReceiver::emitEmergencyLand()
{
  Q_EMIT emergencyLand();
  activateAutonomousMode();
}

QString MissionStateReceiver::getActionArguments()
{
  return this->arguments;
}
