/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  ReceivedDataProcessor
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/received_data_processor.h"

ReceivedDataProcessor::ReceivedDataProcessor(ObjectController* object_controller,
                                             OdometryStateReceiver* odometry_receiver,
                                             ObstaclesReceiver* obstacles_receiver,
                                             MissionStateReceiver* mission_receiver,
                                             TrajectoryAbsRefCommandReceiver* command_receiver,
                                             SocietyPoseReceiver* society_receiver)
  : QObject()
{
  this->odometry_receiver = odometry_receiver;
  this->obstacles_receiver = obstacles_receiver;
  this->mission_receiver = mission_receiver;
  this->command_receiver = command_receiver;
  this->object_controller = object_controller;
  this->society_receiver = society_receiver;

  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");

  this->connectReceiversToReceivedDataProcessor(this->odometry_receiver, this->obstacles_receiver,
                                                this->command_receiver, this->society_receiver, this->mission_receiver);
}

ReceivedDataProcessor::~ReceivedDataProcessor()
{
}

void ReceivedDataProcessor::connectReceiversToReceivedDataProcessor(QObject* odometry_receiver,
                                                                    QObject* obstacles_receiver,
                                                                    QObject* command_receiver,
                                                                    QObject* society_receiver,
                                                                    QObject* mission_receiver)
{
  QObject::connect(odometry_receiver, SIGNAL(updateDronePosition()), this, SLOT(updateDrone()));
  QObject::connect(obstacles_receiver, SIGNAL(updateMapInfo()), this, SLOT(updateObstacles()));
  QObject::connect(command_receiver, SIGNAL(sendTrajectory()), this, SLOT(updateTrajectory()));
  QObject::connect(society_receiver, SIGNAL(poseReceived()), this, SLOT(updateDronesTrajectories()));
  QObject::connect(mission_receiver, SIGNAL(commonMissionStarted()), this, SLOT(setMissionMode()));
  QObject::connect(mission_receiver, SIGNAL(commonMissionCompleted()), this, SLOT(setShowMode()));
}

void ReceivedDataProcessor::updateDrone()
{
  geometry_msgs::PoseStamped msg = odometry_receiver->drone_pose_msgs;
  ObjectController::Objects objects = object_controller->getObjects();

  // Vehicle orientation

  Eigen::Quaternionf q(
      odometry_receiver->drone_pose_msgs.pose.orientation.w, odometry_receiver->drone_pose_msgs.pose.orientation.x,
      odometry_receiver->drone_pose_msgs.pose.orientation.y, odometry_receiver->drone_pose_msgs.pose.orientation.z);

  toEulerAngle(q, roll, pitch, yaw);

  ObjectController::Drone drone;
  drone = objects.drones[0];
  drone.x_coor = msg.pose.position.x;
  drone.y_coor = msg.pose.position.y;
  drone.degrees = angles::to_degrees(yaw);

  ObjectController::Point point;
  point.x = msg.pose.position.x;
  point.y = msg.pose.position.y;

  if (drone.points.size() > 0)
  {
    ObjectController::Point last_point = drone.points.at(drone.points.size() - 1);
    if (sqrt(pow((last_point.x - point.x), 2) + pow((last_point.y - point.y), 2)) > precision)
    {
      drone.points.push_back(point);
    }
  }
  else
  {
    if (point.x != 0 && point.y != 0)
      drone.points.push_back(point);
  }
  object_controller->updateDrone(drone, 0);
}

void ReceivedDataProcessor::updateObstacles()
{
  ObjectController::Objects objects = object_controller->getObjects();
  std::vector<ObjectController::Pole> poles = objects.poles;
  droneMsgsROS::obstaclesTwoDim::_poles_type poles_vector = obstacles_receiver->getPolesVector();
  for (int i = 0; i < poles_vector.size(); i++)
  {
    bool isPole = false;
    bool isLandmark = false;
    for (int j = 0; j < objects.poles.size(); j++)
    {
      if (poles_vector[i].id == objects.poles[j].id)
      {
        isPole = true;
      }
    }
    if (!isPole)
    {
      for (int k = 0; k < objects.landmarks.size(); k++)
      {
        if (poles_vector[i].id == objects.landmarks[k].id)
        {
          isLandmark = true;
        }
      }
    }
    if (!isPole && !isLandmark)
    {
      ObjectController::Pole pole;
      pole.id = poles_vector[i].id;
      pole.x_radius = poles_vector[i].radiusX;
      pole.y_radius = poles_vector[i].radiusY;
      pole.x_coor = poles_vector[i].centerX;
      pole.y_coor = poles_vector[i].centerY;
      pole.x_size = poles_vector[i].radiusX * 2;
      pole.y_size = poles_vector[i].radiusY * 2;
      object_controller->updateObstacles(pole);
    }
  }
}

void ReceivedDataProcessor::updateTrajectory()
{
  droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type trajectory_vector =
      command_receiver->getTrajectoryVector();
  ObjectController::Objects objects = object_controller->getObjects();
  std::vector<std::vector<ObjectController::Point>> trajectories = objects.trajectories;
  std::vector<ObjectController::Point> points;
  for (int i = 0; i < trajectory_vector.size(); i++)
  {
    ObjectController::Point p;
    p.x = trajectory_vector[i].x;
    p.y = trajectory_vector[i].y;
    points.push_back(p);
  }
  if (points.size() > 0)
  {
    object_controller->updateTrajectory(points);
  }
}

void ReceivedDataProcessor::updateDronesTrajectories()
{
  droneMsgsROS::societyPose::_societyDrone_type drone_info_vector = society_receiver->getDronesPosition();
  ObjectController::Objects objects = object_controller->getObjects();
  for (int i = 0; i < drone_info_vector.size(); i++)
  {
    bool found = false;
    ObjectController::Point point;
    point.x = drone_info_vector[i].pose.x;
    point.y = drone_info_vector[i].pose.y;
    for (int j = 0; j < objects.drones.size(); j++)
    {
      if (drone_info_vector[i].id == objects.drones[j].id)
      {
        objects.drones[j].x_coor = drone_info_vector[i].pose.x;
        objects.drones[j].y_coor = drone_info_vector[i].pose.y;
        objects.drones[j].degrees = angles::to_degrees(drone_info_vector[i].pose.yaw);
        if (objects.drones[j].points.size() > 0)
        {
          ObjectController::Point last_point = objects.drones[j].points.at(objects.drones[j].points.size() - 1);
          if (sqrt(pow((last_point.x - point.x), 2) + pow((last_point.y - point.y), 2)) > precision)
          {
            objects.drones[j].points.push_back(point);
          }
        }
        object_controller->updateDrone(objects.drones[j], j);
        found = true;
      }
    }
    if (!found)
    {
      if (drone_info_vector[i].pose.x != 0 && drone_info_vector[i].pose.y != -10)
      {
        ObjectController::Drone drone;
        drone.id = drone_info_vector[i].id;
        drone.x_coor = drone_info_vector[i].pose.x;
        drone.y_coor = drone_info_vector[i].pose.y;
        drone.degrees = angles::to_degrees(drone_info_vector[i].pose.yaw);
        drone.x_size = objects.drones[0].x_size;
        drone.y_size = objects.drones[0].y_size;
        drone.points.push_back(point);
        object_controller->updateDrone(drone, -1);
      }
    }
  }
}
void ReceivedDataProcessor::toEulerAngle(const Eigen::Quaternionf& q, float& roll, float& pitch, float& yaw)
{
  // roll (x-axis rotation)
  double sinr_cosp = +2.0 * (q.w() * q.x() + q.y() * q.z());
  double cosr_cosp = +1.0 - 2.0 * (q.x() * q.x() + q.y() * q.y());
  roll = atan2(sinr_cosp, cosr_cosp);

  // pitch (y-axis rotation)
  double sinp = +2.0 * (q.w() * q.y() - q.z() * q.x());
  if (fabs(sinp) >= 1)
    pitch = copysign(M_PI / 2, sinp);  // use 90 degrees if out of range
  else
    pitch = asin(sinp);

  // yaw (z-axis rotation)
  double siny_cosp = +2.0 * (q.w() * q.z() + q.x() * q.y());
  double cosy_cosp = +1.0 - 2.0 * (q.y() * q.y() + q.z() * q.z());
  yaw = atan2(siny_cosp, cosy_cosp);
}
