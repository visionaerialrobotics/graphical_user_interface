/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  OdometryStateReceiver
  Launch a ROS node to subscribe topics.
  @author  Abraham Carrera, Daniel Del Olmo
  @date    06-2018
  @version 3.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/
#include "../include/odometry_state_receiver.h"

/*****************************************************************************
** Implementation
*****************************************************************************/
OdometryStateReceiver::OdometryStateReceiver()
{
  subscriptions_complete = false;
}

void OdometryStateReceiver::openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace)
{
  nodeHandle.param<std::string>("drone_id", drone_id, "1");
  nodeHandle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  nodeHandle.param<std::string>("self_localization", odometry_namespace, "self_localization");
  nodeHandle.param<std::string>("pose_topic", odometry_estimated_pose_str, "pose");
  geometric_pose_sub = nodeHandle.subscribe('/' + drone_id_namespace + '/' + '/' + odometry_namespace + '/' +
                                                odometry_estimated_pose_str,
                                            1, &OdometryStateReceiver::droneEstimatedPoseCallback, this);
  subscriptions_complete = true;
}

bool OdometryStateReceiver::ready()
{
  if (!subscriptions_complete)
    return false;
  return true;  // Used this way instead of "return subscriptions_complete" due to preserve add more conditions
}

OdometryStateReceiver::~OdometryStateReceiver()
{
}

void OdometryStateReceiver::droneEstimatedPoseCallback(const geometry_msgs::PoseStamped msg)
{
  drone_pose_msgs = msg;
  Q_EMIT updateDronePosition();
  return;
}
