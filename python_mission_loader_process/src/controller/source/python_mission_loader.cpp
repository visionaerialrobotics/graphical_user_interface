/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  PythonMissionLoader
  @author  Abraham Carrera, Daniel Del Olmo
  @date    04-2018
  @version 3.0
*/

#include "../include/python_mission_loader.h"

PythonMissionLoader::PythonMissionLoader(int argc, char** argv, QWidget* parent) : QWidget(parent)
{
  // window always on top
  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);
  setWindowIcon(QIcon(":/images/images/import_mission.png"));

  n.param<std::string>("prefixed_layout", prefixed_layout, "normal");

  screen = QGuiApplication::primaryScreen();
  screenGeometry = screen->geometry();

  this->move(screenGeometry.width() / 2 - this->width() / 2, screenGeometry.height() / 2 - this->height() / 2);

  // Default configuration folder
  homePath = QDir::homePath().toStdString();
  default_folder = homePath + "/workspace/ros/aerostack_catkin_ws/src/aerostack_stack/configs" + "/drone1";

  // Nodes
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("configuration_folder", configuration_folder_service, "configuration_folder");

  // Service communications
  configuration_folder_client = n.serviceClient<droneMsgsROS::configurationFolder>(configuration_folder_service);
  configuration_folder_client.call(req, res);
  configuration_folder = res.folder;
  n.param<std::string>("window_opened", window_opened_topic, "window_opened");
  n.param<std::string>("window_closed", window_closed_topic, "window_closed");

  // Subscribers
  window_opened_subs = n.subscribe("/" + drone_id_namespace + "/" + window_opened_topic, 10,
                                   &PythonMissionLoader::windowOpenCallback, this);

  // Publishers
  window_closed_publ =
      n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace + "/" + window_closed_topic, 1, true);

  // Load python mission plan
  if (configuration_folder == "")
    file_route = default_folder + "/mission.py";

  else
    file_route = configuration_folder + "/mission.py";

  selectMissionPlan();
}

PythonMissionLoader::~PythonMissionLoader()
{
}

void PythonMissionLoader::selectMissionPlan()
{
  python_mission =
      fileDialog.getOpenFileName(this, tr("Select Python Mission Plan"), "", tr("Python file (*.py)")).toStdString();
  if (python_mission != "")
  {
    if (savePythonFile(python_mission))  // if python file saving is successful
    {
      python_file_msg.setWindowTitle(QString::fromStdString("Import Completed"));
      python_file_msg.setText(QString::fromStdString("Python mission plan has been saved in the following route: "
                                                     "\"%1\"")
                                  .arg(QString::fromStdString(file_route)));
      python_file_msg.setWindowFlags(python_file_msg.windowFlags() | Qt::WindowStaysOnTopHint);
      python_file_msg.exec();
    }

    else  // if python file saving fails
    {
      python_file_msg.setWindowTitle(QString::fromStdString("Import Error"));
      python_file_msg.setText(QString::fromStdString("Unsecessfully imported python mission plan"));
      python_file_msg.setWindowFlags(python_file_msg.windowFlags() | Qt::WindowStaysOnTopHint);
      python_file_msg.exec();
    }
  }

  windowClosedMsgs.id = 15;
  window_closed_publ.publish(windowClosedMsgs);

  // killMe();
}

bool PythonMissionLoader::savePythonFile(const std::string python_mission)
{
  // std::cout << "File route: " << file_route << std::endl;

  file = fopen(file_route.c_str(), "w+");
  file_mission = fopen(python_mission.c_str(), "r");

  if (file_mission == NULL || file == NULL)
    return false;

  char c[100];

  while (!feof(file_mission))
  {
    fgets(c, 100, file_mission);
    fputs(c, file);
  }

  fclose(file);
  fclose(file_mission);
  return true;
}

void PythonMissionLoader::closeEvent(QCloseEvent* event)
{
  windowClosedMsgs.id = 15;
  window_closed_publ.publish(windowClosedMsgs);
}

void PythonMissionLoader::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void PythonMissionLoader::windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg)
{
  windowOpenedMsgs = msg;

  if (windowOpenedMsgs.id == 18 || windowOpenedMsgs.id == 2 || windowOpenedMsgs.id == 17 || windowOpenedMsgs.id == 1 ||
      windowOpenedMsgs.id == 7 || windowOpenedMsgs.id == 11 || windowOpenedMsgs.id == 8 || windowOpenedMsgs.id == 14 ||
      windowOpenedMsgs.id == 5 || windowOpenedMsgs.id == 16)
  {
    windowClosedMsgs.id = 15;
    window_closed_publ.publish(windowClosedMsgs);

    fileDialog.close();
    killMe();
  }
}
