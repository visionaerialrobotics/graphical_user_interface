/*!*******************************************************************************************
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *             This program is free software: you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation, either version 3 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#include "python_mission_loader.h"
#include <QApplication>
#include <QMessageBox>
#include <cstdio>
#include <gtest/gtest.h>
#include <ros/ros.h>

#include <iostream>

QApplication* app;
PythonMissionLoader* w;
int ar;
bool finished = false;
int total_subtests = 0;
int passed_subtests = 0;

void spinnerThread()
{
  while (!finished)
  {
    ros::spinOnce();
  }
}

void displaySubtestResults()
{
  std::cout << "\033[1;33m TOTAL SUBTESTS: " << total_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m SUBTESTS PASSED: " << passed_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m % PASSED: " << (passed_subtests * 100.0) / (total_subtests * 1.0) << "\033[0m" << std::endl;
}

void test()
{
  total_subtests++;
  std::string response;
  getline(std::cin, response);

  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in importing mission\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  displaySubtestResults();
  EXPECT_TRUE(response == "Y" || response == "y");

  app->exit();
  finished = true;
}

TEST(GraphicalUserInterfaceTests, importPythonMissionTest)
{
  std::thread thr(&test);
  app->exec();
  thr.join();
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());

  std::cout << "\033[1;34m Select the mission.py file saved in the test folder of this package and press open. Has it "
               "been saved in configs/drone1 as mission.py? (Y/N) \033[0m"
            << std::endl;

  ar = argc;
  app = new QApplication(argc, nullptr);

  std::thread thr(&spinnerThread);

  w = new PythonMissionLoader(argc, nullptr);
  // w->show();

  return RUN_ALL_TESTS();
  thr.join();
}
