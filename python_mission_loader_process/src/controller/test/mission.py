import executive_engine_api as api

def runMission():
  print("Starting mission...")
  print("Localizing...")
  api.executeBehavior('SELF_LOCALIZE_BY_ODOMETRY')
  print("Taking off...")
  api.executeBehavior('TAKE_OFF')
  endPoint = [8, 8, 0.7]
  print("Going to landing point...")
  api.executeBehavior('GO_TO_POINT', coordinates=endPoint)
  print("Landing...")
  api.executeBehavior('LAND')
