/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "aerostack_msgs/BehaviorCommand.h"
#include "aerostack_msgs/RequestBehavior.h"
#include "execution_viewer.h"
#include <QApplication>
#include <QMessageBox>
#include <cstdio>
#include <gtest/gtest.h>
#include <ros/ros.h>
#include <thread>

#include <iostream>

QApplication* app;
ExecutionViewer* w;
int ar;
bool finished = false;
int total_subtests = 0;
int passed_subtests = 0;

void spinnerThread()
{
  while (!finished)
  {
    ros::spinOnce();
  }
}

void displaySubtestResults()
{
  std::cout << "\033[1;33m TOTAL SUBTESTS: " << total_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m SUBTESTS PASSED: " << passed_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m % PASSED: " << (passed_subtests * 100.0) / (total_subtests * 1.0) << "\033[0m" << std::endl;
}

void test()
{
  ros::NodeHandle nh;
  total_subtests++;
  std::string response;
  std::cout << "\033[1;34m Does a Widget called Execution Viewer appear in the right half of the screen? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in position or appearance of the widget\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Does a belief called 'battery_level(self, HIGH)' appear in the Belief Viewer section ? "
               "(Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem receiving beliefs\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Right click on the Belief Viewer section. Does a context dialog with the option 'Add a "
               "belief' appear? (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in creating context dialog for adding new beliefs\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on 'Add a belief' option. ¿Does a window called 'Add a belief' appear? (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Not displaying 'Add a belief' window \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m  Leave blank the 'Belief expression' section and press 'Accept'. Does a dialog called "
               "'Invalid belief format' appears alerting us of an invalid belief name? (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in displaying 'Invalid belief format' dialog\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;

  std::cout << "\033[1;34m Insert 'temperature(air,15)' in the 'Belief expression' section and press 'Accept'. Does a "
               "dialog "
               "called 'Invalid parameters' appears asking us to select if the belief is multivaluated or not? \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in displaying 'Invalid parameters' dialog \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Check the option 'Yes' in 'Multivaluated' section and press 'Accept'. Does the new  belief "
               "appear in 'Belief Viewer' section? \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in adding new beliefs \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Right click on the recently created belief. Does a context dialog with the options 'Add a "
               "belief' and 'Remove this belief' appear? (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in creating context dialog over an existing belief\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on 'Remove this belief' option. Does the belief disapear? \033[0m" << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in removing beliefs \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  std::string drone_id;
  std::string drone_id_namespace;
  nh.param<std::string>("drone_id", drone_id, "1");
  nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  ros::ServiceClient start_behavior_cli =
      nh.serviceClient<aerostack_msgs::RequestBehavior>("/" + drone_id_namespace + "/activate_behavior");
  aerostack_msgs::RequestBehavior::Request req;
  aerostack_msgs::RequestBehavior::Response res;
  aerostack_msgs::BehaviorCommand msg;
  msg.name = "SELF_LOCALIZE_BY_ODOMETRY";
  req.behavior = msg;
  while (!res.ack)
  {
    start_behavior_cli.call(req, res);
  }

  total_subtests++;

  std::cout << "\033[1;34m Has 'SELF_LOCALIZE_BY_ODOMETRY' behavior appeared in 'the'Behavior Viewer' section ? \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in displaying new behaviors \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Right click on the Behavior Viewer section. Does a context dialog with the option 'Add a "
               "behavior' appear? (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in creating context dialog for adding new behaviors\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on 'Add a behavior' option. ¿Does a window called 'Add a behavior' appear? "
               "(Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Not displaying 'Add a belief' window \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Does the drop down menu in the upper part of the window contain a list with all types of "
               "behaviors? (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem charging available behaviors \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Select 'TAKE_OFF' behavior on the list and press 'Accept'. Has appeared a new behavior "
               "'TAKE_OFF' appeared on the list ? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in adding new behaviors \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Right click on 'SELF_LOCALIZE_BY_ODOMETRY' behavior. Has a context dialog with options  "
               "'Add a behavior' and 'Stop the execution of this behavior'? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem creating context dialog for existing behaviors \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on 'Add a behavior' option and add a GO_TO_POINT behavior with argument 'coordinates: "
               "[1, 1, 0.5]'. Press accept and while the new behavior is executing press 'Stop the execution of this "
               "behavior'. Does the behavior stop executing? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem stoping behaviors \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  displaySubtestResults();

  app->exit();
  finished = true;
}

TEST(GraphicalUserInterfaceTests, executionViewerTest)
{
  std::thread thr(&test);
  app->exec();
  thr.join();
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());

  ar = argc;

  system("bash $AEROSTACK_STACK/launchers/launcher_simulated_quadrotor_basic_3.0.sh");
  system(
      "xfce4-terminal  \--tab --title \"Behavior Coordinator\"  --command \"bash -c 'roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
           my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &");
  app = new QApplication(argc, nullptr);
  w = new ExecutionViewer(argc, nullptr);
  w->show();

  std::thread thr(&spinnerThread);

  return RUN_ALL_TESTS();
  thr.join();
}
