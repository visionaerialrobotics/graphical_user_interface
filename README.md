
# Graphical User Interface

The Graphical User Interface allows operators or users of the system to bi-directionally communicate with each aerial robotic agent. Both communication directions are equally important because:

The communication from the robotic agent to the operator is needed to monitor its state and the performance of the requested mission. This will allow the operator to actuate in case of a need to redefine or abort the mission.

The communication from the operator to the robotic agent is needed if a non-rigid behavior is needed to accomplish the goal, making it possible to redefine the mission and also to start, stop and abort the mission.

It is composed of several independent processes intended for the execution of each widget in the Graphical User Interface.

## Project organization

This directory contains 16 folders with the code of each one of the different processes (with the same name as its container folder) that integrate the Graphical User Interface of Aerostack 3.0. The mentioned folders   use an organization acording to their model/view architecture that manages the relationship between data and the way it is presented to the user. There is an aditional folder called layouts that contains  JSON files with the disposition of the windows in each prefixed layout.  


Components/folders:

   

 - **layouts**: disposition of the windows in each prefixed layout
  - **alphanumeric_interface_process**: drives the vehicle using alphanumeric controls
   - **behavior_tree_editor_process**: defines the mission tree structure
   - **behavior_tree_interpreter_process**: executes mission defined by behavior tree file
   - **camera_viewer_process**: shows information captured by the vehicle cameras
   - **edit_environment_process**: defines the map of the mission
   - **environment_viewer_process**: shows the map and vehicles in real time
   - **execution_viewer_process**: shows current beliefs and behaviors
   - **first_person_viewer_process**: show HUD interface
   - **python_mission_loader_process**: imports python file with mission definition
   - **tml_mission_loader_process**: imports python file with mission definition
   - **teleoperation_control_window_process**: drives the vehicle using the keyboard
   - **main_window_process**: launches the GUI components
   - **parameters_viewer_process**: shows and plots many parameters values
   - **python_mission_control_window_process**: executes mission defined by python file
   - **tml_mission_control_window_process**: executes mission defined by tml file
   - **vehicle_dynamics_viewer_process**: shows values of position and speed parameters
    
## License

Copyright (c) 2018 Universidad Politecnica de Madrid
All rights reserved


Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

