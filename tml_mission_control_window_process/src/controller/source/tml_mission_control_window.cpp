/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  TmlMissionControlWindow
  @author  Abraham Carrera, Daniel Del Olmo
  @date    04-2018
  @version 3.0
*/

#include "../include/tml_mission_control_window.h"

TmlMissionControlWindow::TmlMissionControlWindow(QWidget* parent)
  : QWidget(parent), ui(new Ui::TmlMissionControlWindow)  // initialize ui member
{
  ui->setupUi(this);  // connects all ui's triggers
  ui->abort_tml_button->hide();
  ui->emergency_land_tml_button->hide();

  stopped = false;
  running = false;
  fileLoaded = true;

  // Flight time
  this->current_time = new QTime(0, 0, 0);
  setTimerInterval(1000);  // 1 second = 1000
  flight_timer = new QTimer(this);
  flight_timer->start(1000);

  // Default configuration folder
  homePath = QDir::homePath().toStdString();
  default_folder = homePath + "/workspace/ros/aerostack_catkin_ws/src/aerostack_stack/configs" + "/drone1";

  // Nodes
  n.param<std::string>("activate_behavior", activate_behavior, "activate_behavior");
  n.param<std::string>("list_of_active_behaviors", list_of_active_behaviors, "list_of_active_behaviors");
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("mission_state", mission_state_topic, "mission_state");

  if (!n.getParam("drone_Completed_Mission_Topic_Name", completed_mission_topic))
    completed_mission_topic = "completed_mission";

  if (!n.getParam("drone_driver_sensor_battery", drone_driver_sensor_battery))
    drone_driver_sensor_battery = "battery";

  if (!n.getParam("wifiIsOk", wifi_connection_topic))
    wifi_connection_topic = "wifiIsOk";

  n.param<std::string>("configuration_folder", configuration_folder_service, "configuration_folder");
  ui->value_vehicle->setText(QString::fromUtf8(drone_id_namespace.c_str()));
  // Service communications
  configuration_folder_client = n.serviceClient<droneMsgsROS::configurationFolder>(configuration_folder_service);
  configuration_folder_client.call(request, response);
  configuration_folder = response.folder;

  // Service communications
  start_mission_client =
      n.serviceClient<std_srvs::Empty>("/" + drone_id_namespace + "/" + "task_based_mission_planner_process/start");
  stop_mission_client =
      n.serviceClient<std_srvs::Empty>("/" + drone_id_namespace + "/" + "task_based_mission_planner_process/stop");
  activate_behavior_srv = n.serviceClient<aerostack_msgs::RequestBehavior>(activate_behavior);

  // Subscriptions
  list_of_behaviors_sub = n.subscribe("/" + drone_id_namespace + "/" + list_of_active_behaviors, 1000,
                                      &TmlMissionControlWindow::newBehaviorCallback, this);
  completed_mission_subs = n.subscribe("/" + drone_id_namespace + "/" + completed_mission_topic, 1,
                                       &TmlMissionControlWindow::completedMissionCallback, this);
  battery_subs = n.subscribe("/" + drone_id_namespace + "/" + drone_driver_sensor_battery, 1,
                             &TmlMissionControlWindow::batteryCallback, this);
  wificonnection_subs = n.subscribe("/" + drone_id_namespace + "/" + wifi_connection_topic, 1,
                                    &TmlMissionControlWindow::wifiConnectionCheckCallback, this);

  // Publishers
  mission_state_publ = n.advertise<std_msgs::Bool>("/" + drone_id_namespace + "/" + mission_state_topic, 1, true);

  // Load TML File
  loadFile();

  // Connects
  QObject::connect(ui->execute_tml_button, SIGNAL(clicked()), this, SLOT(executeTmlMission()));
  QObject::connect(ui->emergency_land_tml_button, SIGNAL(clicked()), this, SLOT(landTmlButton()));
  QObject::connect(ui->abort_tml_button, SIGNAL(clicked()), this, SLOT(abortTmlMission()));
  QObject::connect(flight_timer, SIGNAL(timeout()), this, SLOT(flightTime()));
  QObject::connect(parent, SIGNAL(missionStopped()), this, SLOT(setMissionStopped()));
}

TmlMissionControlWindow::~TmlMissionControlWindow()
{
  delete ui;
}

void TmlMissionControlWindow::setTimerInterval(double ms)
{
  d_interval = qRound(ms);
  if (d_interval >= 0)
    d_timerId = startTimer(d_interval);
}

void TmlMissionControlWindow::setMissionStopped()
{
  stopped = true;
}

void TmlMissionControlWindow::flightTime()
{
  if (running)
  {
    this->current_time->setHMS(this->current_time->addSecs(+1).hour(), this->current_time->addSecs(+1).minute(),
                               this->current_time->addSecs(+1).second());
    QString text = this->current_time->toString();
    ui->value_flight_time->setText(text);
  }
}

void TmlMissionControlWindow::loadFile()
{
  pugi::xml_document document;
  if (configuration_folder == "")
    file_route = default_folder + "/mission_specification_file.xml";

  else
    file_route = configuration_folder + "/mission_specification_file.xml";

  // std::cout << "file route: " << file_route << std::endl;
  if (!document.load_file(file_route.c_str()))
  {
    //  std::cout << "load error" << std::endl;
    fileLoaded = true;  // ERROR service could not be called
  }
}

void TmlMissionControlWindow::executeTmlMission()
{
  //  std::cout << "Start pressed button" << std::endl;
  if (!fileLoaded)
  {
    QMessageBox error_message;
    error_message.setWindowTitle(QString::fromStdString("No such file"));
    error_message.setText(QString::fromStdString("The file \"mission_specification_file.xml\" dont exist on the next "
                                                 "directory: "
                                                 "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack/configs/"
                                                 "drone1. Please create "
                                                 "the file and restart Aerostack."));
    error_message.exec();
  }
  else
  {
    std_srvs::Empty emptySrvMsg;
    //    stop_mission_client.call(emptySrvMsg);
    if (start_mission_client.call(emptySrvMsg))
    {
      // std::cout << "start mission" << std::endl;
      stopped = false;
      Q_EMIT missionStarted();
      displayMissionStarted();
    }
    else
      QMessageBox::information(this, tr("Mission Not Started"), "ERROR. The mission could not be started.");
  }
}

void TmlMissionControlWindow::landTmlButton()
{
  // std::cout << "Land pressed button" << std::endl;

  if (!stopped)
  {
    std_srvs::Empty emptySrvMsg;

    stop_mission_client.call(emptySrvMsg);
  }

  behavior.name = "LAND";
  req.behavior = behavior;
  activate_behavior_srv.call(req, res);
  if (!res.ack)
    std::cout << res.error_message << std::endl;

  displayMissionCompleted();
  Q_EMIT missionStopped();

  //  std::cout << "Mission aborted. Land issued." << std::endl;
}

void TmlMissionControlWindow::abortTmlMission()
{
  // std::cout << "Abort pressed button" << std::endl;
  std_srvs::Empty emptySrvMsg;
  if (!stopped)
  {
    stop_mission_client.call(emptySrvMsg);
  }
  aerostack_msgs::BehaviorCommand behavior;
  behavior.name = "KEEP_HOVERING";
  req_activate.behavior = behavior;
  activate_behavior_srv.call(req_activate, res_activate);

  if (!res_activate.ack)
    std::cout << res_activate.error_message << std::endl;

  QMessageBox msgBox;
  msgBox.setTextFormat(Qt::TextFormat::RichText);
  msgBox.setText("mission aborted");

  displayMissionCompleted();
  Q_EMIT missionStopped();

  // onHoverButton();
  //   std::cout << "Mission aborted." << std::endl;
}

void TmlMissionControlWindow::completedMissionCallback(const droneMsgsROS::CompletedMission::ConstPtr& ack)
{
  completed_mission = QString::fromStdString(ack->result);
  msgBox.setTextFormat(Qt::TextFormat::RichText);
  msgBox.setText(completed_mission);
  displayMissionCompleted();

  // std::cout << "Mission completed" << std::endl;
}

void TmlMissionControlWindow::displayMissionCompleted()
{
  ui->abort_tml_button->hide();
  ui->emergency_land_tml_button->hide();
  ui->execute_tml_button->show();
  running = false;

  missionStateMsgs.data = false;
  mission_state_publ.publish(missionStateMsgs);
}
void TmlMissionControlWindow::displayMissionStarted()
{
  ui->execute_tml_button->hide();
  ui->abort_tml_button->show();
  ui->emergency_land_tml_button->show();
  running = true;

  missionStateMsgs.data = true;
  mission_state_publ.publish(missionStateMsgs);
}

void TmlMissionControlWindow::newBehaviorCallback(const droneMsgsROS::ListOfBehaviors& msg)
{
  if (msg.behaviors.size() != 0)
  {
    for (int i = 0; i < msg.behaviors.size(); i++)
    {
      if (msg.behaviors[i] == "TAKE_OFF")
      {
        this->current_time->setHMS(00, 00, 00);
        ui->value_flight_time->setText(this->current_time->toString());
        running = true;
      }
      else if (msg.behaviors[i] == "LAND")
      {
        running = false;
      }
    }
    // Battery
    if (battery_msgs.batteryPercent <= 25.0 && battery_msgs.batteryPercent != 0)
    {
      QPalette* palette = new QPalette();
      palette->setColor(QPalette::WindowText, Qt::red);
      ui->value_battery->setPalette(*palette);
    }
    ui->value_battery->setText(QString::number(battery_msgs.batteryPercent) + "%");

    // Connection
    if (is_wifi_connected)
      ui->value_wifi->setText("Connected");
    else
      ui->value_wifi->setText("Disconnected");
  }
}

void TmlMissionControlWindow::batteryCallback(const droneMsgsROS::battery::ConstPtr& msg)
{
  battery_msgs = *msg;
  if (battery_msgs.batteryPercent <= 25.0 && battery_msgs.batteryPercent != 0)
  {
    QPalette* palette = new QPalette();
    palette->setColor(QPalette::WindowText, Qt::red);
    ui->value_battery->setPalette(*palette);
  }
  // Battery
  ui->value_battery->setText(QString::number(battery_msgs.batteryPercent) + "%");
}

void TmlMissionControlWindow::wifiConnectionCheckCallback(const std_msgs::Bool::ConstPtr& msg)
{
  is_wifi_connected = msg->data;
  if (is_wifi_connected)
    ui->value_wifi->setText("Connected");
  else
    ui->value_wifi->setText("Disconnected");
}
