/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  TmlMissionControlWindowView
  @author  Abraham Carrera, Daniel Del Olmo
  @date    02-2018
  @version 3.0
*/

#include "../include/tml_mission_control_window_view.h"

TmlMissionControlWindowView::TmlMissionControlWindowView(int argc, char** argv, QWidget* parent)
  : QWidget(parent), ui(new Ui::TmlMissionControlWindowView)
{
  setWindowIcon(QIcon(":/images/images/tml_control.png"));
  setWindowTitle("Tml Mission Control Window");

  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);

  ui->setupUi(this);  // connects all ui's triggers

  // Add the control panel widget.
  tml_control = new TmlMissionControlWindow(this);
  ui->gridLayout->addWidget(tml_control);

  // Layout
  namespace pt = boost::property_tree;

  std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                        "graphical_user_interface/layouts/layout.json");

  pt::read_json(layout_dir, root);

  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;
  int height = root.get<int>("8.height");
  int width = root.get<int>("8.width");

  this->resize(width, height);
  this->move(x0 + root.get<int>("8.position.x"), y0 + root.get<int>("8.position.y"));

  stopped = true;

  // Settings configurations
  setUp();
}

TmlMissionControlWindowView::~TmlMissionControlWindowView()
{
  delete ui;
  delete tml_control;
}

void TmlMissionControlWindowView::setUp()
{
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");

  n.param<std::string>("window_opened", window_opened_topic, "window_opened");
  n.param<std::string>("window_closed", window_closed_topic, "window_closed");

  // Subscribers
  window_opened_subs = n.subscribe("/" + drone_id_namespace + "/" + window_opened_topic, 10,
                                   &TmlMissionControlWindowView::windowOpenCallback, this);

  // Publishers
  window_closed_publ =
      n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace + "/" + window_closed_topic, 1, true);
  n.param<std::string>("activate_behavior", activate_behavior, "activate_behavior");
  activate_behavior_srv =
      n.serviceClient<aerostack_msgs::RequestBehavior>("/" + drone_id_namespace + "/" + activate_behavior);

  n.param<std::string>("initiate_behaviors", initiate_behaviors, "initiate_behaviors");
  initiate_behaviors_srv =
      n.serviceClient<droneMsgsROS::InitiateBehaviors>("/" + drone_id_namespace + "/" + initiate_behaviors);
  droneMsgsROS::InitiateBehaviors msg;
  initiate_behaviors_srv.call(msg);

  // Key configurations
  isAKeyPressed = false;

  setFocusPolicy(Qt::StrongFocus);
  acceptedKeys.insert(0x01000012, false);  // Tecla UP
  acceptedKeys.insert(0x01000013, false);  // Tecla DOWN
  acceptedKeys.insert(0x01000014, false);  // Tecla LEFT
  acceptedKeys.insert(0x01000015, false);  // Tecla RIGHT
  acceptedKeys.insert(0x51, false);        // Tecla Q
  acceptedKeys.insert(0x41, false);        // Tecla A
  acceptedKeys.insert(0x54, false);        // Tecla T
  acceptedKeys.insert(0x59, false);        // Tecla Y
  acceptedKeys.insert(0x48, false);        // Tecla H
  acceptedKeys.insert(0x5a, false);        // Tecla Z
  acceptedKeys.insert(0x58, false);        // Tecla X
  acceptedKeys.insert(0x52, false);        // Tecla R

  QObject::connect(tml_control, SIGNAL(missionStarted()), this, SLOT(setMissionStarted()));
  QObject::connect(tml_control, SIGNAL(missionStopped()), this, SLOT(setMissionStopped()));

  aerostack_msgs::RequestBehavior::Request msg2;
  aerostack_msgs::RequestBehavior::Response res;
  aerostack_msgs::BehaviorCommand behavior;
  while (!res.ack)
  {
    behavior.name = "SELF_LOCALIZE_BY_ODOMETRY";
    msg2.behavior = behavior;

    // std::cout<<"/" + drone_id_namespace +  "/" + activate_behavior<<std::endl;

    activate_behavior_srv.call(msg2, res);
  }
}

void TmlMissionControlWindowView::setMissionStarted()
{
  stopped = false;
}
void TmlMissionControlWindowView::setMissionStopped()
{
  stopped = true;
}
void TmlMissionControlWindowView::keyPressEvent(QKeyEvent* e)
{
  // std::cout<<"KEY PRESSED"<<std::endl;
  QWidget::keyPressEvent(e);
  if (!isAKeyPressed && acceptedKeys.contains(e->key()))
  {
    isAKeyPressed = true;
    std_srvs::Empty emptySrvMsg;

    if (!stopped)
    {
      tml_control->stop_mission_client.call(emptySrvMsg);
      stopped = true;
      Q_EMIT missionStopped();
    }

    switch (e->key())
    {
      case Qt::Key_T:
      {
        if (!e->isAutoRepeat())
        {
          acceptedKeys.insert(0x54, true);

          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "SELF_LOCALIZE_BY_ODOMETRY";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;

          behavior.name = "TAKE_OFF";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          tml_control->displayMissionStarted();
          if (!res.ack)
            std::cout << res.error_message << std::endl;
          std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
        break;
      }
      case Qt::Key_Y:
      {
        if (!e->isAutoRepeat())
        {
          acceptedKeys.insert(0x59, true);
          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "LAND";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          tml_control->displayMissionCompleted();
          if (!res.ack)
            std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_H:
      {
        acceptedKeys.insert(0x48, true);
        aerostack_msgs::RequestBehavior::Request msg;
        aerostack_msgs::RequestBehavior::Response res;
        aerostack_msgs::BehaviorCommand behavior;
        behavior.name = "KEEP_HOVERING";
        msg.behavior = behavior;
        activate_behavior_srv.call(msg, res);
        if (!res.ack)
          std::cout << res.error_message << std::endl;
        break;
      }
      case Qt::Key_Right:
      {
        if (!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000014, true);
          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "KEEP_MOVING";
          behavior.arguments = "direction: \"RIGHT\"\nspeed: 0.4";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Left:
      {
        if (!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000012, true);
          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "KEEP_MOVING";
          behavior.arguments = "direction: \"LEFT\"\nspeed: 0.4";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Down:
      {
        if (!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000015, true);
          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "KEEP_MOVING";
          behavior.arguments = "direction: \"BACKWARD\"\nspeed: 0.4";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Up:
      {
        if (!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000013, true);
          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "KEEP_MOVING";
          behavior.arguments = "direction: \"FORWARD\"\nspeed: 0.4";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Q:
      {
        if (!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x51, true);
          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "KEEP_MOVING";
          behavior.arguments = "direction: \"UP\"\nspeed: 0.4";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_A:
      {
        if (!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x41, true);
          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "KEEP_MOVING";
          behavior.arguments = "direction: \"DOWN\"\nspeed: 0.4";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_R:
      {
        if (!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x82, true);
          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "ROTATE";
          behavior.arguments = "angle: 0";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
          std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
        break;
      }
      case Qt::Key_Z:
      {
        if (!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x5a, true);
          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "ROTATE";
          behavior.arguments = "relative_angle: +179";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_X:
      {
        if (!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x58, true);
          aerostack_msgs::RequestBehavior::Request msg;
          aerostack_msgs::RequestBehavior::Response res;
          aerostack_msgs::BehaviorCommand behavior;
          behavior.name = "ROTATE";
          behavior.arguments = "relative_angle: -179";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg, res);
          if (!res.ack)
            std::cout << res.error_message << std::endl;
        }
        break;
      }
    }
  }
}

void TmlMissionControlWindowView::keyReleaseEvent(QKeyEvent* e)
{  //  std::cout<<"KEY RELEASED"<<std::endl;
  if (e->isAutoRepeat() || !acceptedKeys.contains(e->key()))
  {
    isAKeyPressed = false;
    e->ignore();
  }
  else if (acceptedKeys.contains(e->key()) && acceptedKeys.value(e->key()))
  {
    if ((e->key() == 0x52))
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    acceptedKeys.insert(e->key(), false);
    aerostack_msgs::RequestBehavior::Request msg;
    aerostack_msgs::RequestBehavior::Response res;
    aerostack_msgs::BehaviorCommand behavior;
    behavior.name = "KEEP_HOVERING";
    msg.behavior = behavior;
    if (e->key() != Qt::Key_Y && e->key() != Qt::Key_T && e->key() != Qt::Key_R)
      activate_behavior_srv.call(msg, res);
    if (!res.ack)
      std::cout << res.error_message << std::endl;
    isAKeyPressed = false;
    QWidget::keyReleaseEvent(e);
  }
  else
  {
    isAKeyPressed = false;
    e->ignore();
    QWidget::keyReleaseEvent(e);
  }
}

TmlMissionControlWindow* TmlMissionControlWindowView::getTmlMissionControlWindow()
{
  return tml_control;
}
void TmlMissionControlWindowView::closeEvent(QCloseEvent* event)
{
  windowClosedMsgs.id = 8;
  window_closed_publ.publish(windowClosedMsgs);
}

void TmlMissionControlWindowView::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void TmlMissionControlWindowView::windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg)
{
  windowOpenedMsgs = msg;

  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_MISSION_INTERPRETER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::KEYBOARD_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::ALPHANUMERIC_INTERFACE_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_PYTHON ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_TML)
  {
    windowClosedMsgs.id = 8;
    window_closed_publ.publish(windowClosedMsgs);
    killMe();
  }
  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::MINIMIZE_MAIN_WINDOW)
    showMinimized();
}
