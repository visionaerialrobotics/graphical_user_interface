/*!*******************************************************************************************
 *  \file      tml_mission_control_window_view.h
 *  \brief     TML Mission Control Window View definition file.
 *  \details   Layout for the Control Panel component. Widgets can be dynamically added to it.
 *  \author    Daniel Del Olmo
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef TML_MISSION_CONTROL_WINDOW_VIEW_H
#define TML_MISSION_CONTROL_WINDOW_VIEW_H

#include <ros/ros.h>
#include <droneMsgsROS/ListOfBehaviors.h>
#include <droneMsgsROS/InitiateBehaviors.h>
#include <aerostack_msgs/BehaviorEvent.h>
#include <aerostack_msgs/ListOfBeliefs.h>
#include <aerostack_msgs/RequestBehavior.h>
#include <aerostack_msgs/BehaviorCommand.h>
#include "aerostack_msgs/WindowIdentifier.h"

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QTextEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QTextCursor>
#include <QString>
#include <QRect>
#include <QGuiApplication>
#include <QScreen>
#include <QKeyEvent>
#include <QProcess>

#include "tml_mission_control_window.h"
#include "ui_tml_mission_control_window_view.h"

#include <thread>
#include <chrono>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "fstream"

namespace Ui
{
class TmlMissionControlWindowView;
}

class TmlMissionControlWindowView : public QWidget
{
  Q_OBJECT

public:
  // Constructor & Destructor
  explicit TmlMissionControlWindowView(int argc, char** argv, QWidget* parent = 0);

  ros::NodeHandle n;

  ros::Subscriber list_of_behaviors_sub;
  ros::Subscriber behavior_event_sub;
  ros::ServiceClient initiate_behaviors_srv;
  ros::ServiceClient activate_behavior_srv;
  ros::ServiceClient cancel_behavior_srv;

  bool active_self_localization_by_odometry;
  bool is_wifi_connected;
  bool is_takenOff;
  bool isAKeyPressed;
  bool stopped;

  std::string activate_behavior;
  std::string drone_id_namespace;
  std::string initiate_behaviors;

  QMap<int, bool> acceptedKeys;
  boost::property_tree::ptree root;

  ros::Publisher window_closed_publ;
  ros::Subscriber window_opened_subs;

  std::string window_opened_topic;
  std::string window_closed_topic;

  aerostack_msgs::WindowIdentifier windowClosedMsgs;
  aerostack_msgs::WindowIdentifier windowOpenedMsgs;

  ~TmlMissionControlWindowView();

private:
  Ui::TmlMissionControlWindowView* ui;
  TmlMissionControlWindow* tml_control;
  TmlMissionControlWindow* getTmlMissionControlWindow();
  /*!********************************************************************************************************************
   *  \brief      This method is the responsible for seting up connections.
   *********************************************************************************************************************/
  void setUp();

  /*!************************************************************************
   *  \brief   Activated when a window is closed.
   ***************************************************************************/
  void windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg);
  /*!************************************************************************
   *  \brief  Kills the process
   ***************************************************************************/
  void killMe();

public Q_SLOTS:
  /*!********************************************************************************************************************
   *  \brief      This method notifies main window that the widget was closed
   *********************************************************************************************************************/
  void closeEvent(QCloseEvent* event);

  /*!********************************************************************************************************************
   *  \brief      This method is the responsible for handling interruption signals.
   *  \details    The signals handled are the user keyboard commands.
   *********************************************************************************************************************/
  void keyPressEvent(QKeyEvent* e);
  void keyReleaseEvent(QKeyEvent* e);
  /*!********************************************************************************************************************
   *  \brief      This method sets the state of the mission to started
   *********************************************************************************************************************/
  void setMissionStarted();

  /*!********************************************************************************************************************
   *  \brief      This method sets the state of the mission to stopped
   *********************************************************************************************************************/
  void setMissionStopped();

Q_SIGNALS:
  /*!********************************************************************************************************************
   *  \brief      Triggered when mission is stopped
   *********************************************************************************************************************/
  void missionStopped();
};

#endif  // TML_MISSION_CONTROL_WINDOW_VIEW_H
