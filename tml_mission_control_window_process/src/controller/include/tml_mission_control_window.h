/*!*******************************************************************************************
 *  \file      tml_mission_control_window.h
 *  \brief     Tml Mission Control Window definition file.
 *  \details   The control panel shows drone-related information and manages the buttons for interaction with the drone.
 *  \author    Daniel Del Olmo
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef TML_MISSION_CONTROL_WINDOW_H
#define TML_MISSION_CONTROL_WINDOW_H

#include <ros/ros.h>
#include <droneMsgsROS/ListOfBehaviors.h>
#include "droneMsgsROS/CompletedMission.h"
#include "droneMsgsROS/battery.h"
#include "droneMsgsROS/configurationFolder.h"
#include "std_msgs/Bool.h"
#include <aerostack_msgs/RequestBehavior.h>
#include <aerostack_msgs/BehaviorCommand.h>

#include <QObject>
#include <QWidget>
#include <QString>
#include <QTime>
#include <QTimer>
#include <QMessageBox>
#include <QDir>

#include <thread>
#include <iostream>
#include <stdio.h>
#include <pugixml.hpp>
#include "std_srvs/Empty.h"
#include "std_msgs/Bool.h"

#include "ui_tml_mission_control_window.h"

namespace Ui
{
class TmlMissionControlWindow;
}

class TmlMissionControlWindow : public QWidget
{
  Q_OBJECT

public:
  // Constructor & Destructor
  explicit TmlMissionControlWindow(QWidget* parent);
  /*!************************************************************************
   *  \brief   This method change the control buttons when mission is finished
   ***************************************************************************/
  void displayMissionCompleted();
  /*!************************************************************************
   *  \brief   This method change the control buttons when mission is started
   ***************************************************************************/
  void displayMissionStarted();
  ros::ServiceClient stop_mission_client;
  ~TmlMissionControlWindow();

private:
  // Layout
  Ui::TmlMissionControlWindow* ui;

  ros::NodeHandle n;

  bool stopped;
  ros::ServiceClient activate_behavior_srv;
  ros::ServiceClient configuration_folder_client;
  ros::ServiceClient start_mission_client;

  ros::Subscriber battery_subs;
  ros::Subscriber wificonnection_subs;
  ros::Subscriber list_of_behaviors_sub;
  ros::Subscriber completed_mission_subs;

  droneMsgsROS::configurationFolder::Request request;
  droneMsgsROS::configurationFolder::Response response;
  aerostack_msgs::RequestBehavior::Request req;
  aerostack_msgs::RequestBehavior::Response res;
  aerostack_msgs::BehaviorCommand behavior;
  droneMsgsROS::battery battery_msgs;
  aerostack_msgs::RequestBehavior::Response res_activate;
  aerostack_msgs::RequestBehavior::Request req_activate;

  QTimer* flight_timer;  // Timer that sends the timeout signal every second.
  QTime* current_time;
  QString completed_mission;
  QMessageBox msgBox;

  std::string list_of_active_behaviors;
  std::string activate_behavior;
  std::string drone_id_namespace;
  std::string completed_mission_topic;
  std::string drone_driver_sensor_battery;
  std::string wifi_connection_topic;
  std::string configuration_folder;
  std::string configuration_folder_service;
  std::string homePath;
  std::string default_folder;
  std::string file_route;
  std::string mission_state_topic;

  int d_interval;
  int d_timerId;
  bool running;
  bool is_wifi_connected;
  bool fileLoaded;

  std_msgs::Bool missionStateMsgs;

  ros::Publisher mission_state_publ;
  /*!********************************************************************************************************************
   *  \brief This method initializes the timer that informs about the time the drone has been flying.
   *  \param ms The interval at which the timer works.
   *********************************************************************************************************************/
  void setTimerInterval(double ms);

  /*!************************************************************************
   *  \brief  This method load the tml file.
   *************************************************************************/
  void loadFile();

  /*!********************************************************************************************************************
   *  \brief      This callback is executed when the list of behaviors is modified.
   *  \details    It's purpose is to control the state of the drone and the actions the GUI should allow the user to
   *execute.
   *********************************************************************************************************************/
  void newBehaviorCallback(const droneMsgsROS::ListOfBehaviors& msg);

  /*!************************************************************************
   *  \brief   Processes the status of the current mission.
   ***************************************************************************/
  void completedMissionCallback(const droneMsgsROS::CompletedMission::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the battery status.
   ***************************************************************************/
  void batteryCallback(const droneMsgsROS::battery::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Receives the state of the WiFi connection
   **************************************************************************/
  void wifiConnectionCheckCallback(const std_msgs::Bool::ConstPtr& msg);

public Q_SLOTS:

  /*!********************************************************************************************************************
   *  \brief      This method takes action when the user wants to make the drone to land.
   *********************************************************************************************************************/
  void landTmlButton();

  /*!********************************************************************************************************************
   *  \brief      This method starts a tml execution
   *********************************************************************************************************************/
  void executeTmlMission();

  /*!********************************************************************************************************************
   *  \brief      This method cancels the current tml execution
   *********************************************************************************************************************/
  void abortTmlMission();

  /*!********************************************************************************************************************
   *  \brief      This method informs about the time the drone has been flying.
   *********************************************************************************************************************/
  void flightTime();

  /*!********************************************************************************************************************
   *  \brief      This method sets the state of the mission to stopped
   *********************************************************************************************************************/
  void setMissionStopped();

Q_SIGNALS:
  /*!********************************************************************************************************************
   *  \brief      Triggered when mission starts
   *********************************************************************************************************************/
  void missionStarted();
  /*!********************************************************************************************************************
   *  \brief      Triggered when mission is stopped
   *********************************************************************************************************************/
  void missionStopped();
};

#endif  // TML_MISSION_CONTROL_WINDOW_H
