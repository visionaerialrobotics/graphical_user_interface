/*!*******************************************************************************************
 *  \file       tool_widget.h
 *  \brief      ToolWidget definition file.
 *  \details    In charge of setting the mouse item and function.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef TOOL_WIDGET_H
#define TOOL_WIDGET_H

#include <QToolButton>
#include <QWidget>
#include <QObject>
#include <iostream>
#include <QSignalMapper>
#include "ui_tool_widget.h"
#include <QBitmap>
#include <QKeyEvent>

namespace Ui
{
class ToolWidget;
}

class ToolWidget : public QWidget
{
  Q_OBJECT

public:
  explicit ToolWidget(QWidget* parent);
  ~ToolWidget();

  QToolButton* getTool();
  bool isSticky();
  void unsetTool();

private:
  Ui::ToolWidget* ui;

  QToolButton* button1;
  QToolButton* button2;
  QToolButton* button3;
  QToolButton* button4;
  QToolButton* button5;
  QToolButton* button6;
  QToolButton* button7;

  QToolButton* last_pressed = NULL;

  QSignalMapper* mapper;

  bool sticky = false;

public Q_SLOTS:

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when a tool is selected
   **********************************************************************************************************************/
  void onClickedButton(QWidget* b);

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when sticky cursor is selected/deselected
   **********************************************************************************************************************/
  void onClickedStikyButton();
};

#endif
