/*!*******************************************************************************************
 *  \file       pole_widget.h
 *  \brief      PoleWidget definition file.
 *  \details    PoleWidget is the widget used for poles representation.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef POLE_WIDGET_H
#define POLE_WIDGET_H

#include <QWidget>
#include <QObject>
#include <iostream>
#include <QSignalMapper>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include "ui_pole_widget.h"
#include "object_controller.h"

namespace Ui
{
class PoleWidget;
}

class PoleWidget : public QWidget
{
  Q_OBJECT

public:
  explicit PoleWidget(QWidget* parent, ObjectController::Pole pole, int i);
  ~PoleWidget();

private:
  Ui::PoleWidget* ui;
  int internal_id;
  ObjectController::Pole pole;
  QSpinBox* spin_id;
  QDoubleSpinBox* spin_x_size;
  QDoubleSpinBox* spin_y_size;
  QDoubleSpinBox* spin_x_coor;
  QDoubleSpinBox* spin_y_coor;
  QSpinBox* spin_aruco0_id;
  QSpinBox* spin_aruco1_id;
  QSpinBox* spin_aruco2_id;
  QSpinBox* spin_aruco3_id;
  QPushButton* accept;

public Q_SLOTS:
  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the Accept button is clicked
   **********************************************************************************************************************/
  void acceptButton();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed whenever a pole's characteristic value is changed
   **********************************************************************************************************************/
  void valueChanged();

Q_SIGNALS:
  /*!********************************************************************************************************************
   *  \brief      This signal is emitted when the Accept button is clicked
   **********************************************************************************************************************/
  void closed();

  /*!********************************************************************************************************************
   *  \brief      This signal is emitted when the pole's characteristics values have changed
   **********************************************************************************************************************/
  void poleChanged(ObjectController::Pole pole, int i);
};

#endif
