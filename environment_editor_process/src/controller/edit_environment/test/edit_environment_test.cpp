/*!*******************************************************************************************
*  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include <cstdio>
#include <gtest/gtest.h>
#include <ros/ros.h>
#include <iostream>

ros::NodeHandle* nh;

TEST(GraphicalUserInterfaceTests, environmentEditorTest)
{
  ros::NodeHandle nh;

  std::string estimated_pose_str;

  std::string drone_id;
  std::string drone_id_namespace;

  int total_subtests = 0;
  int passed_subtests = 0;

  nh.param<std::string>("drone_id", drone_id, "1");
  nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);



  system(
      ("xfce4-terminal  \ --tab --title \"Environment Viewer\"  --command \"bash -c 'roslaunch edit_environment edit_environment.launch --wait \
           drone_id_namespace:=" +
       drone_id_namespace + " \
           drone_id_int:=" +
       drone_id + "  \
           drone_pose_subscription:=estimated_pose \
           drone_speeds_subscription:=estimated_speeds \
           prefixed_layout:=center \
           my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
          .c_str());

  std::string response;

  total_subtests++;
  std::cout << "\033[1;34m Does a window called Environment Editor appear? (Y/N)\033[0m" << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem showing widget\033[0m\n" << std::endl;
  }
   else
  {
    passed_subtests++;
  }

  total_subtests++;
  EXPECT_TRUE(response == "Y" || response == "y");

    std::cout << "\033[1;34m Click on 'New' button. Does a dialog called 'Create new map' appear? (Y/N)\033[0m" << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem showing Creating New Map dialog\033[0m\n" << std::endl;
  }
   else
  {
    passed_subtests++;
  }

  total_subtests++;

  EXPECT_TRUE(response == "Y" || response == "y");
  std::cout << "\033[1;34m Click in 'Accept' button. Does a new empty squared map appear? (Y/N)\033[0m" << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem  creating new map\033[0m\n" << std::endl;
  }
   else
  {
    passed_subtests++;
  }

  total_subtests++;
  EXPECT_TRUE(response == "Y" || response == "y");

    std::cout << "\033[1;34m Click on the wall icon and then click in some point in the map. Does a square (obstacle) appear in the clicked point? (Y/N)\033[0m" << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem with creating new obstacles\033[0m\n" << std::endl;
  }
   else
  {
    passed_subtests++;
  }

  total_subtests++;

  EXPECT_TRUE(response == "Y" || response == "y");

      std::cout << "\033[1;34m Click on the arrow icon and then click on the recently created obstacle. Does the Wall configuration appear in the right side of the window? (Y/N)\033[0m" << std::endl;
  getline(std::cin, response);
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem with showing item configuration\033[0m\n" << std::endl;
  }
   else
  {
    passed_subtests++;
  }



  EXPECT_TRUE(response == "Y" || response == "y");

  std::cout << "\033[1;33m TOTAL SUBTESTS: " << total_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m SUBTESTS PASSED: " << passed_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m % PASSED: " << (passed_subtests * 100.0) / (total_subtests * 1.0) << "\033[0m" << std::endl;

}

/*--------------------------------------------*/
/*  Main  */
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;
  return RUN_ALL_TESTS();
}
