/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  EditEnvironment
  @author  Daniel Del Olmo, Jorge Luis Pascual, Carlos Valencia.
  @date    04-2018
  @version 3.0
*/
#include "../include/edit_environment.h"

EditEnvironment::EditEnvironment(int argc, char** argv, QWidget* parent) : QWidget(parent), ui(new Ui::EditEnvironment)
{
  std::locale::global(std::locale::classic());
  ui->setupUi(this);

  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);
  raise();
  setWindowIcon(QIcon(":/img/img/edit_environment.png"));

  layout = ui->gridLayout;

  tool_widget = new ToolWidget(this);
  object_controller = new ObjectController(this);
  tools_label = new QLabel("Tools");
  config_file_manager = new ConfigFileManager();
  new_map_button = new QPushButton("New");
  load_map_button = new QPushButton("Import");
  save_map_button = new QPushButton("Export");
  cancel_map_button = new QPushButton("Cancel");
  accept_map_button = new QPushButton("Accept");
  layout_buttons = new QGridLayout(0);

  layout->addWidget(tool_widget, 3, 0, Qt::AlignLeft);
  object_controller->setToolWidget(tool_widget);
  layout->addWidget(tools_label, 2, 0);
  layout->addWidget(object_controller, 1, 0, 1, 7);
  layout_buttons->addWidget(new_map_button, 0, 0);
  layout_buttons->addWidget(load_map_button, 0, 1);
  layout_buttons->addWidget(save_map_button, 0, 2);
  layout_buttons->addWidget(cancel_map_button, 0, 3);
  layout_buttons->addWidget(accept_map_button, 0, 4);
  layout->addLayout(layout_buttons, 3, 4, 1, 3);

  // Default namespace
  rosnamespace.append("/drone1");
  this->rosnamespace = rosnamespace;

  // Nodes
  if (!n.getParam("my_stack_directory", my_stack_directory))
    my_stack_directory = "$(env AEROSTACK_STACK)";
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("configuration_folder", configuration_folder_service, "configuration_folder");

  n.param<std::string>("window_opened", window_opened_topic, "window_opened");
  n.param<std::string>("window_closed", window_closed_topic, "window_closed");

  // Service communications
  configuration_folder_client = n.serviceClient<droneMsgsROS::configurationFolder>("/" + configuration_folder_service);
  configuration_folder_client.call(req, res);
  configuration_folder = res.folder;
  std::cout << "CONFIGURATION FOLDER: " << configuration_folder << std::endl;
  if (configuration_folder == "")
    configuration_folder = my_stack_directory + "/configs" + rosnamespace.c_str();

  // Subscribers
  window_opened_subs =
      n.subscribe("/" + drone_id_namespace + "/" + window_opened_topic, 10, &EditEnvironment::windowOpenCallback, this);

  // Publishers
  window_closed_publ =
      n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace + "/" + window_closed_topic, 1, true);

  // Connects
  QObject::connect(new_map_button, SIGNAL(clicked()), this, SLOT(createNewMapMenu()));
  QObject::connect(accept_map_button, SIGNAL(clicked()), this, SLOT(exportMap()));
  QObject::connect(save_map_button, SIGNAL(clicked()), this, SLOT(saveMap()));
  QObject::connect(load_map_button, SIGNAL(clicked()), this, SLOT(loadMap()));
  QObject::connect(cancel_map_button, SIGNAL(clicked()), this, SLOT(cancelMap()));
  connectEnvironmentWidgetObjectController();

  // Load map file
  namespace pt = boost::property_tree;

  std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                        "graphical_user_interface/layouts/layout.json");

  pt::read_json(layout_dir, root);

  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;

  int height = root.get<int>("1.height");
  int width = root.get<int>("1.width");

  this->resize(width, height);
  this->move(x0 + root.get<int>("1.position.x"), y0 + root.get<int>("1.position.y"));
  importMap();
}

EditEnvironment::~EditEnvironment()
{
  delete ui;
  delete object_controller;
  delete layout;
  delete config_file_manager;
  delete tool_widget;
}

void EditEnvironment::connectEnvironmentWidgetObjectController()
{
  QObject::connect(object_controller, SIGNAL(createDroneMenu(ObjectController::Drone, int)), this,
                   SLOT(createDroneMenu(ObjectController::Drone, int)));
  QObject::connect(object_controller, SIGNAL(createWallMenu(ObjectController::Wall, int)), this,
                   SLOT(createWallMenu(ObjectController::Wall, int)));
  QObject::connect(object_controller, SIGNAL(createPoleMenu(ObjectController::Pole, int)), this,
                   SLOT(createPoleMenu(ObjectController::Pole, int)));
  QObject::connect(object_controller, SIGNAL(createLandmarkMenu(ObjectController::Landmark, int)), this,
                   SLOT(createLandmarkMenu(ObjectController::Landmark, int)));
  QObject::connect(object_controller, SIGNAL(unselect()), this, SLOT(deleteMenu()));
}

void EditEnvironment::createNewMapMenu()
{
  new_map_menu = new NewMapMenu(0, object_controller);
  new_map_menu->setAttribute(Qt::WA_DeleteOnClose, true);
  new_map_menu->show();
}

void EditEnvironment::createDroneMenu(ObjectController::Drone drone, int i)
{
  if (!null)
    widget_config->close();
  DroneWidget* drone_widget = new DroneWidget(this, drone, i);
  drone_widget->setAttribute(Qt::WA_DeleteOnClose, true);

  layout->addWidget(object_controller, 1, 0, 1, 5);
  layout->addWidget(drone_widget, 1, 5, 1, 2);

  widget_config = drone_widget;

  QObject::connect(drone_widget, SIGNAL(closed()), this, SLOT(deleteMenu()));
  QObject::connect(drone_widget, SIGNAL(droneChanged(ObjectController::Drone, int)), object_controller,
                   SLOT(changeDrone(ObjectController::Drone, int)));
  QObject::connect(drone_widget, SIGNAL(closed()), object_controller, SLOT(unselectItem()));
  null = false;
}

void EditEnvironment::createWallMenu(ObjectController::Wall w, int i)
{
  if (!null)
    widget_config->close();
  WallWidget* wall_widget = new WallWidget(this, w, i);
  wall_widget->setAttribute(Qt::WA_DeleteOnClose, true);

  layout->addWidget(object_controller, 1, 0, 1, 5);
  layout->addWidget(wall_widget, 1, 5, 1, 2);

  widget_config = wall_widget;

  QObject::connect(wall_widget, SIGNAL(closed()), this, SLOT(deleteMenu()));
  QObject::connect(wall_widget, SIGNAL(wallChanged(ObjectController::Wall, int)), object_controller,
                   SLOT(changeWall(ObjectController::Wall, int)));
  QObject::connect(wall_widget, SIGNAL(closed()), object_controller, SLOT(unselectItem()));
  null = false;
}

void EditEnvironment::createPoleMenu(ObjectController::Pole p, int i)
{
  if (!null)
    widget_config->close();
  PoleWidget* pole_widget = new PoleWidget(this, p, i);
  pole_widget->setAttribute(Qt::WA_DeleteOnClose, true);

  layout->addWidget(object_controller, 1, 0, 1, 5);
  layout->addWidget(pole_widget, 1, 5, 1, 2);

  widget_config = pole_widget;
  QObject::connect(pole_widget, SIGNAL(closed()), this, SLOT(deleteMenu()));
  QObject::connect(pole_widget, SIGNAL(poleChanged(ObjectController::Pole, int)), object_controller,
                   SLOT(changePole(ObjectController::Pole, int)));
  QObject::connect(pole_widget, SIGNAL(closed()), object_controller, SLOT(unselectItem()));
  null = false;
}

void EditEnvironment::createLandmarkMenu(ObjectController::Landmark land, int i)
{
  if (!null)
    widget_config->close();

  LandmarkWidget* landmark_widget = new LandmarkWidget(this, land, i);
  landmark_widget->setAttribute(Qt::WA_DeleteOnClose, true);

  layout->addWidget(object_controller, 1, 0, 1, 5);
  layout->addWidget(landmark_widget, 1, 5, 1, 2);

  widget_config = landmark_widget;

  QObject::connect(landmark_widget, SIGNAL(closed()), this, SLOT(deleteMenu()));
  QObject::connect(landmark_widget, SIGNAL(landmarkChanged(ObjectController::Landmark, int)), object_controller,
                   SLOT(changeLandmark(ObjectController::Landmark, int)));
  QObject::connect(landmark_widget, SIGNAL(closed()), object_controller, SLOT(unselectItem()));
  null = false;
}

void EditEnvironment::deleteMenu()
{
  std::cout << "deleteMenu" << std::endl;
  if (!null)
  {
    std::cout << "cierra" << std::endl;
    widget_config->close();
  }
  layout->addWidget(object_controller, 1, 0, 1, 7);
  null = true;
}

void EditEnvironment::saveMap()
{
  QString filename =
      QFileDialog::getSaveFileName(this, tr("Save File"), configuration_folder.c_str(), "XML File (*.xml)");
  std::string filestd = filename.toStdString();
  if (filestd.find(".xml") == std::string::npos && filename != "")
  {
    filestd = filestd + ".xml";
    ObjectController::Objects objects = object_controller->getObjects();
    if (config_file_manager->saveObjects(filestd, objects))
    {
      map_file_msg.setWindowTitle(QString::fromStdString("Info"));
      map_file_msg.setText(QString::fromStdString("Map file has been saved in the following route: \"%1\"")
                               .arg(QString::fromStdString(filestd)));
      map_file_msg.setWindowFlags(map_file_msg.windowFlags() | Qt::WindowStaysOnTopHint);
      map_file_msg.exec();
    }
  }
}

void EditEnvironment::loadMap()
{
  QString filename =
      QFileDialog::getOpenFileName(this, tr("Open File"), configuration_folder.c_str(), "XML File (*.xml)");
  if (filename != "")
  {
    ObjectController::Objects objects;
    objects = config_file_manager->loadObjects(filename.toStdString());
    object_controller->setObjects(objects);

    map_file_msg.setWindowTitle(QString::fromStdString("Info"));
    map_file_msg.setText(QString::fromStdString("Map file has been loaded in the following route: \"%1\"")
                             .arg(QString::fromStdString(configuration_folder)));
    map_file_msg.setWindowFlags(map_file_msg.windowFlags() | Qt::WindowStaysOnTopHint);
    map_file_msg.exec();
  }
}

void EditEnvironment::exportMap()
{
  bool res = config_file_manager->exportObjects(object_controller);
  if (res)
    this->close();
}

void EditEnvironment::importMap()
{
  ObjectController::Objects objects;
  objects = config_file_manager->importObjects();
  object_controller->setObjects(objects);
}

void EditEnvironment::cancelMap()
{
  QMessageBox cancel_message_window(QMessageBox::Critical, tr("Save file?"),
                                    tr("Your changes will be lost if you cancel.\n\nSave changes to \"%1\" before "
                                       "closing?")
                                        .arg(QString::fromStdString(configuration_folder)));
  cancel_message_window.setWindowFlags(cancel_message_window.windowFlags() | Qt::WindowStaysOnTopHint);

  m_save_button = cancel_message_window.addButton(tr("Save"), QMessageBox::YesRole);

  m_cancel_button = cancel_message_window.addButton(tr("Cancel"), QMessageBox::NoRole);

  m_dont_save_button = cancel_message_window.addButton(tr("Don't Save"), QMessageBox::NoRole);
  cancel_message_window.exec();

  if (cancel_message_window.clickedButton() == m_save_button)
  {
    exportMap();
    this->close();
  }

  if (cancel_message_window.clickedButton() == m_dont_save_button)
  {
    this->close();
  }
}

void EditEnvironment::clearTrajectories()
{
  object_controller->clearTrajectories();
}

void EditEnvironment::closeEvent(QCloseEvent* event)
{
  windowClosedMsgs.id = aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT;
  window_closed_publ.publish(windowClosedMsgs);
}

void EditEnvironment::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void EditEnvironment::windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg)
{
  windowOpenedMsgs = msg;
  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_MISSION_INTERPRETER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::KEYBOARD_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::ALPHANUMERIC_INTERFACE_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::TML_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_PYTHON ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_TML)
  {
    windowClosedMsgs.id = aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT;
    window_closed_publ.publish(windowClosedMsgs);
    killMe();
  }
  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::MINIMIZE_MAIN_WINDOW)
  {
    showMinimized();
  }
}
