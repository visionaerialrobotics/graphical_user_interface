/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  ConfigFileManager
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/

#include "../include/config_file_manager.h"

ConfigFileManager::ConfigFileManager() : QObject()
{
  n.param<std::string>("stackPath", my_stack_directory, "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack");
  config_dir = my_stack_directory + "/configs";
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");

  n.param<std::string>("configuration_folder", configuration_folder_service, "configuration_folder");
  configuration_folder_client = n.serviceClient<droneMsgsROS::configurationFolder>("/" + configuration_folder_service);

  configuration_folder_client.call(req, res);
  configuration_folder = res.folder;
}

ConfigFileManager::~ConfigFileManager()
{
}

bool ConfigFileManager::exportObjects(QWidget* object_controller)
{
  ObjectController* controller = (ObjectController*)object_controller;
  bool res = true;
  if (controller->isMapSet())
  {
    std::cout << "Exporting data in " << config_dir << std::endl;
    objects = controller->getObjects();
    for (int i = 0; i < objects.drones.size(); i++)
    {
      res = res && exportDrone(i);
      if (!res)
      {
        break;
      }
    }
  }
  else
  {
    std::cout << "Map is not set" << std::endl;
  }
  return res;
}

bool ConfigFileManager::exportDrone(int i)
{
  if (configuration_folder != "")
  {
    configuration_folder_client.call(req, res);
    drone_dir = res.folder;
  }
  else
  {
    /*Import drone from drone_id_namespace*/
    drone_dir = config_dir + "/" + drone_id_namespace;
  }
  bool res = exportObstaclesData();
  if (!res)
  {
    return false;
  }
  exportMapData();
  exportDroneData(i);
  exportArucosData();
  return true;
}

void ConfigFileManager::exportMapData()
{
  pugi::xml_document document;
  filename = drone_dir + "/configFile.xml";
  document.load_file(filename.c_str());
  pugi::xml_node node = document;
  std::string str = boost::str(boost::format("%.4d %.4d") % objects.map.x_meters % objects.map.y_meters);
  node.child("map").child("config").child("dimensions").text().set(str.c_str());
  str = boost::str(boost::format("%.4d %.4d") % objects.map.x_init % objects.map.y_init);
  node.child("map").child("config").child("initPoint").text().set(str.c_str());
  document.save_file(filename.c_str(), PUGIXML_TEXT("  "));
  std::cout << "Document" << filename << " saved" << std::endl;
}

void ConfigFileManager::exportDroneData(int i)
{
  /*Open the archive*/
  pugi::xml_document document;
  filename = drone_dir + "/ekf_state_estimator_config.xml";
  document.load_file(filename.c_str());
  pugi::xml_node node = document;

  // Get data and replace it
  std::string str = boost::str(boost::format(" %.4d ") % objects.drones[i].x_coor);
  node.child("take_off_site").child("position").child("x").text().set(str.c_str());
  str = boost::str(boost::format(" %.4d ") % objects.drones[i].y_coor);
  node.child("take_off_site").child("position").child("y").text().set(str.c_str());
  str = boost::str(boost::format(" %.4d ") % 0);
  node.child("take_off_site").child("position").child("z").text().set(str.c_str());
  str = boost::str(boost::format(" %.4d ") % objects.drones[i].degrees);
  node.child("take_off_site").child("attitude").child("yaw").text().set(str.c_str());

  // document.save(std::cout);
  document.save_file(filename.c_str(), PUGIXML_TEXT("  "));
  std::cout << "Document" << filename << " saved" << std::endl;

  /*Open the archive*/
  filename = drone_dir + "/params_localization_obs.xml";
  document.load_file(filename.c_str());
  node = document;

  // Get data and replace it
  str = boost::str(boost::format(" %.4d ") % objects.drones[i].x_coor);
  node.child("main").child("params").child("x_0").text().set(str.c_str());
  str = boost::str(boost::format(" %.4d ") % objects.drones[i].y_coor);
  node.child("main").child("params").child("y_0").text().set(str.c_str());
  str = boost::str(boost::format(" %.4d ") % objects.drones[i].take_off);
  node.child("main").child("params").child("z_0").text().set(str.c_str());
  str = boost::str(boost::format(" %.4d ") % objects.drones[i].degrees);
  node.child("main").child("params").child("yaw_0").text().set(str.c_str());

  // document.save(std::cout);
  document.save_file(filename.c_str(), PUGIXML_TEXT("  "));

  /*Open the archive*/
  filename = drone_dir + "/configFile.xml";
  document.load_file(filename.c_str());
  node = document;

  // Get data and replace it
  str = boost::str(boost::format("%.4d %.4d") % objects.drones[i].x_size % objects.drones[i].x_size);
  node.child("robot").child("dimensions").text().set(str.c_str());

  // document.save(std::cout);
  document.save_file(filename.c_str(), PUGIXML_TEXT("  "));
}

bool ConfigFileManager::exportObstaclesData()
{
  pugi::xml_document document;
  filename = drone_dir + "/obstacle_config.xml";
  document.load_file(filename.c_str());
  pugi::xml_node node = document;

  // Check obstacles id
  std::vector<int> checked;
  bool res = true;
  for (auto object : objects.walls)
  {
    for (int j : checked)
    {
      if (res && object.id == j)
      {
        res = false;
      }
      else if (!res)
      {
        break;
      }
    }
    if (res)
    {
      checked.push_back(object.id);
    }
    else
    {
      break;
    }
  }

  for (auto object : objects.poles)
  {
    for (int j : checked)
    {
      if (res && object.id == j)
      {
        res = false;
      }
      else if (!res)
      {
        break;
      }
    }
    if (res)
    {
      checked.push_back(object.id);
    }
    else
    {
      break;
    }
  }

  for (auto object : objects.landmarks)
  {
    for (int j : checked)
    {
      if (res && object.id == j)
      {
        res = false;
      }
      else if (!res)
      {
        break;
      }
    }
    if (res)
    {
      checked.push_back(object.id);
    }
    else
    {
      break;
    }
  }

  if (!res)
  {
    QMessageBox error_message;
    error_message.setWindowTitle(QString::fromStdString("Parameters error"));
    error_message.setText(QString::fromStdString("There is a object id repeated. Please fix it before exporting"));
    error_message.exec();
    return false;
  }

  // Clean obstacles node

  node.child("map").remove_child("obstaclesInMap");
  node.child("map").append_child("obstaclesInMap");
  std::string str;
  for (int i = 0; i < objects.walls.size(); i++)
  {
    pugi::xml_node rectangle = node.child("map").child("obstaclesInMap").append_child("rectangle");
    rectangle.append_attribute("description").set_value(objects.walls[i].description.c_str());
    node.child("map")
        .child("obstaclesInMap")
        .insert_child_before(pugi::node_comment, rectangle)
        .set_value(objects.walls[i].description.c_str());
    rectangle.append_child("id").text().set(std::to_string(objects.walls[i].id).c_str());
    str = boost::str(boost::format("%.4d %.4d") % objects.walls[i].x_coor % objects.walls[i].y_coor);
    rectangle.append_child("centerPoint").text().set(str.c_str());
    str = boost::str(boost::format("%.4d %.4d") % objects.walls[i].x_size % objects.walls[i].y_size);
    rectangle.append_child("size").text().set(str.c_str());
    str = boost::str(boost::format("%.4d") % objects.walls[i].degrees);
    rectangle.append_child("yawAngle").text().set(str.c_str());
    pugi::xml_node virt = rectangle.append_child("virtual");
    virt.append_attribute("description").set_value(objects.walls[i].virtual_description.c_str());
    virt.text().set(std::to_string(1).c_str());
  }

  for (int i = 0; i < objects.poles.size(); i++)
  {
    pugi::xml_node pole = node.child("map").child("obstaclesInMap").append_child("pole");
    pole.append_attribute("description").set_value("Pole");
    node.child("map").child("obstaclesInMap").insert_child_before(pugi::node_comment, pole).set_value("Pole");
    pole.append_child("id").text().set(std::to_string(objects.poles[i].id).c_str());
    str = boost::str(boost::format("%.4d %.4d") % objects.poles[i].x_radius % objects.poles[i].y_radius);
    pole.append_child("radius").text().set(str.c_str());
    pugi::xml_node aruco = pole.append_child("aruco");
    for (int j = 0; j < 4; j++)
    {
      aruco.append_child("id")
          .append_attribute("aruco_id")
          .set_value(std::to_string(objects.poles[i].aruco[j].id).c_str());
    }
  }
  for (int i = 0; i < objects.landmarks.size(); i++)
  {
    pugi::xml_node land = node.child("map").child("obstaclesInMap").append_child("pole");
    land.append_attribute("description").set_value("Landmark");
    node.child("map").child("obstaclesInMap").insert_child_before(pugi::node_comment, land).set_value("Landmark");
    land.append_child("id").text().set(std::to_string(objects.landmarks[i].id).c_str());
    str =
        boost::str(boost::format("%.4d %.4d") % (objects.landmarks[i].x_size / 2) % (objects.landmarks[i].y_size / 2));
    // Landmark radius is 0.15
    land.append_child("radius").text().set(str.c_str());
    pugi::xml_node aruco = land.append_child("aruco");
    for (int j = 0; j < 2; j++)
    {
      aruco.append_child("id")
          .append_attribute("aruco_id")
          .set_value(std::to_string(objects.landmarks[i].aruco[j].id).c_str());
    }
  }
  document.save_file(filename.c_str(), PUGIXML_TEXT("  "));
  std::cout << "Document" << filename << " saved" << std::endl;

  return true;
}

void ConfigFileManager::exportArucosData()
{
  pugi::xml_document document;
  filename = drone_dir + "/params_localization_obs.xml";
  document.load_file(filename.c_str());
  pugi::xml_node node = document;
  node.child("main").child("params").remove_child("Arucos");
  pugi::xml_node arucos = node.child("main").child("params").append_child("Arucos");
  arucos.append_attribute("description")
      .set_value("List of visual markers considered as landmarks (previously known pose)");
  std::string str;
  for (int i = 0; i < objects.poles.size(); i++)
  {
    arucos.append_child(pugi::node_comment).set_value((" Pole " + std::to_string(objects.poles[i].id)).c_str());
    for (int j = 0; j < 4; j++)
    {
      pugi::xml_node aruco = arucos.append_child("Aruco");
      aruco.append_attribute("id").set_value(std::to_string(objects.poles[i].aruco[j].id).c_str());
      str = boost::str(boost::format("%.4d") % objects.poles[i].aruco[j].x_coor);
      aruco.append_attribute("x").set_value(str.c_str());
      str = boost::str(boost::format("%.4d") % objects.poles[i].aruco[j].y_coor);
      aruco.append_attribute("y").set_value(str.c_str());
      str = boost::str(boost::format("%.4d") % objects.poles[i].aruco[j].z_coor);
      aruco.append_attribute("z").set_value(str.c_str());
      str = boost::str(boost::format("%.4d") % 0);
      aruco.append_attribute("roll").set_value(str.c_str());
      aruco.append_attribute("pitch").set_value(str.c_str());
      str = boost::str(boost::format("%.4d") % objects.poles[i].aruco[j].degrees);
      aruco.append_attribute("yaw").set_value(str.c_str());
    }
  }
  for (int i = 0; i < objects.landmarks.size(); i++)
  {
    arucos.append_child(pugi::node_comment).set_value((" Landmark " + std::to_string(objects.landmarks[i].id)).c_str());
    for (int j = 0; j < 2; j++)
    {
      pugi::xml_node aruco = arucos.append_child("Aruco");
      aruco.append_attribute("id").set_value(std::to_string(objects.landmarks[i].aruco[j].id).c_str());
      str = boost::str(boost::format("%.4d") % objects.landmarks[i].aruco[j].x_coor);
      aruco.append_attribute("x").set_value(str.c_str());
      str = boost::str(boost::format("%.4d") % objects.landmarks[i].aruco[j].y_coor);
      aruco.append_attribute("y").set_value(str.c_str());
      str = boost::str(boost::format("%.4d") % objects.landmarks[i].aruco[j].z_coor);
      aruco.append_attribute("z").set_value(str.c_str());
      str = boost::str(boost::format("%.4d") % 0);
      aruco.append_attribute("roll").set_value(str.c_str());
      aruco.append_attribute("pitch").set_value(str.c_str());
      str = boost::str(boost::format("%.4d") % objects.landmarks[i].aruco[j].degrees);
      aruco.append_attribute("yaw").set_value(str.c_str());
    }
  }
  document.save_file(filename.c_str(), PUGIXML_TEXT("  "));
  std::cout << "Document" << filename << " saved" << std::endl;
}

bool ConfigFileManager::saveObjects(std::string filename, ObjectController::Objects objects)
{
  if (filename != "" && objects.map.set)
  {
    pugi::xml_document document;
    pugi::xml_node node = document;
    node = node.append_child("configuration");
    node = node.append_child("map");
    node.append_child("x_meters").text().set(std::to_string(objects.map.x_meters).c_str());
    node.append_child("y_meters").text().set(std::to_string(objects.map.y_meters).c_str());
    node.append_child("x_init").text().set(std::to_string(objects.map.x_init).c_str());
    node.append_child("y_init").text().set(std::to_string(objects.map.y_init).c_str());
    node = node.parent();
    node = node.append_child("drones");
    for (int i = 0; i < objects.drones.size(); i++)
    {
      node = node.append_child("drone");
      node.append_child("id").text().set(std::to_string(objects.drones[i].id).c_str());
      node.append_child("x_size").text().set(std::to_string(objects.drones[i].x_size).c_str());
      node.append_child("y_size").text().set(std::to_string(objects.drones[i].y_size).c_str());
      node.append_child("x_coor").text().set(std::to_string(objects.drones[i].x_coor).c_str());
      node.append_child("y_coor").text().set(std::to_string(objects.drones[i].y_coor).c_str());
      node.append_child("degrees").text().set(std::to_string(objects.drones[i].degrees).c_str());
      node.append_child("take_off").text().set(std::to_string(objects.drones[i].take_off).c_str());
      node = node.parent();
    }
    node = node.parent();
    node = node.append_child("poles");
    for (int i = 0; i < objects.poles.size(); i++)
    {
      node = node.append_child("pole");
      node.append_child("id").text().set(std::to_string(objects.poles[i].id).c_str());
      node.append_child("x_radius").text().set(std::to_string(objects.poles[i].x_radius).c_str());
      node.append_child("y_radius").text().set(std::to_string(objects.poles[i].y_radius).c_str());
      node.append_child("x_size").text().set(std::to_string(objects.poles[i].x_size).c_str());
      node.append_child("y_size").text().set(std::to_string(objects.poles[i].y_size).c_str());
      node.append_child("x_coor").text().set(std::to_string(objects.poles[i].x_coor).c_str());
      node.append_child("y_coor").text().set(std::to_string(objects.poles[i].y_coor).c_str());
      node = node.append_child("arucos");
      for (int j = 0; j < 4; j++)
      {
        node = node.append_child("aruco");
        node.append_child("id").text().set(std::to_string(objects.poles[i].aruco[j].id).c_str());
        node.append_child("x_coor").text().set(std::to_string(objects.poles[i].aruco[j].x_coor).c_str());
        node.append_child("y_coor").text().set(std::to_string(objects.poles[i].aruco[j].y_coor).c_str());
        node.append_child("z_coor").text().set(std::to_string(objects.poles[i].aruco[j].z_coor).c_str());
        node.append_child("degrees").text().set(std::to_string(objects.poles[i].aruco[j].degrees).c_str());
        node = node.parent();
      }
      node = node.parent().parent();
    }
    node = node.parent();
    node = node.append_child("landmarks");
    for (int i = 0; i < objects.landmarks.size(); i++)
    {
      node = node.append_child("landmark");
      node.append_child("id").text().set(std::to_string(objects.landmarks[i].id).c_str());
      node.append_child("x_size").text().set(std::to_string(objects.landmarks[i].x_size).c_str());
      node.append_child("y_size").text().set(std::to_string(objects.landmarks[i].y_size).c_str());
      node.append_child("x_coor").text().set(std::to_string(objects.landmarks[i].x_coor).c_str());
      node.append_child("y_coor").text().set(std::to_string(objects.landmarks[i].y_coor).c_str());
      node.append_child("degrees").text().set(std::to_string(objects.landmarks[i].degrees).c_str());
      node = node.append_child("arucos");
      for (int j = 0; j < 2; j++)
      {
        node = node.append_child("aruco");
        node.append_child("id").text().set(std::to_string(objects.landmarks[i].aruco[j].id).c_str());
        node.append_child("x_coor").text().set(std::to_string(objects.landmarks[i].aruco[j].x_coor).c_str());
        node.append_child("y_coor").text().set(std::to_string(objects.landmarks[i].aruco[j].y_coor).c_str());
        node.append_child("z_coor").text().set(std::to_string(objects.landmarks[i].aruco[j].z_coor).c_str());
        node.append_child("degrees").text().set(std::to_string(objects.landmarks[i].aruco[j].degrees).c_str());
        node = node.parent();
      }
      node = node.parent().parent();
    }
    node = node.parent();
    node = node.append_child("walls");
    for (int i = 0; i < objects.walls.size(); i++)
    {
      node = node.append_child("wall");
      node.append_child("id").text().set(std::to_string(objects.walls[i].id).c_str());
      node.append_child("x_size").text().set(std::to_string(objects.walls[i].x_size).c_str());
      node.append_child("y_size").text().set(std::to_string(objects.walls[i].y_size).c_str());
      node.append_child("x_coor").text().set(std::to_string(objects.walls[i].x_coor).c_str());
      node.append_child("y_coor").text().set(std::to_string(objects.walls[i].y_coor).c_str());
      node.append_child("degrees").text().set(std::to_string(objects.walls[i].degrees).c_str());
      node.append_child("description").text().set(objects.walls[i].description.c_str());
      node.append_child("virtual_description").text().set(objects.walls[i].virtual_description.c_str());
      node = node.parent();
    }
    node = node.parent();
    document.save_file(filename.c_str(), PUGIXML_TEXT("    "));
  }
  else
  {
    std::cout << "Nothing to save" << std::endl;
    return false;
  }
  return true;
}

ObjectController::Objects ConfigFileManager::loadObjects(std::string filename)
{
  ObjectController::Objects objects;
  pugi::xml_document document;
  document.load_file(filename.c_str());
  pugi::xml_node node = document;
  pugi::xml_node temp = node.child("configuration").child("map");
  pugi::xml_node it;
  objects.map.set = true;
  objects.map.x_meters = temp.child("x_meters").text().as_double();
  objects.map.y_meters = temp.child("y_meters").text().as_double();
  objects.map.y_init = temp.child("x_init").text().as_double();
  objects.map.y_init = temp.child("y_init").text().as_double();

  temp = node.child("configuration").child("drones");
  for (it = temp.first_child(); it; it = it.next_sibling())
  {
    ObjectController::Drone drone;
    drone.id = it.child("id").text().as_int();
    drone.x_size = it.child("x_size").text().as_double();
    drone.y_size = it.child("y_size").text().as_double();
    drone.x_coor = it.child("x_coor").text().as_double();
    drone.y_coor = it.child("y_coor").text().as_double();
    drone.degrees = it.child("degrees").text().as_double();
    drone.take_off = it.child("take_off").text().as_double();
    objects.drones.push_back(drone);
  }

  temp = node.child("configuration").child("poles");
  for (it = temp.first_child(); it; it = it.next_sibling())
  {
    ObjectController::Pole pole;
    pole.id = it.child("id").text().as_int();
    pole.x_radius = it.child("x_radius").text().as_double();
    pole.y_radius = it.child("y_radius").text().as_double();
    pole.x_size = it.child("x_size").text().as_double();
    pole.y_size = it.child("y_size").text().as_double();
    pole.x_coor = it.child("x_coor").text().as_double();
    pole.y_coor = it.child("y_coor").text().as_double();
    pugi::xml_node it2 = it.child("arucos");
    int i = 0;
    for (it2 = it2.first_child(); it2; it2 = it2.next_sibling())
    {
      ObjectController::Aruco aruco;
      aruco.id = it2.child("id").text().as_int();
      aruco.x_coor = it2.child("x_coor").text().as_double();
      aruco.y_coor = it2.child("y_coor").text().as_double();
      aruco.z_coor = it2.child("z_coor").text().as_double();
      aruco.degrees = it2.child("degrees").text().as_double();
      pole.aruco[i] = aruco;
      i++;
    }
    objects.poles.push_back(pole);
  }

  temp = node.child("configuration").child("landmarks");
  for (it = temp.first_child(); it; it = it.next_sibling())
  {
    ObjectController::Landmark landmark;
    landmark.id = it.child("id").text().as_int();
    landmark.x_size = it.child("x_size").text().as_double();
    landmark.y_size = it.child("y_size").text().as_double();
    landmark.x_coor = it.child("x_coor").text().as_double();
    landmark.y_coor = it.child("y_coor").text().as_double();
    landmark.degrees = it.child("degrees").text().as_double();
    pugi::xml_node it2 = it.child("arucos");
    int i = 0;
    for (it2 = it2.first_child(); it2; it2 = it2.next_sibling())
    {
      ObjectController::Aruco aruco;
      aruco.id = it2.child("id").text().as_int();
      aruco.x_coor = it2.child("x_coor").text().as_double();
      aruco.y_coor = it2.child("y_coor").text().as_double();
      aruco.z_coor = it2.child("z_coor").text().as_double();
      aruco.degrees = it2.child("degrees").text().as_double();
      landmark.aruco[i] = aruco;
      i++;
    }
    objects.landmarks.push_back(landmark);
  }

  temp = node.child("configuration").child("walls");
  for (it = temp.first_child(); it; it = it.next_sibling())
  {
    ObjectController::Wall wall;
    wall.id = it.child("id").text().as_int();
    wall.x_size = it.child("x_size").text().as_double();
    wall.y_size = it.child("y_size").text().as_double();
    wall.x_coor = it.child("x_coor").text().as_double();
    wall.y_coor = it.child("y_coor").text().as_double();
    wall.degrees = it.child("degrees").text().as_double();
    wall.description = it.child("description").text().as_string();
    wall.virtual_description = it.child("virtual_description").text().as_string();
    objects.walls.push_back(wall);
  }
  return objects;
}

ObjectController::Objects ConfigFileManager::importObjects()
{
  ObjectController::Objects objects;
  pugi::xml_document document;
  pugi::xml_node node;
  pugi::xml_node temp;
  pugi::xml_node temp2;
  std::string str;
  std::vector<double> values;
  std::vector<ObjectController::Aruco> arucos;

  if (configuration_folder != "")
  {
    configuration_folder_client.call(req, res);
    drone_dir = res.folder;
  }
  else
  {
    /*Import drone from drone_id_namespace*/
    drone_dir = config_dir + "/" + drone_id_namespace;
  }
  /*Import map attributes*/
  filename = drone_dir + "/configFile.xml";  // configFile.xml
  std::cout << "FILENAME: " << filename << std::endl;
  document.load_file(filename.c_str());
  node = document;
  str = node.child("map").child("config").child("dimensions").text().as_string();
  values = returnValuesPtr(str);
  objects.map.set = true;
  objects.map.x_meters = values[0];
  objects.map.y_meters = values[1];
  str = node.child("map").child("config").child("initPoint").text().as_string();
  values = returnValuesPtr(str);
  objects.map.x_init = values[0];
  objects.map.y_init = values[1];

  /*Import drone attributes*/
  ObjectController::Drone drone;
  drone.id = std::stoi(drone_id_namespace.substr(5));
  str = node.child("robot").child("dimensions").text().as_string();
  values = returnValuesPtr(str);
  drone.x_size = values[0];
  drone.y_size = values[1];
  filename = drone_dir + "/ekf_state_estimator_config.xml";  // ekf_state_estimator_config.xml
  document.load_file(filename.c_str());
  node = document;
  str = node.child("take_off_site").child("position").child("x").text().as_string();
  values = returnValuesPtr(str);
  drone.x_coor = values[0];
  str = node.child("take_off_site").child("position").child("y").text().as_string();
  values = returnValuesPtr(str);
  drone.y_coor = values[0];
  str = node.child("take_off_site").child("attitude").child("yaw").text().as_string();
  values = returnValuesPtr(str);
  drone.degrees = values[0];

  filename = drone_dir + "/params_localization_obs.xml";  // params_localization_obs.xml
  document.load_file(filename.c_str());
  node = document;
  str = node.child("main").child("params").child("z_0").text().as_string();
  values = returnValuesPtr(str);
  drone.take_off = values[0];
  objects.drones.push_back(drone);

  /*Create aruco vector*/
  temp = node.child("main").child("params").child("Arucos");
  for (temp = temp.child("Aruco"); temp; temp = temp.next_sibling("Aruco"))
  {
    ObjectController::Aruco aruco;
    aruco.id = std::stoi(temp.attribute("id").value());
    aruco.x_coor = std::stod(temp.attribute("x").value());
    aruco.y_coor = std::stod(temp.attribute("y").value());
    aruco.z_coor = std::stod(temp.attribute("z").value());
    aruco.degrees = std::stod(temp.attribute("yaw").value());
    arucos.push_back(aruco);
  }

  filename = drone_dir + "/obstacle_config.xml";  // obstacle_config.xml
  document.load_file(filename.c_str());
  node = document;

  // Import walls

  temp = node.child("map").child("obstaclesInMap");
  for (temp = temp.child("rectangle"); temp; temp = temp.next_sibling("rectangle"))
  {
    ObjectController::Wall wall;
    wall.description = temp.attribute("description").value();
    wall.id = std::stoi(temp.child("id").text().as_string());

    str = temp.child("centerPoint").text().as_string();
    values = returnValuesPtr(str);
    wall.x_coor = values[0];
    wall.y_coor = values[1];

    str = temp.child("size").text().as_string();
    values = returnValuesPtr(str);
    wall.x_size = values[0];
    wall.y_size = values[1];

    wall.degrees = std::stod(temp.child("yawAngle").text().as_string());
    wall.virtual_description = temp.child("virtual").attribute("description").value();

    objects.walls.push_back(wall);
  }

  // Import poles and landmarks

  temp = node.child("map").child("obstaclesInMap");
  for (temp = temp.child("pole"); temp; temp = temp.next_sibling("pole"))
  {
    if (isPole(temp))
    {
      ObjectController::Pole pole;
      pole.id = std::stol(temp.child("id").text().as_string());
      str = temp.child("radius").text().as_string();
      values = returnValuesPtr(str);
      pole.x_radius = values[0];
      pole.y_radius = values[1];
      pole.x_size = values[0] * 2;
      pole.y_size = values[1] * 2;
      int i = 0;
      for (temp2 = temp.child("aruco").child("id"); temp2; temp2 = temp2.next_sibling("id"))
      {
        pole.aruco[i] = findArucoById(arucos, std::stol(temp2.attribute("aruco_id").value()));
        i++;
      }
      if (pole.aruco[0].id != 0 && pole.aruco[1].id != 0 && pole.aruco[2].id != 0 && pole.aruco[3].id != 0)
      {
        pole = returnCompletedPole(pole);
        objects.poles.push_back(pole);
      }
    }
    else
    {
      ObjectController::Landmark land;
      land.id = std::stol(temp.child("id").text().as_string());
      str = temp.child("radius").text().as_string();
      values = returnValuesPtr(str);
      land.x_size = values[0] * 2;
      land.y_size = values[1] * 2;
      int i = 0;
      for (temp2 = temp.child("aruco").child("id"); temp2; temp2 = temp2.next_sibling("id"))
      {
        land.aruco[i] = findArucoById(arucos, std::stol(temp2.attribute("aruco_id").value()));
        i++;
      }
      land = returnCompletedLandmark(land);
      objects.landmarks.push_back(land);
    }
  }
  return objects;
}

std::vector<double> ConfigFileManager::returnValuesPtr(std::string value)
{
  std::vector<double> result;
  char* copy;
  char* token;
  copy = strdup(value.c_str());
  token = strtok(copy, " \t");
  std::string str;
  while (token != NULL)
  {
    str = boost::str(boost::format("%.4d") % token);
    result.push_back(std::stod(str));
    token = strtok(NULL, " \t");
  }
  return result;
}

ObjectController::Aruco ConfigFileManager::findArucoById(std::vector<ObjectController::Aruco> arucos, int id)
{
  ObjectController::Aruco aruco;
  aruco.id = 0;  // If aruco id is 0, there is not an aruco with that id.
  bool found = false;
  for (int i = 0; i < arucos.size() && !found; i++)
  {
    if (arucos[i].id == id)
    {
      found = true;
      aruco = arucos[i];
    }
  }
  return aruco;
}

bool ConfigFileManager::isPole(pugi::xml_node node)
{
  int number = 0;
  pugi::xml_node temp;
  for (temp = node.child("aruco").child("id"); temp; temp = temp.next_sibling("id"))
  {
    number++;
  }
  if (number == 4)
  {
    return true;
  }
  return false;
}

ObjectController::Pole ConfigFileManager::returnCompletedPole(ObjectController::Pole pole)
{
  ObjectController::Pole pole_result;
  pole_result = pole;
  for (int i = 0; i < 4; i++)
  {
    if (mod(pole.aruco[i].degrees, 360) == 0)
    {
      pole_result.aruco[0] = pole.aruco[i];
    }
    else if (mod(pole.aruco[i].degrees, 360) == 90)
    {
      pole_result.aruco[1] = pole.aruco[i];
    }
    else if (mod(pole.aruco[i].degrees, 360) == 180)
    {
      pole_result.aruco[2] = pole.aruco[i];
    }
    else if (mod(pole.aruco[i].degrees, 360) == 270)
    {
      pole_result.aruco[3] = pole.aruco[i];
    }
  }
  pole_result.x_coor = pole_result.aruco[1].x_coor;
  pole_result.y_coor = pole_result.aruco[0].y_coor;
  return pole_result;
}

ObjectController::Landmark ConfigFileManager::returnCompletedLandmark(ObjectController::Landmark land)
{
  ObjectController::Landmark land_result;
  land_result = land;
  if (mod(land.aruco[0].degrees + 90, 360) == mod(land.aruco[1].degrees, 360))
  {
    land_result.aruco[0] = land.aruco[1];
    land_result.aruco[1] = land.aruco[0];
  }
  land_result.degrees = mod(land_result.aruco[0].degrees - 45, 360);
  land_result.x_coor = land_result.aruco[0].x_coor + (0.11 * sin(land_result.degrees * PI / 180));
  land_result.y_coor = land_result.aruco[0].y_coor - (0.11 * cos(land_result.degrees * PI / 180));
  land_result.x_coor = land_result.x_coor - (0.1 * cos(land_result.degrees * PI / 180));
  land_result.y_coor = land_result.y_coor - (0.1 * sin(land_result.degrees * PI / 180));
  return land_result;
}

double ConfigFileManager::mod(double a, double b)
{
  return fmod(fmod(a, b) + b, b);
}
