/*!*******************************************************************************************
 *  \file      tml_mission_loader.h
 *  \brief     Tml Mission Loader definition file.
 *  \details   The Tml Mission Loader allows loading tml mission file
 *  \author    Abraham Carrera, Daniel Del Olmo
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef TML_MISSION_LOADER_H
#define TML_MISSION_LOADER_H

#include <ros/ros.h>
#include <droneMsgsROS/openMissionFile.h>
#include "droneMsgsROS/configurationFolder.h"
#include "aerostack_msgs/WindowIdentifier.h"

#include <QWidget>
#include <QPushButton>
#include <QFileDialog>
#include <QApplication>
#include <QProcess>
#include <QMessageBox>
#include <QRect>
#include <QScreen>

#include <iostream>
#include <stdio.h>
#include <dirent.h>
#include <thread>

namespace Ui
{
class TmlMissionLoader;
}

class TmlMissionLoader : public QWidget
{
  Q_OBJECT

public:
  // Constructor & Destructor
  explicit TmlMissionLoader(int argc, char** argv, QWidget* parent = 0);
  ~TmlMissionLoader();

private:
  Ui::TmlMissionLoader* ui;

  ros::NodeHandle n;
  ros::ServiceClient configuration_folder_client;

  droneMsgsROS::configurationFolder::Request req;
  droneMsgsROS::configurationFolder::Response res;

  std::string tml_mission;
  std::string drone_id_namespace;
  std::string configuration_folder;
  std::string configuration_folder_service;
  std::string homePath;
  std::string default_folder;
  std::string file_route;
  std::string prefixed_layout;

  QFileDialog fileDialog;
  QMessageBox tml_file_msg;
  QRect screenGeometry;
  QScreen* screen;
  ros::Publisher window_closed_publ;
  ros::Subscriber window_opened_subs;

  std::string window_opened_topic;
  std::string window_closed_topic;

  aerostack_msgs::WindowIdentifier windowClosedMsgs;
  aerostack_msgs::WindowIdentifier windowOpenedMsgs;

  FILE *file, *file_mission;

  /*!********************************************************************************************************************
   *  \brief      This method let's the user select the mission plan he wants to execute
   *********************************************************************************************************************/
  void selectMissionPlan();

  /*!********************************************************************************************************************
   *  \brief      This method save the import file
   *********************************************************************************************************************/
  bool saveTmlFile(const std::string tml_mission);

  /*!************************************************************************
   *  \brief   Activated when a window is closed.
   ***************************************************************************/
  void windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg);
  /*!************************************************************************
   *  \brief  Kills the process
   ***************************************************************************/
  void killMe();

public Q_SLOTS:
  /*!********************************************************************************************************************
   *  \brief      This method notifies main window that the widget was closed
   *********************************************************************************************************************/
  void closeEvent(QCloseEvent* event);

Q_SIGNALS:
};

#endif  // TML_MISSION_LOADER_H
