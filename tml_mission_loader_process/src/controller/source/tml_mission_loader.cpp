/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  TmlMissionLoader
  @author  Abraham Carrera, Daniel Del Olmo
  @date    04-2018
  @version 3.0
*/

#include "../include/tml_mission_loader.h"

TmlMissionLoader::TmlMissionLoader(int argc, char** argv, QWidget* parent) : QWidget(parent)
{
  // window always on top
  setWindowIcon(QIcon(":/images/images/import_mission.png"));
  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);

  n.param<std::string>("prefixed_layout", prefixed_layout, "normal");

  screen = QGuiApplication::primaryScreen();
  screenGeometry = screen->geometry();

  this->move(screenGeometry.width() / 2 - this->width() / 2, screenGeometry.height() / 2 - this->height() / 2);

  // Default configuration folder
  homePath = QDir::homePath().toStdString();
  default_folder = homePath + "/workspace/ros/aerostack_catkin_ws/src/aerostack_stack/configs" + "/drone1";

  // Nodes
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("configuration_folder", configuration_folder_service, "configuration_folder");

  // Service communications
  configuration_folder_client = n.serviceClient<droneMsgsROS::configurationFolder>(configuration_folder_service);
  configuration_folder_client.call(req, res);
  configuration_folder = res.folder;
  n.param<std::string>("window_opened", window_opened_topic, "window_opened");
  n.param<std::string>("window_closed", window_closed_topic, "window_closed");

  // Subscribers
  window_opened_subs = n.subscribe("/" + drone_id_namespace + "/" + window_opened_topic, 10,
                                   &TmlMissionLoader::windowOpenCallback, this);

  // Publishers
  window_closed_publ =
      n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace + "/" + window_closed_topic, 1, true);

  // Load tml mission plan
  if (configuration_folder == "")
    file_route = default_folder + "/mission_specification_file.xml";

  else
    file_route = configuration_folder + "/mission_specification_file.xml";

  selectMissionPlan();
}

TmlMissionLoader::~TmlMissionLoader()
{
}

void TmlMissionLoader::selectMissionPlan()
{
  tml_mission =
      fileDialog.getOpenFileName(this, tr("Select TML Mission Plan"), "", tr("TML file (*.xml)")).toStdString();
  if (tml_mission != "")
  {
    if (saveTmlFile(tml_mission))  // if tml file saving is successful
    {
      tml_file_msg.setWindowTitle(QString::fromStdString("Import Completed"));
      tml_file_msg.setText(QString::fromStdString("Tml mission plan has been saving in the following route: \"%1\"")
                               .arg(QString::fromStdString(file_route)));
      tml_file_msg.setWindowFlags(tml_file_msg.windowFlags() | Qt::WindowStaysOnTopHint);
      tml_file_msg.exec();
    }

    else  // if tml file saving fails
    {
      tml_file_msg.setWindowTitle(QString::fromStdString("Import Error"));
      tml_file_msg.setText(QString::fromStdString("Unsecessfully imported tml mission plan"));
      tml_file_msg.setWindowFlags(tml_file_msg.windowFlags() | Qt::WindowStaysOnTopHint);
      tml_file_msg.exec();
    }
  }

  windowClosedMsgs.id = 16;
  window_closed_publ.publish(windowClosedMsgs);

  // killMe();
}

bool TmlMissionLoader::saveTmlFile(const std::string tml_mission)
{
  // std::cout << "File route: " << file_route << std::endl;

  file = fopen(file_route.c_str(), "w+");
  file_mission = fopen(tml_mission.c_str(), "r");

  if (file_mission == NULL || file == NULL)
    return false;

  char c[100];

  while (!feof(file_mission))
  {
    fgets(c, 100, file_mission);
    fputs(c, file);
  }

  fclose(file);
  fclose(file_mission);
  return true;
}

void TmlMissionLoader::closeEvent(QCloseEvent* event)
{
  windowClosedMsgs.id = 16;
  window_closed_publ.publish(windowClosedMsgs);
}

void TmlMissionLoader::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void TmlMissionLoader::windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg)
{
  aerostack_msgs::WindowIdentifier windowOpenedMsgs = msg;

  if (windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_MISSION_INTERPRETER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::PYTHON_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::ALPHANUMERIC_INTERFACE_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::TML_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::KEYBOARD_CONTROL ||
      windowOpenedMsgs.id == aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_PYTHON)
  {
    windowClosedMsgs.id = 16;
    window_closed_publ.publish(windowClosedMsgs);
    fileDialog.close();
    killMe();
  }
}
