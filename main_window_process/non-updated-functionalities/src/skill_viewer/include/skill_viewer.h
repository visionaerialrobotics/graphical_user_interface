/*!*******************************************************************************************
 *  \file       behaviour_viewer.h
 *  \brief      BehaviourManagerViewer definition file.
 *  \details    This file includes the BehaviourManagerViewer class declaration. To obtain more
 *              information about it's definition consult the performance_monitor.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/
#ifndef SKILLVIEWER_H
#define SKILLVIEWER_H

#include <QWidget>
#include <QDockWidget>

#include <QTableWidgetItem>
#include <QString>
#include <QTime>
#include "aerostack_msgs/ProcessError.h"
#include "aerostack_msgs/ProcessDescriptor.h"
#include "aerostack_msgs/ProcessDescriptorList.h"
#include "droneMsgsROS/SkillsList.h"
#include "droneMsgsROS/SkillState.h"
#include "../../connection/include/ros_graph_receiver.h"
#include "../../user_commander/include/user_commander.h"
#include <vector>
#include <iostream>

#include "ui_skillviewer.h"
#include <QDebug>
#include <QScrollBar>
#include <string>
#include <sstream>
#include <QMenu>
#include <QTableWidget>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

namespace Ui
{
class SkillViewer;
}

class SkillViewer : public QWidget
{
  Q_OBJECT

  int row;
  int error_counter;
  bool initialized_table;
  bool is_display_stopped;

public:
  explicit SkillViewer(QWidget* parent = 0, RosGraphReceiver* collector = 0, UserCommander* usercommander = 0);
  void setSignalHandlers();
  void updateSkillState();
  droneMsgsROS::SkillsList skill_list;
  enum processMonitorStates
  {
    Start,
    Stop,
    Reset
  };

  ~SkillViewer();
  droneMsgsROS::SkillDescriptor skill_containter;

public Q_SLOTS:
  void onSkillStateReceived();
  void updateSkillState(const droneMsgsROS::SkillDescriptor* skill_container, int row_skill_viewer);
  void initializeSkillViewerTable();
  void onTextFilterChange(const QString& arg1);
  void onCustomContextMenuRequested(const QPoint& pos);
  void showContextMenu(QTableWidgetItem* item, const QPoint& globalPos);
  void menuSelection(QAction* action);
  void clearFocus();

private:
  Ui::SkillViewer* ui;
  RosGraphReceiver* skill_receiver;
  UserCommander* user_command;
};

#endif  // SKILLVIEWER
