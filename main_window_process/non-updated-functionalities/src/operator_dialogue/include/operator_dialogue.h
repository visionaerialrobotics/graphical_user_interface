/*!*****************************************************************************
 *  \file      operator_dialogue.h
 *  \brief     Definition of all the classes used in the file
 *             operator_dialogue.cpp .
 *  \details   Receives and displays messages to simulate interaction with the drone
 *             and the interface, both in mission planning mode and cooperative
 *             interaction mode.
 *
 *  \author    German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef OPERATOR_DIALOGUE_H
#define OPERATOR_DIALOGUE_H

#include <QWidget>
#include <QString>

#include "../../control_panel/include/control_panel.h"
#include "../../connection/include/mission_state_receiver.h"
#include "../../connection/include/cooperative_receiver.h"

#include "ui_operator_dialogue.h"

// Modes
#define AUTONOMOUS 0
#define MANUAL 1
#define COOPERATIVE 2

// For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui
{
class OperatorDialogue;
}

class OperatorDialogue : public QWidget
{
  Q_OBJECT

public:
  // Constructor & Destructor
  explicit OperatorDialogue(QWidget* parent = 0, MissionStateReceiver* mission_receiver = 0,
                            CooperativeReceiver* coop_receiver = 0, ControlPanel* controlpanel = 0);
  ~OperatorDialogue();

public:
  /*!************************************************************************
   *  \brief  Establishes the Qt signal/slot connections
   *************************************************************************/
  void setSignalHandlers();

public Q_SLOTS:
  /*!************************************************************************
   *  \brief  This method displays a message showing that the operation mode has changed.
   *  \param  mode An int representing a mode change.
   *************************************************************************/
  void displayOperationModeChange(const int mode);

  /*!************************************************************************
   *  \brief  This method saves the name of the loaded mission.
   *  \param  mission_name The name of the mission that was loaded.
   *************************************************************************/
  void displayMissionLoaded(const QString& mission_name);

  /*!************************************************************************
   *  \brief  This method displays a message showing the current task.
   *  \param  current_task The undergoing task.
   *************************************************************************/
  void displayCurrentTask(const QString& current_task);

  /*!************************************************************************
   *  \brief  This method displays a message showing the current action.
   *  \param  current_action The undergoing action.
   *************************************************************************/
  void displayCurrentAction(const QString& current_action);

  /*!************************************************************************
   *  \brief  This method displays a message showing the completion status of the mission.
   *  \param  mission_completed Whether the mission was successfully completed or not.
   *************************************************************************/
  void displayMissionCompleted(const bool mission_completed);

  /*!************************************************************************
   *  \brief  This method displays a message showing the mission startup.
   *************************************************************************/
  void displayMissionStart();

  /*!************************************************************************
   *  \brief This method displays a message showing the completion status of an action.
   *  \param action_completed Whether the action was successfully completed or not.
   *  \param timeout The action timeout.
   *************************************************************************/
  void displayActionCompleted(const int action_completed_state, const int timeout);

  /*!************************************************************************
   *  \brief This method saves the current action arguments.
   *  \param action_arguments The current action arguments.
   *************************************************************************/
  void actionArgumentsReceived(const QString action_arguments);

  /*!************************************************************************
   *  \brief  This method displays a message when the Operator issues a command that should be shown in the dialogue.
   *  \param text The text to be shown.
   *************************************************************************/
  void operatorText(const QString text);

private:
  /*!************************************************************************
   *  \brief  This method prepares the default text and font that should be always shown when the Operator is sending
   *information/commands. \return The default QString to be shown
   *************************************************************************/
  QString setOperatorFont();

  /*!************************************************************************
   *  \brief  This method prepares the default text and font that should be always shown when the Vehicle is sending
   *information. \return The default QString to be shown
   *************************************************************************/
  QString setVehicleFont();

private:
  // Layout
  Ui::OperatorDialogue* ui;

  QString mission_name;
  bool mission_in_progress;
  QString current_action_arguments;

  MissionStateReceiver* mission_planner_receiver;
  ControlPanel* control_panel;
  CooperativeReceiver* cooperative_receiver;
};

#endif  // OPERATOR_DIALOGUE_H
