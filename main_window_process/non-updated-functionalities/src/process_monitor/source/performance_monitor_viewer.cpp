/*!*******************************************************************************************
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *             This program is free software: you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation, either version 3 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
/*****************************************************************************
** Includes
*****************************************************************************/
#include "../include/performance_monitor_viewer.h"

/*****************************************************************************
** Implementation
*****************************************************************************/

PerformanceMonitorViewer::PerformanceMonitorViewer(QWidget* parent, RosGraphReceiver* collector,
                                                   UserCommander* usercommander)
  : QWidget(parent), ui(new Ui::PerformanceMonitorViewer)
{
  ui->setupUi(this);
  ui->table_process_viewer->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  ui->table_widget->setColumnWidth(0, 640);

  supervisor_receiver = collector;
  user_command = usercommander;
  error_counter = 0;
  is_display_stopped = false;

  connect(supervisor_receiver, SIGNAL(supervisorStateReceived()), this, SLOT(updateProcessViewerTable()));
  connect(supervisor_receiver, SIGNAL(errorInformerReceived()), this, SLOT(onSupervisorStateReceived()));
  connect(ui->line_edit, SIGNAL(textChanged(QString)), this, SLOT(onTextFilterChange(const QString)));
  connect(ui->table_process_viewer, SIGNAL(customContextMenuRequested(const QPoint&)), this,
          SLOT(onCustomContextMenuRequested(const QPoint&)));
  connect(ui->stop_display_button, SIGNAL(clicked()), this, SLOT(onStopClicked()));
  connect(ui->line_edit, SIGNAL(editingFinished()), this, SLOT(clearFocus()));

  QTableWidgetItem* qtablewidgetitem8 = new QTableWidgetItem();
  ui->table_widget->setItem(0, 2, qtablewidgetitem8);

  updateProcessViewerTable();

  initialized_table = false;
  row = 0;
}
void PerformanceMonitorViewer::clearFocus()
{
  std::cout << "Editing line finished" << std::endl;
  ui->line_edit->clearFocus();
}

void PerformanceMonitorViewer::updateProcessViewerTable()
{
  int row_process_viewer = 0;

  // Loop the list to create the items in the table
  for (unsigned int i = 0; i < supervisor_receiver->list_process_state.process_list.size(); i++)
  {
    node_container = supervisor_receiver->list_process_state.process_list.at(i);

    // if(!initialized_table){
    if (ui->table_process_viewer->rowCount() < row_process_viewer)
    {
      ui->table_process_viewer->setRowCount(row_process_viewer + 1);
      ui->table_process_viewer->insertRow(row_process_viewer);
    }
    //}

    char* process_name = getProcessName(node_container.name.c_str());
    ui->table_process_viewer->setItem(row_process_viewer, 0, new QTableWidgetItem(QString(process_name)));

    updateProcessState(node_container, row_process_viewer);
    row_process_viewer++;
  }
  initialized_table = true;
}

void PerformanceMonitorViewer::updateProcessState(aerostack_msgs::ProcessDescriptor node_container,
                                                  int row_process_viewer)
{
  switch (node_container.current_state.state)
  {
    case aerostack_msgs::ProcessState::Created:
      ui->table_process_viewer->setItem(row_process_viewer, 1, new QTableWidgetItem("Created"));
      if (!node_container.is_alive)
        ui->table_process_viewer->setItem(row_process_viewer, 1, new QTableWidgetItem(" Dead  (Last state: Created)"));
      break;

    case aerostack_msgs::ProcessState::ReadyToStart:
      ui->table_process_viewer->setItem(row_process_viewer, 1, new QTableWidgetItem("Ready To Start"));
      if (!node_container.is_alive)
        ui->table_process_viewer->setItem(row_process_viewer, 1,
                                          new QTableWidgetItem("Dead (Last state: Ready To Start)"));
      break;

    case aerostack_msgs::ProcessState::Running:
      ui->table_process_viewer->setItem(row_process_viewer, 1, new QTableWidgetItem("Running"));
      if (!node_container.is_alive)
        ui->table_process_viewer->setItem(row_process_viewer, 1, new QTableWidgetItem("Dead (Last state: Running)"));
      break;

    case aerostack_msgs::ProcessState::Paused:
      ui->table_process_viewer->setItem(row_process_viewer, 1, new QTableWidgetItem("Paused"));
      if (!node_container.is_alive)
        ui->table_process_viewer->setItem(row_process_viewer, 1, new QTableWidgetItem("Dead (Last state: Paused)"));
      break;

    case aerostack_msgs::ProcessState::Started:
      ui->table_process_viewer->setItem(row_process_viewer, 1, new QTableWidgetItem("Started"));
      if (!node_container.is_alive)
      {
        ui->table_process_viewer->setItem(row_process_viewer, 1, new QTableWidgetItem("Dead (Last state: Started)"));
      }
      break;

    case aerostack_msgs::ProcessState::NotStarted:
      ui->table_process_viewer->setItem(row_process_viewer, 1, new QTableWidgetItem("Not Started"));
      if (!node_container.is_alive)
        ui->table_process_viewer->setItem(row_process_viewer, 1,
                                          new QTableWidgetItem(" Dead (Last state: Not Started)"));
      break;
  }
}

char* PerformanceMonitorViewer::getProcessName(const char* process_name_temp)  // TODO::Comprobar si existe namespace.
{
  char output[10012];
  strncpy(output, process_name_temp, sizeof(output));
  output[sizeof(output) - 1] = 0;
  char* process_name = strtok(output, "/");
  process_name = strtok(NULL, "/");
  return process_name;
}

void PerformanceMonitorViewer::onSupervisorStateReceived()
{
  if (initialized_table && !is_display_stopped)
  {
    ui->table_widget->verticalScrollBar()->setSliderPosition(ui->table_widget->verticalScrollBar()->maximum());

    if (ui->table_widget->rowCount() < row)
      ui->table_widget->setRowCount(row);

    ui->table_widget->insertRow(row);
    QTableWidgetItem* itemMessage = new QTableWidgetItem(supervisor_receiver->description);
    QTableWidgetItem* itemProcess = new QTableWidgetItem(supervisor_receiver->node_name);
    QTableWidgetItem* itemHostname = new QTableWidgetItem(supervisor_receiver->hostname);
    QTableWidgetItem* itemErrorType = new QTableWidgetItem(supervisor_receiver->error_type);

    ui->table_widget->setItem(row, 0, itemMessage);
    ui->table_widget->setItem(row, 1, itemErrorType);
    ui->table_widget->setItem(row, 2, itemProcess);
    ui->table_widget->setItem(row, 3, itemHostname);
    row++;

    error_counter++;
  }
}

void PerformanceMonitorViewer::onTextFilterChange(const QString& arg1)
{
  if (initialized_table)
  {
    QString filter = arg1;
    for (int i = 0; i < ui->table_process_viewer->rowCount(); ++i)
    {
      bool match = false;
      for (int j = 0; j < ui->table_process_viewer->columnCount(); ++j)
      {
        QTableWidgetItem* item = ui->table_process_viewer->item(i, j);
        if (item->text().contains(filter))
        {
          match = true;
          break;
        }
      }
      ui->table_process_viewer->setRowHidden(i, !match);
    }
  }
}

void PerformanceMonitorViewer::onCustomContextMenuRequested(const QPoint& pos)
{
  if (initialized_table)
  {
    QTableWidgetItem* item = ui->table_process_viewer->itemAt(pos);
    if (ui->table_process_viewer->column(item) == 0)
    {
      if (item)
      {
        // Map the point to global from the viewport to account for the header.
        showContextMenu(item, ui->table_process_viewer->viewport()->mapToGlobal(pos));
      }
    }
  }
}

void PerformanceMonitorViewer::showContextMenu(QTableWidgetItem* item, const QPoint& globalPos)
{
  QMenu menu;
  // menu.addAction("    ");
  menu.addAction("Start");
  menu.addAction("Reset");
  // menu.addAction("Stop");
  // menu.addAction("Record(Not implemented)");
  // connect(&menu, SIGNAL(triggered(QAction*)), this, SLOT(menuSelection(QAction*)));
  // menu.exec(globalPos);
}

void PerformanceMonitorViewer::menuSelection(QAction* action)
{
  std::cout << "Action clicked:" + action->text().toStdString() << std::endl;

  QModelIndexList selection = ui->table_process_viewer->selectionModel()->selectedRows();

  // Multiple rows can be selected
  for (int i = 0; i < selection.count(); i++)
  {
    QModelIndex index = selection.at(i);
    QTableWidgetItem* item = ui->table_process_viewer->item(index.row(), 0);
    std::cout << item->text().toStdString() << std::endl;
    if (action->text().toStdString().compare("Stop") == 0)
      user_command->processMonitorCommander(item->text().toStdString(), processMonitorStates::Stop);
    if (action->text().toStdString().compare("Start") == 0)
      user_command->processMonitorCommander(item->text().toStdString(), processMonitorStates::Start);
    if (action->text().toStdString().compare("Reset") == 0)
      user_command->processMonitorCommander(item->text().toStdString(), processMonitorStates::Reset);
  }
}

void PerformanceMonitorViewer::onStartModuleClicked()
{
  std::cout << "Start module clicked" << std::endl;
}

void PerformanceMonitorViewer::onStopClicked()
{
  is_display_stopped = true;
}
PerformanceMonitorViewer::~PerformanceMonitorViewer()
{
  delete ui;
}
