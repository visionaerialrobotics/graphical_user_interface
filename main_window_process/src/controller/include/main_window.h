/********************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <ros/ros.h>

#include "droneMsgsROS/configurationFolder.h"
#include "std_msgs/Bool.h"
#include "aerostack_msgs/WindowIdentifier.h"

#include <QMainWindow>
#include <QPixmap>
#include <QEvent>
#include <QPalette>
#include <QAction>
#include <QIcon>
#include <QMessageBox>
#include <QFileDialog>

#include "dialog.h"
#include "dialog2.h"

#include <algorithm>
#include <stdlib.h>
#include <thread>
#include <chrono>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "fstream"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(int argc, char** argv, QWidget* parent = 0);
  void createActions();

  ros::NodeHandle n;
  ~MainWindow();

private:
  Ui::MainWindow* ui;

  ros::Subscriber mission_state_subs;
  ros::Subscriber window_closed_subs;
  ros::Publisher window_opened_publss;

  ros::ServiceServer configuration_folder_server;

  droneMsgsROS::configurationFolder::Request req;
  droneMsgsROS::configurationFolder::Response res;
  std_msgs::Bool missionStateMsgs;

  ros::Publisher window_opened_publ;
  std::string window_opened_topic;

  void connectActions();
  void setUp();
  bool isOpened(int i);
  bool getConfigurationFolder(droneMsgsROS::configurationFolder::Request& req,
                              droneMsgsROS::configurationFolder::Response& res);
  std::string execute(const char* cmd);
  std::string launcher;
  std::string drone_id_namespace;
  std::string simulated;
  std::string drone_id_int;
  std::string main_window_topic;
  std::string configuration_folder;
  std::string my_stack_directory;
  std::string main_window_topic_open;
  std::string configuration_folder_service;
  std::string mission_state_topic;
  aerostack_msgs::WindowIdentifier windowClosedMsgs;

  std::list<int> openedWindows;
  bool launched;
  bool onMission;

  boost::property_tree::ptree root;

  Dialog* dialog;
  Dialog2* dialog2;

  QFileDialog fileDialog;

public Q_SLOTS:
  /*!********************************************************************************************************************
   *  \brief      Launches the widget to import python mission files
   **********************************************************************************************************************/
  void startImportPythonMission();
  /*!********************************************************************************************************************
   *  \brief      Launches the widget to import TML mission files
   **********************************************************************************************************************/
  void startImportTMLMission();
  /*!********************************************************************************************************************
   *  \brief      Launches the graphical user teleoperation layout
   **********************************************************************************************************************/
  void startTeleoperateGUI();
  /*!********************************************************************************************************************
   *  \brief      Launches the alphanumeric interface layout
   **********************************************************************************************************************/
  void startAlphanumericInterface();
  /*!********************************************************************************************************************
   *  \brief      Launches the execute python mission layout
   **********************************************************************************************************************/
  void startExecutePythonMission();
  /*!********************************************************************************************************************
   *  \brief      Launches the execute tml mission layout
   **********************************************************************************************************************/
  void startExecuteTMLMission();
  /*!********************************************************************************************************************
   *  \brief      Launches the execute behavior tree mission layout
   **********************************************************************************************************************/
  void startExecuteBehaviorMission();
  /*!********************************************************************************************************************
   *  \brief      Launches the widget to edit the map
   **********************************************************************************************************************/
  void startEditEnvironment();
  /*!********************************************************************************************************************
   *  \brief      Launches the widget to edit the behavior tree
   **********************************************************************************************************************/
  void startEditBehaviorTree();

  /*!********************************************************************************************************************
   *  \brief      Launches the core of aerostack
   **********************************************************************************************************************/
  void startLaunchAerostack();

  /*!********************************************************************************************************************
   *  \brief      Launches the widget to change the configuration folder
   **********************************************************************************************************************/
  void startSelectConfigurationFolder();
  /*!********************************************************************************************************************
   *  \brief      Launches the widget environment viewer
   **********************************************************************************************************************/
  void startEnvironmentViewer();
  /*!********************************************************************************************************************
   *  \brief      Launches the widget first person viewer
   **********************************************************************************************************************/
  void startFirstPersonViewer();
  /*!********************************************************************************************************************
   *  \brief      Launches the widget vehicle dynamics viewer
   **********************************************************************************************************************/
  void startVehicleDynamicsViewer();
  /*!********************************************************************************************************************
   *  \brief      Launches the widget parameters viewer
   **********************************************************************************************************************/
  void startParametersViewer();
  /*!********************************************************************************************************************
   *  \brief      Launches the widget execution viewer
   **********************************************************************************************************************/
  void startExecutionViewer();
  /*!********************************************************************************************************************
   *  \brief      Launches the widget camera viewer
   **********************************************************************************************************************/
  void startCameraViewer();
  /*!********************************************************************************************************************
   *  \brief      Launches the dialog with information about aerostack
   **********************************************************************************************************************/
  void startAboutAerostack();
  /*!********************************************************************************************************************
   *  \brief      Updates the list of opened windows
   *********************************************************************************************************************/
  void windowCloseCallback(const aerostack_msgs::WindowIdentifier& msg);
  /*!********************************************************************************************************************
   *  \brief      Set the state of the mission: executing or not
   *********************************************************************************************************************/
  void missionStateCallback(const std_msgs::Bool& msg);
  /*!********************************************************************************************************************
   *  \brief      Updates the checkboxes of the upper menu bar
   *********************************************************************************************************************/
  void updateButtons();
  /*!********************************************************************************************************************
   *  \brief      Triggered when an element from iu changes its state
   *********************************************************************************************************************/
  void changeEvent(QEvent* evt);

  /*!********************************************************************************************************************
   *  \brief      This method notifies main window that the widget was closed
   *********************************************************************************************************************/
  void closeEvent(QCloseEvent* event);

Q_SIGNALS:
  void updateMenuBar();
};

#endif  // MAINWINDOW_H
