/********************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "main_window.h"
#include <QApplication>
#include <QMessageBox>
#include <cstdio>
#include <gtest/gtest.h>
#include <ros/ros.h>

#include <iostream>

QApplication* app;
MainWindow* w;
int ar;
bool finished = false;
int total_subtests = 0;
int passed_subtests = 0;

void spinnerThread()
{
  while (!finished)
  {
    ros::spinOnce();
  }
}

void displaySubtestResults()
{
  std::cout << "\033[1;33m TOTAL SUBTESTS: " << total_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m SUBTESTS PASSED: " << passed_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m % PASSED: " << (passed_subtests * 100.0) / (total_subtests * 1.0) << "\033[0m" << std::endl;
}

void test()
{
  total_subtests++;
  std::string response;
  std::cout << "\033[1;34m Does a window called 'Aerostack Graphical User Interface' appear covering the screen ? "
               "(Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in launching Main Window Process\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Click on Help>About Aerostack. Does a window called 'About Aerostack' appear with "
               "information of Aerostack license? (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in showing information about Aaerostack\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Click on 'OK' button. Does the window desapear?  (Y/N)\033[0m" << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in closing 'About Aerostack'\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Click on 'View' option in upper bar menu. Does a menu with two inactive options ('Prefixed "
               "layouts' and 'Viewers') appear?  (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in 'receiving battery charge'View' option before launching Aerostack\033[0m\n"
              << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on   Edit>Environment Map.  Does a widget called 'Edit Environment' appear (pay "
               "attention to the design of the current map) ?(Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in launching Environment Map editor \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on   File>Select configuration folder.  Has 'Edit Environment' disapeared?(Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in automatic closing  system\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Did a window asking for a configuration directory appear after 'Edit Environment' "
               "disapeared?(Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in launching configuration folder selector \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Select 'drone2' folder and click 'Choose' button. Does a window named 'Info' informing "
               "about a change in configuration folder appear?(Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in feedback window when changing configuration folder\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;

  std::cout << "\033[1;34m Click on   Edit>Environment Map again. Is the map different from the one shown before "
               "?(Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in efectively changing configuration folder \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on   Edit>Mission Behavior Tree.  Has 'Edit Environment' disapeared? \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in automatic closing  system \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Has a widget called 'Behavior Tree Mission Design' and a widget called 'Environment Viewer' "
               "appeared in the center of the screen? (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in launching widgets involved in Behavior Tree Mission edition \033[0m\n"
              << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on Run>Launch Aerostack. Has the widgets  'Behavior Tree Mission Design' and "
               "'Environment Viewer' desapeared? \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in automatic closing  system  \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;

  std::cout << "\033[1;34m Has a dialog asking for a launcher appeared? \033[0m" << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem creating  dialog to select launcher \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Select 'launcher_simulated_quadrotor_basic_3.0.sh' and press 'Open' . Does an 'Info' window "
               "appear with a progress bar? \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in creating waiting for launching process dialog \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Does the progress bar reach 100% percentage and the message changes to 'Aerostack is now "
               "launched'? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in launching confirmation dialog\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on View>Prefixed Layouts>Graphical Teleoperation. Does only 5 widgets (Graphical "
               "Teleoperation, First Person Viewer, Environment Viewer,Execution Viewer and Vehicle Dynamics) appear "
               "in the screen ? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching prefixed Layout Graphical Teleoperation\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on View>Prefixed Layouts>Alphanumeric Interface. Does only 2 widgets (Alphanumeric "
               "Interface and Environment Viewer) appear in the screen ? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching prefixed Layout Alphanumeric Interface\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on View>Prefixed Layouts>Behavior Tree Mission. Does only 4 widgets (Behavior Tree "
               "Control, "
               " Environment Viewer,Execution Viewer and Vehicle Dynamics) appear in the screen ? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching prefixed Layout Behavior Tree Mission\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on View>Prefixed Layouts>Python Mission. Does only 5 widgets (Python Mission Control, "
               "Python Based Mission Interpreter, Environment Viewer,Execution Viewer and Vehicle Dynamics) appear in "
               "the screen ? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching prefixed Layout Python Mission\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on View>Prefixed Layouts>TML Mission. Does only  5 widgets (TML Mission Control, Task "
               "Based Mission Interpreter, Environment Viewer,Execution Viewer and Vehicle Dynamics) appear in the "
               "screen ? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching prefixed Layout TML Mission\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Close all widgets. Click on View>Viewers>Environment Viewer. Does the widget  'Environment "
               "Viewer' appear? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching Environment Viewer as an independent widget\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on View>Viewers>First Person Viewer. Does the widget  'First Person Viewer' appear? "
               "(Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching First Person Viewer as an independent widget\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m  Click on View>Viewers>Vehicle Dynamics Viewer. Does the widget  'Vehicle Dynamics Viewer' "
               "appear? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching Vehicle Dynamics Viewer as an independent widget\033[0m\n"
              << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click on View>Viewers>Execution Viewer. Does the widget  'Execution Viewer' appear? (Y/N) "
               "\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching Execution Viewer as an independent widget\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m  Click on View>Viewers>Parameters Viewer. Does the widget  'Parameters Viewer' appear? "
               "(Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching Parameters Viewer as an independent widget\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m  Click on View>Viewers>Camera Viewer. Does the widget  'Camera Viewer' appear? (Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in  launching Camera Viewer as an independent widget\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  displaySubtestResults();

  app->exit();
  finished = true;
}

TEST(GraphicalUserInterfaceTests, mainWindowTest)
{
  std::thread thr(&test);
  app->exec();
  thr.join();
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  system(
      "xfce4-terminal  \--tab --title \"Behavior Coordinator\"  --command \"bash -c 'roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
           my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &");

  ar = argc;
  app = new QApplication(argc, nullptr);
  w = new MainWindow(argc, nullptr);
  w->show();
  w->setWindowState(Qt::WindowMaximized);

  std::thread thr(&spinnerThread);

  return RUN_ALL_TESTS();
  thr.join();
}
