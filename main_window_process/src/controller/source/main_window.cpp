/********************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/main_window.h"

#include "ui_main_window.h"

MainWindow::MainWindow(int argc, char** argv, QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  setWindowIcon(QIcon(":/img/img/drone-icon.png"));
  launched = false;
  onMission = false;
  connectActions();
  // Settings connections
  setUp();
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::connectActions()
{
  connect(ui->actionPython_Mission_File, SIGNAL(triggered()), this, SLOT(startImportPythonMission()));
  connect(ui->actionTML_Mission_File, SIGNAL(triggered()), this, SLOT(startImportTMLMission()));
  connect(ui->actionGraphical_User_Interface, SIGNAL(triggered()), this, SLOT(startTeleoperateGUI()));
  connect(ui->actionAlphanumeric_Interface, SIGNAL(triggered()), this, SLOT(startAlphanumericInterface()));
  connect(ui->actionPython, SIGNAL(triggered()), this, SLOT(startExecutePythonMission()));
  connect(ui->actionTML, SIGNAL(triggered()), this, SLOT(startExecuteTMLMission()));
  connect(this, SIGNAL(updateMenuBar()), this, SLOT(updateButtons()));
  connect(ui->actionBehavior_tree, SIGNAL(triggered()), this, SLOT(startExecuteBehaviorMission()));
  connect(ui->actionEnvironment_Map, SIGNAL(triggered()), this, SLOT(startEditEnvironment()));
  connect(ui->actionSelect_Configuration_Folder, SIGNAL(triggered()), this, SLOT(startSelectConfigurationFolder()));
  connect(ui->actionMission_Behavior_Tree, SIGNAL(triggered()), this, SLOT(startEditBehaviorTree()));
  connect(ui->actionLaunch_Aerostack, SIGNAL(triggered()), this, SLOT(startLaunchAerostack()));
  connect(ui->actionEnvironment_Viewer, SIGNAL(triggered()), this, SLOT(startEnvironmentViewer()));
  connect(ui->actionFirst_Person_Viewer_2, SIGNAL(triggered()), this, SLOT(startFirstPersonViewer()));
  connect(ui->actionVehicle_Dynamics_2, SIGNAL(triggered()), this, SLOT(startVehicleDynamicsViewer()));
  connect(ui->actionExecution_Viewer_2, SIGNAL(triggered()), this, SLOT(startExecutionViewer()));
  connect(ui->actionParameters_Viewer, SIGNAL(triggered()), this, SLOT(startParametersViewer()));
  connect(ui->actionCamera_Viewer_2, SIGNAL(triggered()), this, SLOT(startCameraViewer()));
  connect(ui->actionAbout_Aerostack, SIGNAL(triggered()), this, SLOT(startAboutAerostack()));
}

void MainWindow::setUp()
{
  n.param<std::string>("drone_id_int", drone_id_int, "1");
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("window_closed", main_window_topic, "window_closed");
  n.param<std::string>("configuration_folder", configuration_folder_service, "configuration_folder");
  n.param<std::string>("mission_state", mission_state_topic, "mission_state");
  n.param<std::string>("simulated", simulated, "y");

  mission_state_subs =
      n.subscribe("/" + drone_id_namespace + "/" + mission_state_topic, 10, &MainWindow::missionStateCallback, this);
  window_closed_subs =
      n.subscribe("/" + drone_id_namespace + "/" + main_window_topic, 10, &MainWindow::windowCloseCallback, this);
  configuration_folder_server =
      n.advertiseService(configuration_folder_service, &MainWindow::getConfigurationFolder, this);

  n.param<std::string>("window_opened", window_opened_topic, "window_opened");
  window_opened_publ =
      n.advertise<aerostack_msgs::WindowIdentifier>("/" + drone_id_namespace + "/" + window_opened_topic, 1, true);
}

void MainWindow::windowCloseCallback(const aerostack_msgs::WindowIdentifier& msg)
{
  windowClosedMsgs = msg;

  openedWindows.remove(windowClosedMsgs.id);

  Q_EMIT updateMenuBar();
}

void MainWindow::missionStateCallback(const std_msgs::Bool& msg)
{
  onMission = msg.data;
}

bool MainWindow::isOpened(int i)
{
  return (std::find(openedWindows.begin(), openedWindows.end(), i) != openedWindows.end());
}

void MainWindow::updateButtons()
{
  if (!(isOpened(3)))
  {
    ui->actionEnvironment_Viewer->setChecked(false);
  }

  if (!(isOpened(6)))
  {
    ui->actionFirst_Person_Viewer_2->setChecked(false);
  }

  if (!(isOpened(9)))
  {
    ui->actionVehicle_Dynamics_2->setChecked(false);
  }
  if (!(isOpened(10)))
  {
    ui->actionExecution_Viewer_2->setChecked(false);
  }
  if (!(isOpened(12)))
  {
    ui->actionParameters_Viewer->setChecked(false);
  }

  if (!(isOpened(13)))
  {
    ui->actionCamera_Viewer_2->setChecked(false);
  }
}
bool MainWindow::getConfigurationFolder(droneMsgsROS::configurationFolder::Request& req,
                                        droneMsgsROS::configurationFolder::Response& res)
{
  res.folder = this->res.folder;

  return true;
}

void MainWindow::startAboutAerostack()
{
  QMessageBox::warning(nullptr, tr("About Aerostack"),
                       tr("<center><b>Aerostack - A Software Framework for Aerial Robotic Systems</b><br></center> "
                          "\n<center> Copyright "
                          "(C) 2017 Technical University of Madrid</center> \n <p>This program is free software: you "
                          "can redistribute "
                          "it and/or modify it under the terms of the GNU General Public License as published by the "
                          "Free Software "
                          "Foundation, either version 3 of the License, or (at your option) any later version. </p>\n "
                          "<p>This program "
                          "is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even "
                          "the implied "
                          "warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</p>"));
}

void MainWindow::startEnvironmentViewer()
{
  if (!isOpened(3))
  {
    system(
        ("xfce4-terminal  \ --tab --title \"Environment Viewer\"  --command \"bash -c 'roslaunch environment_viewer environment_viewer.launch --wait \
                drone_id_namespace:=" +
         drone_id_namespace + " \
                drone_id_int:=" +
         drone_id_int + "  \
                drone_pose_subscription:=estimated_pose \
                drone_speeds_subscription:=estimated_speeds \
                prefixed_layout:=center \
                my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
            .c_str());

    openedWindows.push_back(3);

    aerostack_msgs::WindowIdentifier msg;
    msg.id = aerostack_msgs::WindowIdentifier::ENVIRONMENT_VIEWER;
    window_opened_publ.publish(msg);

    ui->actionEnvironment_Viewer->setChecked(true);
  }
}

void MainWindow::startFirstPersonViewer()
{
  if (!isOpened(6))
  {
    system(
        ("xfce4-terminal  \--tab --title \"First Person Viewer\"  --command \"bash -c 'roslaunch first_view_process first_view.launch --wait \
                drone_id_namespace:=" +
         drone_id_namespace + " \
                drone_id_int:=" +
         drone_id_int + "  \
                drone_pose_subscription:=estimated_pose \
                drone_speeds_subscription:=estimated_speeds \
                my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
            .c_str());

    system(
        ("xfce4-terminal  \--tab --title \"First Person Viewer\"  --command \"bash -c 'roslaunch first_person_view_process first_person_view.launch --wait \
                drone_id_namespace:=" +
         drone_id_namespace + " \
                drone_id_int:=" +
         drone_id_int + "  \
                drone_pose_subscription:=estimated_pose \
                drone_speeds_subscription:=estimated_speeds \
                my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
            .c_str());

    openedWindows.push_back(6);

    aerostack_msgs::WindowIdentifier msg;
    msg.id = aerostack_msgs::WindowIdentifier::FIRST_PERSON_VIEW;
    window_opened_publ.publish(msg);

    ui->actionFirst_Person_Viewer_2->setChecked(true);
  }
}

void MainWindow::startVehicleDynamicsViewer()
{
  if (!isOpened(9))
  {
    system(
        ("xfce4-terminal  \ --tab --title \"Vehicle Dynamics\"  --command \"bash -c 'roslaunch vehicle_dynamics_viewer vehicle_dynamics_viewer.launch --wait \
                drone_id_namespace:=" +
         drone_id_namespace + " \
                drone_id_int:=" +
         drone_id_int + "  \
                drone_pose_subscription:=estimated_pose \
                drone_speeds_subscription:=estimated_speeds \
                my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
            .c_str());

    openedWindows.push_back(9);

    aerostack_msgs::WindowIdentifier msg;
    msg.id = aerostack_msgs::WindowIdentifier::VEHICLE_DYNAMICS;
    window_opened_publ.publish(msg);

    ui->actionVehicle_Dynamics_2->setChecked(true);
  }
}

void MainWindow::startExecutionViewer()
{
  if (!isOpened(10))
  {
    system(
        ("xfce4-terminal  \--tab --title \"Execution Viewer\"  --command \"bash -c 'roslaunch execution_viewer execution_viewer.launch --wait \
                drone_id_namespace:=" +
         drone_id_namespace + " \
                drone_id_int:=" +
         drone_id_int + "  \
                drone_pose_subscription:=estimated_pose \
                drone_speeds_subscription:=estimated_speeds \
                my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
            .c_str());

    openedWindows.push_back(10);

    aerostack_msgs::WindowIdentifier msg;
    msg.id = aerostack_msgs::WindowIdentifier::EXECUTION_VIEWER;
    window_opened_publ.publish(msg);

    ui->actionExecution_Viewer_2->setChecked(true);
  }
}

void MainWindow::startParametersViewer()
{
  if (!isOpened(12))
  {
    system(
        ("xfce4-terminal  \ --tab --title \"Parameters Viewer\"  --command \"bash -c 'roslaunch parameters_viewer parameters_viewer.launch --wait \
                drone_id_namespace:=" +
         drone_id_namespace + " \
                drone_id_int:=" +
         drone_id_int + "  \
                drone_pose_subscription:=estimated_pose \
                drone_speeds_subscription:=estimated_speeds \
                my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
            .c_str());

    openedWindows.push_back(12);

    aerostack_msgs::WindowIdentifier msg;
    msg.id = aerostack_msgs::WindowIdentifier::PARAMETERS_VIEWER;
    window_opened_publ.publish(msg);

    ui->actionParameters_Viewer->setChecked(true);
  }
}

void MainWindow::startCameraViewer()
{
  if (!isOpened(13))
  {
    system(
        ("xfce4-terminal  \--tab --title \"Camera Viewer\"  --command \"bash -c 'roslaunch camera_viewer camera_viewer.launch --wait \
                drone_id_namespace:=" +
         drone_id_namespace + " \
                drone_id_int:=" +
         drone_id_int + "  \
                drone_pose_subscription:=estimated_pose \
                drone_speeds_subscription:=estimated_speeds \
                my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
            .c_str());

    openedWindows.push_back(13);

    aerostack_msgs::WindowIdentifier msg;
    msg.id = aerostack_msgs::WindowIdentifier::CAMERA_VIEWER;
    window_opened_publ.publish(msg);

    ui->actionCamera_Viewer_2->setChecked(true);
  }
}

void MainWindow::startLaunchAerostack()
{
  aerostack_msgs::WindowIdentifier msg;
  msg.id = aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER;
  window_opened_publ.publish(msg);
  std::string launchers_path = QDir::homePath().toStdString() + "/workspace/ros/aerostack_catkin_ws/src/"
                                                                "aerostack_stack/launchers";
  launcher = fileDialog
                 .getOpenFileName(nullptr, tr("Select Launcher"), QString::fromStdString(launchers_path),
                                  tr("Launcher file (*.sh)"))
                 .toStdString();
  if (launcher != "")
  {
    system((launcher + " " + drone_id_int).c_str());
    launched = true;
    ui->menuTools_2->setEnabled(true);
    ui->actionLaunch_Aerostack->setEnabled(false);
    ui->menuTeleoperate_drone->setEnabled(true);
    ui->menuViewers->setEnabled(true);
    /* QMessageBox::information(
           nullptr,
           tr("Info"),
           tr("Aerostack started to launch. This may take a few seconds."));*/
    dialog = new Dialog(nullptr);
    dialog->show();
  }
}

void MainWindow::startSelectConfigurationFolder()
{
  if (!onMission)
  {
    aerostack_msgs::WindowIdentifier msg;
    msg.id = aerostack_msgs::WindowIdentifier::SELECT_CONFIGURATION_FOLDER;
    window_opened_publ.publish(msg);

    QString homePath = QDir::homePath();

    configuration_folder =
        QFileDialog::getExistingDirectory(Q_NULLPTR, "Configuration Folder",
                                          homePath + "/workspace/ros/aerostack_catkin_ws/src/aerostack_stack/configs",
                                          QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks)
            .toStdString();
    if (configuration_folder != "")
    {
      QMessageBox::information(nullptr, "Info",
                               "Configuration folder changed to " + QString::fromStdString(configuration_folder));
    }
    res.folder = configuration_folder;

    openedWindows.push_back(17);
  }
  else
  {
    dialog2 = new Dialog2(nullptr);
    dialog2->show();
  }
}

void MainWindow::startEditEnvironment()
{
  if (!onMission)
  {
    if (!isOpened(1))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Edit Environment\"  --command \"bash -c 'roslaunch edit_environment edit_environment.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(1);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::EDIT_ENVIRONMENT;
      window_opened_publ.publish(msg);
    }
  }
  else
  {
    dialog2 = new Dialog2(nullptr);
    dialog2->show();
  }
}

void MainWindow::startEditBehaviorTree()
{
  if (!onMission)
  {
    if (!isOpened(2))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Edit Mission Behavior Tree\"  --command \"bash -c 'roslaunch behavior_tree_editor behavior_tree_editor.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(2);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN;
      window_opened_publ.publish(msg);
    }
    if (!isOpened(3))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Environment Viewer\"   --command \"bash -c 'roslaunch edit_environment edit_environment.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    prefixed_layout:=right \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(3);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::ENVIRONMENT_VIEWER;
      window_opened_publ.publish(msg);

      ui->actionEnvironment_Viewer->setChecked(true);
    }
  }
  else
  {
    dialog2 = new Dialog2(nullptr);
    dialog2->show();
  }
}

void MainWindow::startExecuteBehaviorMission()
{
  if (!onMission)
  {
    if (!isOpened(3))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Environment Viewer\"  --command \"bash -c 'roslaunch environment_viewer environment_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    prefixed_layout:=center \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(3);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::ENVIRONMENT_VIEWER;
      window_opened_publ.publish(msg);

      ui->actionEnvironment_Viewer->setChecked(true);
    }

    if (!isOpened(10))
    {
      system(
          ("xfce4-terminal  \--tab --title \"Execution Viewer\"  --command \"bash -c 'roslaunch execution_viewer execution_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(10);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::EXECUTION_VIEWER;
      window_opened_publ.publish(msg);

      ui->actionExecution_Viewer_2->setChecked(true);
    }

    if (!isOpened(9))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Vehicle Dynamics\"  --command \"bash -c 'roslaunch vehicle_dynamics_viewer vehicle_dynamics_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(9);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::VEHICLE_DYNAMICS;
      window_opened_publ.publish(msg);

      ui->actionVehicle_Dynamics_2->setChecked(true);
    }
    if (!isOpened(14))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Execute Behavior Tree Mission\"  --command \"bash -c 'roslaunch behavior_tree_interpreter behavior_tree_interpreter.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(14);
      //   ui->actionBehavior_tree->setEnabled(false);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_CONTROL;
      window_opened_publ.publish(msg);
    }
  }
  else
  {
    dialog2 = new Dialog2(nullptr);
    dialog2->show();
  }
}
void MainWindow::startImportPythonMission()
{
  if (!onMission)
  {
    system(
        ("xfce4-terminal  \ --tab --title \"Python Mission Loader\"  --command \"bash -c 'roslaunch python_mission_loader python_mission_loader.launch --wait \
                drone_id_namespace:=" +
         drone_id_namespace + " \
                drone_id_int:=" +
         drone_id_int + "  \
                drone_pose_subscription:=estimated_pose \
                drone_speeds_subscription:=estimated_speeds \
                my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
            .c_str());

    openedWindows.push_back(15);

    aerostack_msgs::WindowIdentifier msg;
    msg.id = aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_PYTHON;
    window_opened_publ.publish(msg);
  }
  else
  {
    dialog2 = new Dialog2(nullptr);
    dialog2->show();
  }
}

void MainWindow::startImportTMLMission()
{
  if (!onMission)
  {
    system(
        ("xfce4-terminal  \ --tab --title \"TML Mission Loader\"  --command \"bash -c 'roslaunch tml_mission_loader tml_mission_loader.launch --wait \
                drone_id_namespace:=" +
         drone_id_namespace + " \
                drone_id_int:=" +
         drone_id_int + "  \
                drone_pose_subscription:=estimated_pose \
                drone_speeds_subscription:=estimated_speeds \
                my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
            .c_str());

    openedWindows.push_back(16);

    aerostack_msgs::WindowIdentifier msg;
    msg.id = aerostack_msgs::WindowIdentifier::IMPORT_MISSION_PLAN_TML;
    window_opened_publ.publish(msg);
  }
  else
  {
    dialog2 = new Dialog2(nullptr);
    dialog2->show();
  }
}

void MainWindow::startTeleoperateGUI()
{
  if (!onMission)
  {
    if (!isOpened(3))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Environment Viewer\"  --command \"bash -c 'roslaunch environment_viewer environment_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    prefixed_layout:=center \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(3);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::ENVIRONMENT_VIEWER;
      window_opened_publ.publish(msg);

      ui->actionEnvironment_Viewer->setChecked(true);
    }

    if (!isOpened(6))
    {
      system(
          ("xfce4-terminal  \--tab --title \"First Person Viewer\"  --command \"bash -c 'roslaunch first_view_process first_view.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      system(
          ("xfce4-terminal  \--tab --title \"First Person Viewer\"  --command \"bash -c 'roslaunch first_person_view_process first_person_view.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(6);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::FIRST_PERSON_VIEW;
      window_opened_publ.publish(msg);

      ui->actionFirst_Person_Viewer_2->setChecked(true);
    }

    if (!isOpened(10))
    {
      system(
          ("xfce4-terminal  \--tab --title \"Execution Viewer\"  --command \"bash -c 'roslaunch execution_viewer execution_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(10);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::EXECUTION_VIEWER;
      window_opened_publ.publish(msg);

      ui->actionExecution_Viewer_2->setChecked(true);
    }
    if (!isOpened(9))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Vehicle Dynamics Viewer\"  --command \"bash -c 'roslaunch  vehicle_dynamics_viewer vehicle_dynamics_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(9);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::VEHICLE_DYNAMICS;
      window_opened_publ.publish(msg);

      ui->actionVehicle_Dynamics_2->setChecked(true);
    }

    if (!isOpened(5) & !isOpened(20))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Teleoperation Control Window\"  --command \"bash -c 'roslaunch teleoperation_control_window teleoperation_control_window.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(5);
      //      ui->actionGraphical_User_Interface->setEnabled(false);
      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::KEYBOARD_CONTROL;
      window_opened_publ.publish(msg);
    }
  }
  else
  {
    dialog2 = new Dialog2(nullptr);
    dialog2->show();
  }
}
void MainWindow::startAlphanumericInterface()
{
  if (!onMission)
  {
    if (!isOpened(3))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Environment Viewer\"   --command \"bash -c 'roslaunch environment_viewer environment_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    prefixed_layout:=right \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(3);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::ENVIRONMENT_VIEWER;
      window_opened_publ.publish(msg);

      ui->actionEnvironment_Viewer->setChecked(true);
    }
    if (!isOpened(11))
    {
      namespace pt = boost::property_tree;

      std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                            "graphical_user_interface/layouts/"
                                                                            "layout.json");

      pt::read_json(layout_dir, root);

      std::string height = std::to_string(root.get<int>("11.height"));
      std::string width = std::to_string(root.get<int>("11.width"));

      std::string x = std::to_string(root.get<int>("11.position.x"));
      std::string y = std::to_string(root.get<int>("11.position.y"));
      system(("xfce4-terminal  \ --title \"Alphanumeric Interface Control\"   --geometry " + width + "x" + height +
              "+" + x + "+" + y +
              " --command \"bash -c 'roslaunch alphanumeric_interface alphanumeric_interface.launch --wait \
                    drone_id_namespace:=" +
              drone_id_namespace + " \
                    drone_id_int:=" +
              drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
                 .c_str());

      openedWindows.push_back(11);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::ALPHANUMERIC_INTERFACE_CONTROL;
      window_opened_publ.publish(msg);

      ui->actionAlphanumeric_Interface->setChecked(true);
      //  ui->actionAlphanumeric_Interface->setEnabled(false);
    }
  }
  else
  {
    dialog2 = new Dialog2(nullptr);
    dialog2->show();
  }
}

void MainWindow::startExecutePythonMission()
{
  if (!onMission)
  {
    if (!isOpened(3))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Environment Viewer\"  --command \"bash -c 'roslaunch environment_viewer environment_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    prefixed_layout:=center \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(3);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::ENVIRONMENT_VIEWER;
      window_opened_publ.publish(msg);

      ui->actionEnvironment_Viewer->setChecked(true);
    }

    if (!isOpened(10))
    {
      system(
          ("xfce4-terminal  \--tab --title \"Execution Viewer\"  --command \"bash -c 'roslaunch execution_viewer execution_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(10);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::EXECUTION_VIEWER;
      window_opened_publ.publish(msg);

      ui->actionExecution_Viewer_2->setChecked(true);
    }

    if (!isOpened(9))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Vehicle Dynamics\"  --command \"bash -c 'roslaunch vehicle_dynamics_viewer vehicle_dynamics_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(9);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::VEHICLE_DYNAMICS;
      window_opened_publ.publish(msg);

      ui->actionVehicle_Dynamics_2->setChecked(true);
    }

    if (!isOpened(7))
    {
      namespace pt = boost::property_tree;

      std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                            "graphical_user_interface/layouts/"
                                                                            "layout.json");

      pt::read_json(layout_dir, root);

      std::string height = std::to_string(root.get<int>("18.height"));
      std::string width = std::to_string(root.get<int>("18.width"));

      std::string x = std::to_string(root.get<int>("18.position.x"));
      std::string y = std::to_string(root.get<int>("18.position.y"));
      system(
          ("xfce4-terminal  \ --tab --title \"Python Mission Control Window\"  --command \"bash -c 'roslaunch python_mission_control_window python_mission_control_window.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      system(
          ("xfce4-terminal  \--title \"Python Based Mission Interpreter\"  --geometry " + width + "x" + height + "+" +
           x + "+" + y +
           "  --command \"bash -c 'roslaunch python_based_mission_interpreter_process python_based_mission_interpreter_process.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    mission:=mission.py \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(7);
      // ui->actionPython->setEnabled(false);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::PYTHON_CONTROL;
      window_opened_publ.publish(msg);
    }
  }
  else
  {
    dialog2 = new Dialog2(nullptr);
    dialog2->show();
  }
}

void MainWindow::startExecuteTMLMission()
{
  if (!onMission)
  {
    if (!isOpened(3))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Environment Viewer\"  --command \"bash -c 'roslaunch environment_viewer environment_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    prefixed_layout:=center \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(3);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::ENVIRONMENT_VIEWER;
      window_opened_publ.publish(msg);

      ui->actionEnvironment_Viewer->setChecked(true);
    }
    if (!isOpened(10))
    {
      system(
          ("xfce4-terminal  \--tab --title \"Execution Viewer\"  --command \"bash -c 'roslaunch execution_viewer execution_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(10);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::EXECUTION_VIEWER;
      window_opened_publ.publish(msg);

      ui->actionExecution_Viewer_2->setChecked(true);
    }

    if (!isOpened(9))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"Vehicle Dynamics\"  --command \"bash -c 'roslaunch vehicle_dynamics_viewer vehicle_dynamics_viewer.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(9);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::VEHICLE_DYNAMICS;
      window_opened_publ.publish(msg);

      ui->actionVehicle_Dynamics_2->setChecked(true);
    }

    if (!isOpened(8))
    {
      system(
          ("xfce4-terminal  \ --tab --title \"TML Mission Control Window\"  --command \"bash -c 'roslaunch tml_mission_control_window tml_mission_control_window.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      namespace pt = boost::property_tree;

      std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                            "graphical_user_interface/layouts/"
                                                                            "layout.json");

      pt::read_json(layout_dir, root);

      std::string height = std::to_string(root.get<int>("19.height"));
      std::string width = std::to_string(root.get<int>("19.width"));

      std::string x = std::to_string(root.get<int>("19.position.x"));
      std::string y = std::to_string(root.get<int>("19.position.y"));

      system(
          ("xfce4-terminal  \ --title \"Task Based Mission Planner\" --geometry " + width + "x" + height + "+" + x +
           "+" + y +
           "  --command \"bash -c 'roslaunch task_based_mission_planner_process task_based_mission_planner_process.launch --wait \
                    drone_id_namespace:=" +
           drone_id_namespace + " \
                    drone_id_int:=" +
           drone_id_int + "  \
                    drone_pose_subscription:=estimated_pose \
                    drone_speeds_subscription:=estimated_speeds \
                    my_stack_directory:=${AEROSTACK_STACK};exec bash'\" &")
              .c_str());

      openedWindows.push_back(8);
      // ui->actionTML->setEnabled(false);

      aerostack_msgs::WindowIdentifier msg;
      msg.id = aerostack_msgs::WindowIdentifier::TML_CONTROL;
      window_opened_publ.publish(msg);
    }
  }
  else
  {
    dialog2 = new Dialog2(nullptr);
    dialog2->show();
  }
}

void MainWindow::closeEvent(QCloseEvent* event)
{
  aerostack_msgs::WindowIdentifier msg;
  msg.id = aerostack_msgs::WindowIdentifier::CLOSE_MAIN_WINDOW;
  window_opened_publ.publish(msg);
}

void MainWindow::changeEvent(QEvent* evt)
{
  if ((evt->type() == QEvent::WindowStateChange && isMinimized()))
  {
    aerostack_msgs::WindowIdentifier msg;
    msg.id = aerostack_msgs::WindowIdentifier::MINIMIZE_MAIN_WINDOW;
    window_opened_publ.publish(msg);
  }
}
