/********************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/dialog.h"
#include "ui_dialog.h"
#include <iostream>

Dialog::Dialog(QWidget* parent) : QDialog(parent), ui(new Ui::Dialog)
{
  ui->setupUi(this);

  counter = 0;

  ui->label_2->setVisible(false);
  ui->pushButton->setVisible(false);
  ui->progressBar->setValue(counter);
  ui->progressBar->setRange(0, 100);

  timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(updateProgressBar()));
  timer->start(1000);
}

Dialog::~Dialog()
{
  delete ui;
}

void Dialog::updateProgressBar()
{
  if (counter < 6)
  {
    ui->progressBar->setValue(counter * 20);
    counter++;
  }
  else
  {
    timer->stop();
    ui->label_2->setVisible(true);
    ui->pushButton->setVisible(true);
    ui->label->setVisible(false);
  }
}

void Dialog::on_pushButton_clicked()
{
  this->accept();
}
