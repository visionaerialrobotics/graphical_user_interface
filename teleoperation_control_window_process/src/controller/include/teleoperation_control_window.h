/*!*******************************************************************************************
 *  \file      teleoperation_control_window.h
 *  \brief     Teleoperation Control Window definition file.
 *  \details   The control panel shows drone-related information and manages the buttons for interaction with the drone.
 *  \author    Daniel Del Olmo
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef TELEOPERATION_CONTROL_WINDOW_H
#define TELEOPERATION_CONTROL_WINDOW_H

#include <ros/ros.h>
#include <droneMsgsROS/openMissionFile.h>
#include <droneMsgsROS/ListOfBehaviors.h>
#include "droneMsgsROS/battery.h"
#include <droneMsgsROS/InitiateBehaviors.h>
#include <aerostack_msgs/BehaviorEvent.h>
#include <aerostack_msgs/RequestBehavior.h>
#include <aerostack_msgs/BehaviorCommand.h>
#include <aerostack_msgs/RequestBehavior.h>
#include "aerostack_msgs/WindowIdentifier.h"
#include "std_msgs/Bool.h"

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QTextEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QTextCursor>
#include <QString>
#include <QTime>
#include <QTimer>
#include <QMap>
#include <QMessageBox>
#include <QPalette>
#include <QKeyEvent>
#include <QPixmap>
#include <QCloseEvent>
#include <QRect>
#include <QGuiApplication>
#include <QScreen>
#include <QProcess>
#include <QPixmap>

#include "ui_teleoperation_control_window.h"
#include <thread>
#include <iostream>
#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "fstream"

namespace Ui
{
class TeleoperationControlWindow;
}

class TeleoperationControlWindow : public QMainWindow
{
  Q_OBJECT

public:
  // Constructor & Destructor
  explicit TeleoperationControlWindow(int argc, char** argv, QWidget* parent = 0);
  ~TeleoperationControlWindow();

private:
  // Layout
  Ui::TeleoperationControlWindow* ui;

  ros::NodeHandle n;

  ros::Subscriber list_of_behaviors_sub;
  ros::Subscriber battery_subs;
  ros::Subscriber wificonnection_subs;
  ros::Subscriber behavior_event_sub;

  ros::ServiceClient initiate_behaviors_srv;
  ros::ServiceClient activate_behavior_srv;
  ros::ServiceClient cancel_behavior_srv;

  droneMsgsROS::battery battery_msgs;

  QTimer* flight_timer;  // Timer that sends the timeout signal every second.
  QTime* current_time;
  int d_interval;
  int d_timerId;

  bool active_self_localization_by_odometry;
  bool is_wifi_connected;
  bool is_takenOff;
  bool isAKeyPressed;

  std::string list_of_active_behaviors;
  std::string drone_id_namespace;
  std::string activate_behavior;
  std::string behavior_event;
  std::string drone_driver_sensor_battery;
  std::string wifi_connection_topic;
  std::string initiate_behaviors;
  ros::Publisher window_closed_publ;
  ros::Subscriber window_opened_subs;

  std::string window_opened_topic;
  std::string window_closed_topic;

  aerostack_msgs::WindowIdentifier windowClosedMsgs;
  aerostack_msgs::WindowIdentifier windowOpenedMsgs;

  QMap<int, bool> acceptedKeys;

  boost::property_tree::ptree root;

  /*!********************************************************************************************************************
   *  \brief This method initializes the timer that informs about the time the drone has been flying.
   *  \param ms The interval at which the timer works.
   *********************************************************************************************************************/
  void setTimerInterval(double ms);

  /*!********************************************************************************************************************
   *  \brief      This method is the responsible for seting up connections.
   *********************************************************************************************************************/
  void setUp();

  /*!********************************************************************************************************************
   *  \brief      This callback is executed when the list of behaviors is modified.
   *  \details    It's purpose is to control the state of the drone and the actions the GUI should allow the user to
   *execute.
   *********************************************************************************************************************/
  void newBehaviorCallback(const droneMsgsROS::ListOfBehaviors& msg);

  /*!************************************************************************
   *  \brief   Receives the battery status.
   ***************************************************************************/
  void batteryCallback(const droneMsgsROS::battery::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Activated when a window is closed.
   ***************************************************************************/
  void windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg);

  /*!************************************************************************
   *  \brief   Receives the state of the WiFi connection
   ***************************************************************************/
  void wifiConnectionCheckCallback(const std_msgs::Bool::ConstPtr& msg);
  /*!************************************************************************
   *  \brief  Kills the process
   ***************************************************************************/
  void killMe();
  /*!************************************************************************
   *  \brief  Called when a behavior is finished
   ***************************************************************************/
  void behaviorEventCallBack(const aerostack_msgs::BehaviorEvent& msg);

public Q_SLOTS:
  /*!********************************************************************************************************************
   *  \brief      This method takes action when the user wants to make the drone to take off.
   *********************************************************************************************************************/
  void onTakeOffButton();

  /*!********************************************************************************************************************
   *  \brief      This method takes action when the user wants to make the drone to land.
   *********************************************************************************************************************/
  void onLandButton();

  /*!********************************************************************************************************************
   *  \brief      This method takes action when the user wants to reset the drone.
   *  \details    Resets angles (yaw).
   *********************************************************************************************************************/
  void onResetCommandButton();

  /*!********************************************************************************************************************
   *  \brief      This method informs about the time the drone has been flying.
   *********************************************************************************************************************/
  void flightTime();

  /*!********************************************************************************************************************
   *  \brief      This method is the responsible for handling interruption signals.
   *  \details    The signals handled are the user keyboard commands.
   *********************************************************************************************************************/
  void keyPressEvent(QKeyEvent* e);
  void keyReleaseEvent(QKeyEvent* e);

  /*!********************************************************************************************************************
   *  \brief      Event triggered when the user presses the X to close
   *********************************************************************************************************************/
  void closeEvent(QCloseEvent* event);
Q_SIGNALS:
};

#endif  // TELEOPERATION_CONTROL_WINDOW_H
